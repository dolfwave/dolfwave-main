#!/bin/sh

echo "This script is for background process usage"
echo "Dont forget to leave the shell using 'exit'"
echo "Otherwise the process can be killed"

# $1 program name
# $2  switch: if 1 then tail -f logfile.txt
$1 > output/logfile.txt 2> output/stdERROR.txt &

echo $1 "output will be piped to output/logfile.txt"
echo $1 "stderr will be piped to output/stdERROR.txt"
echo "Warning: a very HEAVY logfile.txt could be produced."


SW="1"

if [ "$2" = "$SW" ];
then
    echo 'tail -f output/logfile.txt '
    tail -f logfile.txt
else
    echo 'Process running. Good LUKE'
 fi

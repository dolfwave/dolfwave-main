#!/usr/bin/python

import sys
import matplotlib.pyplot as plt
from numpy import fromfile





def plot_xyz_2d():
    '''\
    Usage:
    plot_xyz_2d.py file_1.xyz ... file_n.xyz --title='title of plot' --savepng=(1)/(def 0) --legend=(1)/(def 0) --scatter=(1)/(def 0)\
    '''
    font = {'size': 16,}
    plot_title="Plot of xy file"
    savepng=0
    legend=0
    scatter=0

    #print(len(sys.argv))

    if (len(sys.argv)<2):
        print(plot_xyz_2d.__doc__)
        exit()

    for i in range(len(sys.argv)-1):
        if (sys.argv[i+1][:8]=='--title='):
            print(sys.argv[i+1][8:])
            plot_title=sys.argv[i+1][8:]
            print(plot_title)
        if (sys.argv[i+1][:11]=='--savepng=1'):
            savepng=1
        if (sys.argv[i+1][:10]=='--legend=1'):
            legend=1
        if (sys.argv[i+1][:11]=='--scatter=1'):
            scatter=1

    print(r'Plot Title=',plot_title)
    print(r'Save Png=',savepng)
    print(r'Legend=',legend)
    print(r'Scatter=',scatter)

    for i in range(len(sys.argv)-1):
        if (sys.argv[i+1][-4:]=='.xyz') or (sys.argv[i+1][-3:]=='.xy') :
            filename=sys.argv[i+1]
            print(filename)
            xy = fromfile(filename,sep=" ")
            xy.shape=len(xy)//2,2
            x=xy[:,0]
            y=xy[:,1]
            if(scatter==1):
                plt.plot(x,y,'o',label=filename)
            elif(scatter==0):
                plt.plot(x,y,label=filename)


    (xname, yname) = ('$x$', '$y$')
    plt.xlabel(xname,fontsize=24)
    plt.ylabel(yname,fontsize=24)
    if (legend):
        plt.legend(loc='best')
    plt.title(plot_title,fontsize=18)
    plt.grid()
    if (savepng==1):
        plt.savefig('./plot_xyz_2d.png')
    plt.show()
    plt.clf()

plot_xyz_2d()

// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
//
// ---------------------------------------------------------------------------
//  main.cpp
//
//  This program provides an example tool for the DOLFWAVE library. 
//  Post-processor for Physical Potential calculation
//  Transformed potential given in solution files
//  --------------------------------------------------------------------------.
//
// }}}------------------------------------------------------------------------
// {{{ Includes --------------------------------------------------------------
#include "../../../src/dolfwave.h"
// }}} -----------------------------------------------------------------------
using namespace dolfin;
using namespace dolfin::dolfwave;


int main()
{
  // {{{  Clear and maintimer.start()
  Dolfwave dw(
              1 /*MaxSteps*/,
              1.0 /*TimeStep*/, 
              1 /*WriteGap*/,
              "../../../meshes/harbour/real_harbour.xml.gz" /*MeshName*/,
              1. /*PcEpsilon*/,
              "Zhao" /*FormName*/,
              "uBLAS" /*AlgBackEND*/,
              "viper" /*Plotter*/,
              "output/eta.raw" /*EtaFile*/,
              "output/phi.raw"/*PhiFile*/,
              "output/phyphi.raw" /*PhyPhiFile*/,
              "output/horvel.raw" /*HorVelFile*/
              ); 
   // }}}
  // {{{Mesh, boundary definition, Function Spaces
  Mesh mesh(dw.MeshName);
  mesh.order();
    
  dw.FunctionSpaceInit(mesh);
  // }}}
  
  //Solutions Function and subfunctions (potential,elevation)
  
  dw.FunctionsInit( ); 
  
   
  // Loop on eta and phi files
  dolfin::uint first_file_number=2000;
  dolfin::uint last_file_number=2009;
    
  dw.TimeAdd(first_file_number*(100+1)*0.0005);
  for(dolfin::uint i=first_file_number;i<last_file_number+1;i++)
    {
      std::ostringstream counter;
      counter.width(6);
      counter.fill('0');
      counter<<i;
      std::ostringstream eta_file_name;
      std::ostringstream phi_file_name;
      eta_file_name<<"./output/eta"<<counter.str() /*6 digits*/<<".raw";
      
      // Note that the physical potential is loaded 
      phi_file_name<<"./output/phi"<<counter.str() /*6 digits*/<<".raw";
      cout <<phi_file_name.str()<<endl;

      // {{{Start eta_phi[0] with the l2 project of the last eta phi files.  
      dw.InitialCondition(eta_file_name.str(),phi_file_name.str());
      // Plot and save the physical potential
      // dw.Plot(eta_phi,true,false,false);
      dw.PointPlot(-125.,60.,dw.eta,"eta_profile.sol");
      dw.PointPlot(-125.,60.,dw.phi,"phi_profile.sol");
      
      dw.TimeAdd(100*0.0005);
      message("Step=%d",i);

    }

  return (EXIT_SUCCESS);
}

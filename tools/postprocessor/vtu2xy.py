#import easy to use xml parser called minidom:
from xml.dom.minidom import parseString
#all these imports are standard on most modern python implementations
import sys



filenamevtu=sys.argv[1]+'.vtu'
filenamexyz=sys.argv[1]+'.xyz'
#open the xml file for reading
file = open(filenamevtu,'r')
#convert to string
data = file.read()
#close file because we dont need it anymore
file.close()
#parse the xml you got from the file
dom = parseString(data)
#retrieve the first xml tag (<tag>data</tag>) that the parser finds with name tagName
xmlTagX = dom.getElementsByTagName('DataArray')[0].toxml()
xmlTagY = dom.getElementsByTagName('PointData')[0].getElementsByTagName('DataArray')[0].toxml()
#strip off the tag (<tag>data</tag>  --->   data)
xmlDataX=xmlTagX.replace('<DataArray NumberOfComponents="3" format="ascii" type="Float32">','').replace('</DataArray>','')
xmlDataY=xmlTagY.replace('<DataArray Name="u" format="ascii" type="Float32">','').replace('</DataArray>','')

#creat lists with numbers by split the string
splitedxmlDataX=xmlDataX.split()
splitedxmlDataY=xmlDataY.split()

fileXY = open(filenamexyz,'w')
for itrt in range(0,len(splitedxmlDataY)):
    fileXY.write(splitedxmlDataX[itrt*3])
    fileXY.write(' ')
    fileXY.write(splitedxmlDataY[itrt])
    fileXY.write('\n')
fileXY.close()

print 'vtu2xy.py is done'

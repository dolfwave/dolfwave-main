#include<cstdlib>
#include<cmath>
#include<iostream>
#include<iomanip>
#include<fstream>
#include<sstream>
using namespace std;

int main( )
{
  int  NE, first_file_number=0, last_file_number=9;
  int E=2349;
  double XX,YY;
  double  tempo,deltat=0.0005; 
 
  std::ostringstream horvelXX_file_name;
  std::ostringstream horvelYY_file_name;
  std::ostringstream speed_file_name;
  horvelXX_file_name<<"./horvelXX.sol";
  horvelYY_file_name<<"./horvelYY.sol";
  speed_file_name<<"./speed.sol";

  std::ofstream fich_escritaXX (horvelXX_file_name.str().c_str(),
                                std::ios_base::app);
  std::ofstream fich_escritaYY (horvelYY_file_name.str().c_str(),
                                std::ios_base::app);
  std::ofstream fich_escrita_speed (speed_file_name.str().c_str(),
                                std::ios_base::app);
      
 
  tempo=(first_file_number)*(100+1)*deltat;

  for(int i=first_file_number;i<last_file_number+1;i++)
    {
      std::ostringstream counter;
      counter.width(6);
      counter.fill('0');
      counter<<i;
      std::ostringstream horvel_file_name;
      horvel_file_name<<"./output/horvel"<<counter.str() /*6 digits*/<<".raw";
      cout <<horvel_file_name.str()<<endl;
  
     
      std::ifstream fich_leitura (horvel_file_name.str().c_str()); 
      if(!fich_leitura.is_open())
	{
	  cout<<"Unable to open the reading files!"<<endl;
	  exit(0);
	}
      
      fich_leitura>>NE;
      for(int j=0;j<NE;j++)
	{
	  fich_leitura>>XX>>YY;
	  if(j==E)
            {
              fich_escritaXX<<tempo<<"\t"<<XX<<endl;
              fich_escritaYY<<tempo<<"\t"<<YY<<endl;
              fich_escrita_speed<<tempo<<"\t"<<sqrt(XX*XX+YY*YY)<<endl;
            }
	}
      tempo+=(100+1)*deltat;
      fich_leitura.close();
     
    }
  fich_escritaXX.close();
  fich_escritaYY.close();
  fich_escrita_speed.close();
  return 0;
}

// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
//
// ---------------------------------------------------------------------------
//  main.cpp
//
//  This program provides an example tool for the DOLFWAVE library. 
//  Post-processor for Physical Potential calculation
//  Transformed potential given in solution files
//  --------------------------------------------------------------------------.
//
// }}}------------------------------------------------------------------------
// {{{ Includes --------------------------------------------------------------
#include <dolfwave.h>
// }}} -----------------------------------------------------------------------
using namespace dolfin;
using namespace dolfin::dolfwave;

//Depth function
class Depth : public Function
{
  void eval(double* values,const double* x) const
  {
    double retrn=0.0;
    
    if (x[0]<300.0)
      retrn=13.716;
    else if 
      ((x[0]>= 300.0)&&
       (x[0]<= 510.0))
      retrn=13.716+(300.-x[0])/25.0;
    else 
      retrn=5.316;
    
    values[0]=retrn; 
  }
};

// Sub domain switch for Incident (right)
class Incident: public Function
{
  void eval(double* values, const double* x) const
  {

   
    double retrn=0.0;
    
    if ((x[0] < -449.)*(x[1]>120)*(x[1]<240))
      retrn=1.0;
        
    values[0]=retrn;
  }
};


int main()
{
  // {{{  Clear and maintimer.start()
  Dolfwave dw(
              1 /*MaxSteps*/,
              1.0 /*TimeStep*/, 
              1 /*WriteGap*/,
              "../../../meshes/harbour/real_harbour.xml.gz" /*MeshName*/,
              1. /*PcEpsilon*/,
              "Zhao" /*FormName*/,
              "uBLAS" /*AlgBackEND*/,
              "viper" /*Plotter*/,
              "output/eta.raw" /*EtaFile*/,
              "output/phi.raw"/*PhiFile*/,
              "output/phyphi.raw" /*PhyPhiFile*/,
              "output/horvel.raw" /*HorVelFile*/
              ); 
   // }}}
  // {{{Mesh, boundary definition, Function Spaces
  Mesh mesh(dw.MeshName);
  mesh.order();
  // Constant functions
  Depth depth;//Depth
  
  FunctionSpace *V;
  SubSpace *V0,*V1;
  FunctionSpaceInit(dw.FormName,mesh,V,V0,V1);
  // }}}
  
  //Solutions Function and subfunctions (potential,elevation)
  
  Function **eta_phi,**eta,**phi;
  
  FunctionsInit(eta_phi,eta,phi,*V,*V0,*V1); 
  
   
  // Loop on eta and phi files
  dolfin::uint first_file_number=2000;
  dolfin::uint last_file_number=2002;
  Incident isNeum;
  
  for(dolfin::uint i=first_file_number;i<last_file_number+1;i++)
    {
      std::ostringstream counter;
      counter.width(6);
      counter.fill('0');
      counter<<i;
      std::ostringstream eta_file_name;
      std::ostringstream phi_file_name;
      eta_file_name<<"./output/eta"<<counter.str() /*6 digits*/<<".raw";
      phi_file_name<<"./output/phi"<<counter.str() /*6 digits*/<<".raw";
      cout <<phi_file_name.str()<<endl;

      // {{{Start eta_phi[0] with the l2 project of the last eta phi files.  
      // Recover the information from files eta_init and phi_init (raw format) 
      InitialCondition(*V,*V0,eta_phi,eta_file_name.str(),phi_file_name.str(),eta,phi);
      // Plot and save the physical potential
      // dw.Plot(eta_phi,true,false,false);
      dw.PhyPhiPlot(mesh,depth,phi,isNeum,false,true);
      message("Step=%d",i);
    }

  return (EXIT_SUCCESS);
}

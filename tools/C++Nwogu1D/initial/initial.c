#include<math.h>
#include<stdio.h>
#include<stdlib.h>

/*macro*/
#define sqr(A) ((A)*(A))

/* Vari�veis Globais*/
double g=9.8;
int NumNodos;
double A;
double B;
double delta_c;
double H;
double a,a1,a2,b,c;
double centro;

double ini1(int i,double x)
{
  double retrn;
 
  // retrn=a1/sqr(cosh(b*(x-centro)))+a2/sqr(sqr(cosh(b*(x-centro))));
  retrn=0.0;  
  return retrn;
}

double ini2(int i,double x)
{
  double retrn;
  
  //retrn=a/sqr(cosh(b*(x-centro)));
  retrn=0.0;
  return retrn;
}

int main( void )
{
   int i;
  double Deltax;
  double x,aux;
  double const_alpha=-0.33;
  double const_beta;
  FILE *approf,*approfd;

  const_beta=const_alpha+1.0/3.0;
 
  printf("Qual o Extremo Inicial:");scanf("%lf",&A);
  printf("Qual o Extremo Final:");scanf("%lf",&B);
  printf("Qual o n�mero de nodos:");scanf("%d",&NumNodos);
  printf("Qual o delta_c:");scanf("%lf",&delta_c);
  printf("Qual o H:");scanf("%lf",&H);
  printf("Qual o o centro das ondas:");scanf("%lf",&centro);
 
  c=sqrt(1.0+delta_c); 
  a1=(H/3.0)*(sqr(c)-1)/(const_beta-const_alpha*sqr(c));
  a2=-(H/2.0)*sqr((sqr(c)-1)/c)*(const_beta+2.0*const_alpha*sqr(c))/(const_beta- const_alpha*sqr(c));
  a=sqrt(H* g)*(sqr(c)-1)/c;
  b=(1.0/(2.0*H))*sqrt((sqr(c)-1)/(const_beta-const_alpha*sqr(c)));
  
  Deltax=(B-A)/(NumNodos-1);
  
  approf=fopen("cond_inicial1","w");
  if(approf==(FILE *)0)
    {
      printf("Imposs�vel escrever o ficheiro da condi��o inicial!\n");
      exit(0);
    }
  approfd=fopen("cond_inicial2","w");
  if(approf==(FILE *)0)
    {
      printf("Imposs�vel escrever o ficheiro da condi��o inicial2!\n");
      exit(0);
    }


 
  for(i=0;i<NumNodos;i++)
    {
      x=A+i*Deltax;
      aux=ini1(i,x);
      fprintf(approf,"%lf\t%lf\n",x,aux);
      aux=ini2(i,x);
      fprintf(approfd,"%lf\t%lf\n",x,aux);
    }
  fclose(approf);
  fclose(approfd);
  
  return i;
}

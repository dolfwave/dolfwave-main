#include<math.h>
#include<stdio.h>
#include<stdlib.h>

/* Vari�veis Globais*/
int NumNodos=376;
double A=-25.;
double B=50;

double XX1=13.;
double XX2=15;
double XX3=15.;
double XX4=17.;

double YY=0.01;
double YY=0.04;
double YY=0.02;
double YY=0.0167;
double YY=0.0143;
double YY=0.0125;
double YY=0.0111;
double YY=0.01;


double prof(int i,double Deltax)
{
  double retrn;
  double x=A+i*Deltax;


  double XY1[2]={XX1,0.4};
  double XY2[2]={XX2,YY};
  double XY3[2]={XX3,YY};
  double XY4[2]={XX4,0.4};
  double m=((XY2[1]-XY1[1])/(XY2[0]-XY1[0]));
  double m1=((XY4[1]-XY3[1])/(XY4[0]-XY3[0]));
  double b=XY1[1]-m*XY1[0];
  double b1=XY3[1]-m1*XY3[0];

  if(x<=XY1[0])
    retrn=XY1[1];
  else if(x<=XY2[0])
    retrn=m*x+b;
  else if(x<=XY3[0])
    retrn=XY2[1];
  else if(x<=XY4[0])
    retrn=m1*x+b1;
  else
    retrn=XY4[1];


  return retrn;

}


int main( void )
{
   int i;
  double Deltax;
  double x,aux;
  FILE *approf,*approfd;

  //  printf("Qual o Extremo Inicial:");scanf("%lf",&A);
  // printf("Qual o Extremo Final:");scanf("%lf",&B);
  //  printf("Qual o n�mero de nodos:");scanf("%d",&NumNodos);



  Deltax=(B-A)/(NumNodos-1);
  approf=fopen("depth","w");
  if(approf==(FILE *)0)
    {
      printf("Imposs�vel escrever o ficheiro da profundidade!\n");
      exit(0);
    }

  approfd=fopen("depthd","w");
  if(approfd==(FILE *)0)
    {
      printf("Imposs�vel escrever o ficheiro das derivadas da profundidade!\n");
      exit(0);
    }

  for(i=0;i<NumNodos;i++)
    {
      x=A+i*Deltax;
      aux=prof(i,Deltax);
      fprintf(approf,"%lf\t%lf\n",x,aux);
      aux=-prof(i,Deltax);
      fprintf(approfd,"%lf\t%lf\n",x,aux);
    }
  fclose(approf);
  fclose(approfd);

  return i;
}

#include<cmath>
#include<cstdlib>
#include<iostream>
#include<iomanip>

#include"b1dr5.hpp"



int teste_erro(double nDW_old, double nDW_new,int i)
{
  int retrn=0;
  int s=0;
  double aux1=nDW_new/nDW_old; 
  //  cout<<"O erro nDW_new/nDW_old  �: "<<nDW_new<<"/"<<nDW_old<<"="<<aux1<<endl; 
  double aux2=sqr(nDW_new)/(nDW_old-nDW_new);
  
  /*M�todo a divergir: recome�ar com o deltat mais pequeno*/
  if(aux1>=1.0)
    {
      retrn=0;
      V.inicializa_newton_W0( ); //reinicializa o m�todo
      Vc.inicializa_newton_W1( );
      mainpar.deltat=0.5*mainpar.deltat;
      if(mainpar.deltat<mainpar.deltat_min)
	mainpar.deltat=mainpar.deltat_min;
      
      cout<<"Norma do vector incremental a aumentar!"<<endl<<"M�todo a Divergir"<<endl;
      cout<<"Recalcula o passo de tempo para: "<<mainpar.deltat<<endl;
    }/*Recome�a com deltat mais pequeno*/
  /*M�todo a convergir*/
  else
    {  
      if((aux2<=0.01*mainpar.Tol))
	{
	  //if(i<mainpar.LimMaxIter/2) //seguran�a para n�o subir no n�mero de itera��es
	  //  {
	  //    mainpar.deltat=2.0*mainpar.deltat; //aumenta  o Delta_t
	  //    if(mainpar.deltat>mainpar.deltat_max)
	  //	mainpar.deltat=mainpar.deltat_max/2.0;
	  //    cout<<"Recalcula aumentanto o passo de tempo para: "<<mainpar.deltat<<endl;
	  //  }
	  retrn=mainpar.LimMaxIter; //Termina
	}
      else
	{
	  if(i==mainpar.LimMaxIter-1)
	    {
	      retrn=0;
	      V.inicializa_newton_W0( ); //reinicializa o m�todo
	      Vc.inicializa_newton_W1( );
	      mainpar.deltat=0.5*mainpar.deltat;
	      if(mainpar.deltat<mainpar.deltat_min)
		mainpar.deltat=mainpar.deltat_min;
	      cout<<setprecision(10)<<"Mainpar.deltat="<<mainpar.deltat<<endl;
	    }/*Recome�a com deltat mais pequeno*/
	  else
	    retrn=i+1;
	}
    }
  
  return retrn;
}



int newton(matriz_rigidez bigmatJ, matriz_rigidez bigmatM,matriz_complexa bigmatComp)
{
  int i,retrn;
  double nDW_old=0.0;
  double nDW_new=0.0;
  
  //constroi-se o sistema complexo em primeiro lugar porque este necessita das matrizes M e J originais
  bigmatComp.monta_matriz_nxn_sistema_complexo(bigmatJ, bigmatM);//matriz  sistema complexo
  // bigmatComp.escreve( );
  bigmatComp.LU( ); //LU fica guardado na matriz bigmatComp
  //bigmatComp.escreve( );
  Vc.inicializa_newton_W1( );//complexo  

  monta_matriz_nxn_sistema_real( bigmatJ, bigmatM);//modifica e guarda na matriz J 
  // bigmatJ.escreve();
  bigmatJ.LU( ); //LU fica guardado na matriz bigmatJ, portanto esta �ltima � modificada.
  // bigmatJ.escreve();
  V.inicializa_newton_W0( );
  
  
 
  i=0;
  do{
     
    //resolu��o do sistema real
    V.constroi_DeltaW0(bigmatM ); //o termo independente do sistema real
    //Calcula os valores de DeltaW0 em cada itera��o do m�todo de Newton
    V.resolve(bigmatJ);
     
    //resolu��o do sistema complexo
    Vc.constroi_DeltaW1(bigmatM);    
    //Calcula os valores de DeltaW1 em cada itera��o do m�todo de Newton
    Vc.resolve(bigmatComp);
     
     
    nDW_new= sqrt(V.norma_2_DeltaW0()+Vc.norma_2_DeltaW1());//parte real + parte complexa 
    cout<<"nDW_new=sqrt("<<V.norma_2_DeltaW0()<<"+"<<Vc.norma_2_DeltaW1()<<")"<<endl;		 
    //Actualiza o valor de W
    V.soma_Delta_a_W( ); // W=W+DeltaW
    Vc.soma_Delta_a_W( );
    
   
    //teste para parar as itera��es e/ou recome�ar com um passo de tempo diferente  
    //Op��o para fazer apenas 2 itera��es:
    
    if(i>0)
      {  
	if(mainpar.teste_newton==0)
	  {
	    retrn=i+1;
	  i=mainpar.LimMaxIter; //executa apenas duas itera��es sem efectuar o teste ao erro;
	  }
	else if  (mainpar.teste_newton==1)
	  {
	    retrn=i+1; //n�mero de itera��es executadas
	    i=teste_erro(nDW_old,nDW_new,i);
	  }
      }
    else  
      i++;
    
    nDW_old=nDW_new;
    nDW_new=0.0;
    
    
  }while(i<mainpar.LimMaxIter);
  
  return retrn;
}



#include<iostream>
#include<cstdlib>
#include<cmath>
#include<stdio.h>

#include"b1dr5.hpp"

using namespace std;


//fun��es::membros p�blicos
//construtor das matrizes de rigidez; inicializa com zeros.

matriz_rigidez::matriz_rigidez(   )
{

  nlinhas=2*mainpar.numnodos;
  ncolunas=(mainpar.l_banda)*2+1;

  matriz= new double * [nlinhas];
  if(matriz==0)
    {
      printf("ERRO na cria��o de mem�ria para as bigmats");
      exit(0);
    }
  for(int i=0;i<2*mainpar.numnodos;i++)
    {
      matriz[i]= new double [ncolunas];
      if(matriz[i]==0)
	{
	  printf("ERRO na cria��o de mem�ria para as bigmat[i]");
	  exit(0);
	}
    }

  for(int i=0;i<nlinhas;i++)
    for(int  j=0;j<ncolunas;j++)
      matriz[i][j]=0.0;
}

int matriz_rigidez::mpc(int l, int c )
{
  return (ncolunas-1)/2-(l-c);
}


double matriz_rigidez::G(int i,int j)
{
  int k,l,LB=(ncolunas-1)/2;
  double retrn;
  if((i-j<LB+1)*(j-i<LB+1))
    {
      k=i;
      l=LB-(i-j);
      retrn=matriz[k][l];
    }
  else retrn=0.0;

  return retrn;
}


// Fun��o para a constru��o da matriz de rigidez e para a matriz associada ao vector de carga
void matriz_rigidez::monta_matriz(int choice )
{
  int LB=(ncolunas-1)/2;
  int i,j;
  double aux1;

  for( int l=0;l<nlinhas;l++)
    for(  int k=0;k<ncolunas;k++)
      matriz[l][k]=0.0;


  if(choice==0) //Matriz M
    {

      i=0;
      matriz[i][mpc(i,i)]=2.0*mainpar.deltax/3.0;
      matriz[i][mpc(i,i+2)]=mainpar.deltax/6.0;
      matriz[i+1][mpc(i+1,i+1)]=2.0*(mainpar.deltax/3.0-(mainpar.A1+mainpar.A2)*sqr(V.Depth[0])/mainpar.deltax);
      matriz[i+1][mpc(i+1,i+3)]=mainpar.deltax/6.0+mainpar.A1*sqr(V.Depth[0])/mainpar.deltax+(mainpar.A2*V.Depth[1]*V.Depth[0])/mainpar.deltax;

      for( j=1;j<mainpar.numnodos-1;j++)
	{
	  i=2*j;
	  matriz[i][mpc(i,i)]=2.0*mainpar.deltax/3.0;
	  matriz[i+1][mpc(i+1,i+1)]=2.0*(mainpar.deltax/3.0-(mainpar.A1+mainpar.A2)*sqr(V.Depth[j])/mainpar.deltax);
	  matriz[i][mpc(i,i-2)]=mainpar.deltax/6.0;
	  matriz[i][mpc(i,i+2)]=mainpar.deltax/6.0;
	  matriz[i+1][mpc(i+1,i-1)]=mainpar.deltax/6.0+(mainpar.A1*sqr(V.Depth[j])+mainpar.A2*V.Depth[j-1]*V.Depth[j])/mainpar.deltax;//segunda equa��o
	  matriz[i+1][mpc(i+1,i+3)]=mainpar.deltax/6.0+(mainpar.A1*sqr(V.Depth[j])+mainpar.A2*V.Depth[j+1]*V.Depth[j])/mainpar.deltax;//segunda equa��o
	}

      i=2*(mainpar.numnodos-1);
      matriz[i][mpc(i,i)]=mainpar.deltax/3.0;
      matriz[i+1][mpc(i+1,i+1)]=mainpar.deltax/3.0-(mainpar.A1+mainpar.A2)*sqr(V.Depth[j])/mainpar.deltax;
      matriz[i][mpc(i,i-2)]=mainpar.deltax/6.0;
      matriz[i+1][mpc(i+1,i-1)]=mainpar.deltax/6.0+(mainpar.A1*sqr(V.Depth[j])+mainpar.A2*V.Depth[j-1]*V.Depth[j])/mainpar.deltax;;

    }






  if(choice==1)//Matriz J do jacobiano
    {

      i=0;
      /*correspondente � primeira equa��o eta*/
      // derivada em ordem a eta[i] ou seja VecCarga[i]
      matriz[i][mpc(i,i)]=-2.0*visco_constante(i);
      // derivada em ordem a eta[i+1] ou seja VecCarga[i+2]
      matriz[i][mpc(i,i+2)]=-0.5*V.Carga[i+3]+visco_constante(i);
      // derivada em ordem a u[i+1] ou seja VecCarga[i+3]
      matriz[i][mpc(i,i+3)]=-0.5*(V.Depth[1]+V.Carga[i+2]);

      /*correspondente � segunda equa��o u*/
      // a derivada em ordem a eta[i+1] ou seja VecCarga[i+2]
      matriz[i+1][mpc(i+1,i+2)]=-0.5*mainpar.g;
      // a derivada em ordem a u[i+1] ou seja VecCarga[i+2]
      matriz[i+1][mpc(i+1,i+3)]=-0.5*V.Carga[i+3];

      for(j=1;j<mainpar.numnodos-1;j++)
	{
	  i=2*j;
	  /*correspondente � primeira equa��o eta*/
	  // derivada em ordem a eta[i] ou seja VecCarga[i]
	  matriz[i][mpc(i,i)]=-2.0*visco_constante(i);
	  // a derivada em ordem a u[i] ou seja VecCarga[i+1] � nula
	  // a derivada em ordem a eta[i-1] ou seja VecCarga[i-2]
	  matriz[i][mpc(i,i-2)]=0.5*V.Carga[i-1]+visco_constante(i); //Aten��o
	  // a derivada em ordem a u[i-1] ou seja VecCarga[i-1]
	  matriz[i][mpc(i,i-1)]=0.5*(V.Depth[j-1]+V.Carga[i-2]);             //Aten��o
	  // derivada em ordem a eta[i+1] ou seja VecCarga[i+2]
	  matriz[i][mpc(i,i+2)]=-0.5*V.Carga[i+3]+visco_constante(i);
	  // derivada em ordem a u[i+1] ou seja VecCarga[i+3]
	  matriz[i][mpc(i,i+3)]=-0.5*(V.Depth[j+1]+V.Carga[i+2]);

	  /*correspondente � segunda equa��o u*/
	  // derivada em ordem a eta[i] ou seja VecCarga[i] � nula
	  // a derivada em ordem a u[i] ou seja VecCarga[i+1] � nula
	  // a derivada em ordem a eta[i-1] ou seja VecCarga[i-2]
	  matriz[i+1][mpc(i+1,i-2)]=0.5*mainpar.g;
	  // a derivada em ordem a u[i-1] ou seja VecCarga[i-1]
	  matriz[i+1][mpc(i+1,i-1)]=0.5*V.Carga[i-1];
	  // a derivada em ordem a eta[i+1] ou seja VecCarga[i+2]
	  matriz[i+1][mpc(i+1,i+2)]=-0.5*mainpar.g;
	  // a derivada em ordem a u[i+1] ou seja VecCarga[i+2]
	  matriz[i+1][mpc(i+1,i+3)]=-0.5*V.Carga[i+3];

	}

      /*correspondente � primeira equa��o eta*/
      i=2*(mainpar.numnodos-1);
      matriz[i][mpc(i,i)]=0.5*V.Carga[i+1]-visco_constante(i);
      matriz[i][mpc(i,i-1)]=0.5*(V.Depth[j-1]+V.Carga[i]);
      matriz[i][mpc(i,i-2)]=0.5*V.Carga[i]+visco_constante(i);
      //correspondente � segunda equa��o u
      // a derivada em ordem a eta[i-1] ou seja VecCarga[i-2]
      matriz[i+1][mpc(i+1,i-2)]=0.5*mainpar.g;
      // a derivada em ordem a u[i-1] ou seja VecCarga[i-1]
      matriz[i+1][mpc(i+1,i-1)]=0.5*V.Carga[i-1];
      // a derivada em ordem a eta[i] ou seja VecCarga[i]
      matriz[i+1][mpc(i+1,i)]=0.5*mainpar.g;
      // a derivada em ordem a u[i] ou seja VecCarga[i+1]
      matriz[i+1][mpc(i+1,i+1)]=0.5*V.Carga[i+1];
    }
}


void matriz_rigidez::LU( )
{
  int i,j,m;
  int N=nlinhas;
  int LB=(ncolunas-1)/2;
  double temp;

  for(j=0;j<N-1;j++)
    for(i=j+1;((i<j+LB+1)*(i<N));i++)
      {
	matriz[i][mpc(i,j)]=G(i,j)/G(j,j);
	for(m=j+1;((m<j+LB+1)*(m<N));m++)
	  matriz[i][mpc(i,m)]=G(i,m)-G(i,j)*G(j,m);
      }
}


//Fun��o s� para verificar resultados
void matriz_rigidez::escreve(  )
{
  cout<<" O n�mero de linhas �:"<<nlinhas<<endl;
  cout<<"O n�mero de colunas �:"<<ncolunas<<endl;
  for(int i=0;i<20;i++)
    {
      for(int j=0;j<ncolunas;j++)
	cout<<matriz[i][j]<<"\t";

      cout<<endl;
    }
  cout.flush( );
}


void monta_matriz_nxn_sistema_real(matriz_rigidez bigmatJ, matriz_rigidez bigmatM)
{
  double gamma=radau5.gamma;
  for(int i=0;i<bigmatJ.nlinhas;i++)
    for(int j=0;j<bigmatJ.ncolunas;j++)
      bigmatJ.matriz[i][j]=(gamma/mainpar.deltat)*bigmatM.matriz[i][j]-bigmatJ.matriz[i][j];
}
















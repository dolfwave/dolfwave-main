#include <fstream>
#include <iostream>
#include<cstdlib>
#include<cmath>
#include<iomanip>
#include<string.h>

#include"b1dr5.hpp"

using namespace std;

//fun��es::membro privados
void parametros::read_inteiros()
{
  char nome[256],value[256];

  //nomes dos parametros que figuram no ficheiro dos parametros
  strcpy(inteiros[0].nome, "numnodos");
  strcpy(inteiros[1].nome,"numpassos");
  strcpy(inteiros[2].nome,"condini");
  strcpy(inteiros[3].nome,"int_escrita");
  strcpy(inteiros[4].nome,"limmaxiter");
  strcpy(inteiros[5].nome,"cpu_info");
  strcpy(inteiros[6].nome,"l_banda");
  strcpy(inteiros[7].nome,"inflow");
  strcpy(inteiros[8].nome,"teste_newton");

  // fun��o que l� os parametros do ficheiro dos parametros
  for(int i=0;i<9;i++)
    {
      ifstream fich_param (fich);

      if(! fich_param.is_open())
	{
	  cout<< "Erro na abertura do ficheiro dos par�metros"<<endl;
	  exit(0);
	}
      while(! fich_param.eof())
	{

	  fich_param.getline(nome,256,'=');
	  int  comp=strcmp(nome,inteiros[i].nome);
	  if(comp==0)
	    {
	      fich_param.getline(value,256,'\t');
	      inteiros[i].value=atoi(value);
	      fich_param.ignore(256,'\n');
	    }
	  else
	    {
	      fich_param.ignore(256,'\n');
	    }
	}

      fich_param.close();
    }
  //parametros inteiros pedidos ao utilizador
  numnodos=inteiros[0].value;
  numpassos=inteiros[1].value;
  condini=inteiros[2].value;
  int_escrita=inteiros[3].value;
  LimMaxIter=inteiros[4].value;
  cpu_info=inteiros[5].value;
  l_banda=inteiros[6].value;
  inflow=inteiros[7].value;
  teste_newton=inteiros[8].value;

  //modifica��o do numero de nodos em fun��o da condi��o de dirichlet
  numnodos-=1;
}

void parametros::read_doubles()
{
  char nome[256],value[256];

  //nomes dos parametros que figuram no ficheiro dos parametros
  strcpy(doubles[0].nome,"X1");
  strcpy(doubles[1].nome,"X2");
  strcpy(doubles[2].nome,"deltat");
  strcpy(doubles[3].nome,"deltat_min");
  strcpy(doubles[4].nome,"deltat_max");
  strcpy(doubles[5].nome,"theta");
  strcpy(doubles[6].nome,"Tol");
  strcpy(doubles[7].nome,"a");
  strcpy(doubles[8].nome,"b");
  strcpy(doubles[9].nome,"g");
  strcpy(doubles[10].nome,"kappa");
  strcpy(doubles[11].nome,"omega");
  strcpy(doubles[12].nome,"m1");
  strcpy(doubles[13].nome,"m2");
  strcpy(doubles[14].nome,"tau");
  strcpy(doubles[15].nome,"n1");
  strcpy(doubles[16].nome,"n2");
  strcpy(doubles[17].nome,"Xs");

  // fun��o que l� os parametros do ficheiro dos parametros
   for(int i=0;i<18;i++)
    {
      ifstream fich_param (fich);

      if(! fich_param.is_open())
	{
	  cout<< "Erro na abertura do ficheiro dos par�metros"<<endl;
	  exit(0);
	}
      while(! fich_param.eof())
	{

	  fich_param.getline(nome,256,'=');
	  int  comp=strcmp(nome,doubles[i].nome);
	  if(comp==0)
	    {
	      fich_param.getline(value,256,'\t');
	      doubles[i].value=atof(value);
	      fich_param.ignore(256,'\n');
	    }
	  else
	    {
	      fich_param.ignore(256,'\n');
	    }
	}

      fich_param.close();
    }



   //parametros doubles pedidos ao utilizador
   X1=doubles[0].value;
   X2=doubles[1].value;
   deltat=doubles[2].value;
   deltat_min=doubles[3].value;
   deltat_max =doubles[4].value;
   theta=doubles[5].value;
   Tol=doubles[6].value;
   a=doubles[7].value;
   b=doubles[8].value;
   g=doubles[9].value;
   kappa=doubles[10].value;
   omega=doubles[11].value;
   m1=doubles[12].value;
   m2=doubles[13].value;
   tau=doubles[14].value;
   n1=doubles[15].value;
   n2=doubles[16].value;
   Xs=doubles[17].value;


   //parametros  doubles n�o pedidos ao utilizador
   A1=0.5*sqr(theta);
   A2=theta;
   B1=0.5*sqr(theta)-1.0/6.0;
   B2=theta+0.5;
   deltax=(X2-X1)/(numnodos);
   //aten��o deveria vir numnodos -1 mas o
   //numnodos j� foi alterado em fun��o da condi��o de dirichlet
   //ver  a fun��o read_inteiros
   tempo=0.0; //inicializa��o do tempo em 0

   //parametro para escrever o nome da directoria onde s�o guardadas as solu��es
   identifica=0.0;
   for(int l=0;l<18;l++)
     identifica+=doubles[l].value;
}



//fun��es::membros p�blicos

void parametros::read()
{
  read_inteiros();
  read_doubles();
}


void parametros::print_values()
{
  char fich[99];
  char comando[256];


  sprintf(comando,"mkdir ./sol%f",identifica);
  system(comando);
  sprintf(fich,"./sol%f/param%f.log",identifica,identifica);
  ofstream log_file (fich);

  log_file<<"Os parametros inteiros s�o os seguintes:"<<endl;

  log_file<<"int_escrita="<<int_escrita<<endl;
  log_file<<"cpu_info="<<cpu_info<<endl;
  log_file<<"numpassos="<<numpassos<<endl;
  log_file<<"condini="<<condini<<endl;
  log_file<<"limmaxiter="<<LimMaxIter<<endl;
  log_file<<"numnodos="<<numnodos<<endl;
  log_file<<"l_banda="<<l_banda<<endl;
  log_file<<"inflow="<<inflow<<endl;
  log_file<<"teste_newton="<<teste_newton<<endl<<endl;

  log_file<<"Os parametros decimais s�o os seguintes:"<<endl<<endl;

  log_file<< "X1"<<"="<<setprecision(10)<<X1<<endl;
  log_file<< "X2"<<"="<<setprecision(10)<<X2<<endl;
  log_file<< "deltat"<<"="<<setprecision(10)<<deltat<<endl;
  log_file<< "deltat_min"<<"="<<setprecision(10)<<deltat_min<<endl;
  log_file<< "deltat_max"<<"="<<setprecision(10)<<deltat_max<<endl;
  log_file<<"theta="<<setprecision(10)<<theta<<endl;
  log_file<<"A1="<<setprecision(10)<<A1<<endl;
  log_file<<"A2="<<setprecision(10)<<A2<<endl;
  log_file<<"B1="<<setprecision(10)<<B1<<endl;
  log_file<<"B2="<<setprecision(10)<<B2<<endl;
  log_file<<"beta=B1+B2="<<setprecision(10)<<B1+B2<<endl;
  log_file<<"alpha=A1+A2="<<setprecision(10)<<A1+A2<<endl;
  log_file<<"Tol="<<setprecision(10)<<Tol<<endl;
  log_file<<"a="<<setprecision(10)<<a<<endl;
  log_file<<"b="<<setprecision(10)<<b<<endl;
  log_file<<"g="<<setprecision(10)<<g<<endl;
   log_file<<"kappa="<<setprecision(10)<<kappa<<endl;
  log_file<<"omega="<<setprecision(10)<<omega<<endl;
  log_file<<"m1="<<setprecision(10)<<m1<<endl;
  log_file<<"m2="<<setprecision(10)<<m2<<endl;
  log_file<<"tau="<<setprecision(10)<<tau<<endl;
  log_file<<"n1="<<setprecision(10)<<n1<<endl;
  log_file<<"n2="<<setprecision(10)<<n2<<endl;
  log_file<<"Xs="<<setprecision(10)<<Xs<<endl;
  log_file<<"deltax="<<setprecision(10)<<deltax<<endl;
  log_file<<"identifica="<<setprecision(10)<<identifica<<endl;
  log_file.close();

}



#include<cstdlib>
#include<cmath>
#include<iostream>
#include<iomanip>
#include<complex>
#include<string.h>
#include"b1dr5.hpp"

using namespace std;

/*Vari�veis Globais do Programa*/
char fich[100];


//N�mero complexo I
const complex<double> I(0.0,1.0);

/****Matrizes  e vectores de carga,nodos e elementos externos *******/
vectores V;
vectores_complexos Vc;


/*Classe dos parametros*/
parametros mainpar;

/*Constantes de Runge-Kutta*/
// fun��o construtora da Constante radau5 pertencente � classe runge_kutta_constantes
runge_kutta_constantes radau5;


int main (int argc,char *argv[])
{


  system("clear");
  if(argc!=2)
    {
      cout << "\n >>>Para executar o programa � necess�rio dar o comando: rlw [nome do ficheiro de parametros]"<< endl;
      exit(0);
    }

  strcpy(fich,argv[1]);


  //funcoes para ler os parametros da equa��o e o n�mero de elementos e de nodos
  mainpar.read( );
  V.profundidades( );
  mainpar.print_values( );
  //leitura da condi��o inicial e cria��o dos vectores
  V.condini( );
  Vc.condini( );

  escreve(-1);

  //Espa�o para  as matrizes e inicializa��o
  matriz_rigidez bigmatM, bigmatJ ;
  matriz_complexa bigmatComp;

  bigmatM.monta_matriz(0);
  //escrita para confirmar resultados
  //bigmatM.escreve( );

  for(int  contador=0;contador<mainpar.numpassos;contador++)
  {

    mainpar.tempo=contador*mainpar.deltat;

    //Montagem da matriz  dos Jacobianos necess�ria as itera��es de Newton
    bigmatJ.monta_matriz(1);
    //escrita para confirmar resultados
    //bigmatJ.escreve( );

    //M�todo de Newton
    int  newt=newton(bigmatJ,bigmatM,bigmatComp);
    //constru��o do vector das solu��es
    V.monta_carga( );
    V.solve_w( );


    if((contador+1)%mainpar.int_escrita==0)
      escreve(contador);

    program_info(contador+1,newt);
  }



  return 0;

}

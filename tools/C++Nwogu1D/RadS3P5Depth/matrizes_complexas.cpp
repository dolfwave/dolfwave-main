#include<cstdlib>
#include<cmath>
#include<iostream>
#include<iomanip>
#include<complex>
#include<stdio.h>

#include"b1dr5.hpp"

using namespace std;

//fun��es::membros p�blicos
//construtor das matrizes de rigidez; inicializa com zeros.
matriz_complexa::matriz_complexa(   )
{
  int i,j;

  nlinhas=2*mainpar.numnodos;
  ncolunas=(mainpar.l_banda)*2+1;

  matriz= new complex<double> * [nlinhas];
  if(matriz==0)
    {
      printf("ERRO na cria��o de mem�ria para a matriz complexa");
      exit(0);
    }
  for(i=0;i<nlinhas;i++)
    {
      matriz[i]= new complex<double> [ncolunas];
      if(matriz[i]==0)
	{
	  printf("ERRO na cria��o de mem�ria para a matriz complexa");
	  exit(0);
	}
    }

  for( i=0;i<nlinhas;i++)
    for(  j=0;j<ncolunas;j++)
      matriz[i][j]=0.0;
}

void matriz_complexa::monta_matriz_nxn_sistema_complexo(matriz_rigidez bigmatJ, matriz_rigidez bigmatM)
{
  int i,j;
  complex<double> alphabeta=radau5.alphabeta;

  for(  i=0;i<nlinhas;i++)
    for( j=0;j<ncolunas;j++)
      matriz[i][j]=(alphabeta/mainpar.deltat)*bigmatM.matriz[i][j]-bigmatJ.matriz[i][j];
}


int matriz_complexa::mpc(int l, int c )
{
  return (ncolunas-1)/2-(l-c);
}


complex<double> matriz_complexa::G(int i,int j)
{
  int k,l,LB=(ncolunas-1)/2;
  complex<double>  retrn;
  if((i-j<LB+1)*(j-i<LB+1))
    {
      k=i;
      l=LB-(i-j);
      retrn=matriz[k][l];
    }
  else retrn=0.0;

  return retrn;
}



void matriz_complexa::LU( )
{
  int i,j,m;
  int N=nlinhas;
  int LB=(ncolunas-1)/2;
  double temp;

  for(j=0;j<N-1;j++)
    for(i=j+1;((i<j+LB+1)*(i<N));i++)
      {
	matriz[i][mpc(i,j)]=G(i,j)/G(j,j);
	for(m=j+1;((m<j+LB+1)*(m<N));m++)
	  matriz[i][mpc(i,m)]=G(i,m)-G(i,j)*G(j,m);
      }
}

void matriz_complexa::escreve(  )
{
  cout<<" O n�mero de linhas �:"<<nlinhas<<endl;
  cout<<"O n�mero de colunas �:"<<ncolunas<<endl;
  for(int i=0;i<20;i++)
    {
      for(int j=0;j<ncolunas;j++)
	cout<<matriz[i][j]<<"\t";

      cout<<endl;
    }
  cout.flush( );
}

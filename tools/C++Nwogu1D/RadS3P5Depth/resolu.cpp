#include<iostream>
#include<iomanip>
#include<fstream>
#include<cstdlib>
#include<cmath>
#include<ctime>

#include"b1dr5.hpp"

using namespace std;





void escreve(int passo)
{  
 char resuleta[99];
  char  resulu[99];
   
  
  //condi��es de dirichlet
  double eta0=mainpar.a*sin(mainpar.kappa*mainpar.X1-mainpar.omega*mainpar.tempo);
  double  u0=mainpar.b*sin(mainpar.kappa*mainpar.X1-mainpar.omega*mainpar.tempo);
  
  sprintf(resuleta,"./sol%f/eta%d.sol",mainpar.identifica,passo+1);
  sprintf(resulu,"./sol%f/u%d.sol",mainpar.identifica,passo+1);
 
 
 ofstream fich_sol_eta (resuleta );
  if(!fich_sol_eta.is_open( ))
    {
      cout<<"N�o foi possivel abrir o ficheiro da solu��o eta "<<endl;
      exit(0);
    }

  ofstream fich_sol_u ( resulu );
  if(!fich_sol_u.is_open( ))
    {
      cout<<"N�o foi possivel abrir o ficheiro da solu��o u"<<endl;
      exit(0);
    }
  
  
  fich_sol_eta<<setprecision(10)<< mainpar.X1<<"\t"<<eta0<<endl;
  fich_sol_u<<setprecision(10)<<mainpar.X1<<"\t"<< u0<<endl;
  
  for(int i=0;i<mainpar.numnodos;i++)
    {
      fich_sol_eta<<setprecision(10)<<mainpar.X1+(i+1)*mainpar.deltax<<"\t" <<V.Carga[2*i]<<endl;
      fich_sol_u<<setprecision(10)<<mainpar.X1+(i+1)*mainpar.deltax<<"\t"<<V.Carga[2*i+1]<<endl;
    }
  
  fich_sol_eta.close( );
  fich_sol_u.close( );

}
  



/* Informa��es sobre o decorrer do programa*/

void program_info(int iter,int newt )
{
  int d,u,h,m,s,ds;
  char c[1024];
  
  system("clear");
  cout<<"\n\nItera��es executadas-"<<iter<< "de"<< mainpar.numpassos<<endl;
  cout<<"Tempo simulado-"<<mainpar.tempo+mainpar.deltat<<endl<<flush;
 
 
  
  ifstream lida("/proc/self/stat");
  if(!lida.is_open( ))
    {
      cout<<"N�o foi possivel abrir o ficheiro das informa��es "<<endl;
      exit(0);
    }
 
  lida>>d;cout<<"pid="<<d<<"\t";
  lida>>c;cout<<"nome="<<c<<"\t";
  lida>>c;cout<<"state="<<c<<endl;
  lida>>d;//printf("ppid=%d\n",d);
  lida>>d;//printf("pgrp=%d\n",d);
  lida>>d;//printf("session=%d\n",d);
  lida>>d;//printf("tty=%d\n",d);
  lida>>d;//printf("tpgid=%d\n",d);
  lida>>d;//printf("flags=%u\n",u);
  lida>>d;//printf("minflt=%u\n",u);
  lida>>u;//printf("cminflt=%u\n",u);
  lida>>u;//printf("majflt=%u\n",u);
  lida>>u;//printf("cmajflt=%u\n",u);
  lida>>d;
  d=(d/100);
  s=d%60;
  d=d/60;
  m=(d)%60;
  d=d/60;
  h=d%24;
  ds=d/24;
  cout<<" up time="<<ds<<":"<<h<<":"<<m<<":"<<s<<endl; 
  lida>>d;//printf("stime=%d\n",d);
  lida>>d;//printf("cutime=%d\n",d);
  lida>>d;//printf("cstime=%d\n",d);
  lida>>d;//printf("counter=%d\n",d);
  lida>>d;//printf("priority=%d\n",d);
  lida>>u;//printf("timeout=%d\n",u);
  lida>>u;//printf("itrealvalue=%d\n",u);
  lida>>d;//printf("starttime=%d\n",d);
  lida>>u;cout<<"Virtual Memory Size="<<u/1024<<" k\t";
  lida>>u;cout<<"Resident Set Size="<<u<<endl<<endl;
  lida>>u;//printf("rlim=%d\n",u);
  cout<<"O passo de tempo �:"<<mainpar.deltat<<endl;
  cout<<"Neste passo foram executadas "<<newt<<"itera��es no Newton."<<endl;

  lida.close( );

  if(mainpar.cpu_info==1)
    {  
      ifstream lida("/proc/acpi/thermal_zone/ATF0/temperature");
      
      lida>>c>>c;cout<<"A temperatura do CPU � de "<<c<<endl;
      lida.close( );
    }
  
  fflush(stdout);
  
}

  






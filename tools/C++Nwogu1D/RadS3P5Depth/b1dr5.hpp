#ifndef PROTOTIPOS_H_INCLUDED
#define PROTOTIPOS_H_INCLUDED

/***************includes do sistema***************/
#include<cstdlib>
#include<cmath>
#include<iostream>
#include<iomanip>
#include<complex>


/***************macros***************/

#define sqr(A) ((A)*(A))
#define norma(A,B) (sqrt((sqr(A))+(sqr(B))))
#define cubo(A) ((A)*(A)*(A))

using namespace std;


//Defini��o das Classes

/**************** Classe dos parametros *****************/

//classe para vari�veis externas associadas a Radau5 

class runge_kutta_constantes
{
public:

  double gamma,alpha,beta;
  complex<double> alphabeta;
  double mT[3][3];
  double mTI[3][3];
  
  runge_kutta_constantes ( );
};

class parametro_inteiro
{
public:
  int value;
  char nome[15];

};

class parametro_double
{
public:
  double value;
  char nome[15];
};


class parametros
{
private:
  parametro_inteiro inteiros[8];
  parametro_double doubles[19];
  void read_inteiros();
  void read_doubles();


public:
  int numnodos; /*utilizador*/
  int numpassos; /*utilizador*/
  int condini;        /*utilizador*/
  int int_escrita;  /*utilizador*/
  int LimMaxIter; /*utilizador*/  
  int cpu_info;/*utilizador*/
  int inflow;  //utilizador
  int teste_newton;

  //parametros n�o pedidos ao utilizador
  int l_banda;
  //int l_banda_jacobiano; 

  double  X1; /*utilizador*/
  double  X2; /*utilizador*/
  double  deltat; /*utilizador*/
  double deltat_min; /*utilizador*/
  double deltat_max; /*utilizador*/
  double theta; /*utilizador*/
  double Tol;               /*utilizador*/
  
  
  double a;  /*utilizador*/
  double b;  /*utilizador*/
  double g;  /*utilizador*/
  double kappa;  /*utilizador*/
  double omega;  /*utilizador*/
  
  /*constantes associadas � viscosidade*/
  
  double m1; /*utilizador*/
  double m2; /*utilizador*/
  double tau; /*utilizador*/
  
  double n1; /*utilizador*/
  double n2; /*utilizador*/
  double Xs; /*utilizador*/
  
  //parametros n�o pedidos ao utilizador
  double deltax;   
  double A1,A2,B1,B2;  

  double tempo; //vari�vel para contar o tempo; n�o pedido

  double identifica; //vari�vel de identifica��o do processo para escrita das solu��es.
  
  void read();
  void print_values();
};




class matriz_rigidez
{

                                 
public:
  int nlinhas;
  int ncolunas;
  double **matriz;
  
  
  matriz_rigidez(   ); 
  int mpc(int, int);
  double G(int , int );   
  void monta_matriz(int  );
  void LU(  ); 
  void escreve(  ); 
  friend void monta_matriz_nxn_sistema_real(matriz_rigidez , matriz_rigidez );
  
};

class vectores
{
public:
  double *Carga;
  double *DeltaW0;
  double *W0;
  double *w;
  double *Depth;
 
  void condini( );
  void solve_w( );
  void constroi_DeltaW0(matriz_rigidez );
  void resolve(matriz_rigidez );
  void inicializa_newton_W0( );
  void monta_carga( );
  double norma_2_DeltaW0( );
  void soma_Delta_a_W( );
  void profundidades( );
};





class matriz_complexa
{

public:
  int nlinhas;
  int ncolunas;
  complex<double> **matriz;

  matriz_complexa( ); //construct para espa�o na mem�ria
  void monta_matriz_nxn_sistema_complexo(matriz_rigidez ,matriz_rigidez ); // atribui os valores.
  int mpc(int, int);
  complex<double> G(int , int );   
  void LU( );
  void escreve( );
};

class vectores_complexos 
{
public:
 
  complex<double> *W1;
  complex<double>*DeltaW1;
 
  void condini( );//funciona como constructor
  void inicializa_newton_W1( );
  void constroi_DeltaW1(matriz_rigidez );
  void resolve(matriz_complexa );
  double norma_2_DeltaW1( );
  void soma_Delta_a_W( );
};


class funcoes_vecF
{
private:
  double eta(int, int );
  double u(int, int );
  double p(int ,int );
  double f(int ,int );
  double arg_F_radau(int ,int );
public:
  double visco_constante(int );  
  double F(int, int, int );
};

/**************vari�veis globais*************************/

extern char fich[100];
extern runge_kutta_constantes radau5; 
extern parametros mainpar;
extern vectores V;
extern vectores_complexos Vc;
extern const complex<double> I;
/***************prot�tipos das fun��es**************/



extern int newton(matriz_rigidez ,matriz_rigidez, matriz_complexa);

extern int teste_erro(double ,double, int);
extern void escreve(int);
extern void program_info(int ,int );
extern double eta(int, int );
extern  double u(int, int );
extern  double p(int ,int );
extern  double f(int ,int );
extern double arg_F_radau(int ,int );
extern double visco_constante(int );  
extern double F(int, int, int );

extern double dirichlet(int );


#endif

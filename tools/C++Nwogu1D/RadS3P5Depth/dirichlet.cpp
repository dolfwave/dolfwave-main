#include<cstdlib>
#include<cmath>


#include"b1dr5.hpp"



double dirichlet(int L)
{ 

  double p0,w0,f0,eta0,u0,visc;
  double retrn;
  
  visc=visco_constante(-1);  
  


  //assume-se h constante junto � fronteira. As equa��es na fronteira coincidem com as de profundidade constante, portanto n�o � necess�rio proceder  a altera��es.

  if (mainpar.inflow==1)
    {
      w0=-(mainpar.B1+mainpar.B2)*cubo(V.Depth[0])*sqr(mainpar.kappa)*mainpar.b*sin(mainpar.kappa*mainpar.X1-mainpar.omega*mainpar.tempo);
      eta0=mainpar.a*sin(mainpar.kappa*mainpar.X1-mainpar.omega*mainpar.tempo);
      u0=mainpar.b*sin(mainpar.kappa*mainpar.X1-mainpar.omega*mainpar.tempo);
      
      
      p0=(V.Depth[0]+eta0)*u0;
      f0=0.5*sqr(u0)+mainpar.g*eta0;
      
      if(L==1)
	retrn=mainpar.deltax*mainpar.a*mainpar.omega*cos(mainpar.kappa*mainpar.X1-mainpar.omega*mainpar.tempo)/6.0+0.5*(p0+w0)+visc*eta0;
      if(L==2)
	retrn=(mainpar.deltax/6.0+(mainpar.A1+mainpar.A2)*sqr(V.Depth[0])/mainpar.deltax)*mainpar.b*mainpar.omega*cos(mainpar.kappa*mainpar.X1-mainpar.omega*mainpar.tempo)+0.5*f0;
    }
  
return retrn;
}



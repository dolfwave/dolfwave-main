#include<cmath>
#include<cstdlib>
#include<iostream>
#include<iomanip>

#include"b1dr5.hpp"


double arg_F_radau(int s, int j)
{
  return radau5.mT[s][0]*V.W0[j]+radau5.mT[s][1]*Vc.W1[j].real( )+radau5.mT[s][2]*Vc.W1[j].imag( );
}

double eta(int s,int k)
 {
   return V.Carga[2*k]+arg_F_radau(s,2*k);
 }

double u(int s,int k)
{ 
  return V.Carga[2*k+1]+arg_F_radau(s,2*k+1);
}

double p(int s,int k)
{
  return (V.Depth[k]+V.Carga[2*k]+arg_F_radau(s,2*k))*(V.Carga[2*k+1]+arg_F_radau(s,2*k+1)); 
}

double f(int s,int k)
{
  return 0.5*sqr(V.Carga[2*k+1]+arg_F_radau(s,2*k+1))+mainpar.g*(V.Carga[2*k]+arg_F_radau(s,2*k)); 
}


double visco_constante(int j)
{
  double retrn;
  double x=(j+1)*mainpar.deltax+mainpar.X1;
  
  
  retrn=(mainpar.m1/(mainpar.deltax))*exp(-mainpar.m2*mainpar.tempo/mainpar.tau);
  
  if(x>mainpar.Xs)
    retrn+=(mainpar.n1/(mainpar.deltax*(exp(1.0)-1.0)))*(exp(pow((x-mainpar.Xs)/(mainpar.X2-mainpar.Xs),mainpar.n2))-1.0);
  
  return retrn;
}


double F(int stage,int equation, int j)
{
  double aux;
  double visc;
  
  visc=visco_constante(j);
  
  if(equation==1)
    {
      if(j==0)
	aux= -0.5*(p(stage,j+1)+V.w[j+1])-visc*(2.0*eta(stage,j)-eta(stage,j+1))+dirichlet(1);
      else if(j==mainpar.numnodos-1)
	aux=0.5*(p(stage,j-1)+V.w[j-1])-0.5*(p(stage,j)+V.w[j])-visc*(-eta(stage,j-1)+eta(stage,j));
      else
	aux= 0.5*(p(stage,j-1)+V.w[j-1])-0.5*(p(stage,j+1)+V.w[j+1])-visc*(-eta(stage,j-1)+2.0*eta(stage,j)-eta(stage,j+1));
    }
  
  else if(equation==2)
    {
      
      if(j==0)
	aux= 0.5*(-f(stage,j+1))+dirichlet(2);
      else if(j==mainpar.numnodos-1)
	aux=0.5*(f(stage,j)+f(stage,j-1));
      else
	aux= 0.5*(f(stage,j-1)-f(stage,j+1));
    }
  
  return aux; 
}

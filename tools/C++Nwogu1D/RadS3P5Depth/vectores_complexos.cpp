#include<cstdlib>
#include<cmath>
#include<iostream>
#include<iomanip>
#include<complex>


#include"b1dr5.hpp"

using namespace std;


//fun��es::membros p�blicos


void vectores_complexos::condini( )
{

  W1= new complex<double> [2*mainpar.numnodos];
  if(W1==0)
    {
      cout<<"Erro na cria��o de mem�ria para W1"<<endl;
      exit(0);
    }

  
  DeltaW1= new complex<double> [2*mainpar.numnodos];
  if(DeltaW1==0)
    {
      cout<<"Erro na cria��o de mem�ria para DeltaW1"<<endl;
      exit(0);
    }

  for (int i;i<2*mainpar.numnodos;i++)
    {
      DeltaW1[i]=0.0;
      W1[i]=0.0;
    }
}

void vectores_complexos::inicializa_newton_W1( )
{
  for(int i=0;i<2*mainpar.numnodos;i++)
    {
      DeltaW1[i]=0.0;
      W1[i]=0.0; // N�o � a melhor escolha para a inicializa��o de W
    }
}


void vectores_complexos::constroi_DeltaW1(matriz_rigidez bigmatM )
{
  int i,j;
  complex<double> alphabeta=radau5.alpha+radau5.beta*I;   
  
  double ti22=radau5.mTI[1][1],ti23=radau5.mTI[1][2];  
  double ti32=radau5.mTI[2][1],ti33=radau5.mTI[2][2];  

  complex<double> soma=0.0;
  
  for(i=0;i<bigmatM.nlinhas;i++)
    {
      for(j=0;j<bigmatM.nlinhas;j++)
	soma+=bigmatM.G(i,j)*W1[j];      
      DeltaW1[i]=soma;
      soma=0.0;
    }
  
  for(j=0;j<mainpar.numnodos;j++)
    {
      DeltaW1[2*j]=-(alphabeta/mainpar.deltat)*DeltaW1[2*j]+
	(F(0,1,j)+ti22*F(1,1,j)+ti23*F(2,1,j))+
	(ti32*F(1,1,j)+ti33*F(2,1,j))*I;
      
      DeltaW1[2*j+1]=-(alphabeta/mainpar.deltat)*DeltaW1[2*j+1]
	+(F(0,2,j)+ti22*F(1,2,j)+ti23*F(2,2,j))+
	(ti32*F(1,2,j)+ti33*F(2,2,j))*I;
    }

}



void vectores_complexos::resolve( matriz_complexa bigmat)
{
  
  int i,j,LB=(bigmat.ncolunas-1)/2;
  complex<double> S=0.0;
  
   
       for(i=0;i<bigmat.nlinhas;i++)
	{
	  S=DeltaW1[i];
	  for(j=0;j<i;j++)
	    if(i-j<LB+1)
	      {
		S=S-bigmat.G(i,j)*DeltaW1[j];
	      }
	  DeltaW1[i]=S;
	}     
      for(i=bigmat.nlinhas-1;i>=0;i--)
	{
	  S=DeltaW1[i]/bigmat.G(i,i);
	  for(j=bigmat.nlinhas-1;j>i;j--)
	    if(j-i<LB+1)
	      S=S-bigmat.G(i,j)*DeltaW1[j]/bigmat.G(i,i);
	  DeltaW1[i]=S;
	}
}


double vectores_complexos::norma_2_DeltaW1( )
{
  double retrn=0.0;
  // liberta o quadrado da norma do vector de complexos
  for(int i=0; i<2*mainpar.numnodos;i++)
    retrn+=sqr(DeltaW1[i].real())+sqr(DeltaW1[i].imag());

  return retrn;
}


void vectores_complexos::soma_Delta_a_W( )
{
  for(int j=0;j<2*mainpar.numnodos;j++)
    W1[j]+=DeltaW1[j];
}

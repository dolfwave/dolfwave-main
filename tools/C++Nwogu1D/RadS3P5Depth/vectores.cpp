#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include"b1dr5.hpp"

using namespace std;


//membros p�blicos


void vectores::condini( )
{

  Carga = new double [2*mainpar.numnodos];
  if(Carga== 0)
    {
      cout<<" Erro na cria��o de mem�ria para VecCarga"<<endl;
      exit(0);
    }
  w= new double [mainpar.numnodos];
  if(w== 0)
    {
      cout<<" Erro na cria��o de mem�ria para w"<<endl;
      exit(0);
    }
  DeltaW0= new double [2*mainpar.numnodos];
  if(DeltaW0== 0)
    {
      cout<<" Erro na cria��o de mem�ria para DeltaW0"<<endl;
      exit(0);
    }
  W0= new double [2*mainpar.numnodos];
  if(W0== 0)
    {
      cout<<" Erro na cria��o de mem�ria para W0"<<endl;
      exit(0);
    }

  if (mainpar.condini==0)
    {

      for(int i=0;i<mainpar.numnodos;i++)
	{
	  Carga[2*i]=0.0;
	  Carga[2*i+1]=0.0;
	  w[i]=0.0;
	}
    }
  else if (mainpar.condini==1)
    {
      double c=sqrt(1.025), H=0.4;
      double ca=-0.4, cb=ca+1.0/3.0;
      double center=-5.0;
      double a1=(H/3.0)*(sqr(c)-1)/(cb-ca*sqr(c));
      double a2=-(H/2.0)*sqr((sqr(c)-1)/c)*(cb+2.0*ca*sqr(c))/(cb-ca*sqr(c));
      double k=(1.0/(2.0*H))*sqrt((sqr(c)-1)/(cb-ca*sqr(c)));
      double a=sqrt(9.81*H)*(sqr(c)-1.)/c;
        for(int i=0;i<mainpar.numnodos;i++)
          {
            double x=mainpar.X1+(i+1)*mainpar.deltax;
            Carga[2*i]=a1/sqr(cosh(k*(x-center)))+a2/sqr(sqr(cosh(k*(x-center))));
            Carga[2*i+1]=a/sqr(cosh(k*(x-center)));
            w[i]=0.0;
          }
    }
  else if (mainpar.condini==2)
    {
      double c=sqrt(1.025), H=0.4;
      double ca=-0.4, cb=ca+1.0/3.0;
      double center=-10.0;
      double a1=(H/3.0)*(sqr(c)-1)/(cb-ca*sqr(c));
      double a2=-(H/2.0)*sqr((sqr(c)-1)/c)*(cb+2.0*ca*sqr(c))/(cb-ca*sqr(c));
      double k=(1.0/(2.0*H))*sqrt((sqr(c)-1)/(cb-ca*sqr(c)));
      double a=sqrt(9.81*H)*(sqr(c)-1.)/c;
        for(int i=0;i<mainpar.numnodos;i++)
          {
            double x=mainpar.X1+(i+1)*mainpar.deltax;
            Carga[2*i]=a1/sqr(cosh(k*(x-center)))+a2/sqr(sqr(cosh(k*(x-center))));
            Carga[2*i+1]=a/sqr(cosh(k*(x-center)));
            w[i]=0.0;
          }
    }

}


void vectores::solve_w(  )
{
  int i;
  double aux=sqr(mainpar.deltax);
  double u0;


  u0 =mainpar.b*sin(mainpar.kappa*mainpar.X1-mainpar.omega*(mainpar.tempo));



  w[0]=(mainpar.B1*cubo(V.Depth[1])+mainpar.B2*V.Depth[0]*sqr(V.Depth[1]))*u0;
  w[0]-=2.0*(mainpar.B1+mainpar.B2)*cubo(V.Depth[1])*Carga[1];
  w[0]+=(mainpar.B1*cubo(V.Depth[1])+mainpar.B2*V.Depth[2]*sqr(V.Depth[1]))*Carga[3];
  w[0]=w[0]/aux;

  for(i=1;i<mainpar.numnodos-1;i++)
    {
      w[i]=(mainpar.B1*cubo(V.Depth[i])+mainpar.B2*V.Depth[i-1]*sqr(V.Depth[i]))*Carga[2*i-1];
      w[i]-=2.0*(mainpar.B1+mainpar.B2)*cubo(V.Depth[i])*V.Carga[2*i+1];
      w[i]+=(mainpar.B1*cubo(V.Depth[i])+mainpar.B2*V.Depth[i+1]*sqr(V.Depth[i]))*Carga[2*i+3] ;
      w[i]=w[i]/aux;
    }

  i=mainpar.numnodos-1;
  w[i]=(mainpar.B1*cubo(V.Depth[i])+mainpar.B2*V.Depth[i-1]*sqr(V.Depth[i]))*Carga[2*i-1];
  w[i]-=(mainpar.B1+mainpar.B2)*cubo(V.Depth[i])*Carga[2*i+1];
  w[i]=w[i]/aux;


}



void vectores::constroi_DeltaW0(matriz_rigidez bigmatM )
{
  int i,j;
  double gamma=radau5.gamma;
  double ti12=radau5.mTI[0][1],ti13=radau5.mTI[0][2];

  double soma=0.0;

  for(i=0;i<bigmatM.nlinhas;i++)
    {
      for(j=0;j<bigmatM.nlinhas;j++)
	soma+=bigmatM.G(i,j)*W0[j];
      DeltaW0[i]=soma;
      soma=0.0;
    }

  for(j=0;j<mainpar.numnodos;j++)
     {
       DeltaW0[2*j]=-(gamma/mainpar.deltat)*DeltaW0[2*j]+F(0,1,j)+ti12*F(1,1,j)+ti13*F(2,1,j);
       DeltaW0[2*j+1]=-(gamma/mainpar.deltat)*DeltaW0[2*j+1]+F(0,2,j)+ti12*F(1,2,j)+ti13*F(2,2,j);
     }
}


void vectores::resolve(matriz_rigidez bigmat)
{
  int i,j,LB=(bigmat.ncolunas-1)/2;
  double S=0.0;


       for(i=0;i<bigmat.nlinhas;i++)
	{
	  S=DeltaW0[i];
	  for(j=0;j<i;j++)
	    if(i-j<LB+1)
	      {
		S=S-bigmat.G(i,j)*DeltaW0[j];
	      }
	  DeltaW0[i]=S;
	}
      for(i=bigmat.nlinhas-1;i>=0;i--)
	{
	  S=DeltaW0[i]/bigmat.G(i,i);
	  for(j=bigmat.nlinhas-1;j>i;j--)
	    if(j-i<LB+1)
	      S=S-bigmat.G(i,j)*DeltaW0[j]/bigmat.G(i,i);
	  DeltaW0[i]=S;
	}
}


void vectores::inicializa_newton_W0( )
{
  for(int i=0;i<2*mainpar.numnodos;i++)
    {
      DeltaW0[i]=0.0;
      W0[i]=0.0; //N�o � a melhor escolha para a inicializa��o de Z
    }
}

void vectores::monta_carga(  )
{

  //(T ox I)(W0,W1.real,W1.imag)^T=(Z1,Z2,Z3)^T  mudan�a de vari�vel;
  //Como o m�todo de radau IIA s� necessita do Z3 fica-se com:
  //Z3=T31*W0+T32*W1.real+T33*W1.imag

  //No m�todo RadauIIA tem-se d=(0,0,1)

  for(int i=0;i<2*mainpar.numnodos;i++)
    {
      Carga[i]+=radau5.mT[2][0]*W0[i];
      Carga[i]+=radau5.mT[2][1]*Vc.W1[i].real();
      Carga[i]+=radau5.mT[2][2]*Vc.W1[i].imag();
    }
}


double vectores::norma_2_DeltaW0( )
{
  double retrn=0.0;
  // liberta o quadrado da norma do vector de complexos
  for(int i=0; i<2*mainpar.numnodos;i++)
    retrn+=sqr(DeltaW0[i]);

  return retrn;
}

void vectores::soma_Delta_a_W( )
{
  for(int j=0;j<2*mainpar.numnodos;j++)
    W0[j]+=DeltaW0[j];
}



void vectores::profundidades(void )
{
  int i;
  double aux1,aux2;

  Depth = new double [mainpar.numnodos];
  if(Depth== 0)
    {
      cout<<" Erro na cria��o de mem�ria para Depth"<<endl;
      exit(0);
    }



  ifstream fich_depth;
  fich_depth.open("./depth");

  if(! fich_depth.is_open())
    {
      cout<< "Erro na abertura do ficheiro das profundidades"<<endl;
      exit(0);
    }




  char fich[99];
  sprintf(fich,"./sol%f/depth_sim.sol",mainpar.identifica,mainpar.identifica);
  ofstream fich_depth_sim (fich);


  for(i=0;i<mainpar.numnodos;i++)
    {
      fich_depth>>aux1>>aux2;
      Depth[i]=aux2;
      fich_depth_sim<<aux1<<"\t"<<-aux2<<endl;
    }

  fich_depth.close();
  fich_depth_sim.close( );
}

#include<cstdlib>
#include<cmath>
#include<iostream>
#include<iomanip>
#include<fstream>
#include<sstream>
using namespace std;

int main( )
{
  int intervalo_escrita, ultima_solucao, numero_nodos;
  double  nodo,tempo,deltat; 
  char nome_fich_leitura[99],nome_fich_escrita[99];
  double aux1,aux2;
 
      
      cout<<"Qual o n�mero de intervalo para a escrita das solu��es?";
      cin>>intervalo_escrita;
      cout<<intervalo_escrita<<endl;
      cout<<"Qual o n�mero do �ltimo ficheiro das solu��es?";
      cin>>ultima_solucao; 
      cout<<ultima_solucao<<endl; 
      cout<<"Qual o nodo a considerar?";
      cin>>nodo;
      cout<<nodo<<endl;
      cout<<"Qual o numero total de nodos?";
      cin>>numero_nodos;
      cout<<numero_nodos<<endl;
      cout<<"Qual o passo de tempo?";
      cin>>deltat;
      cout<<deltat<<endl;
  
  sprintf(nome_fich_escrita,"perfil%f.sol",nodo);
  ofstream fich_escrita (nome_fich_escrita);
  
  tempo=0.0;

  for(int i=0;i<ultima_solucao+1;i+=intervalo_escrita)
    {
      sprintf(nome_fich_leitura,"eta%d.sol",i);
      ifstream fich_leitura (nome_fich_leitura); 
      if(!fich_leitura.is_open())
	{
	  cout<<"N�o foi poss�vel abrir os ficheiros da leitura das solu��es em x!"<<endl;
	  exit(0);
	}
      
      for(int j=0;j<numero_nodos;j++)
	{
	  fich_leitura>>aux1>>aux2;
	  if(aux1==nodo)
	    fich_escrita<<tempo<<"\t"<<aux2<<endl;
	}
      tempo+=deltat*intervalo_escrita;
      fich_leitura.close();
    }
  fich_escrita.close();

  return 0;
}

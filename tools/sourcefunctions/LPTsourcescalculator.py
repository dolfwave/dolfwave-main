## {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
##
## e-mail: ndl@ptmat.fc.ul.pt
## http://ptmat.fc.ul.pt/~ndl/
##
## ---------------------------------------------------------------------------
## Licensed under the GNU LGPL Version 3.0 or later.
## ---------------------------------------------------------------------------
##  LPTsourcescalculator.py
##
## First added:  2009-10-19
## Last changed: 2009-10-30
##
## }}}------------------------------------------------------------------------

from sympy import *
from sympy import __version__ as sympy_version

print 'sympy version is '+ sympy_version
print 'Works with versions >= SymPy 0.6.3'


#Use capital X and Y to simplify integration with DOLFIN

X=Symbol('X',real=True, unbounded=True)
Y=Symbol('Y',real=True, unbounded=True)
t=Symbol('t',real=True, unbounded=True)

#define the depth function

C=Symbol('C',real=True, unbounded=True)
D=Symbol('D',real=True, unbounded=True)
w=Symbol('w',real=True, unbounded=True)
g=Symbol('g',real=True, unbounded=True)
Y0=Symbol('Y0',real=True, unbounded=True)
X0=Symbol('X0',real=True, unbounded=True)
B3=Symbol('B3',real=True, unbounded=True)
B4=Symbol('B4',real=True, unbounded=True)
alpha=Symbol('alpha',real=True, unbounded=True)
beta1=Symbol('beta1',real=True, unbounded=True)

CC=0.4572  #C
DD=0.04572 #D
ETIME=.5  #End time for movement
GG=3.0     #gamma exponential parameter
XS=8.0     #X center of the hump
YS=8.0     #Y center of the hump
OMEGA=3.14159265   #period parameter

## Depth definition ##
def h(X,Y,t):
    return C-D*sin(w*t)*exp(-g*((X-X0)**2+(Y-Y0)**2))

#####################################################
print 'the depth function h'
depth=ccode(h(X,Y,t))
print depth
print '\n'


#### OUTPUT ######
print 'first time derivative of h dh/dt\n'
dtdepth=ccode(diff(h(X,Y,t),t))
print dtdepth
print'\n'

print 'second time derivative of h d/dt dh/dt\n'
dtdtdepth=ccode(diff(h(X,Y,t),t,2))
print dtdtdepth
print'\n'

print 'Source function for eta is S_1(h)=\n'
srceta= ccode(\
                    -diff(h(X,Y,t),t)\
                    -1./2.*diff(h(X,Y,t)**2*diff(diff(h(X,Y,t),t),X),X)\
                    -1./2.*diff(h(X,Y,t)**2*diff(diff(h(X,Y,t),t),Y),Y)\
                    +B3*h(X,Y,t)**4*diff(diff(diff(h(X,Y,t),X,2)+diff(h(X,Y,t),Y,2),X,2)\
                                         +diff(diff(h(X,Y,t),X,2)+diff(h(X,Y,t),Y,2),Y,2),t)\
                    +alpha*(1.-beta1)*(diff(h(X,Y,t)*diff(h(X,Y,t)*diff(h(X,Y,t),t),X),X)\
                                       +diff(h(X,Y,t)*diff(h(X,Y,t)*diff(h(X,Y,t),t),Y),Y))\
                    )
print srceta
print '\n'

print 'Source function for low order eta is S*_1(h)=\n'
srcetalow= ccode(\
                    -diff(h(X,Y,t),t)\
                    -1./2.*diff(h(X,Y,t)**2*diff(diff(h(X,Y,t),t),X),X)\
                    -1./2.*diff(h(X,Y,t)**2*diff(diff(h(X,Y,t),t),Y),Y)\
                    )
print srcetalow
print '\n'

print 'Source function for phi is S_2(h)=\n'
srcphi=ccode(-1./2.*diff(h(X,Y,t),t)**2+alpha*diff(h(X,Y,t)*diff(h(X,Y,t),t),t)\
    -B4*h(X,Y,t)**3*(diff(diff(h(X,Y,t),t,2),X,2)+diff(diff(h(X,Y,t),t,2),Y,2)))

print srcphi




######## print an Depth.h file to use #include "Depth.h" in the main.cpp ##########

file = open('Depth.h','w')

print >>file, '''
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.h
//
//
// To compile  with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#ifndef __DEPTH_H
#define __DEPTH_H

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

class Depth : public Function
{
 public:
  Depth (const double& t): t(t)
  {}
private:
  void eval(double* values, const double* x) const
  {
    double time=t;
    double endtime='''+str(ETIME)+''';
    double C='''+str(CC)+''';
    double D='''+str(DD)+''';
    double X0='''+str(XS)+''';
    double Y0='''+str(YS)+''';
    double w='''+str(OMEGA)+''';//omega
    double g='''+str(GG)+''';//gamma
    double X=x[0];
    double Y=x[1];
    if (time<endtime)
       values[0]='''+str(depth)+''';
     else values[0]=C;
   }
   const double& t;
 };

class DtDepth : public Function
 {
  public:
   DtDepth (const double& t): t(t)
   {}
 private:
   void eval(double* values, const double* x) const
   {
    double time=t;
    double endtime='''+str(ETIME)+''';
    double D='''+str(DD)+''';
    double X0='''+str(XS)+''';
    double Y0='''+str(YS)+''';
    double w='''+str(OMEGA)+''';//omega
    double g='''+str(GG)+''';//gamma
    double X=x[0];
    double Y=x[1];
    if (time<endtime)
       values[0]='''+str(dtdepth)+''';
     else values[0]=0.0;
   }
   const double& t;
 };

class DtDtDepth : public Function
 {
  public:
   DtDtDepth (const double& t): t(t)
   {}
 private:
   void eval(double* values, const double* x) const
   {
    double time=t;
    double endtime='''+str(ETIME)+''';
    double D='''+str(DD)+''';
    double X0='''+str(XS)+''';
    double Y0='''+str(YS)+''';
    double w='''+str(OMEGA)+''';//omega
    double g='''+str(GG)+''';//gamma
    double X=x[0];
    double Y=x[1];
    if (time<endtime)
       values[0]='''+str(dtdtdepth)+''';
     else values[0]=0.0;
   }
   const double& t;
 };

class SourceEta : public Function
{
  public:
   SourceEta (const double& t): t(t)
   {}
 private:
   void eval(double* values, const double* x) const
   {
    double time=t;
    double endtime='''+str(ETIME)+''';
    double C='''+str(CC)+''';
    double D='''+str(DD)+''';
    double X0='''+str(XS)+''';
    double Y0='''+str(YS)+''';
    double w='''+str(OMEGA)+''';//omega
    double g='''+str(GG)+''';//gamma
    double X=x[0];
    double Y=x[1];
    double alpha=0.5917517028;
    double beta1=4.971623160e-7;
    double B3=pow(alpha,3)/3.-sqr(alpha)+5./6.*alpha-alpha*beta1/2.-5./24.;
    if (time<endtime)
       values[0]='''+str(srceta)+''';
     else values[0]=0.0;
   }
   const double& t;
 };

class SourcePhi : public Function
{
 public:
   SourcePhi (const double& t): t(t)
   {}
 private:
   void eval(double* values, const double* x) const
   {
    double time=t;
    double endtime='''+str(ETIME)+''';
    double C='''+str(CC)+''';
    double D='''+str(DD)+''';
    double X0='''+str(XS)+''';
    double Y0='''+str(YS)+''';
    double w='''+str(OMEGA)+''';//omega
     double g='''+str(GG)+''';//gamma
    double X=x[0];
    double Y=x[1];
    double alpha=0.5917517028;
    double B4=-pow(alpha,3)/3.+sqr(alpha)-alpha/2.;
    if (time<endtime)
        values[0]='''+str(srcphi)+''';
    else values[0]=0.0;
   }
   const double& t;
 };

#endif'''

## For low order tests

######## print an Depth2z0.h file to use #include "Depth2z0.h" in the main.cpp ##########

file = open('Depth2z0.h','w')

print >>file, '''
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth2z0.h
//
//
// To compile  with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#ifndef __DEPTH2Z0_H
#define __DEPTH2Z0_H

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

class Depth : public Function
{
 public:
  Depth (const double& t): t(t)
  {}
private:
  void eval(double* values, const double* x) const
  {
    double time=t;
    double endtime='''+str(ETIME)+''';
    double C='''+str(CC)+''';
    double D='''+str(DD)+''';
    double X0='''+str(XS)+''';
    double Y0='''+str(YS)+''';
    double w='''+str(OMEGA)+''';//omega
    double g='''+str(GG)+''';//gamma
    double X=x[0];
    double Y=x[1];
    if (time<endtime)
       values[0]='''+str(depth)+''';
     else values[0]=C;
   }
   const double& t;
 };

class SourceEta : public Function
{
  public:
   SourceEta (const double& t): t(t)
   {}
 private:
   void eval(double* values, const double* x) const
   {
    double time=t;
    double endtime='''+str(ETIME)+''';
    double C='''+str(CC)+''';
    double D='''+str(DD)+''';
    double X0='''+str(XS)+''';
    double Y0='''+str(YS)+''';
    double w='''+str(OMEGA)+'''; //omega
     double g='''+str(GG)+''';//gamma
    double X=x[0];
    double Y=x[1];
    if (time<endtime)
       values[0]='''+str(srcetalow)+''';
     else values[0]=0.0;
   }
   const double& t;
 };

 #endif'''

# {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
# e-mail: ndl@ptmat.fc.ul.pt
# http://ptmat.fc.ul.pt/~ndl/
# ---------------------------------------------------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
# ---------------------------------------------------------------------------
# LPTcdgHnl.ufl
#
# First added:  2009-10-12
# Last changed: 2014-07-07
#
# Bilinear and linear variational forms for
# Simplified LPT equations.
#
# The problem is solved in a form M*[eta_t,phi_t]= f(U,eta)
# Using Runge-Kutta and predictor corrector-schemes
# Using interior penalty method with CDG method
#
# Some notes:
# Note that if the Matrix M isn't rebuilt in every time step
# then t=0 is considered for the bilinear form
#
# Nonlinear terms of order O(h_0 mu^2 epsilon) are taken into account
# Thus, it is more unstable than the LPTcdg.ufl form
#
# Compile this form with FFC: ffc -f split -l dolfin -O LPTcdgHnl_1D.ufl
# }}}------------------------------------------------------------------------


##############  Constant Depth version ###########################

P=FiniteElement("Lagrange",interval,2)
Th=P*P

(eta_t,phi_t)=TrialFunctions(Th)#
(p,q)=TestFunctions(Th) #p=fb1 q=fb2

## Pay attention to the order  of argument calling
#Previous steps of the unkown functions
eta0=Coefficient(P)
phi0=Coefficient(P)
#Depth Function
h=Coefficient(P)
#Constant component of depth (LPT model)
h0=Coefficient(P)
# Time derivatives of h
dh=Coefficient(P)
ddh=Coefficient(P)
#Gravity acceleration passed as Constant in *.cpp
g=Constant(interval)
#SpongeLayer
spng=Coefficient(P) #spng('+')==spng('-')
#Artificial Surface Tension Coefficient
tension = Constant(interval) #Surface tension coefficient
#Source Function
srceta=Coefficient(P)
srcphi=Coefficient(P)

# External  normal vector
n = FacetNormal(interval)
#Penalty parameters
tau = Constant(interval)
e = 2.0*Circumradius(interval)
#we assume that the mesh nodes are equidistante

# Fluxes  coefficients
s1 = Constant(interval)
s2 = Constant(interval)

#Level related parameters
alpha=Constant(interval)
beta1=Constant(interval)
beta2=Constant(interval)

B0=-(1.-beta2)*(alpha**2/6.-alpha/3.+2./15.)-beta1*(alpha**3/2.+alpha/3.)
B1=alpha**2/2.-alpha
C1=1./2.-alpha*(1.-beta1)
C2=beta2*(alpha**2/6.-alpha/3.+2./15.)


#############a(h,h0,tau,e) definition ################################
####Bilinear Variational form for M matrix
## Interior elements contribution


a_int=eta_t*p*dx+phi_t*q*dx\
       -alpha*beta1*h**2*inner(grad(eta_t),grad(p))*dx\
       -alpha*beta1*h*inner(eta_t*grad(h),grad(p))*dx\
       -B1*h**2*inner(grad(phi_t),grad(q))*dx\
       +(alpha-alpha**2)*h*inner(grad(phi_t),grad(h))*q*dx\
       +alpha**2*beta1*h0**4*div(grad(eta_t))*div(grad(p))*dx\
       +B0*h0**4*div(grad(phi_t))*div(grad(q))*dx


## Interior Facets contribution

a_fac=-s1('+')*alpha('+')**2*beta1('+')*h0('+')**4*avg(div(grad(eta_t)))*jump(p.dx(0))*dS\
       -s2('+')*alpha('+')**2*beta1('+')*h0('+')**4*avg(div(grad(p)))*jump(eta_t.dx(0))*dS\
       -s1('+')*B0('+')*h0('+')**4*avg(div(grad(phi_t)))*jump(q.dx(0))*dS\
       -s2('+')*B0('+')*h0('+')**4*avg(div(grad(q)))*jump(phi_t.dx(0))*dS

## Boundary terms
a_bd=-s1*alpha**2*beta1*h0**4*div(grad(eta_t))*p.dx(0)*n[0]*ds\
      -s2*alpha**2*beta1*h0**4*div(grad(p))*eta_t.dx(0)*n[0]*ds\
      -s1*B0*h0**4*div(grad(phi_t))*q.dx(0)*n[0]*ds\
      -s2*B0*h0**4*div(grad(q))*phi_t.dx(0)*n[0]*ds

## Penalty terms
a_pen=tau('+')/e('+')*jump(eta_t.dx(0))*jump(p.dx(0))*dS\
       +tau('+')/e('+')*jump(phi_t.dx(0))*jump(q.dx(0))*dS\
       +tau/e*eta_t.dx(0)*p.dx(0)*ds\
       +tau/e*phi_t.dx(0)*q.dx(0)*ds

a=a_int + a_fac + a_bd + a_pen


############L(eta0,phi0,h,h0,dh,ddh,g,spng,tension,srceta,srcphi)############################
#Linear Variational form for f(U,eta)
## Interior elements contribution
l_int=(h+eta0)*inner(grad(phi0),grad(p))*dx\
       -1./2.*inner(grad(phi0),grad(phi0))*q*dx\
       -1./2.*h0**2*div(grad(phi0))*div(grad(phi0))*q*dx\
       +h0**2*div(grad(phi0))*div(grad(phi0)*q)*dx\
       +g*h0*inner(grad(eta0),grad(eta0*q))*dx\
       -B1*h0**2*div(grad(phi0))*div(eta0*grad(p))*dx\
       -alpha*beta1*h0**2*div(grad(p))*div(eta0*grad(phi0))*dx\
       +1./6.*div(grad(phi0))*div(h**3*grad(p))*dx\
       -1./2.*div(h*grad(phi0))*div(h**2*grad(p))*dx\
       +alpha*(1.-beta1)*h*div(h*grad(phi0))*div(h*grad(p))*dx\
       -alpha**2/2.*h**2*div(grad(phi0))*div(h*grad(p))*dx\
       -g*eta0*q*dx\
       -spng*inner(grad(eta0),grad(p))*dx\
       -spng*inner(grad(phi0),grad(q))*dx\
       +srceta*p*dx\
       +srcphi*q*dx\
       -tension*inner(grad(eta0),grad(q))*dx\
       -alpha*inner(h*grad(phi0),grad(dh*q))*dx\
       -alpha*inner(dh*grad(phi0),grad(h*q))*dx\
       +alpha**2*inner(grad(phi0),grad(h*dh*q))*dx\
       +eta0*ddh*q*dx\
       -g*C2*h0**4*div(grad(eta0))*div(grad(q))*dx\
       +spng*alpha*beta1*h0**2*div(grad(eta0))*div(grad(p))*dx\
       +spng*B1*h0**2*div(grad(phi0))*div(grad(q))*dx\
       -alpha*h0*inner(eta0*grad(dh),grad(p))*dx\
       +alpha*h0*inner(grad(phi0),grad(dh))*q*dx\
       +h0*inner(grad(phi0),grad(dh*q))*dx



## Interior Facets contribution
l_fac=-s1('+')*1./6.*avg(div(grad(phi0)))*jump(h**3*p.dx(0))*dS\
       +s1('+')*C1('+')*avg(div(h*grad(phi0)))*jump(h**2*p.dx(0))*dS\
       +s1('+')*alpha('+')**2/2.*avg(h**2*div(grad(phi0)))*jump(h*p.dx(0))*dS\
       -s2('+')*1./6.*avg(div(grad(p)))*jump(h**3*phi0.dx(0))*dS\
       +s2('+')*C1('+')*avg(div(h*grad(p)))*jump(h**2*phi0.dx(0))*dS\
       +s2('+')*alpha('+')**2/2.*avg(h**2*div(grad(p)))*jump(h*phi0.dx(0))*dS\
       +s1('+')*g('+')*C2('+')*h0('+')**4*avg(div(grad(eta0)))*jump(q.dx(0))*dS\
       +s2('+')*g('+')*C2('+')*h0('+')**4*avg(div(grad(q)))*jump(eta0.dx(0))*dS\
       -s1('+')*spng('+')*alpha('+')*beta1('+')*h0('+')**2*avg(div(grad(eta0)))*jump(p.dx(0))*dS\
       -s2('+')*spng('+')*alpha('+')*beta1('+')*h0('+')**2*avg(div(grad(p)))*jump(eta0.dx(0))*dS\
       -s1('+')*spng('+')*B1('+')*h0('+')**2*avg(div(grad(phi0)))*jump(q.dx(0))*dS\
       -s2('+')*spng('+')*B1('+')*h0('+')**2*avg(div(grad(q)))*jump(phi0.dx(0))*dS\
       +s1('+')*B1('+')*h0('+')**2*avg(div(grad(phi0)))*jump(eta0*p.dx(0))*dS\
       +s2('+')*B1('+')*h0('+')**2*avg(div(grad(p)))*jump(eta0*phi0.dx(0))*dS\
       +s1('+')*alpha('+')*beta1('+')*h0('+')**2*avg(div(eta0*grad(phi0)))*jump(p.dx(0))*dS\
       +s2('+')*alpha('+')*beta1('+')*h0('+')**2*avg(div(eta0*grad(p)))*jump(phi0.dx(0))*dS



## Boundary terms
l_bd=-s1*1./6.*div(grad(phi0))*h**3*p.dx(0)*n[0]*ds\
      +s1*C1*div(h*grad(phi0))*h**2*p.dx(0)*n[0]*ds\
      +s2*alpha**2/2.*h**2*div(grad(phi0))*h*p.dx(0)*n[0]*ds\
      -s2*1./6.*div(grad(p))*h**3*phi0.dx(0)*n[0]*ds\
      +s2*C1*div(h*grad(p))*h**2*phi0.dx(0)*n[0]*ds\
      +s2*alpha**2/2.*h**2*div(grad(p))*h*phi0.dx(0)*n[0]*ds\
      +s1*g*C2*h0**4*div(grad(eta0))*q.dx(0)*n[0]*ds\
      +s2*g*C2*h0**4*div(grad(q))*eta0.dx(0)*n[0]*ds\
      -s1*spng*alpha*beta1*h0**2*div(grad(eta0))*p.dx(0)*n[0]*ds\
      -s2*spng*alpha*beta1*h0**2*div(grad(p))*eta0.dx(0)*n[0]*ds\
      -s1*spng*B1*h0**2*div(grad(phi0))*q.dx(0)*n[0]*ds\
      -s2*spng*B1*h0**2*div(grad(q))*phi0.dx(0)*n[0]*ds\
      +s1*B1*h0**2*div(grad(phi0))*eta0*p.dx(0)*n[0]*ds\
      +s2*B1*h0**2*div(grad(p))*eta0*phi0.dx(0)*n[0]*ds\
      +s1*alpha*beta1*h0**2*div(eta0*grad(phi0))*p.dx(0)*n[0]*ds\
      +s2*alpha*beta1*h0**2*div(eta0*grad(p))*phi0.dx(0)*n[0]*ds


L=l_int + l_fac + l_bd

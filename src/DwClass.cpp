// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// DwClass.cpp
// First added:  2009-04-14
// Last changed: 2016-10-26
// Compiled with DOLFIN:0.9.9

#include"DwClass.h"
#include"DwConstants.h" //For dolfin::dolfwave:: CONSTANTS


using namespace dolfin;
using namespace dolfin::dolfwave;



/// Constructors--------------------------------------------------------
dolfin::dolfwave::Dolfwave::Dolfwave( )
{ NotImplemented("This constructor should not be used",__LINE__,__FILE__);}

dolfin::dolfwave::Dolfwave::Dolfwave(std::string FILENAME)
{
  std::ifstream  parametersfile (FILENAME.c_str( ));
  NotImplemented("File parameter reading",__LINE__,__FILE__);
  Assert(parametersfile.is_open(),__LINE__,__FILE__,true);
  /*
    I've a param.c and param.h for this in the old C++
    codes library
  */
  InitialOutput();
  NotImplemented("This constructor should not be used",__LINE__,__FILE__);
}


dolfin::dolfwave::Dolfwave::Dolfwave(int MAXSTEPS ,
                                     double TIMESTEP ,
                                     int WRITEGAP,
                                     std::string FORMNAME,
                                     std::string ALGBACKEND,
                                     std::string PLOTTER,
                                     std::string OUTDIR,
                                     std::string OUTFORMAT,
                                     std::string COMMENT):
  petsclu(new PETScLUSolver),
  M(new Matrix()),
  Mf(new Matrix()),
  maintimer(new Timer("X:DOLFWAVE:END"))
{


  /* TODO: Describe all the parameters */

  /// Nested dolfwave parameters
  Parameters dolfwave_parameters("dolfwave_parameters");
  // List of all the parameters
  if (OSX==0)
    dolfwave_parameters.add("pc_type","hypre_amg");
  else
    dolfwave_parameters.add("pc_type","ilu");

  dolfwave_parameters.add("pred_corr_blow_up",PC_BLOW_UP);
  dolfwave_parameters.add("pred_corr_epsilon",MSTEP_CORRECTOR_ERROR);
  dolfwave_parameters.add("solver_type",ALGBACKEND);
  dolfwave_parameters.add("lu_reuse_factorization",true);
  dolfwave_parameters.add("form_name",FORMNAME);
  dolfwave_parameters.add("write_gap",WRITEGAP);
  dolfwave_parameters.add("time_step",TIMESTEP);
  dolfwave_parameters.add("preview_program",PLOTTER);
  dolfwave_parameters.add("solutions_dir",OUTDIR);
  dolfwave_parameters.add("solutions_format",OUTFORMAT);
  dolfwave_parameters.add("max_step_number",MAXSTEPS);
  dolfwave_parameters.add("VerbosityLevel",VERBOSITY_LEVEL);

  dolfwave_parameters.add("dbcapply",false);

  dolfwave_parameters.add("Gravity",G_E);
  dolfwave_parameters.add("PenaltyTau",PENALTY_TAU);
  dolfwave_parameters.add("NonLinearSwitch",NONLINEAR_SWITCH);
  dolfwave_parameters.add("S1Flux",S1_FLUX);
  dolfwave_parameters.add("S2Flux",S2_FLUX);

  dolfwave_parameters.add("S1KdVFlux",S1_KDV_FLUX);
  dolfwave_parameters.add("S2KdVFlux",S2_KDV_FLUX);

  dolfwave_parameters.add("KdVPade",KDV_22_PADE);

  dolfwave_parameters.add("aKdVDelta0",1.0);
  dolfwave_parameters.add("aKdVEpsilon",1.0);
  dolfwave_parameters.add("aKdVDelta1",(KDV_22_PADE-1.)/6.0);
  dolfwave_parameters.add("aKdVDelta2",KDV_22_PADE/6.0);



  dolfwave_parameters.add("xKdVDelta0",1.0);
  dolfwave_parameters.add("xKdVEpsilon",1.0);
  dolfwave_parameters.add("xKdVDelta1",(KDV_22_PADE-1.)/6.0 );
  dolfwave_parameters.add("xKdVDelta2",KDV_22_PADE/6.0);
  dolfwave_parameters.add("xKdVGamma", KDV_DUMPING);
  dolfwave_parameters.add("xKdVPower", KDV_POWER);


  dolfwave_parameters.add("ExtraTermSwitch",EXTRATERM_SWITCH);


  dolfwave_parameters.add("Alpha",LPT_ALPHA);
  dolfwave_parameters.add("Beta1",LPT_BETA1);
  dolfwave_parameters.add("Beta2",LPT_BETA2);




  // Setting ranges for parameters
  {
    std::set<std::string> rng;
    rng.insert("hypre_amg");
    rng.insert("ilu");
    rng.insert("icc");

    /* options  should be:
       "none" 	No preconditioner
       "ilu" 	Incomplete LU factorization
       "icc" 	Incomplete Cholesky factorization
       "jacobi" 	Jacobi iteration
       "bjacobi" 	Block Jacobi iteration
       "sor" 	Successive over-relaxation
       "amg" 	Algebraic multigrid (BoomerAMG or ML)
       "additive_schwarz" 	Additive Schwarz
       "hypre_amg" 	Hypre algebraic multigrid (BoomerAMG)
       "hypre_euclid" 	Hypre parallel incomplete LU factorization
       "hypre_parasails" 	Hypre parallel sparse approximate inverse
       "ml_amg" 	ML algebraic multigrid
       see http://fenicsproject.org/releases/1.0-beta/index.html
 */
    dolfwave_parameters["pc_type"].set_range(rng);
  }
  {
    std::set<std::string> rng;
    rng.insert("LU_P");
    rng.insert("GMRES_P");
    dolfwave_parameters["solver_type"].set_range(rng);
  }
  {
    //TODO complete the list
    std::set<std::string> rng;
    rng.insert("Zhao");
    rng.insert("Zhao_1D");
    rng.insert("ZhaoP2");
    rng.insert("ZhaoP2_1D");
    rng.insert("LPTcdg");
    rng.insert("LPTcdg_1D");
    rng.insert("LPTcdgHnl");
    rng.insert("LPTcdgHnl_1D");
    rng.insert("BBM_1D");
    rng.insert("RLW_1D");
    rng.insert("KdVcdg_1D"); // DEPRECATED
    rng.insert("aKdVP1P2cdg_1D"); // TO BE POSTPONED
    rng.insert("xKdVcdg_1D");
    rng.insert("PA_1D");
    rng.insert("JFR_1D");
    dolfwave_parameters["form_name"].set_range(rng);
  }
  {
    std::set<std::string> rng;
    rng.insert("dolfin_interactive");
    rng.insert("dolfin_animated");
    rng.insert("xd3d");
    rng.insert("xgraphic");
    rng.insert("gnuplot");
    dolfwave_parameters["preview_program"].set_range(rng);
  }
  {
    std::set<std::string> rng;
    rng.insert("xyz");
    rng.insert("raw");
    rng.insert("pvd");
    dolfwave_parameters["solutions_format"].set_range(rng);
  }


  ///nest the parameters in dolfin parameters
  parameters.add(dolfwave_parameters);

  //Start:Private members to be initialized
  TimerStart();
  dbcapply=dolfwave_parameters["dbcapply"];
  //End:Private members to be initialized

  /// Time and Step iterator init
  Time=0.0;
  Step=0.0;

  /// Time Stoping Criterions
  IsDiverging = false;
  IsNull = false;

  // int
  MaxSteps=dolfwave_parameters["max_step_number"];
  WriteGap=dolfwave_parameters["write_gap"];
  //

  // int
  Verbosity=dolfwave_parameters["VerbosityLevel"];

  //Doubles
  TimeStep=dolfwave_parameters["time_step"];

  //Strings
  AlgBackEnd=dolfwave_parameters["solver_type"].value_str();
  Comment<<COMMENT;
  //

  std::stringstream etafilename;
  etafilename
    <<dolfwave_parameters["solutions_dir"].value_str()
    <<"/eta."
    <<dolfwave_parameters["solutions_format"].value_str();

  std::stringstream phifilename;
  phifilename
    <<dolfwave_parameters["solutions_dir"].value_str()
    <<"/phi."
    <<dolfwave_parameters["solutions_format"].value_str();

  std::stringstream depthfilename;
  depthfilename
    <<dolfwave_parameters["solutions_dir"].value_str()
    <<"/depth."
    <<dolfwave_parameters["solutions_format"].value_str();

  std::stringstream horvelfilename;
  if (dolfwave_parameters["solutions_format"].value_str()=="xyz")
    {
      horvelfilename
        <<dolfwave_parameters["solutions_dir"].value_str()
        <<"/horvel.pvd";
    }
  else
    {
      horvelfilename
        <<dolfwave_parameters["solutions_dir"].value_str()
        <<"/horvel."
        <<dolfwave_parameters["solutions_format"].value_str();
    }

  etafile.reset(new File(etafilename.str()));
  phifile.reset(new File(phifilename.str()));
  horvelfile.reset(new File(horvelfilename.str()));
  depthfile.reset(new  File(depthfilename.str()));


  std::stringstream logfilename;
  logfilename
    <<dolfwave_parameters["solutions_dir"].value_str()
    <<"/dolfwave_logfile.log";


  logfile.reset(new  std::ofstream(logfilename.str().c_str(),std::ios_base::app));
  /// Setting Linear Solver Environment
  SetLinearSolver( );


  /// These are arguments  for some ufl form files.
  /* Be sure that all the constants are
     initialized before functions initialization */

  /// Initialization of Gravitational function
  g.reset(new Constant(dolfwave_parameters["Gravity"]));

  // TODO Simplify this
  // Initialization of Penalty function
  // It is required by LPT cdg formulations
  if ((dolfwave_parameters["form_name"].value_str()=="LPTcdg")||
      (dolfwave_parameters["form_name"].value_str()=="LPTcdg_1D")||
      (dolfwave_parameters["form_name"].value_str()=="LPTcdgHnl")||
      (dolfwave_parameters["form_name"].value_str()=="LPTcdgHnl_1D"))
    tau.reset(new Constant(dolfwave_parameters["PenaltyTau"]));
  else
    tau.reset(new Constant(0.0));
  // Initialization of numerical fluxes coefficients for LPTcdg
  s1flux.reset(new Constant(dolfwave_parameters["S1Flux"]));
  s2flux.reset(new Constant(dolfwave_parameters["S2Flux"]));
  // Initialization of numerical fluxes coefficients for KdV
  s1kdvflux.reset(new Constant(dolfwave_parameters["S1KdVFlux"]));
  s2kdvflux.reset(new Constant(dolfwave_parameters["S2KdVFlux"]));
  // Initialization of dumping coefficient in xKdV
  xkdvdumping.reset(new Constant(dolfwave_parameters["xKdVGamma"]));
  // Initialization of dumping coefficient in xKdV
  xkdvpower.reset(new Constant(dolfwave_parameters["xKdVPower"]));
  // Initialization of phase velocity improvement parameter for KdV
  kdvpade.reset(new Constant(dolfwave_parameters["KdVPade"]));
  // Initialization of alpha level parameter for LPTcdg*
  alpha.reset(new Constant(dolfwave_parameters["Alpha"]));
  // Initialization of beta_1 dispersion parameter LPTcdg
  beta1.reset(new Constant(dolfwave_parameters["Beta1"]));
  // Initialization of beta_2 dispersion parameter for LPTcdg
  beta2.reset(new Constant(dolfwave_parameters["Beta2"]));
  // Initialization nonlinear switch for ufl forms
  nonlinear.reset(new Constant(dolfwave_parameters["NonLinearSwitch"]));
 // Initialization extra sixth-order term switch for LPTcdg*.ufl forms
  extraterm.reset(new Constant(dolfwave_parameters["ExtraTermSwitch"]));
  //Display information
  InitialOutput();
}
// }}}------------------------------------------------------------------

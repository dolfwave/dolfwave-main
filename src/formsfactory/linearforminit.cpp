// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
// DwLinearFormInit.cpp
//
// First added:  2009-04-14
// Last changed - nlopes: 2017-04-28
//
// Compiled with DOLFIN:1.6.0
// }}}------------------------------------------------------------------------

#include"../DwClass.h"
#include"../forms.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


      /*
         For the order of argument calling,  look at fooform.h (fooform.ufl) for
         virtual dolfin::int coefficient_number(const std::string& name) const
         and
         virtual dolfin::uint coefficient_name(const std::string& name) const
      */

// {{{ LinearFormsInit ---------------------------------------------



void dolfin::dolfwave::Dolfwave::LinearFormsInit(Expression &depth   ,Expression &constantdepth,
                                                 Expression &dtdepth ,Expression &dtdtdepth,
                                                 Expression &sponge  ,Expression &tension,
                                                 Expression &srceta  ,Expression &srcphi)
{
  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  L.reset(new Form_Ptr [5]);
  if (FormName=="LPTcdg")
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new LPTcdg::LinearForm(*V,(*eta_phi[i])[0],
                                            (*eta_phi[i])[1],
                                            depth,constantdepth,
                                            dtdepth,dtdtdepth,
                                            *g,sponge,tension,
                                            srceta,srcphi,
                                            *s1flux,*s2flux,
                                            *alpha,*beta1,*beta2));
        }
    }
  else if (FormName=="LPTcdg_1D")
    {
      for(int i=0;i<5;i++)
        {

          L[i].reset(new LPTcdg_1D::LinearForm(*V,(*eta_phi[i])[0],
                                               (*eta_phi[i])[1],depth,
                                               constantdepth,dtdepth,
                                               dtdtdepth,
                                               *g,sponge,tension,
                                               srceta,srcphi,
                                               *s1flux,*s2flux,
                                               *alpha,*beta1,*beta2,
                                               *nonlinear,*extraterm));
        }
    }
  else if (FormName=="LPTcdgHnl")
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new LPTcdgHnl::LinearForm(*V,(*eta_phi[i])[0],
                                               (*eta_phi[i])[1],
                                               depth,constantdepth,dtdepth,
                                               dtdtdepth,*g,sponge,tension,
                                               srceta,srcphi,
                                               *s1flux,*s2flux,
                                               *alpha,*beta1,*beta2));
        }
    }
  else if (FormName=="LPTcdgHnl_1D")
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new LPTcdgHnl_1D::LinearForm(*V,(*eta_phi[i])[0],
                                                  (*eta_phi[i])[1],
                                                  depth,constantdepth,
                                                  dtdepth,dtdtdepth,
                                                  *g,sponge,tension,
                                                  srceta,srcphi,
                                                  *s1flux,*s2flux,
                                                  *alpha,*beta1,*beta2));
        }
    }
  else
    NotImplemented("Linear Forms Init",__LINE__,__FILE__);
}


void dolfin::dolfwave::Dolfwave::LinearFormsInit(Expression &depth,
                                                 Expression &sp_eta,
                                                 Expression &sp_lap_eta,
                                                 Expression &sp_phi,
                                                 Expression &sp_lap_phi,
                                                 Expression &srceta,
                                                 Expression &pcf)
{
  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  L.reset(new Form_Ptr [5]);
  if (FormName=="Zhao_1D")//Zhao_1D
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new Zhao_1D::LinearForm(*V,(*eta_phi[i])[0],
                                             (*eta_phi[i])[1],
                                             depth,*g,sp_eta,
                                             sp_lap_eta,sp_phi,
                                             sp_lap_phi,srceta,
                                             pcf,
                                             *nonlinear));
        }
    }
  else if (FormName=="Zhao")//Zhao
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new Zhao::LinearForm(*V,(*eta_phi[i])[0],
                                          (*eta_phi[i])[1],
                                          depth,*bathymetry,
                                          *g,
                                          sp_eta,sp_lap_eta,sp_phi,
                                          sp_lap_phi,srceta,
                                          *nonlinear,pcf));
        }
    }
  else
    NotImplemented("Linear Forms Init Several sponges.",__LINE__,__FILE__);
}

void dolfin::dolfwave::Dolfwave::LinearFormsInit(Expression &depth,
                                                 Expression &sp_eta,
                                                 Expression &sp_lap_eta,
                                                 Expression &sp_phi,
                                                 Expression &sp_lap_phi,
                                                 Expression &srceta)
{
  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  L.reset(new Form_Ptr [5]);
  /*if (FormName=="Zhao")//Zhao
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new Zhao::LinearForm(*V,(*eta_phi[i])[0],
                                          (*eta_phi[i])[1],
                                          depth,*bathymetry,
                                          *g,
                                          sp_eta,sp_lap_eta,sp_phi,
                                          sp_lap_phi,srceta,
                                          *nonlinear));
        }
    }
  else
  */
 if (FormName=="ZhaoP2_1D")//Zhao
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new ZhaoP2_1D::LinearForm(*V,(*eta_phi[i])[0],
                                               (*eta_phi[i])[1],depth,*g,
                                               sp_eta,sp_lap_eta,sp_phi,
                                               sp_lap_phi,srceta,
                                               *nonlinear));
        }
    }
  else
    NotImplemented("Linear Forms Init Several sponges.",__LINE__,__FILE__);
}



void dolfin::dolfwave::Dolfwave::LinearFormsInit(Expression &depth)
{

  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  L.reset(new Form_Ptr [5]);
  if (FormName=="BBM_1D")//BBM_1D
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new BBM_1D::LinearForm(*V,*eta_phi[i],depth,*g));
        }
    }
  else
    NotImplemented("Linear Forms Init Several sponges.",__LINE__,__FILE__);
}


void dolfin::dolfwave::Dolfwave::LinearFormsInit(Expression &pf,Expression &u0,Expression &u0_dt_dx, MeshFunction<std::size_t> &boundary_parts)
{

  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  L.reset(new Form_Ptr [5]);

  if (FormName=="xKdVcdg_1D")//dimensionaless xKdVcdg_1D
    {
      for(int i=0;i<5;i++)
        {
          // For this form 'xpression' stands for the 'initial_condition_dt_dx'
          L[i].reset(new xKdVcdg_1D::LinearForm(*V,*eta_phi[i],*xkdvd0,
                                                *xkdveps,/* *xkdvpower,*/
                                                *xkdvd2, *xkdvdumping, *tau,
                                                *s1kdvflux,*s2kdvflux,
                                                pf,u0,u0_dt_dx));
          L[i]->ds=boundary_parts;
        }
    }
  else
    NotImplemented("Linear Forms Init Several sponges.",__LINE__,__FILE__);
}

/*
void dolfin::dolfwave::Dolfwave::LinearFormsInit(Expression &pf_t, Expression &pf, Expression &u0, Expression &wt0,Expression &wtx0)
{
  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  L.reset(new Form_Ptr [5]);
  if (FormName=="xKdVcdg_1D")//dimensionaless xKdVcdg_1D
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new xKdVcdg_1D::LinearForm(*V,*eta_phi[i],
                                                *xkdvd0,*xkdveps,*xkdvd2,
                                                *s1kdvflux,*s2kdvflux));
        }
    }
  else
    NotImplemented("Linear Forms Init Several sponges.",__LINE__,__FILE__);
}
*/




void dolfin::dolfwave::Dolfwave::LinearFormsInit( )
{
  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  L.reset(new Form_Ptr [5]);

  // POSTPONED
  if (FormName=="aKdVcdgP1P2_1D")//dimensionaless KdVcdg_1D
    {
      for(int i=0;i<5;i++)
        {
          L[i].reset(new aKdVcdgP1P2_1D::LinearForm(*V,*eta_phi[i],*akdvd0,*akdveps,*akdvd2,
                                               *s1kdvflux,*s2kdvflux));
        }
    }
  else
    NotImplemented("Wrong arguments. Form constructor only available for xKdVcdg_1D experimental forms",__LINE__,__FILE__);
}


// }}} --------------------------------------------------------

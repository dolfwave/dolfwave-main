// Copyright (C) 2009 Nuno David Lopes.----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// bilininearforminit.cpp
// First added:  2009-04-14
// Last changed - nlopes: 2017-04-28
// Compiled with DOLFIN:1.6

#include"../DwClass.h"
#include"../forms.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


      /*
         For the order of argument calling,  look at fooform.h (fooform.ufl) for
         virtual dolfin::uint coefficient_number(const std::string& name) const
         and
         virtual dolfin::uint coefficient_name(const std::string& name) const
      */

// {{{ BilinearFormInit----------------------------------------------
void dolfin::dolfwave::Dolfwave::BilinearFormInit(MeshFunction<std::size_t> &boundary_parts)
{

  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  if (FormName=="aKdVcdgP1P2_1D")// dimensionaless KdV_1D
    {
      a.reset(new aKdVcdgP1P2_1D::BilinearForm(*V,*V,*akdvd1,*tau));//h=h0
    }
  else if (FormName=="xKdVcdg_1D")// dimensionaless KdV_1D
    {
      a.reset(new xKdVcdg_1D::BilinearForm(*V,*V,*xkdvd1,*tau));
      a->ds=boundary_parts;
      // high-order terms disabled for now
    }
  else
    {
      NotImplemented("Bilinear Form Init",__LINE__,__FILE__);
    }
}



void dolfin::dolfwave::Dolfwave::BilinearFormInit(Mesh &mesh, Expression &depth)
{

  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();


  if (FormName=="Zhao")//Zhao
    {
      //a.reset(new Zhao::BilinearForm(*V,*V,depth,*bathymetry));
    }
  else if (FormName=="ZhaoP2")//Zhao
    {
      a.reset(new ZhaoP2::BilinearForm(*V,*V,depth));
    }
  else if (FormName=="ZhaoP2_1D")//Zhao
    {
      a.reset(new ZhaoP2_1D::BilinearForm(*V,*V,depth));
    }
  else if (FormName=="BBM_1D")//BBM_1D
    {
       a.reset(new BBM_1D::BilinearForm(*V,*V,depth));//h=h0
    }
  else
    {
      NotImplemented("Bilinear Form Init",__LINE__,__FILE__);
    }
}

void dolfin::dolfwave::Dolfwave::BilinearFormInit(Mesh&mesh, Expression &depth, Expression &constantdepth)
{

   std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  if (FormName=="LPTcdg")
    {

      a.reset(new LPTcdg::BilinearForm(*V,*V,depth,constantdepth,
                                       *tau,
                                       *s1flux,*s2flux,
                                       *alpha,*beta1,*beta2));
    }
  // Temporay
  else if (FormName=="Zhao")
    {
      a.reset(new Zhao::BilinearForm(*V,*V,depth,*bathymetry,constantdepth));
    }
  else if (FormName=="LPTcdg_1D")
    {
      a.reset(new LPTcdg_1D::BilinearForm(*V,*V,depth,constantdepth,
                                          *tau,
                                          *s1flux,*s2flux,
                                          *alpha,*beta1,*beta2,
                                          *extraterm));
    }
  else if (FormName=="LPTcdgHnl")
    {
      a.reset(new LPTcdgHnl::BilinearForm(*V,*V,depth,constantdepth,
                                          *tau,
                                          *s1flux,*s2flux,
                                          *alpha,*beta1,*beta2));
    }
  else if (FormName=="LPTcdgHnl_1D")
    {
      a.reset(new LPTcdgHnl_1D::BilinearForm(*V,*V,depth,constantdepth,
                                             *tau,
                                             *s1flux,*s2flux,
                                             *alpha,*beta1,*beta2));
    }
  else if (FormName=="Zhao_1D")//Zhao
    {
      // Note that here constantdepth is a penalization parameter for
      // switching transparent bcs
      a.reset(new Zhao_1D::BilinearForm(*V,*V,depth,constantdepth));
    }
  else
    {
      NotImplemented("Bilinear Form Init",__LINE__,__FILE__);
    }
}

// }}}---------------------------------------------------------------

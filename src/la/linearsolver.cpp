// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  DolfwaveClassLSolver.cpp
//
// First added:  2009-04-14
// Last changed: 2014-07-07
//
// Compiled with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#include"../DwClass.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


// {{{ Linear Solver ----------------------------------

void dolfin::dolfwave::Dolfwave::SetLinearSolver( )
{

  /// Possibilities are :
  ///  "GMRES_P": gmres with amg_hypre krylov solver via PETSc ,
  ///  "GMRES_E": gmres with amg_hypre krylov solver via Epetra ,
  ///  "LU_P": LU solver via PETSc
  ///  "LU_E":LU solver via PETSc

  if (AlgBackEnd=="LU_P")
    {
      parameters["linear_algebra_backend"]="PETSc";
    }
  else if (AlgBackEnd=="LU_E")
    {
      //parameters["linear_algebra_backend"]="Epetra";
      info("TRILINOS NOT USED");
    }
  else if (AlgBackEnd=="GMRES_P")
    {
      parameters["linear_algebra_backend"]="PETSc";
      parameters("krylov_solver")["relative_tolerance"]= 1e-6;
      parameters("krylov_solver")["absolute_tolerance"]= 1e-15;
      parameters("krylov_solver")["maximum_iterations"]=100;
    }
  else
    {
      NotImplemented("Please define the Linear Solver",__LINE__,__FILE__);
    }

}


void dolfin::dolfwave::Dolfwave::LUFactorization(bool reuse )
{

  if (AlgBackEnd=="LU_P")
    {
      if (reuse==true)
        info("#dw# Reuse the LU factorization of matrix\n");
      else
        info("#dw# Do not reuse the LU factorization of matrix\n");

      petsclu.reset(new PETScLUSolver("default"));
      petsclu->set_operator(Mf);
      if(dbcapply)
        dbc->apply(*Mf,*pcrk_b[5]);
      //pestsclu->parameters["same_nonzero_pattern"] = true;
      parameters("dolfwave_parameters")["lu_reuse_factorization"]= reuse;
      petsclu->parameters["reuse_factorization"]=
        bool(parameters("dolfwave_parameters")["lu_reuse_factorization"]);
    }
  else if ((AlgBackEnd=="GMRES_P"))
    {
      std::cout<<"#dw# The solver "<<AlgBackEnd<<" do not require factorization"<<std::endl;
    }
  else
    {
     NotImplemented("This message should never appear :D",__LINE__,__FILE__);
    }
}
// }}}-----------------------------------------------



// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  DolfwaveClassMatrix.cpp
//
// First added:  2009-04-14
// Last changed: 2012-01-27
//
// Compiled with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#include"../DwClass.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


// {{{MatricesAssemble.. ---------------------
void dolfin::dolfwave::Dolfwave::MatricesAssemble( )
{
  // M.reset(new Matrix(0,0));
  // Mf.reset(new Matrix(0,0));
  assemble(*M,*a);
  assemble(*Mf,*a);

  if(dbcapply) //apply dirichlet boundary condition
    dbc->apply(*Mf,*pcrk_b[5]);
}

// }}} ----------------------------------------------------

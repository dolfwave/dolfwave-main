// Copyright (C) 2009 Nuno David Lopes.------------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// DwConstants.h
// First added:  2009-10-27
// Last changed: 2016-10-26
// Compiled with DOLFIN:1.0.X

#ifndef __DOLFWAVECONSTANTS_H
#define __DOLFWAVECONSTANTS_H

#include<cmath>

// In this file only namespace protected consts should be defined

namespace dolfin
{
  namespace dolfwave
  {
    /// Most used gravitational constant g=9.81
    /**  Earth's Gravitational constant m/s^2 in Lisbon
         is 9.801
         Reference WGS 1984 Blakely, p. 136 */

    const double G_E=9.81; //default units
    const double G_E_M_S2=9.81; //default units
    const double G_E_K_M2=35.316; // Km/mnts^2
    const double G_E_K_H2=127137.6; // Km/h^2

    /* Some usefull math.h constants */

    ///pi=3.14...
    const double DW_PI=M_PI;

    /// e=2.7...
    const double DW_E=M_E;

    /* Some DOLFWAVE parameters */
    /* Use "parameter_name" format */

    /// Blow up criterion for predictor corrector
    const int PC_BLOW_UP=5;

    /// setting the verbosity level
    const int VERBOSITY_LEVEL=1;

    /// Error for multi-step corrector
    const double MSTEP_CORRECTOR_ERROR= 1.0e-8;

    /*Signature  of the Finite Elements Used in the code */
    /// Signature of  continuous lagrange P1
    const std::string CONTINUOUS_P1_1D1S="FiniteElement('Lagrange', Domain(Cell('interval', 1)), 1, None)";

    /// Signature of continuous lagrange P2
    const std::string CONTINUOUS_P2_1D1S="FiniteElement('Lagrange', Domain(Cell('interval', 1)), 2, None)";



    /// Signature of mixed continuous lagrange P1
    const std::string CONTINUOUS_P1="MixedElement(FiniteElement('Lagrange', Domain(Cell('triangle', 2)), 1, None), FiniteElement('Lagrange', Domain(Cell('triangle', 2)), 1, None), **{'value_shape': (2,) })";


    /// Signature of mixed continuous lagrange P2
    const std::string CONTINUOUS_P2="MixedElement(FiniteElement('Lagrange', Domain(Cell('triangle', 2)), 2, None), FiniteElement('Lagrange', Domain(Cell('triangle', 2)), 2, None), **{'value_shape': (2,) })";




    /// Signature of mixed discontinuous lagrange P1 (TO DECIDE WHAT TO DO)
    const std::string DISCONTINUOUS_P1="FiniteElement('Discontinuous Lagrange', Cell('triangle', 1, Space(2)), 1)";

    /// Signature of mixed continuous lagrange P1_1D
    const std::string CONTINUOUS_P1_1D="MixedElement(FiniteElement('Lagrange', Domain(Cell('interval', 1)), 1, None), FiniteElement('Lagrange', Domain(Cell('interval', 1)), 1, None), **{'value_shape': (2,) })";

    /// Signature of mixed continuous lagrange P2_1D
    const std::string CONTINUOUS_P2_1D="MixedElement(FiniteElement('Lagrange', Domain(Cell('interval', 1)), 2, None), FiniteElement('Lagrange', Domain(Cell('interval', 1)), 2, None), **{'value_shape': (2,) })";


    /*lpt equations default parameters */

    /// default level for lpt equations
    const double LPT_ALPHA=0.5917517028;
    /// default lpt beta1 dispersion parameter
    const double LPT_BETA1=4.971623160e-7;
    /// default lpt beta2 dispersion parameter
    const double LPT_BETA2=-8.100610226e-4 ;

    /// default flux coefficients for LPTcdg forms
    /* These coefficients are derived for the 1HD case */
    /* But they are used by default also in the 2HD Forms */
    const double S1_FLUX=1.382576169;
    const double S2_FLUX=-0.905932629;

    /// default coefficients for 3rd KdV (generalized) form
    /*kdv_pade=1.0 is pure KdV -> Pade [2,1] (Taylor) */
    /*kdv_pade=0.0 is pure BBM -> Pade [1,2] use BBM form*/

    const double KDV_22_PADE=-0.9; // Pade [2,2]
    const double KDV_21_PADE=1.0; // Pade [2,1]


    /// default flux coefficients for (x)KdV forms
    /* These coefficients are  improved for consistency */
    /* These coefficients are independent of the kdv_pade improvement */
    const double S1_KDV_FLUX=1.0; // To decide
    /* To decide The dispersion is independent */
    const double S2_KDV_FLUX=0.0;

    /// Dumping coef. for xKdV form.
    const double KDV_DUMPING=0.0;

    /// Standard Power for  xKdV (generalized) nonlinear term.
    const double KDV_POWER=2.0;

    ///default shoaling improvement coefficient for LPT forms
    const double SHOALING=0.29; // TODO

    /// default penalty parameter for LPT formulations;
    const double PENALTY_TAU=0.1;

    /// default  nonlinear_switch is on
    const double NONLINEAR_SWITCH=1.0;

    /// default  extraterm_switch is on
    const double EXTRATERM_SWITCH=1.0;


    /// Small DOLFWAVE number
    const double DW_EPS=1.0e-16;

    /// Huge DOLFWAVE number
    const double DW_HUGE=1.0e+15;

   /// Overloading pow function
    inline double pow(double x, int p)
    {
      return std::pow(x,p);
    }

    inline double sqr(double x)
    {
      return (x*x);
    }
  }
}
#endif //__DOLFWAVECONSTANTS_H

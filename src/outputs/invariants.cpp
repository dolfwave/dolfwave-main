// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  plots.cpp
// First added:  2009-04-14
// Last changed: 2014-07-10
// Compiled with DOLFIN:1.0.X

#include "../DwClass.h"
#include "../DwConstants.h"
#include "../2hdforms/Vol.h"
#include "../1hdforms/Vol_1D.h"
#include "../2hdforms/VolDG.h"
#include "../2hdforms/VolP2.h"
#include "../1hdforms/VolP2_1D.h"

/// Only for 1HD1S type problems
#include "../1hd1sforms/Inv1BBM_1D.h" //Invariants for BBM equations
#include "../1hd1sforms/Inv2BBM_1D.h" //Invariants for BBM equations
#include "../1hd1sforms/Inv3BBM_1D.h" //Invariants for BBM equations
#include "../1hd1sforms/Inv1KdVBBMP2_1D.h" //Invariants for KdV-BBM equations
#include "../1hd1sforms/Inv2KdVBBMP2_1D.h" //Invariants for KdV-BBM equations
#include "../1hd1sforms/Inv3KdVBBMP2_1D.h" //Invariants for KdV-BBM equations


#include <fstream> // For file stream
#include<iomanip> //For setprecision

using namespace dolfin;
using namespace dolfin::dolfwave;

// {{{ Invariants---------------------------------------


/// Save the volume (or function integral)
double dolfin::dolfwave::Dolfwave::Volume(const Mesh &mesh,
                                        Expression &depth,
                                        std::string filename ) const
{
  if (filename=="")
    {
      std::stringstream default_filename;
      default_filename
        <<parameters("dolfwave_parameters")["solutions_dir"].value_str()
        <<"/volume.tv";
      filename=default_filename.str();
    }

  double  volume=0.0;
  std::ofstream fp(filename.c_str(),std::ios_base::app);//open and appending
  fp<<std::fixed;
  fp<<std::setprecision(15);
  std::shared_ptr<Form> functional; //integral functional


  if (V->element()->signature()==CONTINUOUS_P1)
    functional.reset(new Vol::Functional(mesh,(*eta_phi[4])[0],depth));
  else if (V->element()->signature()==CONTINUOUS_P1_1D)
    functional.reset(new Vol_1D::Functional(mesh,(*eta_phi[4])[0],depth));
  else if (V->element()->signature()==CONTINUOUS_P2)
    functional.reset(new VolP2::Functional(mesh,(*eta_phi[4])[0],depth));
  else if (V->element()->signature()==CONTINUOUS_P2_1D)
    functional.reset(new VolP2_1D::Functional(mesh,(*eta_phi[4])[0],depth));
  else if (V->element()->signature()==DISCONTINUOUS_P1)
    functional.reset(new VolDG::Functional(mesh,(*eta_phi[4])[0],depth));
  else NotImplemented(V->element()->signature(),__LINE__,__FILE__);

  volume=assemble(*functional);
  fp<<Time<<" "<<volume<<std::endl;
  fp.close();

  return volume;
 }

//----------------------------------------------------



/// Save the First Invariant for KdV-BBM type equations
double dolfin::dolfwave::Dolfwave::FirstInvariant(const Mesh &mesh,
                                        std::string filename ) const
{
  if (filename=="")
    {
      std::stringstream default_filename;
      default_filename
        <<parameters("dolfwave_parameters")["solutions_dir"].value_str()
        <<"/first_invariant.tv";
      filename=default_filename.str();
    }

  double  firstinvariant=0.0;
  std::ofstream fp(filename.c_str(),std::ios_base::app);//open and appending
  fp<<std::fixed;
  fp<<std::setprecision(15);
  std::shared_ptr<Form> functional; //integral functional

  if (V->element()->signature()==CONTINUOUS_P1_1D1S)
    functional.reset(new Inv1BBM_1D::Functional(mesh,*eta_phi[4]));
  else if (V->element()->signature()==CONTINUOUS_P2_1D1S)
    functional.reset(new Inv1KdVBBMP2_1D::Functional(mesh,*eta_phi[4]));
  else NotImplemented(V->element()->signature(),__LINE__,__FILE__);

  firstinvariant=assemble(*functional);
  fp<<Time<<" "<<firstinvariant<<std::endl;
  fp.close();

  return firstinvariant;
 }

//----------------------------------------------------


/// Save the Second Invariant for KdV-BBM type equations
double dolfin::dolfwave::Dolfwave::SecondInvariant(const Mesh &mesh,
                                                 std::string filename ) const
{
  if (filename=="")
    {
      std::stringstream default_filename;
      default_filename
        <<parameters("dolfwave_parameters")["solutions_dir"].value_str()
        <<"/second_invariant.tv";
      filename=default_filename.str();
    }

  double  secondinvariant=0.0;
  std::ofstream fp(filename.c_str(),std::ios_base::app);//open and appending
  fp<<std::fixed;
  fp<<std::setprecision(15);
  std::shared_ptr<Form> functional; //integral functional

  if (V->element()->signature()==CONTINUOUS_P1_1D1S)
    functional.reset(new Inv2BBM_1D::Functional(mesh,*eta_phi[4],*xkdvd1));
  else if (V->element()->signature()==CONTINUOUS_P2_1D1S)
    functional.reset(new Inv2KdVBBMP2_1D::Functional(mesh,*eta_phi[4],*xkdvd1));
  else NotImplemented(V->element()->signature(),__LINE__,__FILE__);

  secondinvariant=assemble(*functional);
  fp<<Time<<" "<<secondinvariant<<std::endl;
  fp.close();

  return secondinvariant;
}

//----------------------------------------------------


/// Save the Second Invariant for KdV-BBM type equations
double dolfin::dolfwave::Dolfwave::ThirdInvariant(const Mesh &mesh,
                                                const Constant &urs,
                                                std::string filename ) const
{
  if (filename=="")
    {
      std::stringstream default_filename;
      default_filename
        <<parameters("dolfwave_parameters")["solutions_dir"].value_str()
        <<"/third_invariant.tv";
      filename=default_filename.str();
    }

  double  thirdinvariant=0.0;
  std::ofstream fp(filename.c_str(),std::ios_base::app);//open and appending
  fp<<std::fixed;
  fp<<std::setprecision(15);
  std::shared_ptr<Form> functional; //integral functional

  if (V->element()->signature()==CONTINUOUS_P1_1D1S)
    functional.reset(new Inv3BBM_1D::Functional(mesh,*eta_phi[4],urs));
  else if (V->element()->signature()==CONTINUOUS_P2_1D1S)
    functional.reset(new Inv3KdVBBMP2_1D::Functional(mesh,*eta_phi[4],urs));
  else NotImplemented(V->element()->signature(),__LINE__,__FILE__);

  thirdinvariant=assemble(*functional);
  fp<<Time<<" "<<thirdinvariant<<std::endl;
  fp.close();

  return thirdinvariant;
}

//----------------------------------------------------

// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  plots.cpp
// First added:  2009-04-14
// Last changed: 2016-06-29
// Compiled with DOLFIN:1.6.0

#include "../DwClass.h"
#include "../DwConstants.h"
#include "../2hdforms/SymProj.h"
#include "../1hdforms/SymProj_1D.h"
#include "../2hdforms/SymProjP2.h"
#include "../1hdforms/SymProjP2_1D.h"
#include "../2hdforms/Proj.h"
#include "../1hdforms/Proj_1D.h"
#include "../2hdforms/ProjP2.h"
#include "../1hdforms/ProjP2_1D.h"
#include "../2hdforms/GradP0.h"


#include <fstream> // For file stream
#include<iomanip> //For setprecision

using namespace dolfin;
using namespace dolfin::dolfwave;

// {{{ Plot Functions---------------------------------------

/*
preview the Function func with dolfin plotter
*/
void dolfin::dolfwave::Dolfwave::DolfinPlot(const dolfin::Function &func) const
{

  /// Checkout all the parameters available for dolfin plotter in dolfin/plot/VTKPlotter.h


  std::string dolfin_plotter=parameters("dolfwave_parameters")["preview_program"].value_str();

  Parameters plotparameters;
  plotparameters.add("rescale", true);
  plotparameters.add("title", "Solution");
  if(dolfin_plotter == "dolfin_interactive")
    plotparameters.add("interactive",true);
  else if (dolfin_plotter == "dolfin_animated")
    plotparameters.add("interactive",false);
  plot(func,plotparameters);

}

/*
Preview a scalar function for 1D only
using gnuplot
*/

void dolfin::dolfwave::Dolfwave::GnuPlot(const dolfin::Function &func) const
{
  std::stringstream command;
  std::stringstream gnuplot_commands;
  // Save to file
  //gnuplot comand file  output file
  std::shared_ptr<std::ofstream> gnuplot_file;
  gnuplot_file.reset(new  std::ofstream("dolfwave.gnuplot",std::ios_base::out));

  File file("dolfin_gnuplot.xyz");
  file << func;


  gnuplot_commands<<"set autoscale"<<std::endl;
  gnuplot_commands<<"unset label"<<std::endl;
  gnuplot_commands<<"set title 'Gnuplot preview'"<<std::endl;
  if ((ModelType=="1HD1S")||(ModelType=="1HD"))
    {
      gnuplot_commands<<"set style fill solid 0.1"<<std::endl;
      gnuplot_commands<<"set style data lines"<<std::endl;
      gnuplot_commands<<"plot  'dolfin_gnuplot000000.xyz'  w  filledcu above x1  lc rgb '#00bbdd'  notitle , 'dolfin_gnuplot000000.xyz'  lw 2  lc rgb '#000000'  title 'Solution at Step:"<<Step<<"'"<<std::endl;
    }
  else if (ModelType=="2HD")
    {
      gnuplot_commands<<"set style data lines"<<std::endl;
      gnuplot_commands<<"set pm3d scansforward"<<std::endl;
      gnuplot_commands<<"splot  'dolfin_gnuplot000000.xyz' pal title 'Solution at Step:"<<Step<<"'"<<std::endl;
    }
      *gnuplot_file<<gnuplot_commands.str()<<std::flush;

  warning("#dw# TODO: Improve 3D: Preview with gnuplot.");



  // Plot data from file
  command << "gnuplot -p dolfwave.gnuplot";
  if (system(command.str().c_str()) != 0)
    warning("#dw# Unable to plot.");
}

/*
Preview the function func with Xd3d
*/
void dolfin::dolfwave::Dolfwave::Xd3dXyzPlot(const dolfin::Function &func) const
{
  std::stringstream command;
  info("#dw# Preview with  xd3d 'xyz' file.");

  // Save to file
  File file("dolfin_xd3d.xyz");

  file << func;
  // Plot data from file
  command << "xd3d -english -bord=3 -iso=z -iso=1 -nbcol=128  dolfin_xd3d000000.xyz";
  if (system(command.str().c_str()) != 0)
    warning("Unable to plot.");

}

/*
Preview the function func with Xgraphic
*/
void dolfin::dolfwave::Dolfwave::XgraphicXyPlot(const dolfin::Function &func) const
{
  std::stringstream command;
  info("#dw# Preview with  xgraphic 'xyz' file, Press 'q' to resume");

  // Save to file
  File file("dolfin_xgraphic.xyz");

  file << func;
  // Plot data from file
  command << "xgraphic   dolfin_xgraphic000000.xyz";
  if (system(command.str().c_str()) != 0)
    warning("Unable to plot.");

}

/*
 Plot eta_phi functions
 show=true will show a preview the surface elevation eta with dolfin plotter
 surface=true will save eta in raw file format
 potential=true  will save (potential or pseudo potential) phi in raw format
*/


void dolfin::dolfwave::Dolfwave::PreviewPhi(unsigned int i) const
{

  std::string dolfin_plotter=parameters("dolfwave_parameters")["preview_program"].value_str();

  if (i>4){info("#dw# WARNING: Invalid index i for eta_phi[i], using i=4\n"); i=4;}

  /// Preview program: "xd3d" "xgraphic" or "dolfin" plotters
  /** "xd3d" is safe in convex domains only
      "xgraphic" only for 1HD */

  /// Only for Boussinesq-type systems
  /// of the form (ModelType=="1HD") or (ModelType=="2HD")

  if (ModelType=="1HD1S")
    NotImplemented("Plot functions not implemented for this equations ",__LINE__,__FILE__);

  if  ((dolfin_plotter=="dolfin_interactive") ||
       (dolfin_plotter=="dolfin_animated"))
    DolfinPlot((*eta_phi[i])[1]);
  else if (parameters("dolfwave_parameters")["preview_program"].
           value_str()=="xd3d")
    Xd3dXyzPlot((*eta_phi[i])[1]);
  else if (parameters("dolfwave_parameters")["preview_program"].
           value_str()=="xgraphic")
    XgraphicXyPlot((*eta_phi[i])[1]);
  else if (parameters("dolfwave_parameters")["preview_program"].
           value_str()=="gnuplot")
    GnuPlot((*eta_phi[i])[1]);
  else
    NotImplemented("Preview option",__LINE__,__FILE__);

}



void dolfin::dolfwave::Dolfwave::PreviewEta(unsigned int i) const
{

  std::string dolfin_plotter=parameters("dolfwave_parameters")["preview_program"].value_str();

  /// Preview program: "xd3d" "xgraphic" or "dolfin" plotters
  /** "xd3d" is safe in convex domains only
      "xgraphic" only for 1HD */

  if (i>4){info("#dw# WARNING: Invalid index i for eta_phi[i], using i=4\n"); i=4;}

  if ((dolfin_plotter=="dolfin_interactive") ||
      (dolfin_plotter=="dolfin_animated"))
    {
      /// For KdV-type equations (ModelType=="1HD1S")
      if (ModelType=="1HD1S")
        DolfinPlot(*eta_phi[i]);
      /// For Boussinesq-type equations of the type
      /// (ModelType=="2HD") or (ModelType=="1HD"))
      else if ((ModelType=="2HD")||(ModelType=="1HD"))
        DolfinPlot((*eta_phi[i])[0]);
      else
        NotImplemented("Plot functions not implemented for this equations ",__LINE__,__FILE__);
    }
  else if (parameters("dolfwave_parameters")["preview_program"].
           value_str()=="xd3d")
        {
          ///Only for 2HD problems
          Xd3dXyzPlot((*eta_phi[i])[0]);
        }
  else if ( parameters("dolfwave_parameters")["preview_program"].
            value_str()=="xgraphic")
    {
      //Only for 1HD problems
      /// For KdV-type equations ModelType=="1HD1S"
      if (ModelType=="1HD1S")
        XgraphicXyPlot(*eta_phi[i]);
      /// For Boussinesq-type equations (ModelType=="1HD")
      else if (ModelType=="1HD")
        XgraphicXyPlot((*eta_phi[i])[0]);
      else
        NotImplemented("Plot functions not implemented for this equations ",__LINE__,__FILE__);
    }
  else if ( parameters("dolfwave_parameters")["preview_program"].
            value_str()=="gnuplot")
    {
      //Only for 1HD problems
      /// For KdV-type equations (ModelType=="1HD1S")
      if (ModelType=="1HD1S")
        GnuPlot(*eta_phi[i]);
      /// For Boussinesq-type equations (ModelType=="1HD") or (ModelType=="2HD")
      else if ((ModelType=="1HD")||(ModelType=="2HD"))
        GnuPlot((*eta_phi[i])[0]);
      else
        NotImplemented("Plot functions not implemented for this equations ",__LINE__,__FILE__);
    }
  else
    NotImplemented("Preview option",__LINE__,__FILE__);
}


/*
Saving solutiion Files
*/

void dolfin::dolfwave::Dolfwave::SaveEta(unsigned int i) const
{
  if (i>4){info("#dw# WARNING: Invalid index i for eta_phi[i], using i=4\n"); i=4;}

  if (ModelType=="1HD1S")
    *etafile<< *eta_phi[i];
  else if ((ModelType=="1HD")||(ModelType=="2HD"))
    *etafile<< (*eta_phi[i])[0];
  else
    NotImplemented("Plot functions not implemented for this equations ",__LINE__,__FILE__);
}

void dolfin::dolfwave::Dolfwave::SavePhi(unsigned int i) const
{
  if (i>4){info("#dw# WARNING: Invalid index i for eta_phi[i], using i=4\n"); i=4;}

  if ((ModelType=="1HD")||(ModelType=="2HD"))
    *etafile<< (*eta_phi[i])[1];
  else
    NotImplemented("Plot functions not implemented for this equations ",__LINE__,__FILE__);
}


/*
Saving and/or preview with dolfin(show=true) the depth function
It uses the symmetric projection of function
*/
void dolfin::dolfwave::Dolfwave::DepthPlot(const Mesh &mesh ,Expression &depth,const bool show) const
{
  std::shared_ptr<FunctionSpace> W;
  std::shared_ptr<Form> a_sp;
  std::shared_ptr<Form> Lsp;

  if (V->element()->signature()==CONTINUOUS_P2)
    {
      W.reset(new SymProjP2::FunctionSpace(mesh));
      a_sp.reset(new SymProjP2::BilinearForm(*W,*W));
      Lsp.reset(new  SymProjP2::LinearForm(*W,depth,*bathymetry));
}
  else if (V->element()->signature()==CONTINUOUS_P2_1D)
    {
      W.reset(new SymProjP2_1D::FunctionSpace(mesh));
      a_sp.reset(new SymProjP2_1D::BilinearForm(*W,*W));
      Lsp.reset(new  SymProjP2_1D::LinearForm(*W,depth,*bathymetry));
    }
  else if (V->element()->signature()==CONTINUOUS_P1)
    {
      W.reset(new SymProj::FunctionSpace(mesh));
      a_sp.reset(new SymProj::BilinearForm(*W,*W));
      Lsp.reset(new  SymProj::LinearForm(*W,depth,*bathymetry));
    }
  else if (V->element()->signature()==CONTINUOUS_P1_1D)
    {
      W.reset(new SymProj_1D::FunctionSpace(mesh));
      a_sp.reset(new SymProj_1D::BilinearForm(*W,*W));
      Lsp.reset(new  SymProj_1D::LinearForm(*W,depth,*bathymetry));
    }
  else
    {
      NotImplemented("DepthPlot element type",__LINE__,__FILE__);
    }

  Function d(*W);
  solve(*a_sp == *Lsp, d);
  *depthfile<<d;


  std::string dolfin_plotter=parameters("dolfwave_parameters")["preview_program"].value_str();

  if (show)
    {
      if  ((dolfin_plotter=="dolfin_interactive") ||
           (dolfin_plotter=="dolfin_animated"))
        DolfinPlot(d);
      else if (parameters("dolfwave_parameters")["preview_program"].
               value_str()=="xd3d")
        Xd3dXyzPlot(d);
      else if (parameters("dolfwave_parameters")["preview_program"].
               value_str()=="xgraphic")
        XgraphicXyPlot(d);
    }
}

/*
Saving and/or preview with dolfin plotter(show=true) a user defined function
(Sponge is used in this case)
 */
void dolfin::dolfwave::Dolfwave::SpongePlot(const Mesh &mesh ,Expression &sponge,const bool show) const
{
  std::shared_ptr<FunctionSpace> W;
  std::shared_ptr<Form> a_sp;
  std::shared_ptr<Form> Lsp;

  if ((V->element()->signature())==CONTINUOUS_P1)
    {
      W.reset(new Proj::FunctionSpace(mesh));
      a_sp.reset(new Proj::BilinearForm(*W,*W));
      Lsp.reset(new Proj::LinearForm(*W,sponge));
    }
  else if ((V->element()->signature())==CONTINUOUS_P1_1D)
    {
      W.reset(new Proj_1D::FunctionSpace(mesh));
      a_sp.reset(new Proj_1D::BilinearForm(*W,*W));
      Lsp.reset(new Proj_1D::LinearForm(*W,sponge));
    }
  else if ((V->element()->signature())==CONTINUOUS_P2)
    {
      W.reset(new ProjP2::FunctionSpace(mesh));
      a_sp.reset(new ProjP2::BilinearForm(*W,*W));
      Lsp.reset(new ProjP2::LinearForm(*W,sponge));
    }
  else if ((V->element()->signature())==CONTINUOUS_P2_1D)
    {
      W.reset(new ProjP2_1D::FunctionSpace(mesh));
      a_sp.reset(new ProjP2_1D::BilinearForm(*W,*W));
      Lsp.reset(new ProjP2_1D::LinearForm(*W,sponge));
    }
  else NotImplemented((V->element()->signature()),__LINE__,__FILE__);

  /* TODO: WE ONLY NEED ONE PLOT FILE AND IN THE CORRECT PATH */

  Function s(*W);
  solve(*a_sp==*Lsp,s);
  File file_raw("output/sponge.raw");
  File file_xyz("output/sponge.xyz");
  File file_vtk("output/sponge.pvd");
  file_raw<<s;//just for post-processor
  file_xyz<<s;//just for post-processor
  file_vtk<<s;//just for post-processor
  if (show)
    DolfinPlot(s);
 }

/*
Saving and/or preview Horizontal Gradient of phi
*/

void dolfin::dolfwave::Dolfwave::HorVelPlot(const Mesh &mesh ,const bool show, const bool save) const
{
  info("#dw# Horizontal Velocities calculation");
  info("#dw# TODO: To be improved using Physical Potential(see GradP0.ufl)");


  std::string dolfin_plotter=parameters("dolfwave_parameters")["preview_program"].value_str();

  if (parameters("dolfwave_parameters")["solutions_format"].
      value_str()=="pvd")
    {
      info("#dw# Not supported for horizontal velocity plot.");
      info("#dw# Using pvd instead");
    }
  GradP0::FunctionSpace W(mesh);
  GradP0::BilinearForm a(W,W);
  GradP0::LinearForm L(W);

  L.phi=(*eta_phi[4])[1];
  Function uv(W);
  solve(a == L, uv);
  if(save)
    *horvelfile<<uv;//just for post-processor
  if(show)
    {
      if ((dolfin_plotter=="dolfin_interactive") ||
          (dolfin_plotter=="dolfin_animated"))
        DolfinPlot(uv);
      else
        info("#dw# Plotter type Not implemented yet");

    }

}


//Save the function profile in a Specific Point
void dolfin::dolfwave::Dolfwave::PointPlot(const double x ,
                                           const double y,
                                           const std::string eta_or_phi,
                                           const std::string filename) const
{

  int eta_or_phi_choice;
  if (eta_or_phi=="eta")
    eta_or_phi_choice=0;
  else if (eta_or_phi=="phi")
    eta_or_phi_choice=1;
  else
    {
      info("Wrong choice for point plot. Plotting eta");
      eta_or_phi_choice=0;
    }

  Array<double> value(1);
  value[0]=0.0;
  std::ofstream fp(filename.c_str(),std::ios_base::app); //open and appending
  fp<<std::fixed;
  fp<<std::setprecision(15);

  double _p[2]={x,y};
  Array<double> p(2,_p);
  (*eta_phi[4])[eta_or_phi_choice].eval(value,p);
  fp<< Time <<" " <<value[0]<<std::endl;
  fp.close();
}

//Save the function profile in a Specific Point
void dolfin::dolfwave::Dolfwave::PointPlot(const double x ,
                                           const std::string eta_or_phi,
                                           const std::string filename) const
{

  int eta_or_phi_choice;
  if (eta_or_phi=="eta")
    eta_or_phi_choice=0;
  else if (eta_or_phi=="phi")
    eta_or_phi_choice=1;
  else if (eta_or_phi=="eta1HD1S")
    eta_or_phi_choice=2;
  else
    {
      info("Wrong choice for point plot. Plotting eta");
      eta_or_phi_choice=0;
    }

  Array<double> value(1);
  value[0]=0.0;
  std::ofstream fp(filename.c_str(),std::ios_base::app); //open and appending
  fp<<std::fixed;
  fp<<std::setprecision(15);

  double _p[1]={x};
  Array<double> p(1,_p);
  if((eta_or_phi_choice==0)||(eta_or_phi_choice==1))
    (*eta_phi[4])[eta_or_phi_choice].eval(value,p);
  else if (eta_or_phi_choice==2)
    (*eta_phi[4]).eval(value,p);

  fp<< Time <<" " <<value[0]<<std::endl;
  fp.close();
}

// Save a function profile
void dolfin::dolfwave::Dolfwave::LinePlot(
                                          const double xs , const double ys,
                                          const double  xe,const double ye,
                                          const int np,
                                          const std::string eta_or_phi,
                                          const std::string filename ) const
{

  int eta_or_phi_choice;

  if (eta_or_phi=="eta")
    eta_or_phi_choice=0;
  else if (eta_or_phi=="phi")
    eta_or_phi_choice=1;
  else
    {
      info("Wrong choice for point plot. Plotting eta");
      eta_or_phi_choice=0;
    }


  std::ofstream fp(filename.c_str()); //open  write and close at the end
  fp<<std::fixed;
  fp<<std::setprecision(15);
  Array<double> value(1);
  value[0]=0.0;
  double _p[2]={0.,0.};
  for (int i=0; i<np;i++)
    {
      _p[0]=xs+i*(xe-xs)/(np-1);
      _p[1]=ys+i*(ye-ys)/(np-1);
      Array<double> p(2,_p);
      double distance_to_ps=sqrt((xs-p[0])*(xs-p[0])+(ys-p[1])*(ys-p[1]));
      (*eta_phi[4])[eta_or_phi_choice].eval(value,p);
      fp<< distance_to_ps<<" "<<value[0]<<std::endl;
    }
  fp.close();
}

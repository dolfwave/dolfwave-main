// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// infos.cpp
// First added:  2009-04-14
// Last changed: 2016-06-29
// Compiled with DOLFIN:1.6.0

#include"../DwClass.h"
#include"../../DwConfig.h"
#include"../DwConstants.h"
#include"./debugfunctions.h"

#include<iomanip> //For setprecision
#include <fstream> //For file reading
#include <string> //For file reading


using namespace dolfin;
using namespace dolfin::dolfwave;



// {{{ Info functions --------------------------------------------
std::string dolfin::dolfwave::DolfwaveInfo(const unsigned int iter,const unsigned int maxiter,const double totaltime,const double timestep,const bool systeminfo )
{

  std::stringstream outputs;
  outputs<<"#dw# ** Step= "<<iter<< " of a total= "<< maxiter<<std::endl;
  outputs<<"#dw# ** Simulated time= "<<totaltime<< " of a total time= "
           << maxiter*timestep<<std::endl;
  if (systeminfo)
     outputs<<SystemInfo();

  outputs<<std::flush;

  return outputs.str();
}

std::string dolfin::dolfwave::SystemInfo()
{
  std::stringstream outputs;
  int d,u,h,m,s,ds;
  char c[1024];

  if (OSX==0)
    {
      std::ifstream lida("/proc/self/stat");
      Assert(lida.is_open( ),__LINE__,__FILE__,true);
      outputs<<"#dw# *************Start System Info ****************"<<std::endl;
      lida>>d;outputs<<"#dw# ** pid = "<<d<<std::endl;
      lida>>c;outputs<<"#dw# ** name = "<<c<<std::endl;
      lida>>c;outputs<<"#dw# ** state="<<c<<std::endl;
      lida>>d;//printf("ppid=%d\n",d);
      lida>>d;//printf("pgrp=%d\n",d);
      lida>>d;//printf("session=%d\n",d);
      lida>>d;//printf("tty=%d\n",d);
      lida>>d;//printf("tpgid=%d\n",d);
      lida>>d;//printf("flags=%u\n",u);
      lida>>d;//printf("minflt=%u\n",u);
      lida>>u;//printf("cminflt=%u\n",u);
      lida>>u;//printf("majflt=%u\n",u);
      lida>>u;//printf("cmajflt=%u\n",u);
      lida>>d;
      d=(d/100);
      s=d%60;
      d=d/60;
      m=(d)%60;
      d=d/60;
      h=d%24;
      ds=d/24;
      outputs<<"#dw# ** up time="<<ds<<":"<<h<<":"<<m<<":"<<s<<std::endl;
      lida>>d;//printf("stime=%d\n",d);
      lida>>d;//printf("cutime=%d\n",d);
      lida>>d;//printf("cstime=%d\n",d);
      lida>>d;//printf("counter=%d\n",d);
      lida>>d; outputs<<"#dw# ** priority "<<d<<std::endl;
      lida>>u;//printf("timeout=%d\n",u);
      lida>>u;//printf("itrealvalue=%d\n",u);
      lida>>d;//printf("starttime=%d\n",d);
      d=(d/100);
      s=d%60;
      d=d/60;
      m=(d)%60;
      d=d/60;
      h=d%24;
      ds=d/24;
      outputs<<"#dw# ** Start time="<<ds<<":"<<h<<":"<<m<<":"<<s<<std::endl;
      lida>>u;outputs<<"#dw# ** Virtual Memory Size="<<u/1024<<" k\t"<<std::endl;;
      lida>>u;outputs<<"#dw# ** Resident Set Size="<<u<<std::endl;
      lida>>u;//printf("rlim=%d\n",u);
      outputs<<"#dw# ************End System Info ****************"<<std::endl;
      lida.close( );
      outputs<<std::flush;
    }
  else
    {
      outputs<<"#dw#  WARNING: No System infos in OSX"<<std::endl;
    }
  return outputs.str();
}


void dolfin::dolfwave::Dolfwave::InitialOutput() const
{

  std::stringstream outputs;

  int rank,size;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  // Outputing info from the first processor only (if mpi is in use)
    if (rank==0)
      {
      outputs<<"#dw# *******************************************************"<<std::endl;
      outputs<<"#dw# ***      DOLFWAVE version " <<DOLFWAVE_VERSION_MAJOR<<"."<<
        DOLFWAVE_VERSION_MINOR<<"."<< DOLFWAVE_VERSION_YEAR<<"."<<
        DOLFWAVE_VERSION_MONTH<<std::endl;
      if (OSX==0)
        outputs <<"#dw# ***      Running on Linux    *************************"<<std::endl;
      else if (OSX==1)
        outputs <<"#dw# ***      Running on OSX      *************************"<<std::endl;
      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<<"#dw#  First Steps towards a Surface Water Waves Library "<<std::endl;
      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<<"#dw#   Author: Nuno David Lopes *************************"<<std::endl;
      outputs<<"#dw#   Webpage: http://ptmat.fc.ul.pt/~ndl/"<<std::endl;
      outputs<<"#dw#   https://launchpad.net/dolfwave"<<std::endl;
      outputs<<"#dw#   Surface Elevation-Velocity Potential"<<std::endl;
      outputs<<"#dw#   Implented Formulations:"<<std::endl;
      outputs<<"#dw#   --BBM regular long wave equation 1D"<<std::endl;
      outputs<<"#dw#   --K.d'Vries long wave equation 1D"<<std::endl;
      outputs<<"#dw#   --Improvement of Zhao Et Al. 2004 1HD/2HD"<<std::endl;
      outputs<<"#dw#   --LPT C/DG-FEM 1HD and 2HD"<<std::endl;
      outputs<<"#dw#   References: "<<std::endl;
      outputs<<"#dw#   N.D. Lopes, P.J.S Pereira and L.Trabucho"<<std::endl;
      outputs<<"#dw#   --Improved Boussinesq Equations for Surface Water Waves."<<std::endl;
      outputs<<"#dw#   --Automated Solutions of Differential Equations by the Finite Element Method"<<std::endl;
      outputs<<"#dw#   --Eds. A. Logg, K.-A. Mardal and G.N. Wells, Springer 2012. (FEniCS Book)"<<std::endl;
      outputs<<"#dw#   A numerical analysis  of a class of generalized"<<std::endl;
      outputs<<"#dw#   Boussinesq-type equations using continuous/discontinuous FEM."<<std::endl;
      outputs<<"#dw#   --Int. Jour. Num. Meth. Fluids, Wiley. DOI:10.1002/fld.2631"<<std::endl;
      outputs<<"#dw#   A numerical analysis of a class of KdV-BBM equations" <<std::endl;
      outputs<<"#dw#  using Continuous/Discontinuous Galerkin Finite Element Method" <<std::endl;
      outputs<<"#dw#  --Proceedings of MEFTE 2012, LNEC, 2012."<<std::endl;
      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<<"#dw# ****************** Comment****************************"<<std::endl;
      outputs<<Comment.str()<<std::endl;
      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<<"#dw# ***************Initial Dolfwave Parameters************"<<std::endl;
      outputs<< "#dw"<< parameters.str(true)<<std::endl;

      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<< "#dw the main.cpp file used  for  this demo/test is the following"<<std::endl;

      std::ifstream mainfile ("main.cpp");
      std::string line;
      if (mainfile.is_open())
        {
          while (mainfile.good() )
            {
              getline (mainfile,line);
              outputs << line << std::endl;
            }
          mainfile.close();
        }
      else  outputs<< "Unable to open the main.cpp file";

      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<<SystemInfo();

      // output to std::cout
      std::cout<<outputs.str()<<std::flush;
      // output to logfile (dolfwave_logfile.log)
      *logfile<<outputs.str()<<std::flush;
      }
}

void dolfin::dolfwave::Dolfwave::Info(const bool systeminfo) const
{
  std::stringstream outputs;
  std::stringstream outputlogs;

  int rank,size;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  // Outputing info from the first processor only (if mpi is in use)
  if (rank==0)
      {
        // output to std::cout
        outputs<<DolfwaveInfo(Step,MaxSteps,Time,TimeStep,systeminfo);
        std::cout<<outputs.str()<<std::flush;

        // output to logfile (dolfwave_logfile.log)
        outputlogs<<"#dw# ** Progress: "<<Step<<" of "<< MaxSteps <<"#"<< ((double) Step)/((double) MaxSteps)*100.
                  <<std::setprecision(2)<<" %"<<std::endl;
        *logfile<<outputlogs.str()<<std::flush;
      }
}


void dolfin::dolfwave::Dolfwave::FinalOutput() const
{
  std::stringstream outputs;

  int rank,size;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  // Outputing info from the first processor only (if mpi is in use)
  if (rank==0)
    {
      outputs<<"#dw# ***************Final Dolfwave Parameters************"<<std::endl;
      outputs<<"#dw# ****************************************************"<<std::endl;
      outputs<< parameters("dolfwave_parameters").str(true)<<std::endl;
      outputs<<"#dw# ******************************************************"<<std::endl;
      outputs<<"#dw# ****************** Comment ***************************"<<std::endl;
      if (IsDiverging)
        outputs<<"#dw# ************ Dolfwave Diverged *********************"<<std::endl;
      if (IsNull)
        outputs<<"#dw# ********** Dolfwave Ended with Null Solution *********"<<std::endl;
      outputs<<Comment.str()<<std::endl;
      outputs<<"#dw# ******************************************************"<<std::endl;
      std::cout<<outputs.str()<<std::flush;
      *logfile<<outputs.str()<<std::flush;
    }
}

// }}}-----------------------------------------------------

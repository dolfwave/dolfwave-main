// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// errors.cpp
// First added:  2013-01-10
// Last changed: 2014-07-10
// Compiled with DOLFIN:1.0.X

#include "../DwClass.h"
#include "../DwConstants.h"
#include "../2hdforms/ProjP2.h"
#include "../1hdforms/ProjP2_1D.h"

#include <fstream> // For file stream
#include<iomanip> //For setprecision

using namespace dolfin;
using namespace dolfin::dolfwave;


double dolfin::dolfwave::Dolfwave::VectorNorm(const Mesh &mesh ,std::string norm_type,const bool file_write, std::string filename ) const
{

  double lnorm=eta_phi[4]->vector()->norm(norm_type);


  if(file_write){
    if (filename=="")
      {
        std::stringstream default_filename;
        default_filename
          <<parameters("dolfwave_parameters")["solutions_dir"].value_str()
          <<"/"<<norm_type<<"_vector_norm.tn";
        filename=default_filename.str();
      }

    std::ofstream fp(filename.c_str(),std::ios_base::app);//open and appending
    fp<<std::fixed;
    fp<<std::setprecision(15);
    fp<<Time<<" "<<lnorm<< std::endl;
    fp.close();
  }


  return lnorm;
 }


double dolfin::dolfwave::Dolfwave::VectorNormError(const Mesh &mesh , Expression &exact_solution,std::string norm_type,const bool file_write, std::string filename ) const
{

  double lnormerror=0.0;

  std::shared_ptr<FunctionSpace> W;
  std::shared_ptr<Form> a_sp;
  std::shared_ptr<Form> Lsp;

  if ((V->element()->signature())==CONTINUOUS_P2_1D1S)
    {
      W.reset(new ProjP2_1D::FunctionSpace(mesh));
      a_sp.reset(new ProjP2_1D::BilinearForm(*W,*W));
      Lsp.reset(new ProjP2_1D::LinearForm(*W,exact_solution));
    }
  else NotImplemented((V->element()->signature()),__LINE__,__FILE__);

  Function exact_sol(*W);
  solve(*a_sp==*Lsp,exact_sol);

  Vector aux(*(exact_sol.vector()));
  aux.axpy(-1.0,*(eta_phi[4]->vector()));
  lnormerror=aux.norm(norm_type);
  double exact_sol_norm=exact_sol.vector()->norm(norm_type);
  double rlnormerror=0.0;
  if (std::abs(exact_sol_norm)>1.e-10)
    rlnormerror=lnormerror/exact_sol_norm;

  if(file_write){
    if (filename=="")
      {
        std::stringstream default_filename;
        default_filename
          <<parameters("dolfwave_parameters")["solutions_dir"].value_str()
          <<"/"<<norm_type<<"_vector_norm_error.te";
        filename=default_filename.str();
      }

    std::ofstream fp(filename.c_str(),std::ios_base::app);//open and appending
    fp<<std::fixed;
    fp<<std::setprecision(15);
    fp<<Time<<" "<<lnormerror<<" "<<rlnormerror<< std::endl;
    fp.close();
  }


  return lnormerror;
 }

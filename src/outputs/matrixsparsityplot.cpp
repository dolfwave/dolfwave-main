// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
// MatrixSparsityPlot.cpp
//
// First added:  2009-04-13
// Last changed: 2013-01-04
//
// Compiled with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------


#include <dolfin.h>
#include <fstream> //for ofstream File saving
#include <string> //for string
#include "../DwClass.h"

#include "matrixsparsityplot.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


void dolfin::dolfwave::Dolfwave::GetMatrixM( )
{
  /// This Functions generates a raw txt file with the matrix M

  GetMatrixRaw(*M, "M.raw");
  info("#dw# Matrix M written in a raw (txt) format");
}

void dolfin::dolfwave::Dolfwave::GetMatrixMf( )
{
  /// This Functions generates a raw txt file with the matrix M

  GetMatrixRaw(*Mf, "Mf.raw");
  info("#dw# Matrix Mf written in a raw (txt) format");
}

void dolfin::dolfwave::Dolfwave::MSparsityPlot( )
{
  /// This Functions generates a png file
  /// with the sparsity plot of the matrix M

  MatrixSparsityPlotPBM(*M,"M",1);
}

void dolfin::dolfwave::Dolfwave::MfSparsityPlot( )
{
  /// This Functions generates a png file
  /// with the sparsity plot of the matrix M

  MatrixSparsityPlotPBM(*Mf,"Mf",1);

}

// {{{ Ploting matrix structures in a pbm format
void MatrixSparsityPlotPBM(const Matrix &M,const std::string name, const bool png)
{
  int cor;
  int preto=1;
  int branco=0;

  ///The fact that the number of rows cames first than the number of lines
  /// has to due with the pbm format

  std::ofstream fich_image(name.c_str( ));
  fich_image<<"P1"<<std::endl;
  fich_image<<"# Matrix Sparsity Plot "<<std::endl;
  fich_image<<M.size(1)<<"\t"<<M.size(0)<<std::endl;
  for(int i=0;i<M.size(1);i++)
    {
      for(int j=0;j<M.size(0);j++)
        {
          if(M(j,i)) cor=preto; else cor=branco;
                fich_image<<cor<<" ";
                if(!((j+1)%70)) fich_image<<std::endl;
        }
      fich_image<<std::endl;
    }
  fich_image.close( );
  info("#dw# pbm image done");

  if(png)
    {
      info("#dw# Convert the image to png (convert required)");
      std::stringstream command;
      command << "convert  "<<name <<" " <<name
              <<".png && rm -f "<<name;
      if (system(command.str().c_str()) != 0)
        warning("#dw# png image done and ppm deleted");
    }


}

// {{{ Ploting matrix structures in a pbm format
void GetMatrixRaw(const Matrix &M, const std::string name)
{

  std::ofstream fichraw(name.c_str( ));

  for(int i=0;i<M.size(0);i++)
    {
      for(int j=0;j<M.size(1);j++)
        {
          fichraw<<M(i,j)<<" ";
        }
      fichraw<<std::endl;
    }
  fichraw.close( );

}

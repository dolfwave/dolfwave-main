// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
// DebugFunctions.h
//
// First added:  2009-04-14
// Last changed - nlopes: 2017-03-04
//
// Compiled with DOLFIN:1.6.0
// }}}------------------------------------------------------------------------


#ifndef __DEBUGFUNCTIONS_H
#define __DEBUGFUNCTIONS_H


#include <fstream> //for ofstream File saving
#include <string> //for string



namespace dolfin
{
  namespace dolfwave
  {
    // {{{ External inline functions ------------------------
    // Assert and NotImplemented Functions

    ///Assert function
    template<class DI> inline  int Assert(DI x,
                                          unsigned int line,
                                          std::string file, bool code_exit)
    {
      int exit_status=0;
      if (!(x))
        {
          exit_status=1;
          std::ostringstream ss;
          ss<<"#dw# ************Assert ERROR!! \n"<<"#dw# ************Line " << line  << "\n"
            <<"#dw# ************File " << file << "\n"<<std::flush;
          fputs(ss.str().c_str(),stderr);
          if (code_exit == true)
            exit(EXIT_FAILURE);

        }
      return exit_status;
    }

    template<class DI> inline  int Assert(DI x,
                                          unsigned int line,
                                          std::string file,
                                          std::string message,
                                          bool code_exit
                                          )
    {
      int exit_status=0;
      if (!(x))
        {
          exit_status=1;
          fputs(message.c_str(),stderr);
          std::ostringstream ss;
          ss<<"#dw# ************Assert ERROR!! \n"
            <<"#dw# ************Line " << line  << "\n"
            <<"#dw# ************File " << file << std::endl;
          fputs(ss.str().c_str(),stderr);
          if (code_exit == true)
            exit(EXIT_FAILURE);
        }
      return exit_status;
    }



    /// Not implemented  function
    /**
       Prints a message and exit the process with a stdERROR
    */
    void NotImplemented(std::string s,
                        unsigned int line,
                        std::string file,bool assert_test=false, bool code_exit=true);

    // }}} --------------------------------------------

    std::string DolfwaveInfo(const unsigned int iter,const unsigned int maxiter,const double totaltime,const double timestep, bool systeminfo );

    std::string  SystemInfo();
// }}}
  }
}

#endif

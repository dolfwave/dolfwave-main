// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  MatrixSparsityPlot.h
//
// First added:  2009-04-13
// Last changed: 2010-09-11
//
// Compiled with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#ifndef __FUNFUNCTIONS_H
#define __FUNFUNCTIONS_H


#include <dolfin.h>
#include <fstream> //for ofstream File saving
#include <string> //for string

using namespace dolfin;

// {{{ Ploting matrix structures in a pbm format
void MatrixSparsityPlotPBM(const Matrix &,const std::string , const bool png=true);
void GetMatrixRaw(const Matrix &, const std::string );
#endif

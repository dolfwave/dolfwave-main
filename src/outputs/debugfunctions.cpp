//  Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// DebugFunctions.cpp
// First added:  2009-04-14
// Last changed - nlopes: 2017-03-04
// Compiled with DOLFIN:1.6.0

#include<dolfin.h>

#include "debugfunctions.h"


/// Not implemented function
void dolfin::dolfwave::NotImplemented(std::string s,
                                      unsigned int line,
                                      std::string file, bool assert_test, bool code_exit)
{

  fputs(s.c_str(),stderr);
  Assert(assert_test,line,file,"\n Not implemented yet, Aborting\n", code_exit);
}

// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  dolfwave.h
//
// First added:  2009-02-27
// Last changed: 2011-06-24
//
// Compiled with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------


#ifndef __DOLFWAVE_H
#define __DOLFWAVE_H



#include <dolfin.h>
#include <fstream> //for ofstream File saving
#include <string> //for string

#include "DwClass.h"
#include "rungekutta/rungekutta.h"
#include "predictorcorrector/predictorcorrector.h"
#include "outputs/matrixsparsityplot.h"

#include "DwConstants.h"
#include "meshtransform/transform.h"

#endif

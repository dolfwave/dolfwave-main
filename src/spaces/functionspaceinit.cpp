// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  functionspaceinit.cpp
//
// First added:  2009-04-14
// Last changed: 2016-06-29
//
// Compiled with DOLFIN:1.6.0
// }}}------------------------------------------------------------------------

#include "functionspaceinit.h"

using namespace dolfin;
using namespace dolfin::dolfwave;

// {{{ FunctionSpaceInit------------------------------------------------

void dolfin::dolfwave::Dolfwave::FunctionSpaceInit(Mesh &mesh)
{
 std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  if (FormName=="Zhao")
    {
      ModelType="2HD"; /*Setting the number of unkowns */
      V.reset(new Zhao::FunctionSpace(mesh));
      // U is the single P1 space for bathymetry
      U.reset(new P1::FunctionSpace(mesh));
    }
  else if (FormName=="Zhao_1D")
    {
      ModelType="1HD"; /*Setting the number of unkowns */
      V.reset(new Zhao_1D::FunctionSpace(mesh));
      U.reset(new P1_1D::FunctionSpace(mesh));
    }
  else if (FormName=="ZhaoP2")
    {
      ModelType="2HD"; /*Setting the number of unkowns */
      V.reset(new ZhaoP2::FunctionSpace(mesh));
      U.reset(new P2::FunctionSpace(mesh));
    }
  else if (FormName=="ZhaoP2_1D")
    {
      ModelType="1HD"; /*Setting the number of unkowns */
      V.reset(new ZhaoP2_1D::FunctionSpace(mesh));
      U.reset(new P2_1D::FunctionSpace(mesh));
    }
  else if (FormName=="LPTcdg")
    {
      ModelType="2HD"; /*Setting the number of unkowns */
      V.reset(new LPTcdg::FunctionSpace(mesh));
      U.reset(new P2::FunctionSpace(mesh));
    }
  else if (FormName=="LPTcdg_1D")
    {
      ModelType="1HD"; /*Setting the number of unkowns */
      V.reset(new LPTcdg_1D::FunctionSpace(mesh));
      U.reset(new P2_1D::FunctionSpace(mesh));
    }
  else if (FormName=="LPTcdgHnl")
    {
      ModelType="2HD"; /*Setting the number of unkowns */
      V.reset(new LPTcdgHnl::FunctionSpace(mesh));
      U.reset(new P2::FunctionSpace(mesh));
    }
  else if (FormName=="LPTcdgHnl_1D")
    {
      ModelType="1HD"; /*Setting the number of unkowns */
      V.reset(new LPTcdgHnl_1D::FunctionSpace(mesh));
      U.reset(new P2_1D::FunctionSpace(mesh));
    }
  else if (FormName=="BBM_1D")
    {
      ModelType="1HD1S"; /*Setting the number of unkowns */
      V.reset(new BBM_1D::FunctionSpace(mesh));
      // U is Useless but it is required that it is initialized.
      U.reset(new P2_1D::FunctionSpace(mesh));
    }
 //  TODO/TO ABANDON/TO DECIDE
 else if (FormName=="aKdVcdgP1P2_1D")
    {
      ModelType="1HD1S"; /*Setting the number of unkowns */
      V.reset(new aKdVcdgP1P2_1D::FunctionSpace(mesh));
      // U is Useless but it is required that it is initialized.
      U.reset(new P2_1D::FunctionSpace(mesh));
    }
 else if (FormName=="xKdVcdg_1D")
    {
      ModelType="1HD1S"; /*Setting the number of unkowns */
      V.reset(new xKdVcdg_1D::FunctionSpace(mesh));
      // U is Useless but it is required that it is initialized.
      U.reset(new P2_1D::FunctionSpace(mesh));
    }
  // Experimental
  else
    NotImplemented("FunctionSpaceInit form",__LINE__,__FILE__);

}



void dolfin::dolfwave::Dolfwave::FunctionSpaceInit(Mesh &mesh, SubDomain &periodic_boundary)
{
  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  if (FormName=="BBM_1D")
    {
      ModelType="1HD1S"; /*Setting the number of unkowns */
      V.reset(new BBM_1D::FunctionSpace(mesh, periodic_boundary));
      // U is Useless but it is required that it is initialized.
      U.reset(new P2_1D::FunctionSpace(mesh));
    }
  else if (FormName=="xKdVcdg_1D")
    {
      ModelType="1HD1S"; /*Setting the number of unkowns */
      V.reset(new xKdVcdg_1D::FunctionSpace(mesh,periodic_boundary));
      // U is Useless but it is required that it is initialized.
      U.reset(new P2_1D::FunctionSpace(mesh));
    }
  else
    NotImplemented("FunctionSpaceInit form",__LINE__,__FILE__);

}

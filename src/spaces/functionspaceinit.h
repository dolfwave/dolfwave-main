// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  functionspaceinit.h
//
// First added:  2009-09-15
// Last changed: 2015-06-01
//
// Compiled with DOLFIN:1.6.0
// }}}------------------------------------------------------------------------

#ifndef __FUNCTIONSPACEINIT_H
#define __FUNCTIONSPACEINIT_H

#include"../DwClass.h"
#include"../forms.h"

#endif

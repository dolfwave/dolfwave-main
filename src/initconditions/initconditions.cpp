// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  initconditions.cpp
// First added:  2009-04-14
// Last changed: 2015-05-18
// Compiled with DOLFIN:1.6.0-dev

#include "initconditions.h"

using namespace dolfin;
using namespace dolfin::dolfwave;



// {{{ InitialCondition P1xP1 and P2xP2 From Files
void dolfin::dolfwave::Dolfwave::InitialCondition( std::string eta_file, std::string phi_file)
{

  info("#dw# This Function is deprecated: TODO using XML files as in bathymetry");
  /*
  info("#dw# Initial conditions from files");
  info("#dw# Only for raw files\n");
  info("#dw# Working only with CG elements");
  info("#dw# Functions SubSpaces must be the same.");

  int N=0;

  std::ifstream fich_eta(eta_file.c_str());
  Assert(fich_eta.is_open(),__LINE__,__FILE__);
  std::ifstream fich_phi(phi_file.c_str());
  Assert(fich_phi.is_open(),__LINE__,__FILE__);

  fich_eta>>N;
  Assert(N,__LINE__,__FILE__);
  fich_phi>>N;
  Assert(N,__LINE__,__FILE__);
  cout<<N<<endl;

  double etavalues[N];
  double phivalues[N];

  double aux=0.0;
  for(int i=0;i<N;i++)
    {
      fich_eta>>aux;
      etavalues[i]=aux;
      fich_phi>>aux;
      phivalues[i]=aux;
    }

  fich_eta.close();
  fich_phi.close();


  Vector vector_eta(N);
  Vector vector_phi(N);


/*********** See the advection-diffusion demo
to avoid subspaces and to read files form XML dolfin format
already parallelized....

#include "Velocity.h"
"
Velocity.ufl:

element = VectorElement("Lagrange", "triangle", 2)
"
------------

   // Create velocity FunctionSpace
  Velocity::FunctionSpace V_u(mesh);

  // Create velocity function
  Function velocity(V_u);
  XMLFile file_u("../velocity.xml.gz");
  file_u >> velocity;


  AdvectionDiffusion::BilinearForm a(V, V);
  a.b = velocity;

  ****************/
  /*

  std::shared_ptr<SubSpace> SubV;
  SubV.reset(new SubSpace(*V,0));



  Function eta_init(*SubV,vector_eta);
  Function phi_init(*SubV,vector_phi);

  Array<double> _etavalues(N,etavalues);
  Array<double> _phivalues(N,phivalues);

  vector_eta.set_local(_etavalues);
  vector_phi.set_local(_phivalues);


  Matrix ML2P;
  Vector b_init;
  std::shared_ptr<Form> bil2p;
  std::shared_ptr<Form> lil2p;

  std::string element_signature=V->element()->signature();

  if (element_signature==continuous_p2)
    {
      bil2p.reset(new L2ProjP2::BilinearForm(*V,*V));
      lil2p.reset(new L2ProjP2::LinearForm (*V,eta_init,phi_init));
    }
  else if (element_signature==continuous_p2_1D)
    {
      bil2p.reset(new L2ProjP2_1D::BilinearForm(*V,*V));
      lil2p.reset(new L2ProjP2_1D::LinearForm (*V,eta_init,phi_init));
    }
  else if (element_signature==continuous_p1)
    {
      bil2p.reset(new L2Proj::BilinearForm(*V,*V));
      lil2p.reset(new L2Proj::LinearForm (*V,eta_init,phi_init));
    }
  else if (element_signature==continuous_p1_1D)
    {
      bil2p.reset(new L2Proj_1D::BilinearForm(*V,*V));
      lil2p.reset(new L2Proj_1D::LinearForm (*V,eta_init,phi_init));
    }
  else
    {
      NotImplemented("Initial conditions type TODO",__LINE__,__FILE__);
    }

  assemble(ML2P,*bil2p);
  assemble(b_init,*lil2p);

  if (AlgBackEnd=="LU_P")
    {
      PETScLUSolver solver("default");
      solver.solve(ML2P,*(eta_phi[0]->vector()),b_init);
    }
  else if (AlgBackEnd=="GMRES_P")
    {
      PETScKrylovSolver solver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      solver.solve(ML2P,eta_phi[0]->vector(),b_init);
    }
  else NotImplemented("AlgBackEnd at initial conditions\n",__LINE__,__FILE__);
  //initialize all "step" functions with initial conditions
  for (int i=1;i<5;i++)
    eta_phi[i]->vector()=eta_phi[0]->vector();

*/

  info("#dw# ***********End Reading the Initial Conditions**********\n");


}
// }}}----------------------------------------------------------

// {{{ InitialCondition  Most Used
void dolfin::dolfwave::Dolfwave::InitialCondition(Expression &eta_init, Expression& phi_init)
{
  Matrix ML2P;
  Vector b_init;
  std::shared_ptr<Form> bil2p;
  std::shared_ptr<Form> lil2p;

  info("#dw# Initial conditions from Expressions");
  std::string element_signature=V->element()->signature();
  if (element_signature==CONTINUOUS_P2)
    {
      bil2p.reset(new L2ProjP2::BilinearForm(*V,*V));
      lil2p.reset(new L2ProjP2::LinearForm (*V,eta_init,phi_init));
    }
  else if (element_signature==CONTINUOUS_P2_1D)
    {
      bil2p.reset(new L2ProjP2_1D::BilinearForm(*V,*V));
      lil2p.reset(new L2ProjP2_1D::LinearForm (*V,eta_init,phi_init));
    }
  else if (element_signature==CONTINUOUS_P1)
    {
      bil2p.reset(new L2Proj::BilinearForm(*V,*V));
      lil2p.reset(new L2Proj::LinearForm (*V,eta_init,phi_init));
    }
  else  if (element_signature==CONTINUOUS_P1_1D)
    {
      bil2p.reset(new L2Proj_1D::BilinearForm(*V,*V));
      lil2p.reset(new L2Proj_1D::LinearForm (*V,eta_init,phi_init));
    }
  else
    {
      NotImplemented("DG Initial conditions TODO",__LINE__,__FILE__);
    }

  assemble(ML2P,*bil2p);
  assemble(b_init,*lil2p);

  if (AlgBackEnd=="LU_P")
    {
      PETScLUSolver solver("default");
      solver.solve(ML2P,*(eta_phi[0]->vector()),b_init);
    }
  else if (AlgBackEnd=="GMRES_P")
    {
      PETScKrylovSolver solver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      solver.solve(ML2P,*(eta_phi[0]->vector()),b_init);
    }
  else
    NotImplemented("Solver type at initial conditions\n",__LINE__,__FILE__);

  //initialize all "step" functions with initial conditions
  for (int i=1;i<5;i++)
    *(eta_phi[i]->vector())=*(eta_phi[0]->vector());

  info("#dw# ***********End Reading the Initial Conditions**********\n");
}

// }}}--------------------------------------------------------


// {{{ InitialCondition  Most Used
void dolfin::dolfwave::Dolfwave::InitialCondition(Expression &eta_init)
{
  // For the (x)KdV-BBM-type equations (1D1S)

  Matrix ML2P;
  Vector b_init;
  std::shared_ptr<Form> bil2p;
  std::shared_ptr<Form> lil2p;

  info("#dw# Initial condition from Expression");
  std::string element_signature=V->element()->signature();
  if (element_signature==CONTINUOUS_P2_1D1S)
    {
      bil2p.reset(new L2ProjP2_1D1S::BilinearForm(*V,*V));
      lil2p.reset(new L2ProjP2_1D1S::LinearForm (*V,eta_init));
    }
  else  if (element_signature==CONTINUOUS_P1_1D1S)
    {
      bil2p.reset(new L2Proj_1D1S::BilinearForm(*V,*V));
      lil2p.reset(new L2Proj_1D1S::LinearForm (*V,eta_init));
    }
  else
    {
      NotImplemented("DG Initial conditions TODO",__LINE__,__FILE__);
    }

  assemble(ML2P,*bil2p);
  assemble(b_init,*lil2p);

  if (AlgBackEnd=="LU_P")
    {
      PETScLUSolver solver("default");
      solver.solve(ML2P,*(eta_phi[0]->vector()),b_init);
    }
  else if (AlgBackEnd=="GMRES_P")
    {
      PETScKrylovSolver solver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      solver.solve(ML2P,*(eta_phi[0]->vector()),b_init);
    }
  else NotImplemented("AlgBackEnd at initial conditions\n",__LINE__,__FILE__);
//initialize all "step" functions with initial conditions
  for (int i=1;i<5;i++)
    *(eta_phi[i]->vector())=*(eta_phi[0]->vector());

   info("#dw# ***********End Reading the Initial Conditions**********\n");
}

// }}}--------------------------------------------------------

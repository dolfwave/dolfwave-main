// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  initconditions.h
//
// First added:  2010-09-13
// Last changed: 2011-10-03
//
// Compiled with DOLFIN:1.0
// }}}------------------------------------------------------------------------

#ifndef __INITCONDITIONS_H
#define __INITCONDITIONS_H

#include"../DwClass.h"
#include"../DwConstants.h"

#include "../2hdforms/L2Proj.h"
#include "../2hdforms/L2ProjP2.h"
#include "../1hdforms/L2Proj_1D.h"
#include "../1hdforms/L2ProjP2_1D.h"
#include "../1hd1sforms/L2Proj_1D1S.h"
#include "../1hd1sforms/L2ProjP2_1D1S.h"

#endif

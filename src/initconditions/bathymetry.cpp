// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  initconditions.cpp
// First added:  2009-04-14
// Last changed: 2014-07-08
// Compiled with DOLFIN:1.4.0

#include"../DwClass.h"
#include<dolfin/io/XMLFile.h>

using namespace dolfin;
using namespace dolfin::dolfwave;


// {{{ InitialCondition P1xP1 and P2xP2 From Files
void dolfin::dolfwave::Dolfwave::LoadBathymetry(const Mesh &mesh, std::string bathymetry_file,bool show)
{
  info("#dw# Load Bathymetry from file");
  info("#dw# Only for Native DOLFIN XML files\n");
  info("#dw# Currently, available only in Zhao form\n");

  XMLFile file_bath(mesh.mpi_comm(),bathymetry_file.c_str());
  file_bath>>*bathymetry;

  if(show==true)
    plot(*bathymetry);

}

// }}}----------------------------------------------------------

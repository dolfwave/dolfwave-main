// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  DolfwaveClass.cpp
//
// First added:  2009-04-14
// Last changed - nlopes: 2017-04-07
//
// Compiled with DOLFIN:0.9.9
// }}}------------------------------------------------------------------------

#include"../DwClass.h"

using namespace dolfin;
using namespace dolfin::dolfwave;

// {{{Predictor Corrector interface ---------------------


void dolfin::dolfwave::Dolfwave::PCInit(const Mesh &mesh, bool correctormultisteps)
{
  //Control Predictor-Corrector options here
  double pcepsilon=parameters("dolfwave_parameters")["pred_corr_epsilon"];
  int pcblowupiter=parameters("dolfwave_parameters")["pred_corr_blow_up"];
  int verbosity=parameters("dolfwave_parameters")["VerbosityLevel"];

  std::string element_signature;

  // Multi-Step  corrector
  if (correctormultisteps==true)
    {
      element_signature=V->element()->signature();
      pc.reset(new PredCorr(mesh,*V,element_signature,
                            pcepsilon,pcblowupiter-1,verbosity));
    }
  else
    //One step corrector
    pc.reset(new PredCorr( ));
}



void dolfin::dolfwave::Dolfwave::PCSolve( )
{
  int is_diverging=0;
  if (AlgBackEnd=="LU_P")
    {

      is_diverging=pc->SolveLU(pcrk_b,L,*M,eta_phi,
                            TimeStep,*dbc,dbcapply,*petsclu,*Mf,
                            AlgBackEnd);
    }
  else if (AlgBackEnd=="GMRES_P")
    {
      is_diverging=pc->SolveGMRES(pcrk_b,L,*M,eta_phi,
                     TimeStep,*dbc,dbcapply,*Mf,AlgBackEnd);
    }
  else
    {
      NotImplemented("#dw# ************Solver type not implemented",__LINE__,__FILE__);
    }

  /************************************************************************
   /// Some LPT2z0dg.ufl related stuff -----------------------------------
   /// TODO: CLEAN THIS IS NOT WORKING FOR THE UPDATED INTERFACES
   /// Auxiliar Function space for dg formulation and convection forward in
  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();
  if (FormName=="LPT2z0dg")
    GradPhiDG();
  ************************************************************************/

  if (is_diverging)
    IsDiverging = true;
  else {
    TimeUpdate();
    StepUpdate();
  }

}

// }}}-------------------------------------------------

// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  PredictorCorrector.h
//
// First added:  2009-04-09
// Last changed: 2014-07-10
// Compiled with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------


#ifndef __PREDICTORCORRECTOR_H
#define __PREDICTORCORRECTOR_H

#include <dolfin.h>
#include <cstdlib> //For system commands
#include <fstream> //for ofstream File saving

#include <boost/shared_array.hpp> //For boost::shared_array

#include "../2hdforms/L2Norm.h"
#include "../1hdforms/L2Norm_1D.h"
#include "../2hdforms/L2NormP2.h"
#include "../1hdforms/L2NormP2_1D.h"
#include "../2hdforms/L2NormDG.h"

// 1 equation model required Forms
#include "../1hd1sforms/L2Norm_1D1S.h"
#include "../1hd1sforms/L2NormP2_1D1S.h"



namespace dolfin
{
  namespace dolfwave
  {

    typedef std::shared_ptr<Function> Function_Ptr;
    typedef std::shared_ptr<Form> Form_Ptr;
    typedef std::shared_ptr<Vector> Vector_Ptr;

    // {{{  Predictor Corrector-Method---------------------------
    class PredCorr
    {
    public:
      PredCorr();
      PredCorr(const Mesh &,FunctionSpace& ,std::string ,
	       double , int, int verbosity_level=1);
      void PredAssemble(boost::shared_array<Vector_Ptr>& pcrk_b,
                         const dolfin::Form &L0,
                         const dolfin::Form &L1,
                         const dolfin::Form &L2,
                         const dolfin::Form &L3,
                         Matrix &M,
                         GenericVector& y,
                         double timestep);

      void CorrAssemble(boost::shared_array<Vector_Ptr>& pcrk_b,
                         const dolfin::Form &L1,
                         const dolfin::Form &L2,
                         const dolfin::Form &L3,
                         const dolfin::Form &L4,
                         Matrix &M,
                         GenericVector& y,
                         double timestep);

      void Disp();

      /// PC LU  solvers
      int SolveLU(boost::shared_array<Vector_Ptr>& pcrk_b,
                   boost::shared_array<Form_Ptr>& L,
                   Matrix &M,
                   boost::shared_array<Function_Ptr>& eta_phi,
                   double  timestep,
                   DirichletBC &dbc,
                   bool dbcapply,
                   PETScLUSolver &petsclu,
                   Matrix &Mf,
                   std::string);


      /// PC GMRES solvers
      int SolveGMRES(boost::shared_array<Vector_Ptr>& pcrk_b,
                      boost::shared_array<Form_Ptr>& L,
                      Matrix &M,
                      boost::shared_array<Function_Ptr>&
                      eta_phi,
                      double  timestep,
                      DirichletBC &dbc,
                      bool dbcapply,
                      Matrix &Mf,
                      std::string);



    private:
      double p_cts[4];
      double c_cts[4];
      double l2error;

      ///Error for multi-step corrector
      std::shared_ptr<Function> error;
      std::shared_ptr<Form> ML2N;

      bool multicorrector;
      double PcEpsilon;
      int PcMultiStepMax;
      int Verbosity;


      /// PC Factorized LU based solvers
      void PredSolveLU(boost::shared_array<Vector_Ptr>& pcrk_b,
                       const dolfin::Form &L0,
                       const dolfin::Form &L1,
                       const dolfin::Form &L2,
                       const dolfin::Form &L3,
                       Matrix &M,
                       Function &eta_phi3,
                       Function &eta_phi4,
                       double  timestep,
                       DirichletBC &dbc,
                       bool dbcapply,
                       PETScLUSolver &petsclu,
                       //EpetraLUSolver &epetralu,
                       Matrix &Mf,
                       std::string algbcknd);




      /// PC Factorized LU based solvers
      void CorrSolveLU(boost::shared_array<Vector_Ptr>& pcrk_b,
                       const dolfin::Form &L1,
                       const dolfin::Form &L2,
                       const dolfin::Form &L3,
                       const dolfin::Form &L4,
                       Matrix &M,
                       Function &eta_phi3,
                       Function &eta_phi4,
                       double  timestep,
                       DirichletBC &dbc,
                       bool dbcapply,
                       PETScLUSolver &petsclu,
                       //EpetraLUSolver &epetralu,
                       Matrix &Mf,
                       std::string algbcknd);





      /// PC UnFactorized  solvers
      /// predictor step with Dirichlet boundary conditions
      void PredSolveGMRES(boost::shared_array<Vector_Ptr>& pcrk_b,
                           const dolfin::Form &L0,
                           const dolfin::Form &L1,
                           const dolfin::Form &L2,
                           const dolfin::Form &L3,
                           Matrix &M,
                           Function &eta_phi3,
                           Function &eta_phi4,
                          double  timestep,
                           DirichletBC &dbc,
                           bool dbcapply,
                           Matrix &Mf,
                           std::string);



      /// PC UnFactorized based solvers
      void CorrSolveGMRES(boost::shared_array<Vector_Ptr>& pcrk_b,
                           const dolfin::Form &L1,
                           const dolfin::Form &L2,
                           const dolfin::Form &L3,
                           const dolfin::Form &L4,
                           Matrix &M,
                           Function &eta_phi3,
                           Function &eta_phi4,
                          double  timestep,
                           DirichletBC &dbc,
                           bool dbcapply,
                           Matrix &Mf,
                           std::string);


    };
    // }}}
  }
}
#endif

// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  PredictorCorrectorc.cpp
// First added:  2009-04-09
// Last changed: 2015-05-19
// Compiled with DOLFIN:1.0


#include <dolfin.h>
#include "predictorcorrector.h"
#include "../outputs/debugfunctions.h"
#include "../functionsfactory/updatefunctions.h"
#include "../DwConstants.h"

using namespace dolfin;
using namespace dolfin::dolfwave;



// {{{PREDICTOR STEP SOLVER KrylovSOLVER------------------------------------------
/*
with and without Dirichlet boundary conditions
*/


void PredCorr::PredSolveGMRES(boost::shared_array<Vector_Ptr>& pcrk_b,
                              const dolfin::Form &L0,
                              const dolfin::Form &L1,
                              const dolfin::Form &L2,
                              const dolfin::Form &L3,
                              Matrix &M,
                              Function &eta_phi3,
                              Function &eta_phi4,
                              double  timestep,
                              DirichletBC &dbc,
                              bool dbcapply,
                              Matrix &Mf,
                              std::string algbcknd)
{
  PredAssemble(pcrk_b,L0,L1,L2,L3,M,
               *(eta_phi3.vector()),timestep);
  if (dbcapply)
    dbc.apply(Mf,*pcrk_b[5]);
  if (algbcknd=="GMRES_P")
    {
      PETScKrylovSolver gmressolver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      gmressolver.solve(Mf,*(eta_phi4.vector()),*pcrk_b[5]);//eta_phi4^0 is calculated

    }
  else
    {
      NotImplemented("Solver type not implemented",__LINE__,__FILE__);
    }
}



// }}}---------------------------------------------------------------------------

// {{{CORRECTOR STEP SOLVER------------------------------------------
/*
with and without Dirichlet boundary conditions
*/

void PredCorr::CorrSolveGMRES(boost::shared_array<Vector_Ptr>& pcrk_b,
                               const dolfin::Form &L1,
                               const dolfin::Form &L2,
                               const dolfin::Form &L3,
                               const dolfin::Form &L4,
                               Matrix &M,
                               Function &eta_phi3,
                               Function &eta_phi4,
                              double  timestep,
                               DirichletBC &dbc,
                               bool dbcapply,
                               Matrix &Mf,
                               std::string algbcknd)
{
  CorrAssemble(pcrk_b,L1,L2,L3,L4,M,
               *(eta_phi3.vector()),timestep);
  if (dbcapply)
    dbc.apply(Mf,*pcrk_b[5]);

  if (algbcknd=="GMRES_P")
    {
      PETScKrylovSolver gmressolver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      gmressolver.solve(Mf,*(eta_phi4.vector()),*pcrk_b[5]);
    }
  else
    {
      NotImplemented("Solver type not implemented",__LINE__,__FILE__);
    }
}



// }}}---------------------------------------------------------------------------



// {{{ GENERIC PREDICTOR CORRECTOR LU Factorized SOLVER------------------------------------------
/*
with and without Dirichlet boundary conditions
*/


int PredCorr::SolveGMRES(boost::shared_array<Vector_Ptr>& pcrk_b,
                          boost::shared_array<Form_Ptr>& L,
                          Matrix &M,
                          boost::shared_array<Function_Ptr>& eta_phi,
                          double  timestep,
                          DirichletBC &dbc,
                          bool dbcapply,
                          Matrix &Mf,
                          std::string algbcknd)
{

  int exit_status=0;

  PredSolveGMRES(pcrk_b,*L[0],*L[1],*L[2],*L[3],
                 M,*eta_phi[3],*eta_phi[4],
                 timestep,dbc,dbcapply,Mf,algbcknd);
  if(!multicorrector)
    CorrSolveGMRES(pcrk_b,*L[1],*L[2],*L[3],*L[4],
                   M,*eta_phi[3],*eta_phi[4],
                   timestep,dbc,dbcapply,Mf,algbcknd);
  else
    {
      unsigned int counter=0;
      while (l2error>PcEpsilon)
        {
          *error=*eta_phi[4];

          CorrSolveGMRES(pcrk_b,*L[1],*L[2],*L[3],*L[4],
                         M,*eta_phi[3],*eta_phi[4],
                         timestep,dbc,dbcapply,Mf,algbcknd);

          error->vector()->axpy(-1.0,*(eta_phi[4]->vector()));
          l2error=assemble(*ML2N);
          l2error=sqrt(l2error);

          if(Assert(!(isnan(l2error)||isinf(l2error)),__LINE__,
                    __FILE__,"#dw# ************NAN or INF values of multi-step corrector \n",false))
            exit_status=1;

          if(Assert(!(counter>PcMultiStepMax),__LINE__,
                    __FILE__,"#dw# ************Multi-step corrector max number of iterations exceeded \n",false))
             exit_status=1;

          counter++;
          info("#dw# ************Corrector L2  error=%e, Step=%d",l2error,counter);
        }
      l2error=1.0;//reset l2error for the next step;
    }
  Update(eta_phi,"PC");

  return exit_status;
}

// }}}---------------------------------------------------------------------------

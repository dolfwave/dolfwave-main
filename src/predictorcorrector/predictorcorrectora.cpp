// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  PredictorCorrector.cpp
// First added:  2009-04-09
// Last changed: 2014-07-10
// Compiled with DOLFIN:0.9.9


#include <dolfin.h>

#include "predictorcorrector.h"
#include "../outputs/debugfunctions.h"

#include "../DwConstants.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


// {{{  Predictor Corrector-Method---------------------------
PredCorr::PredCorr()
{
  // Constants for the 4th order Adams-Basfourth-Molton
  p_cts[0]=55./24.;
  p_cts[1]=-59./24.;
  p_cts[2]=37./24.;
  p_cts[3]=-9./24.;

  c_cts[0]=9./24.;
  c_cts[1]=19./24.;
  c_cts[2]=-5./24.;
  c_cts[3]=1./24.;

  multicorrector=false;
  PcEpsilon=1.0;

  Disp();
}

PredCorr::PredCorr(const Mesh &mesh,FunctionSpace &V,
                   std::string element_signature, double eps,
                   int max_iter_number, int verbosity_level)
{
  // Constants for the 4th order Adams-Basfourth-Molton
  p_cts[0]=55./24.;
  p_cts[1]=-59./24.;
  p_cts[2]=37./24.;
  p_cts[3]=-9./24.;

  c_cts[0]=9./24.;
  c_cts[1]=19./24.;
  c_cts[2]=-5./24.;
  c_cts[3]=1./24.;

  multicorrector=true;
  l2error=1.0;
  PcEpsilon=eps;
  PcMultiStepMax=max_iter_number;
  Verbosity=verbosity_level;

  error.reset(new Function(V));
  error->vector()->zero();


  std::shared_ptr<Form> funct;

  if (element_signature==CONTINUOUS_P1)
    funct.reset(new L2Norm::Functional(mesh,*error));
  else if (element_signature==CONTINUOUS_P1_1D)
    funct.reset(new L2Norm_1D::Functional(mesh,*error));
  else if (element_signature==CONTINUOUS_P1_1D1S)
    funct.reset(new L2Norm_1D1S::Functional(mesh,*error));
  else if (element_signature==CONTINUOUS_P2)
    funct.reset(new L2NormP2::Functional(mesh,*error));
  else if (element_signature==CONTINUOUS_P2_1D)
    funct.reset(new L2NormP2_1D::Functional(mesh,*error));
  else if (element_signature==CONTINUOUS_P2_1D1S)
    funct.reset(new L2NormP2_1D1S::Functional(mesh,*error));
  else if (element_signature==DISCONTINUOUS_P1)
    funct.reset(new L2NormDG::Functional(mesh,*error));
  else NotImplemented(element_signature,__LINE__,__FILE__);

  ML2N=funct;
  Disp();
 };

void PredCorr::PredAssemble(boost::shared_array<Vector_Ptr>& pcrk_b,
                             const dolfin::Form &L0,
                             const dolfin::Form &L1,
                             const dolfin::Form &L2,
                             const dolfin::Form &L3,
                             Matrix &M,
                             GenericVector& y,
                             double timestep)
{
  assemble(*pcrk_b[0],L0);//f(y[0])
  assemble(*pcrk_b[1],L1);//f(y[1])
  assemble(*pcrk_b[2],L2);//f(y[2])
  assemble(*pcrk_b[3],L3);//f(y[3])
  M.mult(y,*pcrk_b[5]);//pcrk_b[5]=M*y[3]
  for(int j=0;j<4;j++)
    pcrk_b[5]->axpy((timestep*p_cts[j]),*pcrk_b[j]);

};

void PredCorr::CorrAssemble(boost::shared_array<Vector_Ptr>& pcrk_b,
                             const dolfin::Form &L1,
                             const dolfin::Form &L2,
                             const dolfin::Form &L3,
                             const dolfin::Form &L4,
                             Matrix &M,
                             GenericVector& y,
                             double timestep)
{
    assemble(*pcrk_b[1],L1);//f(y[1])
    assemble(*pcrk_b[2],L2);//f(y[2])
    assemble(*pcrk_b[3],L3);//f(y[3])
    assemble(*pcrk_b[4],L4);//f(y[4]) //using y[4]^0 from the predictor step
    M.mult(y,*pcrk_b[5]);//pcrk_b[5]=M*y[3]
    for(int j=1;j<5;j++)
      pcrk_b[5]->axpy((timestep*c_cts[j-1]),*pcrk_b[j]);
  };

void PredCorr::Disp()
  {
    cout<<"**************************************"<<endl;
    cout<<"ABM Predict.Correct. 4 order constants"<<endl;
    cout<<"******************PRED****************"<<endl;
    for(int i=0;i<4;i++)
      cout<<p_cts[i]<<"\t";
    cout<<endl;
    cout<<"******************CORR****************"<<endl;
    for(int i=0;i<4;i++)
      cout<<c_cts[i]<<"\t";
    cout<<endl;
    if (multicorrector)
      cout<<"Multicorrector Steps=True"<<endl;
    else
      cout<<"Multicorrector Steps=False"<<endl;
    cout<<"**************************************"<<endl;
  };

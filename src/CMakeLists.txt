set(DOLFWAVE_H dolfwave.h)

#------------------------------------------------------------------------------------------------------
#DOLFWAVE source directories

set(DOLFWAVE_DIRS
  ./
  1hdforms
  1hd1sforms
  2hdforms
  outputs
  formsfactory
  rungekutta
  predictorcorrector
  debug
  timings
  functionsfactory
  specialfunctions
  initconditions
  inputs
  dbcs
  la
  matrixassemble
  meshtransform
  spaces
  )

#--------------------------------------------------------------------------------
# Header files names and paths

foreach(DIR ${DOLFWAVE_DIRS})
  file(GLOB _HEADERS ${DIR}/*.h)
  list(APPEND HEADERS ${_HEADERS})
   file(GLOB _SOURCES ${DIR}/*.cpp)
  list(APPEND SOURCES ${_SOURCES})
endforeach()

#------------------------------------------------------------------------------
# Set compiler flags, include directories and library dependencies

# Compiler definitions
# Add CXX defintions
add_definitions(${DOLFWAVE_CXX_DEFINITIONS})
set(DOLFWAVE_CXX_DEFINITIONS "${DOLFIN_CXX_DEFINITIONS}")
message( "DOLFWAVE_CXX_DEFINITIONS are ${DOLWAVE_CXX_DEFINITIONS}")

# Add compiler include directories
include_directories(${DOLFWAVE_SOURCE_DIR} ${DOLFWAVE_INCLUDE_DIRECTORIES})

# Add flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${DOLFWAVE_CXX_FLAGS}")

# Define libraries
add_library(dolfwave SHARED ${DOLFWAVE_H} ${HEADERS} ${SOURCES})

#add_executable(maintest main.cpp)
# Target libraries
target_link_libraries(dolfwave ${DOLFIN_LIBRARIES} ${DOLFIN_3RD_PARTY_LIBRARIES})

#------------------------------------------------------------------------------
# Generate pkg-config file and install it

# Define packages that should be required by pkg-config file
get_directory_property(cmake_inc INCLUDE_DIRECTORIES)
foreach(_inc_dir ${cmake_inc})
  set(PKG_CXXFLAGS "-I${_inc_dir} ${PKG_CXXFLAGS}")
endforeach()
string(REGEX REPLACE ";" " " PKG_DEFINITIONS ${PKG_CXXFLAGS})

# Add CXX COMPILER flags
set(PKG_CXXFLAGS ${CMAKE_CXX_FLAGS} ${PKG_CXXFLAGS})
string(REGEX REPLACE ";" " " PKG_CXXFLAGS ${PKG_CXXFLAGS})

# Convert compiler defintions into a list for pkg-config
# Append definitions to PKG_CXXFLAGS
string(REGEX REPLACE ";" " " PKG_DEFINITIONS ${DOLFWAVE_CXX_DEFINITIONS})
set(PKG_CXXFLAGS ${PKG_DEFINITIONS} ${PKG_CXXFLAGS})

# Add definitions to PKG_CXXFLAGS
set(PKG_CXXFLAGS ${PKG_DEFINITIONS} ${PKG_CXXFLAGS})

# Configure and install pkg-config file
configure_file(${DOLFWAVE_CMAKE_DIR}/templates/dolfwave.pc.in ${CMAKE_BINARY_DIR}/dolfwave.pc @ONLY)

#------------------------------------------------------------------------------
# Generate CMake config file (dolfwave-config.cmake)
# Get library location
get_target_property(DOLFWAVE_LIBRARY dolfwave LOCATION)

configure_file(${DOLFWAVE_CMAKE_DIR}/templates/dolfwave-config.cmake.in ${CMAKE_BINARY_DIR}/src/dolfwave-config.cmake @ONLY)
message("DOLFWAVE_CMAKE_DIR=${DOLFWAVE_CMAKE_DIR}")
message("CMAKE_BINARY_DIR=${CMAKE_BINARY_DIR}")
#------------------------------------------------------------------------------

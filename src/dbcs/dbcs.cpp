// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  DolfwaveClassDBC.cpp
//
// First added:  2009-04-14
// Last changed: 2012-02-01
//
// Compiled with DOLFIN-1.0.0
// }}}------------------------------------------------------------------------

#include"../DwClass.h"

using namespace dolfin;
using namespace dolfin::dolfwave;

// {{{ Dirichlet Boundary Conditions
void dolfin::dolfwave::Dolfwave::DolfwaveDBC(dolfin::Expression &bc,dolfin::SubDomain &boundary)
{
  dbc=new dolfin::DirichletBC(*V,bc,boundary);
  dbcapply=true;
}
// }}}

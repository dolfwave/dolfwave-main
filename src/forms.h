// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// forms.h
// First added:  2010-06-13
// Last changed: 2016-02-03
// Compiled with DOLFIN:0.9.7


#ifndef __FORMS_H
#define __FORMS_H

// 2HD forms
#include "2hdforms/Zhao.h"
#include "2hdforms/ZhaoP2.h"

// DG  and CDG forms
#include "2hdforms/LPTcdg.h"
#include "2hdforms/LPTcdgHnl.h"
#include "2hdforms/GradDG.h"

// 1HD forms
#include "1hdforms/Zhao_1D.h"
#include "1hdforms/LPTcdg_1D.h"
#include "1hdforms/LPTcdgHnl_1D.h"
#include "1hdforms/ZhaoP2_1D.h"


// 1D forms 1S (Single equation models)
// KdV-Type models
#include "1hd1sforms/BBM_1D.h" // BBM equation
#include "1hd1sforms/aKdVcdgP1P2_1D.h" //KdV-BBM equation (dimensionless generic form with P1xP2 Spaces) //POSTPONED
#include "1hd1sforms/xKdVcdg_1D.h" //KdV-BBM with txxxx correction term INPROGRESS

// Single Function Spaces in 2HD
#include "2hdforms/P1.h"
#include "2hdforms/P2.h"

// Single Function Spaces in 1HD
#include "1hdforms/P1_1D.h"
#include "1hdforms/P2_1D.h"
#endif

// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
// transform.cpp
//
// First added:  2010-09-15
// Last changed: 2013-01-04
//
// Compiled with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#include "transform.h"

void dolfin::dolfwave::Rotate(Mesh& mesh, const double angle)
{

  const int N = mesh.num_vertices();
  const int gdim = mesh.geometry().dim();

  if(gdim!=2)
    error("Unable to move mesh, illegal value dimension of mesh geometry.");

  // Rotate vertex coordinates
  std::vector<double> x = mesh.geometry().x();
  double xold=0.0,yold=0.0;
  for (int i = 0; i < N; i++)
    {
      xold=x[i*gdim ];
      yold=x[i*gdim+1];
      x[i*gdim ] = cos(angle)*xold-sin(angle)*yold;
      x[i*gdim + 1] = sin(angle)*xold+cos(angle)*yold;
    }
}

void dolfin::dolfwave::Rescale(Mesh& mesh,
                               const double old_left_X,
                               const double old_bottom_Y,
                               const double old_right_X,
                               const double old_top_Y,
                               const double new_left_X,
                               const double new_bottom_Y,
                               const double new_right_X,
                               const double new_top_Y)
{

  const int N = mesh.num_vertices();
  const int gdim = mesh.geometry().dim();

  if(gdim!=2)
    error("Unable to move mesh, illegal value dimension of mesh geometry.");

  double delta1=(new_right_X-new_left_X)/(old_right_X-old_left_X);
  double delta2=new_left_X-old_left_X*delta1;
  double delta3=(new_top_Y-new_bottom_Y)/(old_top_Y-old_bottom_Y);
  double delta4=new_bottom_Y-old_bottom_Y*delta3;

  // Rotate vertex coordinates
  std::vector<double> x = mesh.geometry().x();
  double xold=0.0,yold=0.0;
  for (int i = 0; i < N; i++)
    {
      xold=x[i*gdim ];
      yold=x[i*gdim+1];
      x[i*gdim ] = delta1*xold+delta2;
      x[i*gdim + 1] =delta3*yold+delta4;
    }
}

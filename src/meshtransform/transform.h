// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
// transform.h
//
// First added:  2010-09-15
// Last changed: 2011-07-19
//
// Compiled with DOLFIN:0.9.9
// }}}------------------------------------------------------------------------


#ifndef __TRANSFORM_H
#define __TRANSFORM_H

#include<dolfin.h>

namespace dolfin
{
  namespace dolfwave
  {
    void Rotate(Mesh& mesh, const double angle);
    void Rescale(Mesh& mesh,
                 const double old_left_bottom_corner_X,
                 const double old_left_bottom_corner_Y,
                 const double old_right_top_corner_X,
                 const double old_right_top_corner_Y,
                 const double new_left_bottom_corner_X,
                 const double new_left_bottom_corner_Y,
                 const double new_right_top_corner_X,
                 const double new_right_top_corner_Y);
  }
}

#endif

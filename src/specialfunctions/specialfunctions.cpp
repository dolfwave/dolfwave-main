// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
// specialfunctions.cpp
//
// First added:  2012-12-16
// Last changed: 2016-07-05
//
// Compiled with DOLFIN:1.0.X
// }}}------------------------------------------------------------------------




#include "../DwClass.h"
#include "../DwConstants.h"
#include "../2hdforms/L2ProjSponge.h"

using namespace dolfin;
using namespace dolfin::dolfwave;

/// Experimental Stuff
/// TO BE Modified
/// INPROGRESS




void dolfin::dolfwave::Dolfwave::DerFAntiDerEta(const double meshsize)
{
  /* Here we should set the vector eta_phi[0]->vector() with the following
     vector f'(\int_{-L}^x eta_phi[3]->vector())  */

  /* Since here f(u)=u^2/2. f is not written explicitly */
  double sum=0.0;
  const double dx=meshsize;

  for (int i=0;i<eta_phi[0]->vector()->size();i++) //iterate over vector index
    {
      //      std::cout<<eta_phi[0]->vector()->getitem(i)<<std::endl;
      double value=eta_phi[3]->vector()->getitem(i);
      sum+=value*dx;
      eta_phi[0]->vector()->setitem(i,sum);
    }
  eta_phi[0]->vector()->apply("insert");
}

void dolfin::dolfwave::Dolfwave::GEta(const double M)
{
  /* Here we should set the vector eta_phi[1]->vector() with the following
     vector g(eta_phi[3]->vector())  */

  for (int i=0;i<eta_phi[1]->vector()->size();i++) //iterate over vector index
    { // Definition of function g(w)
      double value=eta_phi[3]->vector()->getitem(i);
          if (fabs(value)>M)
         eta_phi[1]->vector()->setitem(i,0);
       else
        eta_phi[1]->vector()->setitem(i,value);
    }
  eta_phi[1]->vector()->apply("insert");
}


void dolfin::dolfwave::Dolfwave::G2Eta(const double M)
{
  /* Here we should set the vector eta_phi[1]->vector() with the following
     vector g(eta_phi[3]->vector())  */

  for (int i=0;i<eta_phi[1]->vector()->size();i++) //iterate over vector index
    { // Definition of function g(w)
      eta_phi[0]->vector()->setitem(i,M*M);
    }
  eta_phi[0]->vector()->apply("insert");
}


void dolfin::dolfwave::Dolfwave::PhiEta( )
{
  /* Here we should set the vector eta_phi[0]->vector() with the following
     vector f(eta_phi[3]->vector()) where f(u)=-u^2/2  */


  for (int i=0;i<eta_phi[2]->vector()->size();i++) //iterate over vector index
    {
      double value=eta_phi[3]->vector()->getitem(i);
      if (isnan(value)||isinf(value))
        {
          Assert(0,__LINE__,__FILE__,"NAN or INF values of PhiEta \n");
        }
      eta_phi[1]->vector()->setitem(i,-value*value/2.); //-u^2/2
    }
  eta_phi[1]->vector()->apply("insert");
}


void dolfin::dolfwave::Dolfwave::ApplyVanishingLayer(Expression &vanishinglayer)
{

  Matrix ML2P;
  Vector b_init0;
  Vector b_init1;
  Vector b_init2;
  Vector b_init3;
  Vector b_init4;


  std::shared_ptr<Form> bil2p;
  std::shared_ptr<Form> lil2p0;
  std::shared_ptr<Form> lil2p1;
  std::shared_ptr<Form> lil2p2;
  std::shared_ptr<Form> lil2p3;
  std::shared_ptr<Form> lil2p4;



  info("#dw# Apply Vanishing Layers");
  std::string element_signature=V->element()->signature();


  if (element_signature==CONTINUOUS_P1)
    {
      bil2p.reset(new L2ProjSponge::BilinearForm(*V,*V));
      lil2p0.reset(new L2ProjSponge::LinearForm (*V,(*eta_phi[0])[0],(*eta_phi[0])[1],vanishinglayer));
      lil2p1.reset(new L2ProjSponge::LinearForm (*V,(*eta_phi[1])[0],(*eta_phi[1])[1],vanishinglayer));
      lil2p2.reset(new L2ProjSponge::LinearForm (*V,(*eta_phi[2])[0],(*eta_phi[2])[1],vanishinglayer));
      lil2p3.reset(new L2ProjSponge::LinearForm (*V,(*eta_phi[3])[0],(*eta_phi[3])[1],vanishinglayer));
      lil2p4.reset(new L2ProjSponge::LinearForm (*V,(*eta_phi[4])[0],(*eta_phi[4])[1],vanishinglayer));
    }
  else
    {
      NotImplemented("Vanishing Layer yet not Implemented for this Formulation",__LINE__,__FILE__);
    }

  assemble(ML2P,*bil2p);
  assemble(b_init0,*lil2p0);
  assemble(b_init1,*lil2p1);
  assemble(b_init2,*lil2p2);
  assemble(b_init3,*lil2p3);
  assemble(b_init4,*lil2p4);


  if (AlgBackEnd=="LU_P")
    {
      PETScLUSolver solver("default");
      solver.solve(ML2P,*(eta_phi[0]->vector()),b_init0);
      solver.solve(ML2P,*(eta_phi[1]->vector()),b_init1);
      solver.solve(ML2P,*(eta_phi[2]->vector()),b_init2);
      solver.solve(ML2P,*(eta_phi[3]->vector()),b_init3);
      solver.solve(ML2P,*(eta_phi[4]->vector()),b_init4);

    }
  else if (AlgBackEnd=="GMRES_P")
    {
      PETScKrylovSolver solver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      solver.solve(ML2P,*(eta_phi[0]->vector()),b_init0);
      solver.solve(ML2P,*(eta_phi[1]->vector()),b_init1);
      solver.solve(ML2P,*(eta_phi[2]->vector()),b_init2);
      solver.solve(ML2P,*(eta_phi[3]->vector()),b_init3);
      solver.solve(ML2P,*(eta_phi[4]->vector()),b_init4);
     }
  else
    NotImplemented("Solver type for vanishing layer \n",__LINE__,__FILE__);


  info("#dw# ***********End applying vanishing layer**********\n");

}

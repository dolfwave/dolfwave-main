from ufl import *
from ffc.compiler.uflcompiler import compile as compile_ufl

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
#
# e-mail: ndl@ptmat.fc.ul.pt
# http://ptmat.fc.ul.pt/~ndl/
#
# ---------------------------------------------------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
#
# ---------------------------------------------------------------------------
#  LPT2z0.ufl
#
# First added:  2009-03-26
# Last changed: 2009-11-25
#
# bilinear and linear variational forms for
# Lopes, Pereira, Trabucho.
#
# The problem is solved in a form M*[eta_t,phi_t]= f(U,eta)
# Using Runge-Kutta and predictor corrector-schemes
#
# Modified Boussinesq classical form with extra term due to
# Lopes, Pereira,Trabucho Approach.
#
# Compile this form with FFC: ffc -l dolfin ZPT2z0.ufl
# }}}------------------------------------------------------------------------


P=FiniteElement("Lagrange",triangle,1)
Th=P+P

(eta_t,phi_t)=TrialFunctions(Th)
(p,q)=TestFunctions(Th)


## Pay attention to the order  of argument calling
#Previous steps of the unkown functions
eta0=Function(P)
phi0=Function(P)
#Depht
h=Function(P)
#Gravity acceleration passed as Constant in *.cpp
g=Constant(triangle)
#SpongeLayer
spng=Function(P)
#Artificial Surface Tension Coefficient
tension = Constant(triangle) #Surface tension coefficient
#Source Function
srceta=Function(P)


#a macros

#Bilinear Variational form for M matrix
a=eta_t*p*dx\
   +phi_t*q*dx\
   +1./3.*h**2*inner(grad(phi_t),grad(q))*dx

#call with: a(h)

#Linear Variational form for f(U,eta)
#call with L(h, eta0,phi0,spng)
L=inner((h+eta0)*grad(phi0),grad(p))*dx\
   -1./2.*inner(grad(phi0),grad(phi0))*q*dx\
   -g*eta0*q*dx\
   -spng*inner(grad(eta0),grad(p))*dx\
   -tension*inner(grad(eta0),grad(q))*dx\
   +srceta*p*dx


compile_ufl([a, L, M, element], "LPT2z0", {'language': 'dolfin', 'format': 'dolfin', 'form_postfix': True, 'quadrature_order': 'auto', 'precision': '15', 'split_implementation': True, 'cache_dir': None, 'output_dir': '.', 'representation': 'auto', 'optimize': False}, globals())

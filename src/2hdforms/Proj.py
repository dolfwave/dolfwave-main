from ufl import *
from ffc.compiler.uflcompiler import compile as compile_ufl

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
#
# e-mail: ndl@ptmat.fc.ul.pt
# http://ptmat.fc.ul.pt/~ndl/
#
# ---------------------------------------------------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
#
# ---------------------------------------------------------------------------
# Proj.ufl
#
# First added:  2009-06-20
# Last changed: 2009-08-31
#
# Standard projection for sponge layer plot(for instance)
# Copied from a DOLFIN demo.
#
# Compile this form with FFC: ffc -l dolfin Proj.ufl
# }}}------------------------------------------------------------------------



P1 = FiniteElement("Lagrange", triangle, 1)

v = TestFunction(P1)
u = TrialFunction(P1)
f = Function(P1)

a = v*u*dx
L = v*f*dx

compile_ufl([a, L, M, element], "Proj", {'language': 'dolfin', 'format': 'dolfin', 'form_postfix': True, 'quadrature_order': 'auto', 'precision': '15', 'split_implementation': True, 'cache_dir': None, 'output_dir': '.', 'representation': 'auto', 'optimize': False}, globals())

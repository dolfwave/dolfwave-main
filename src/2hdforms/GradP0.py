from ufl import *
from ffc.compiler.uflcompiler import compile as compile_ufl

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
#
# e-mail: ndl@ptmat.fc.ul.pt
# http://ptmat.fc.ul.pt/~ndl/
#
# ---------------------------------------------------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
#
# ---------------------------------------------------------------------------
# SymProj.form
#
# First added:  2009-06-20
# Last changed: 2009-08-31
#
# Grad P0 calculation for horizontal velocities
# Copied from a DOLFIN demo.
#
# Compile this form with FFC: ffc -l dolfin GradP0.ufl
# }}}------------------------------------------------------------------------



VP0 = VectorElement("DG", triangle, 0)
SP1 =FiniteElement("Lagrange",triangle,1)

v = TestFunction(VP0)
u = TrialFunction(VP0)
phi = Function(SP1)

a = inner(v,u)*dx
L = inner(v,grad(phi))*dx

compile_ufl([a, L, M, element], "GradP0", {'language': 'dolfin', 'format': 'dolfin', 'form_postfix': True, 'quadrature_order': 'auto', 'precision': '15', 'split_implementation': True, 'cache_dir': None, 'output_dir': '.', 'representation': 'auto', 'optimize': False}, globals())

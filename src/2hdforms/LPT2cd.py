from ufl import *
from ffc.compiler.uflcompiler import compile as compile_ufl

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
#
# e-mail: ndl@ptmat.fc.ul.pt
# http://ptmat.fc.ul.pt/~ndl/
#
# ---------------------------------------------------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
#
# ---------------------------------------------------------------------------
#  LPT2cd.form
#
# First added:  2009-03-26
# Last changed: 2009-08-31
#
# bilinear and linear variational forms for
# Lopes, Pereira, Trabucho constant depth equations.
#
# The problem is solved in a form M*[eta_t,phi_t]= f(U,eta)
# Using Runge-Kutta and predictor corrector-schemes
#
# Another Modified Boussinesq form
# Lopes, Pereira,Trabucho Approach Bottom Potential.
#
# Compile this form with FFC: ffc -l dolfin LPT2cd.ufl
# }}}------------------------------------------------------------------------

########### Constant depth LPT second order model ##########################
########### Probably to BE removed #########################################

P=FiniteElement("Lagrange","triangle",1)
Th=P+P

(eta_t,phi_t)=TrialFunctions(Th)
(p,q)=TestFunctions(Th)


#Depht
h0=Function(P) #Constant depth
eta0=Function(P)
phi0=Function(P)

#Gravity acceleration passes as Constant in *.cpp
g=Function(P)


#SpongeLayer
spng=Function(P)

#Source Function
src=Function(P)

#Flux Functions
f1=Function(P)
f2=Function(P)

#a macros

#Bilinear Variational form for M matrix
a=(eta_t*p+(phi_t*q)+1./3.*h0*h0*inner(grad(phi_t),grad(q)))*dx

#call with: a(h)

#Linear Variational form for f(U,eta)
def t1(h,eta0,phi0,p):
    return inner((h0+eta0)*grad(phi0),grad(p))

def t2(phi0,q):
    return 0.5*inner(grad(phi0),grad(phi0))*q

def flux1(f1,f2,p):
    return f1*f2*p

def flux2(f1,f2,q):
    return f1*f2*q


#call with L(h0, eta0,phi0,spng)
L=(t1(h0,eta0,phi0,p)-t2(phi0,q)-g*(eta0*q)-spng*eta0*p+src*p)*dx+flux1(f1,f2,p)*ds(0)+flux2(f1,f2,q)*ds(0)

compile_ufl([a, L, M, element], "LPT2cd", {'language': 'dolfin', 'format': 'dolfin', 'form_postfix': True, 'quadrature_order': 'auto', 'precision': '15', 'split_implementation': True, 'cache_dir': None, 'output_dir': '.', 'representation': 'auto', 'optimize': False}, globals())

from ufl import *
from ffc.compiler.uflcompiler import compile as compile_ufl

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
#
# e-mail: ndl@ptmat.fc.ul.pt
# http://ptmat.fc.ul.pt/~ndl/
#
# ---------------------------------------------------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
#
# ---------------------------------------------------------------------------
#  L2Proj.ufl
#
# First added:  2008-06-20
# Last changed: 2009-08-31
#
# bilinear and linear variational forms for
# Zhao Teng and Cheng equations.
#
# The problem is solved in a form M*[eta_t,phi_t]= f(U,eta)
# Using Runge-Kutta and predictor corrector-schemes
#
# Compile this form with FFC: ffc -l dolfin L2Proj.ufl
# }}}------------------------------------------------------------------------



P=FiniteElement("Lagrange",triangle,1) #wave elevation space
Th=P+P

(eta,phi)=TrialFunctions(Th)
(p,q)=TestFunctions(Th)

eta_init=Function(P);
phi_init=Function(P);

a=(p*eta+q*phi)*dx
L=(p*eta_init+q*phi_init)*dx

compile_ufl([a, L, M, element], "L2Proj", {'language': 'dolfin', 'format': 'dolfin', 'form_postfix': True, 'quadrature_order': 'auto', 'precision': '15', 'split_implementation': True, 'cache_dir': None, 'output_dir': '.', 'representation': 'auto', 'optimize': False}, globals())

from ufl import *
from ffc.compiler.uflcompiler import compile as compile_ufl

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
#
# e-mail: ndl@ptmat.fc.ul.pt
# http://ptmat.fc.ul.pt/~ndl/
#
# ---------------------------------------------------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
#
# ---------------------------------------------------------------------------
#  L2Norm.ufl
#
# First added:  2009-02-27
# Last changed: 2009-08-31
#
# Functional
#
# Compile this form with FFC: ffc -l dolfin L2Norm.ufl
# }}}------------------------------------------------------------------------



P=FiniteElement("Lagrange",triangle,1);

eta_new=Function(P)
eta_old=Function(P)
phi_new=Function(P)
phi_old=Function(P)

M=((eta_new-eta_old)*(eta_new-eta_old) + (phi_new-phi_old)*(phi_new-phi_old))*dx


compile_ufl([a, L, M, element], "L2Norm", {'language': 'dolfin', 'format': 'dolfin', 'form_postfix': True, 'quadrature_order': 'auto', 'precision': '15', 'split_implementation': True, 'cache_dir': None, 'output_dir': '.', 'representation': 'auto', 'optimize': False}, globals())

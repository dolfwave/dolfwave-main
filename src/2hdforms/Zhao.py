from ufl import *
from ffc.compiler.uflcompiler import compile as compile_ufl

# Reserved variables for forms
(a, L, M) = (None, None, None)

# Reserved variable for element
element = None

# {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
#
# e-mail: ndl@ptmat.fc.ul.pt
# http://ptmat.fc.ul.pt/~ndl/
#
# ---------------------------------------------------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
#
# ---------------------------------------------------------------------------
# Zhao.ufl
#
# First added:  2008-06-20
# Lats changed:  2008-06-20
#
# bilinear and linear variational forms for
# Zhao, Cheng and Teng Boussinesq equations.
#
# The problem is solved in a form M*[eta_t,phi_t]= f(U,eta)
# Using Runge-Kutta and predictor corrector-schemes
#
# Compile this form with FFC: ffc -l dolfin Zhao.ufl
# }}}------------------------------------------------------------------------

P=FiniteElement("Lagrange",triangle,1)
Th=P+P

(eta_t,phi_t)=TrialFunctions(Th)#
(p,q)=TestFunctions(Th)



## Pay attention to the order  of argument calling
#Previous steps of the unkown functions
eta0=Function(P)
phi0=Function(P)
#Depht
h=Function(P)
#Gravity acceleration passed as Constant in *.cpp
g=Constant(triangle)
#SpongeLayer
sp_eta=Function(P) #eta0
sp_lap_eta=Function(P) #Lap eta0
sp_phi=Function(P) #phi0
sp_lap_phi=Function(P) #Lap phi0
#Source Function
srceta=Function(P)
#Normal Vector
n=VectorConstant(triangle)

#a macros
a0=eta_t*p*dx
a1=0.5*inner(h*h*grad(eta_t),grad(p))*dx
a2=-(1.0/6.0)*inner(grad(eta_t),grad(h*h*p))*dx
a3=(1.0/15.0)*inner(h*grad(h*eta_t),grad(p))*dx
a4=(phi_t*q)*dx
a5=-1./15.*h*inner(grad(h),n)*eta_t*p*ds

#Bilinear Variational form for M matrix
#call with: a(h,n)
a=a0+a1+a2+a3+a4+a5


#Linear Variational form for F(U,eta)
l0=inner(((h+eta0))*grad(phi0),grad(p))*dx
l1=-1./2.*inner(grad(phi0),grad(phi0))*q*dx
l2=-g*(eta0*q)*dx
l3=-g*(1.0/15.0)*inner(h*grad(eta0),grad(h*q))*dx

l4=-sp_eta*eta0*p*dx-sp_lap_eta*inner(grad(eta0),grad(p))*dx
l5=-sp_phi*phi0*q*dx-sp_lap_phi*inner(grad(phi0),grad(q))*dx
l6=srceta*p*dx

#call with L(eta0,phi0,h,sp_eta,sp_lap_eta,sp_phi,sp_lap_phi,srceta)
L=l0+l1+l2+l3+l4+l5+l6

compile_ufl([a, L, M, element], "Zhao", {'language': 'dolfin', 'format': 'dolfin', 'form_postfix': True, 'quadrature_order': 'auto', 'precision': '15', 'split_implementation': True, 'cache_dir': None, 'output_dir': '.', 'representation': 'auto', 'optimize': False}, globals())

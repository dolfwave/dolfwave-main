// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
// updatefunctions.h
//
// First added:  2010-03-29
// Last changed: 2014-07-10
//
// Compiled with DOLFIN:1.0.X
// }}}------------------------------------------------------------------------

#ifndef __UPDATEFUNTIONS_H
#define __UPDATEFUNTIONS_H

#include <boost/shared_array.hpp> //For boost::shared_array
#include "../outputs/debugfunctions.h" // For NotImplemented

namespace dolfin
{
  namespace dolfwave
  {

    typedef std::shared_ptr<Function> Function_Ptr;

    /// Update functions in time step +1
    void Update( boost::shared_array<Function_Ptr>&  , std::string );

  }
}

#endif

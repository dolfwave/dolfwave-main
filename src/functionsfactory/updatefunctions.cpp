// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
// UpdateFunctions.h
//
// First added:  2010-03-29
// Last changed: 2016-07-05
//
// Compiled with DOLFIN:1.6.0
// }}}------------------------------------------------------------------------

#include<dolfin.h>
#include "updatefunctions.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


void dolfin::dolfwave::Update( boost::shared_array<Function_Ptr>& eta_phi, std::string time_integrator_type)
{

  int imin=0;

  if (time_integrator_type=="RK")
    // Runge-Kutta 4th-order
    // Uses eta_phi[0:4]
    // RK to be RK4
    imin=0;
  else if (time_integrator_type=="PC")
    //ADams-Bashforth predictor-corrector 4th order
    // Uses eta_phi[0:4]
    // PC to be AB4
    imin=0;
  else
    NotImplemented("Time Integrator Method NOT Implemented.",__LINE__,__FILE__);

    for(int i=imin;i<4;i++)
      *(eta_phi[i]->vector())=*(eta_phi[i+1]->vector());
}

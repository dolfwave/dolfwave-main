// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  DolfwaveClassInit.cpp
//
// First added:  2009-04-14
// Last changed: 2015-06-02
//
// Compiled with DOLFIN:1.0.X
// }}}------------------------------------------------------------------------

#include "../DwClass.h"
using namespace dolfin;
using namespace dolfin::dolfwave;




void dolfin::dolfwave::Dolfwave::SpacesFunctionsVectorsInit(Mesh &mesh)
{
   FunctionSpaceInit(mesh);
   FunctionsInit();
   VectorInit();
}

void dolfin::dolfwave::Dolfwave::SpacesFunctionsVectorsInit(Mesh &mesh,SubDomain &periodic_boundary)
{
  FunctionSpaceInit(mesh, periodic_boundary);
   FunctionsInit();
   VectorInit();
}


// {{{ TestInit --------------------------------------------------------
/*************************************************************
 /// Some LPT2z0dg.ufl related stuff -----------------------------------
 /// TODO: CLEAN THIS IS NOT WORKING FOR THE UPDATED INTERFACES

void dolfin::dolfwave::Dolfwave::GradPhiDG( )
{
  info("GradPhi calculation");
  // Change to a faster factorized_solve
  solve(*grad_phi_a == *grad_phi_L, *grad_phi);
}
**************************************************************/
// }}} --------------------------------------------------------


// {{{ FunctionsInit -------------------------------------------------------
void dolfin::dolfwave::Dolfwave::FunctionsInit( )
{
  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();


  eta_phi.reset(new Function_Ptr [5]);
  for (int i=0;i<5;i++)
    {
      eta_phi[i].reset(new Function(*V));
      eta_phi[i]->vector()->zero();

    }

  // U is the single P1 or P2 space for bathymetry
  bathymetry.reset(new Function(*U));
  bathymetry->vector()->zero();

  if (FormName=="LPT2z0dg")
    {
      /***************************************************/
      // Experimental Code for Upwinding
      // *W is a vector valued Function Space DG0xDG0
      // For this to make sense we need
      // See GradDG.ufl
      //grad_phi=new Function(*W);
      //grad_phi->vector().zero();
      /***************************************************/
      //of=new IsOutflowFacet(*V_of,*grad_phi);
      NotImplemented(" TODO LPT2z0dg IsOutFlowFacet obsolete",__LINE__,__FILE__);
    }


}
// }}}---------------------------------------------------------------------

// {{{ VectorInit ------------------------------------------------
void dolfin::dolfwave::Dolfwave::VectorInit( )
{

  pcrk_b.reset(new Vector_Ptr [6]);//predictor-correct and runge-kutta auxiliar righ-hand vectors

  for(int i=0;i<6;i++)
    pcrk_b[i].reset(new Vector(*(eta_phi[0]->vector())) ) ; //pcrk_b[5] initialization
}
// }}}----------------------------------------------------------


void dolfin::dolfwave::Dolfwave::ReInitSolutionFunctions( )
{

  eta_phi.reset(new Function_Ptr  [5]);
  for (int i=0;i<5;i++)
    {
      eta_phi[i].reset(new Function(*V));
      eta_phi[i]->vector()->zero();
    }
}

// }}}---------------------------------------------------------------------

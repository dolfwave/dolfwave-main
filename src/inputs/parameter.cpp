// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// DwParameter.cpp
// First added:  2009-03-11
// Last changed: 2012-10-16
// Compiled with DOLFIN:0.9.9

#include "../DwClass.h"

using namespace dolfin;
using namespace dolfin::dolfwave;



void dolfin::dolfwave::Dolfwave::SetParameter(std::string name,double value )
{

  if (name=="Gravity")
    {
      /// ReInitialization of function
      parameters("dolfwave_parameters")["Gravity"]=value;
      g.reset(new Constant(value));
    }
  else if (name=="PenaltyTau")
    {
      /// ReInitialization of  parameter
      parameters("dolfwave_parameters")["PenaltyTau"]=value;
      tau.reset(new Constant(value));
    }
  else if (name=="S1Flux")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["S1Flux"]=value;
      s1flux.reset(new Constant(value));
    }
  else if (name=="S2Flux")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["S2Flux"]=value;
      s2flux.reset(new Constant(value));
    }
 else if (name=="KdVPade")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["KdVPade"]=value;
      kdvpade.reset(new Constant(value));
    }
 else if (name=="aKdVDelta0")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["aKdVDelta0"]=value;
      akdvd0.reset(new Constant(value));
    }
 else if (name=="aKdVDelta1")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["aKdVDelta1"]=value;
      akdvd1.reset(new Constant(value));
    }
 else if (name=="aKdVDelta2")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["aKdVDelta2"]=value;
      akdvd2.reset(new Constant(value));
    }
 else if (name=="aKdVEpsilon")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["aKdVEpsilon"]=value;
      akdveps.reset(new Constant(value));
    }
 else if (name=="xKdVPadeA")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["xKdVPadeA"]=value;
      xkdvpadea.reset(new Constant(value));
    }
 else if (name=="xKdVPadeB")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["xKdVPadeB"]=value;
      xkdvpadeb.reset(new Constant(value));
    }

else if (name=="xKdVDelta0")
   {
      /// ReInitialization of parameter
     parameters("dolfwave_parameters")["xKdVDelta0"]=value;
      xkdvd0.reset(new Constant(value));
    }
 else if (name=="xKdVDelta1")
   {
      /// ReInitialization of parameter
     parameters("dolfwave_parameters")["xKdVDelta1"]=value;
      xkdvd1.reset(new Constant(value));
    }
 else if (name=="xKdVDelta2")
   {
      /// ReInitialization of parameter
     parameters("dolfwave_parameters")["xKdVDelta2"]=value;
      xkdvd2.reset(new Constant(value));
    }
 else if (name=="xKdVDelta3")
   {
      /// ReInitialization of parameter
     parameters("dolfwave_parameters")["xKdVDelta3"]=value;
      xkdvd3.reset(new Constant(value));
    }
 else if (name=="xKdVEpsilon")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["xKdVEpsilon"]=value;
      xkdveps.reset(new Constant(value));
    }
  else if (name=="S1KdVFlux")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["S1KdVFlux"]=value;
      s1kdvflux.reset(new Constant(value));
    }
  else if (name=="S2KdVFlux")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["S2KdVFlux"]=value;
      s2kdvflux.reset(new Constant(value));
    }
  else if (name=="xKdVGamma")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["xKdVGamma"]=value;
      xkdvdumping.reset(new Constant(value));
    }
  else if (name=="xKdVPower")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["xKdVPower"]=value;
      xkdvpower.reset(new Constant(value));
    }
 else if (name=="Q1KdVFlux")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["Q1KdVFlux"]=value;
      q1kdvflux.reset(new Constant(value));
    }
  else if (name=="Q2KdVFlux")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["Q2KdVFlux"]=value;
      q2kdvflux.reset(new Constant(value));
    }
  else if (name=="NonLinearSwitch")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["NonLinearSwitch"]=value;
      nonlinear.reset(new Constant(value));
    }
  else if (name=="ExtraTermSwitch")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["ExtraTermSwitch"]=value;
      nonlinear.reset(new Constant(value));
    }
  else if (name=="Alpha")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["Alpha"]=value;
      alpha.reset(new Constant(value));
    }
  else if (name=="Beta1")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["Beta1"]=value;
      beta1.reset(new Constant(value));
    }
  else if (name=="Beta2")
    {
      /// ReInitialization of parameter
      parameters("dolfwave_parameters")["Beta2"]=value;
      beta2.reset(new Constant(value));
    }
  else
    NotImplemented("Wrong parameter name",__LINE__,__FILE__);

  std::cout <<"#dw# ** Update  Form Parameter "<< name << " to " <<value<<std::endl;
  *logfile  <<"#dw# ** Update  Form Parameter "<< name << " to " <<value<<std::endl;
}

void dolfin::dolfwave::Dolfwave::SetParameter(std::string name,bool  value )
{
  if (name=="dbcapply")
    {
      /// Initialization of parameter
      parameters("dolfwave_parameters")["dbcapply"]=value;
      dbcapply=value;
    }
  else
    NotImplemented("Wrong parameter name",__LINE__,__FILE__);

  std::cout <<"#dw# ** Update   Parameter "<< name << " to " <<value<<std::endl;
  *logfile  <<"#dw# ** Update   Parameter "<< name << " to " <<value<<std::endl;

}


void dolfin::dolfwave::Dolfwave::SetParameter(std::string name,int value)
{
  if (name=="VerbosityLevel")
    {
      /// Initialization of parameter
      parameters("dolfwave_parameters")["VerbosityLevel"]=value;
      Verbosity=value;
    }
  else
    NotImplemented("Wrong parameter name",__LINE__,__FILE__);

  std::cout <<"#dw# ** Update   Parameter "<< name << " to " <<value<<std::endl;
  *logfile  <<"#dw# ** Update   Parameter "<< name << " to " <<value<<std::endl;

}

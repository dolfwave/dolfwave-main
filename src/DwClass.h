// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// file:DwClass.h
// First added:  2009-04-14
// Last changed: 2016-06-29
// Compiled with DOLFIN:1.4.0
// ------------------------------------------------------------------------


#ifndef __DWCLASS_H
#define __DWCLASS_H

#include <dolfin.h>
#include <fstream> //for ofstream File saving
#include <string> //for string
#include <boost/shared_array.hpp> //For boost::shared_array

#include "rungekutta/rungekutta.h"
#include "predictorcorrector/predictorcorrector.h"

#ifdef __APPLE__
#define OSX 1
#else
#define OSX 0
#endif


namespace dolfin
{
  namespace dolfwave
  {

    typedef std::shared_ptr<Function> Function_Ptr;
    typedef std::shared_ptr<Form> Form_Ptr;
    typedef std::shared_ptr<Vector> Vector_Ptr;

    class Dolfwave
    {
    public:

      ///  Constructors-------------------------------------------------

      ///  Not used: returns an error and stops the process
      Dolfwave( );//Useless

      /// TODO:Not implemented yet: Read parameters from file
      Dolfwave(std::string FILENAME="dolfwave.xml");

      /// Default and esxplicit constructor.
      /** This constructor is used to initialize
          all the parameters passed by the users */

      Dolfwave(int MAXSTEPS,
               double TIMESTEP ,
               int WRITEGAP,
               std::string FORMNAME="LPTcdg",
               std::string ALGBACKEND="LU_P",
               std::string PLOTTER="dolfin",
               std::string OUTDIR="output",
               std::string OUTFORMAT="xyz",
               std::string COMMENT="");
      /// -----------------------------------------------------------------

      /// Destructor of DolfwaveClass
      inline ~Dolfwave(){TimerStop();FinalOutput();};
      /// To use in Debugs
      inline void AbortDolfwave(){
        TimerStop();fputs("ABORTED_DOLFWAVE",stderr);abort();
      };

      /// Options to be set by constructor---------------------------------

      /// Max step number to be executed
      int MaxSteps;

      /// Step iterator
      int Step;

      /// Advance Step
      void StepUpdate( );

      /// Time step for time-integration scheme
      double TimeStep;

      /// Output results interval
      int WriteGap;

      /** Linear System Solver. Possibilities are :
          "uBLAS":  factorized  LU solver via Umfpack, (for static Matrices)
          "PETSc": gmres ilu krylov solver via PETSc ,
          "PETScLU": unfactorized LU solver via PETSc */
      std::string AlgBackEnd;

      /// A comment to be displayed in the Initial and Final output
      /// It may be used to output extra results obtained during the computation
      std::stringstream Comment;


      /// Modifying a  parameter value------------------------------------
      void SetParameter(std::string , double );
      void SetParameter(std::string , bool );
      void SetParameter(std::string , int );

      //  Time related members-----------------------------------------------
      /// Time variable
      double Time;

      /// Time Stoping Criterions
      bool IsNull;
      bool IsDiverging;

      /// Verbosity leve;
      int Verbosity;

      /// Update time with TimeStep
      void TimeUpdate( );

      /// Add to time  a given value
      void TimeAdd(double );// Change Time
      // ---------------------------------------------------------------------

      // Plot and info functions----------------------------------------------
      /// Public Output functions


      ///Extra time steping process information
      void Info(bool systeminfo=true) const;

      /// Preview the solutions
      void PreviewEta(unsigned int i=4) const; //preview eta
      void PreviewPhi(unsigned int i=4) const; //preview phi

      /// Save solutions (
      void SaveEta(unsigned int i=4) const; //save eta
      void SavePhi(unsigned int i=4) const; //save phi


      /// Plot and save the symmetric of a given function (Depth for instance
      void DepthPlot(const Mesh & ,Expression &,const bool show=false) const;

      /// Plot and save the  a given function (sponge layers for instance)
      void SpongePlot(const Mesh & ,Expression &,const bool show=false) const;

      /// Calculates plots and saves the  horizontal velocity approximation (Restricted to some forms)
      void HorVelPlot(const Mesh & ,const bool show=false, const bool save=true) const;

      ///Saves the time profile  of a function at a given point(XX,YY)
      /**Saves the time profile  at the point (const double XX,const double YY)
        of the third argument function
        in the file with name given by the last argument */
      // For the 2HD problems
      void PointPlot(const double ,const double,
                     const std::string, const std::string ) const;
      // For the 1HD and 1HD1S problems
      void PointPlot(const double ,
                   const std::string, const std::string ) const;

      /// Saves a line profile  at a fixed time
      /**Saves a line profile  at a fixed time
         of the third argument function
         (Xs,Ys) first point of the line
         (Xe,Ye) last point of the line
         in the file with name given by the last argument
         The plot X'axis is the distance to the Start Point*/

      void LinePlot(const double /*Xstart*/ , const double /*Ystart*/,
                    const double  /*Xend*/,const double /*Yend*/,
                    const int /*Number of points in the line*/,
                    const std::string eta_or_phi, /*choose eta or phi*/
                    const std::string /*file name*/ ) const;

      /// Saves  "Volume.tv" which contains the integral of argument function. Needs a Depth function,  1HD and 2HD forms
      double Volume(const Mesh &, Expression & /* Depth*/,
                  std::string filename="") const;


      /// Applies a vanishing layer using L2 projection :: TO TWEAK :: (eta_phi,v)=((1-layer)eta_phi,v)
      void  ApplyVanishingLayer(Expression &);

      /* The following functions are to calculate solution invariants for
         1HD1S problems*/

      /// Saves  "first_invariant.tv" which contains the first invariant for KdV-BBM equations  1HD1Sforms
      double FirstInvariant(const Mesh &, std::string filename="" ) const;

      /// Saves  "second_invariant.tv" which contains the second invariant for KdV-BBM equations  1HD1Sforms
      double SecondInvariant(const Mesh &, std::string filename="" ) const;

      /// Saves  "third_invariant.tv" which contains the third invariant for KdV-BBM equations  1HD1Sforms
      double ThirdInvariant(const Mesh &,const Constant & , std::string filename="" ) const;
      // ---------------------------------------------------------------------


      /// The following functions are used to calculate discrete vector errors using l1, l2 ... linf norms
      double VectorNormError(const Mesh &, Expression &,std::string norm_type="l2",const bool file_write=true, std::string filename="" ) const;

       /// The following functions are used to calculate discrete vector norms using l1, l2 ... linf norms
    double VectorNorm(const Mesh &,std::string norm_type="l2",const bool file_write=true, std::string filename="" ) const;



      /// Initialize Function Spaces and Functions
      void SpacesFunctionsVectorsInit(Mesh &);
      void SpacesFunctionsVectorsInit(Mesh &, SubDomain &);

      /// Dolfwave Problem set and solve Functions -------------------------
      /// Initialize Bilinear Forms
      void BilinearFormInit(Mesh &, Expression &);

      /// Initialize Bilinear Forms
      void BilinearFormInit( MeshFunction<std::size_t> &);



      /// Initialize Bilinear Forms
      void BilinearFormInit(Mesh &, Expression &, Expression &);


      /// Initialize Linear Forms
      void LinearFormsInit(Expression &,Expression &,
                           Expression &,Expression &,
                           Expression &,Expression &);
      /// Initialize Linear Forms
      void LinearFormsInit(Expression &,Expression &,
                           Expression &,Expression &,
                           Expression &,Expression &,
                           Expression &);
      /// Initialize Linear Forms
      void LinearFormsInit(Expression &,Expression &,
                           Expression &,Expression &,
                           Expression &,Expression &,
                           Expression &, Expression &);

      /// Initialize Linear Forms //KdV and RlW family
      void LinearFormsInit(Expression &);
      void LinearFormsInit(Expression &, Expression &, Expression &, MeshFunction<std::size_t> &);
      void LinearFormsInit( ); /// Initialize Linear Forms //adimensional KdV family

      /// ReInitialize Unknown Functions
      void ReInitSolutionFunctions( );


      /// Loading Initial conditions from Functions
      void InitialCondition(Expression &,Expression &);

      /// Loading Initial conditions from file solutions (Restricted to some formats)
      void InitialCondition(std::string ,std::string );

      /// Loading Initial conditions from Functions
      void InitialCondition(Expression&);

      /// TODO Loading Initial conditions from file solutions (Restricted to some formats)
      void InitialCondition(std::string );

      /// Load Initial Bathymetry from file
      void LoadBathymetry(const Mesh &,std::string , bool show=false);


      /// Init and assemble system matrices M and Mf
      void MatricesAssemble(  );
      /// Reassemble the matrix for  moving bottom cases
      /** In this case a LU factorized Linear solver could be a
          very Bad choice
          Note: PETScLU is faster then uBLAS without reuse of factorization*/

      /// Linear Solver init
      void LUFactorization(bool reuse=true);

      /// Runge-Kutta init
      void RKInit(std::string rktype="exp4");

      /// Runge-Kutta solver
      void RKSolve(std::string reassemble="");

      /// Runge-Kutta explicit one step solver
      void RKSolveLoop(std::string reassemble="");

      /// Predictor-Corrector init
      void PCInit(const Mesh &, bool correctormultisteps=true );

      ///Predictor-Corrector solver
      void PCSolve( );

      ///DirichletBoundary Conditions
      void DolfwaveDBC(Expression &, SubDomain & );

      /// Matrices functions
      void MSparsityPlot( );
      void MfSparsityPlot( );
      void GetMatrixM( );
      void GetMatrixMf( );

      /// P.A. Experimental Stuff
      /************************************************************/
      void DerFAntiDerEta(const double );
      void GEta(const double );
      void G2Eta(const double );
      void PhiEta( );
      void Nabla2Eta( );
      /************************************************************/


    private:
      // Functions,Spaces,Subspaces,forms and Vectors------------------------
      /// Bilinear Form
      std::shared_ptr<Form> a;

      /// Linear Forms
      boost::shared_array<Form_Ptr> L;

      /// Unknown Functions
      boost::shared_array<Function_Ptr> eta_phi;

      /// Bathymetry Function
      std::shared_ptr<Function> bathymetry;

      /// Auxiliar vectors for Predictor Corrector and Runge-Kutta methods
      boost::shared_array<Vector_Ptr> pcrk_b;//Auxiliar Vector

      // ---------------------------------------------------------------------
      /// Identification string for problem type
      /// '1HD' '1HD1S' or '2HD'
      std::string ModelType;

      /// Mixed Function Space V=UxU
      std::shared_ptr<FunctionSpace> V;

      /// Single Function Space U (For bathymetry for instance)
      std::shared_ptr<FunctionSpace> U;


      /// Problem Matrices
      /// left-hand side matrix
      std::shared_ptr<Matrix> M;

      /// right-hand side matrix
      std::shared_ptr<Matrix> Mf;



      /*************************************************************
       /// Some LPT2z0dg.ufl related stuff -----------------------------------
       /// TODO: CLEAN THIS IS NOT WORKING FOR THE UPDATED INTERFACES
       /// Auxiliar Function space for dg formulation and convection forward in

      std::shared_ptr<FunctionSpace> V_of; //Function space for of function

      /// Auxiliar Function spaces for velocity (In HorVelPlot)
      std::shared_ptr<FunctionSpace> W;

      /// Gradient of potential unknown function (velocity)
      std::shared_ptr<Function> grad_phi;


      /// IsOutflowFacet function:
      Expression *of;

      /// Velocity related bilinear form
      std::shared_ptr<Form> grad_phi_a;

      /// Velocity related linear form
      std::shared_ptr<Form> grad_phi_L;

      /// Velocity calculations
      void GradPhiDG( );
      ************************************************************/

      /// Surface Tension Coefficient
      //  double TensionCoefficient;

      ///Predictor-Corrector pointer
      std::shared_ptr<PredCorr> pc;
      ///Runge-Kutta pointer
      std::shared_ptr<RungeKutta> rk;
      /// Main Process Timer
      std::shared_ptr<Timer> maintimer;
      /// Timer Stop Used by destructor
      void TimerStop( );
      ///Timer Start Used by constructor
      void TimerStart();
      ///Surface elevation Output Files
      std::shared_ptr<File> etafile;
      ///Potential  Output Files
      std::shared_ptr<File> phifile;
      ///Horizontal  velocity Output Files
      std::shared_ptr<File> horvelfile;
      ///Depth velocity output Files
      std::shared_ptr<File> depthfile;

      ///Log  output file
      std::shared_ptr<std::ofstream> logfile;

      ///DirichletBC
      DirichletBC *dbc;
      ///DirichletBC switch
      bool dbcapply;


      /// {{{ Arguments for form files------------------------------------
      ///Gravitational constant to be passed to all the form ufl files.
      std::shared_ptr<Expression> g;
      ///Penalty constant to be passed to DG and C/DG formulations
      std::shared_ptr<Expression> tau;
      /// Coefficents passed to C/DG numerical fluxes in LPT equations
      std::shared_ptr<Expression> s1flux;
      std::shared_ptr<Expression> s2flux;
      /// Coefficents passed to C/DG numerical fluxes in KdV equations
      std::shared_ptr<Expression> s1kdvflux;
      std::shared_ptr<Expression> s2kdvflux;
      std::shared_ptr<Expression> xkdvdumping;
      std::shared_ptr<Expression> xkdvpower;

      std::shared_ptr<Expression> q1kdvflux;
      std::shared_ptr<Expression> q2kdvflux;

      /// Coefficent passed KdV equations to phase velocity improvement
      std::shared_ptr<Expression> kdvpade;
      /// Coefficent passed aKdV equation
      std::shared_ptr<Expression> akdvd0;
      /// Coefficent passed aKdV equation
      std::shared_ptr<Expression> akdvd1;
      /// Coefficent passed aKdV equation
      std::shared_ptr<Expression> akdvd2;
      /// Coefficent passed aKdV equation
      std::shared_ptr<Expression> akdveps;


      /// Coefficent passed KdV equations to phase velocity improvement
      std::shared_ptr<Expression> xkdvpadea;
 /// Coefficent passed KdV equations to phase velocity improvement
      std::shared_ptr<Expression> xkdvpadeb;
      /// Coefficent passed xKdV equation
      std::shared_ptr<Expression> xkdvd0;
      /// Coefficent passed xKdV equation
      std::shared_ptr<Expression> xkdvd1;
      /// Coefficent passed xKdV equation
      std::shared_ptr<Expression> xkdvd2;
      /// Coefficent passed xKdV equation
      std::shared_ptr<Expression> xkdvd3;
      /// Coefficent passed xKdV equation
      std::shared_ptr<Expression> xkdveps;


      /// Level parameter to be passed to LPT equations
      std::shared_ptr<Expression> alpha;
      /// Dispersion parameter beta_1 to be passed to LPT equations
      std::shared_ptr<Expression> beta1;
      /// Dispersion parameter beta_2 to be passed to LPT equations
      std::shared_ptr<Expression> beta2;
      /// NonlinearSwitch constant to be passed to  LPT and Zhao equations
      std::shared_ptr<Expression> nonlinear;
      /// ExtraTermSwitch constant to be passed to  LPT equations
      std::shared_ptr<Expression> extraterm;
      /// }}} -------------------------------------------------------------

      /// Factorized LU solver
      std::shared_ptr<PETScLUSolver> petsclu;


      /// Display Initial Dolfwave options
      void InitialOutput() const;
      /// Display Final Dolfwave messages
      /// For instance to output some global results (via Comment)
      void FinalOutput() const;

      /// Plot a function with dolfin
      void DolfinPlot(const Function &) const;

      /// Plot a function with xd3d (XYZ)
      void Xd3dXyzPlot(const Function &) const;

      /// Plot a function with xgraphic (XY)
      void XgraphicXyPlot(const Function &) const;

      /// Plot a function with gnuplot (XYZ)
      void GnuPlot(const Function &) const;


      ///Setting Linear Solver. Used in constructor
      void SetLinearSolver( );
      ///To be used by RKSolve (Euler)
      void RKSolveEuler(std::string reassemble="",
                        std::string swtchloop="init");
      ///To be used by RKSolve (Exp4)
      void RKSolveExp4(std::string reassemble="",
                       std::string swtchloop="init");


      /// Initialize Function Spaces
      void FunctionSpaceInit(Mesh &);
      /// Initialize Function Spaces
      void FunctionSpaceInit(Mesh &, SubDomain &);

      /// Initialize Unknown Functions
      void FunctionsInit( );//Unknown functions Init

      /// Auxiliar Vectors Initialization
      void VectorInit( );

    };

  }
}
#endif

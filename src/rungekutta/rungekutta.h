// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  RungeKutta.h
//
// First added:  2009-04-09
// Last changed: 2014-07-10

#ifndef __RUNGEKUTTA_H
#define __RUNGEKUTTA_H

#include <dolfin.h>
#include <boost/shared_array.hpp> //For boost::shared_array

#include "../outputs/debugfunctions.h"


namespace dolfin
{
  namespace dolfwave
  {

    typedef std::shared_ptr<Function> Function_Ptr;
    typedef std::shared_ptr<Form> Form_Ptr;
    typedef std::shared_ptr<Vector> Vector_Ptr;
    // {{{ Runge-Kutta Method
    class RungeKutta
    {
    public:
      std::string RKType;
      /// Runge-Kutta constructor defines the method (explicit 1st "euler" and 4th-order "exp4")
      RungeKutta(std::string );

      /// Initilization for Auxiliar vector K
      void InitAuxVector(boost::shared_array<Function_Ptr>&);

      /// Euler Runge-Kutta Solvers
      void SolveEuler(boost::shared_array<Vector_Ptr>& pcrk_b,
                      boost::shared_array<Form_Ptr>& L,
                      Matrix &M,
                      boost::shared_array<Function_Ptr>& eta_phi,
                      double  &timestep,
                      DirichletBC &dbc,
                      bool dbcapply,
                      Matrix &Mf,
                      std::string ,
                      PETScLUSolver &petsclu,
                      //EpetraLUSolver &epetralu,
                      const unsigned int );

      /// Exp4 Runge-Kutta Solvers for non factorized Solvers
      void SolveExp4(boost::shared_array<Vector_Ptr>& pcrk_b,
                     boost::shared_array<Form_Ptr>& L,
                     Matrix &M,
                     boost::shared_array<Function_Ptr >& eta_phi,
                     double  &timestep,
                     DirichletBC &dbc,
                     bool dbcapply,
                     Matrix &Mf,
                     std::string ,
                     PETScLUSolver &petsclu,
                     //EpetraLUSolver &epetralu,
                     const unsigned int,
                     const unsigned int );



      /// Display Runge-Kutta method information on Initialization
      void Disp()
      {
        if(RKType=="euler")
          {
            cout<<"***************************************"<<endl;
            cout<<"Euler explicit Runge-Kutta method      "<<endl;
            cout<<"***************************************"<<endl;
          }
        else if(RKType=="exp4")
          {
            cout<<"***************************************"<<endl;
            cout<<"Explicit Runge-Kutta  4th order method "<<endl;
            cout<<"***************************************"<<endl;
          }
      };


    private:
      /// Auxiliar Vector K for Exp4
      boost::shared_array<Vector_Ptr> K;
      ///Vector *K;


      /// Explicit Exp4 step 1
      void  RKExp4Step1(boost::shared_array<Vector_Ptr>& pcrk_b,
                        dolfin::Form &L,
                        Matrix &M,
                        Function& eta_phi0,
                        Function& eta_phi1,
                        DirichletBC& dbc,
                        bool dbcapply,
                        double timestep,
                        Matrix &Mf,
                        std::string algbcknd,
                        PETScLUSolver &petsclu//,
                        //EpetraLUSolver &epetralu
                        );

      /// Explicit Exp4 step 2 method
      void  RKExp4Step2(boost::shared_array<Vector_Ptr>& pcrk_b,
                        dolfin::Form &L,
                        Matrix &M,
                        Function& eta_phi0,
                        Function& eta_phi1,
                        DirichletBC& dbc,
                        bool dbcapply,
                        double timestep,
                        Matrix &Mf,
                        std::string algbcknd,
                        PETScLUSolver &petsclu//,
                        //EpetraLUSolver &epetralu
                        );

      /// Explicit Exp4 step 3 method
      void  RKExp4Step3(boost::shared_array<Vector_Ptr>& pcrk_b,
                        dolfin::Form &L,
                        Matrix &M,
                        Function& eta_phi0,
                        Function& eta_phi1,
                        DirichletBC &dbc,
                        bool dbcapply,
                        double timestep,
                        Matrix &Mf,
                        std::string algbcknd,
                        PETScLUSolver &petsclu//,
                        //EpetraLUSolver &epetralu
                        );

     /// Explicit Exp4 step 4 method
      void  RKExp4Step4(boost::shared_array<Vector_Ptr>& pcrk_b,
                          dolfin::Form &L,
                          Matrix &M,
                          Function& eta_phi0,
                          Function& eta_phi1,
                          DirichletBC& dbc,
                          bool dbcapply,
                          double timestep,
                          Matrix &Mf,
                          std::string algbcknd,
                          PETScLUSolver &petsclu//,
                          //EpetraLUSolver &epetralu
                          );

    };
    // }}}
  }
}
#endif

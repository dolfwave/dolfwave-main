// Copyright (C) 2009 Nuno David Lopes.----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// rungekutta.cpp
// First added:  2009-04-09
// Last changed: 2015-05-19
// Compiled with DOLFIN:1.6.0dev

#include "rungekutta.h"
#include "../functionsfactory/updatefunctions.h"
#include "../DwConstants.h"

using namespace dolfin;
using namespace dolfin::dolfwave;

// {{{ Runge-Kutta Method

RungeKutta::RungeKutta(std::string rktype)
{
  if(rktype=="euler")
    {
      RKType="euler";
    }
  else if(rktype=="exp4")
    {
      RKType="exp4";
      K.reset(new Vector_Ptr [5]);
    }

  Disp();
}

void RungeKutta::InitAuxVector(boost::shared_array<Function_Ptr>& eta_phi)
{

  for (unsigned int i=0;i<5;i++)
    K[i].reset(new Vector(*(eta_phi[0]->vector())) );
  info("#dw# Initializing Aux Vectors for Exp4");
}

// }}}--------------------------------------------------------------------------



///Euler Solver
void RungeKutta::SolveEuler(boost::shared_array<Vector_Ptr>& pcrk_b,
                            boost::shared_array<Form_Ptr>& L,
                            Matrix &M,
                            boost::shared_array<Function_Ptr>& eta_phi,
                            double  &timestep,
                            DirichletBC &dbc,
                            bool dbcapply,
                            Matrix &Mf,
                            std::string algbcknd,
                            PETScLUSolver& petsclu,
                            const unsigned int i)
{
  assemble(*pcrk_b[0],*L[i]);//f(y[0])
  M.mult(*(eta_phi[i]->vector()),*pcrk_b[5]);//pcrk_b[5]=M*y[n]
  pcrk_b[5]->axpy(timestep,*pcrk_b[0]); //pcrk_b[5]=M*y[n]+h*f(y[n])

  if (dbcapply)
    dbc.apply(Mf,*pcrk_b[5]);

  if(algbcknd=="LU_P")
    {
      petsclu.solve(*(eta_phi[i+1]->vector()),*pcrk_b[5]);
    }
  else if (algbcknd=="GMRES_P")
    {
      PETScKrylovSolver gmressolver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      gmressolver.solve(Mf,*(eta_phi[i+1]->vector()),*pcrk_b[5]);
    }
  else
    {
      NotImplemented("Solver type not implented", __LINE__,__FILE__);
    }
}




/// Explicit 4th-order solver
void RungeKutta::SolveExp4(boost::shared_array<Vector_Ptr>& pcrk_b,
                           boost::shared_array<Form_Ptr>& L,
                           Matrix &M,
                           boost::shared_array<Function_Ptr>& eta_phi,
                           double  &timestep,
                           DirichletBC &dbc,
                           bool dbcapply,
                           Matrix &Mf,
                           std::string algbcknd,
                           PETScLUSolver &petsclu,
                           const unsigned int i,
                           const unsigned int substep)
{
  if (substep==1)
    RKExp4Step1(pcrk_b,*L[i],M,*eta_phi[i],*eta_phi[i+1],dbc,dbcapply,timestep,Mf,algbcknd,petsclu);
  else if (substep==2)
    RKExp4Step2(pcrk_b,*L[i],M,*eta_phi[i],*eta_phi[i+1],dbc,dbcapply,timestep,Mf,algbcknd,petsclu);
  else if (substep==3)
    RKExp4Step3(pcrk_b,*L[i],M,*eta_phi[i],*eta_phi[i+1],dbc,dbcapply,timestep,Mf,algbcknd,petsclu);
  else if (substep==4)
    RKExp4Step4(pcrk_b,*L[i],M,*eta_phi[i],*eta_phi[i+1],dbc,dbcapply,timestep,Mf,algbcknd,petsclu);
  else NotImplemented("Wrong SubStep for Exp4 Runge-Kutta method",__LINE__,__FILE__);
}



///Sub steps for Exp4  solver
void  RungeKutta::RKExp4Step1(boost::shared_array<Vector_Ptr>& pcrk_b,
                              dolfin::Form &L,
                              Matrix &M,
                              Function& eta_phi0,
                              Function& eta_phi1,
                              DirichletBC &dbc,
                              bool dbcapply,
                              double timestep,
                              Matrix &Mf,
                              std::string algbcknd,
                              PETScLUSolver &petsclu//,
                              )
{
  info("#dw# Runge-Kutta explicit 4-order substep 1");

  /* Y_(n+1)=Y_n+dt*(k_1/6+k_2/3+k_3/3+k_4/6)
     k_1=M^-1 x f(t_n,Y_n)
     k_2=M^-1 x f(t_n+1/2*dt,y[n]+1/2*dt*k_1)
     k_3=M^-1 x f(t_n+1/2*dt,y[n]+1/2*dt*k_2)
     k_4=M^-1 x f(t_n+dt,y[n]+dt*k_3)

     dt=timestep

     The Matrix M is passed by the function
     One just has to rebuild the LinearForms */

  *K[0]=*(eta_phi0.vector());
  *pcrk_b[0]=*K[0];

  *pcrk_b[1]=*pcrk_b[0];
  assemble(*pcrk_b[1],L);//f(y[n])
  if (dbcapply)
    dbc.apply(Mf,*pcrk_b[1]);

  if (algbcknd=="LU_P")
    petsclu.solve(*K[1],*pcrk_b[1]);
  else if (algbcknd=="GMRES_P")
    {
      PETScKrylovSolver gmressolver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      gmressolver.solve(Mf,*K[1],*pcrk_b[1]);
    }
  else
    {
      NotImplemented("Solver type not implented", __LINE__,__FILE__);
    }
}


void  RungeKutta::RKExp4Step2(boost::shared_array<Vector_Ptr>& pcrk_b,
                              dolfin::Form& L,
                              Matrix &M,
                              Function& eta_phi0,
                              Function& eta_phi1,
                              DirichletBC & dbc,
                              bool dbcapply,
                              double timestep,
                              Matrix &Mf,
                              std::string algbcknd,
                              PETScLUSolver &petsclu//,
                              //EpetraLUSolver &epetralu
                              )
{
  info("#dw# Runge-Kutta explicit 4-order substep 2");
  *pcrk_b[2]=*pcrk_b[0];
  pcrk_b[2]->axpy(timestep/2.0,*K[1]);
  assemble(*pcrk_b[2],L);//f(y[n]+1/2*dt*k_1)
  if (dbcapply)
    dbc.apply(Mf,*pcrk_b[2]);

  if (algbcknd=="LU_P")
    petsclu.solve(*K[2],*pcrk_b[2]);
  else if (algbcknd=="GMRES_P")
    {
      PETScKrylovSolver gmressolver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      gmressolver.solve(Mf,*K[2],*pcrk_b[2]);
    }
  else
    {
      NotImplemented("Solver type not implented", __LINE__,__FILE__);
    }
}

void  RungeKutta::RKExp4Step3(boost::shared_array<Vector_Ptr>& pcrk_b,
                              dolfin::Form& L,
                              Matrix &M,
                              Function& eta_phi0,
                              Function& eta_phi1,
                              DirichletBC & dbc,
                              bool dbcapply,
                              double timestep,
                              Matrix &Mf,
                              std::string algbcknd,
                              PETScLUSolver &petsclu
                              )
{
  info("#dw# Runge-Kutta explicit 4-order substep 3");
  *pcrk_b[3]=*pcrk_b[0];
  pcrk_b[3]->axpy(timestep/2.0,*K[2]);
  assemble(*pcrk_b[3],L);//f(y[n]+1/2*dt*k_2)
  if (dbcapply)
    dbc.apply(Mf,*pcrk_b[3]);

  if (algbcknd=="LU_P")
    petsclu.solve(*K[3],*pcrk_b[3]);
  else if (algbcknd=="GMRES_P")
    {
      PETScKrylovSolver gmressolver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      gmressolver.solve(Mf,*K[3],*pcrk_b[3]);
    }
  else
    {
      NotImplemented("Solver type not implented", __LINE__,__FILE__);
    }
}


void  RungeKutta::RKExp4Step4(boost::shared_array<Vector_Ptr>& pcrk_b,
                              dolfin::Form& L,
                              Matrix &M,
                              Function& eta_phi0,
                              Function& eta_phi1,
                              DirichletBC& dbc,
                              bool dbcapply,
                              double timestep,
                              Matrix &Mf,
                              std::string algbcknd,
                              PETScLUSolver &petsclu
                              )
{
  info("#dw# Runge-Kutta explicit 4-order substep 4");
  *pcrk_b[4]=*pcrk_b[0];
  pcrk_b[4]->axpy(timestep,*K[3]);
  assemble(*pcrk_b[4],L);//f(y[n]+dt*k_3)
  if (dbcapply)
    dbc.apply(Mf,*pcrk_b[4]);

  if (algbcknd=="LU_P")
    petsclu.solve(*K[4],*pcrk_b[4]);
  else if (algbcknd=="GMRES_P")
    {
      PETScKrylovSolver gmressolver("gmres",parameters("dolfwave_parameters")["pc_type"].value_str());
      gmressolver.solve(Mf,*K[4],*pcrk_b[4]);
    }
 else
   {
     NotImplemented("Solver type not implented", __LINE__,__FILE__);
   }

  *pcrk_b[5]=*K[0];
  pcrk_b[5]->axpy(timestep/6.0,*K[1]);
  pcrk_b[5]->axpy(timestep/3.0,*K[2]);
  pcrk_b[5]->axpy(timestep/3.0,*K[3]);
  pcrk_b[5]->axpy(timestep/6.0,*K[4]);
  if (dbcapply)
    dbc.apply(Mf,*pcrk_b[5]);

  *eta_phi1.vector()=*pcrk_b[5];
}



// }}} ---------------------------------------------------------------------------------

// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  DolfwaveClassRK.cpp
//
// First added:  2009-04-14
// Last changed: 2015-05-19
//
// Compiled with DOLFIN:1.6.0dev
// }}}------------------------------------------------------------------------

#include"../DwClass.h"
#include"../functionsfactory/updatefunctions.h"
using namespace dolfin;
using namespace dolfin::dolfwave;




// {{{Runge Kutta interface ---------------------
void dolfin::dolfwave::Dolfwave::RKInit(std::string rktype )
{
  if (rktype=="euler")
    {
      rk.reset(new  RungeKutta("euler"));
    }
    else if (rktype=="exp4")
      {
        rk.reset(new  RungeKutta("exp4"));
      }
  else
    NotImplemented("RungeKutta type not implemented yet",__LINE__,__FILE__);

}

void dolfin::dolfwave::Dolfwave::RKSolve(std::string reassemble)
{

  if (rk->RKType=="euler")
    {
      RKSolveEuler(reassemble,"init");

    }
  else if (rk->RKType=="exp4")
    {
      RKSolveExp4(reassemble,"init");
    }
  else  NotImplemented("RungeKutta type not implemented yet",__LINE__,__FILE__);
}

void dolfin::dolfwave::Dolfwave::RKSolveLoop(std::string reassemble)
{


  if (rk->RKType=="euler")
    {
      RKSolveEuler(reassemble,"loop");
    }
  else if (rk->RKType=="exp4")
    {
      RKSolveExp4(reassemble,"loop");
    }
  else  NotImplemented("RungeKutta type not implemented yet",__LINE__,__FILE__);
}



void dolfin::dolfwave::Dolfwave::RKSolveEuler(std::string reassemble,
                                              std::string swtchloop)
{
  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  if ((FormName=="LPT2z0dg")*(dbcapply)) NotImplemented("Dirichlet in LPT2z0dg",__LINE__,__FILE__);

  unsigned int startloop=0;
  unsigned int endloop=0;
  if (swtchloop=="init"){startloop=0; endloop=3;}
  else if (swtchloop=="loop"){startloop=3; endloop=4;}

  for (unsigned int i=startloop;i<endloop;i++)
    {
      rk->SolveEuler(pcrk_b,L,*M,eta_phi,TimeStep,
                       *dbc,dbcapply,
                       *Mf,AlgBackEnd,*petsclu/*,*epetralu*/,i);
      TimeUpdate();
      StepUpdate();
      info("#dw# Step=%d \t Simulated time= %.4f",i+1,Time);

      if ((reassemble=="MatricesReAssemble")*
          (bool(parameters("dolfwave_parameters")["lu_reuse_factorization"])==false))
        MatricesAssemble();
    }
  if(swtchloop=="loop")
    {
      Update(eta_phi, "RK");
    }

}

void dolfin::dolfwave::Dolfwave::RKSolveExp4(std::string reassemble,
                                             std::string swtchloop)
{


  //Initializing Auxiliar Vectors K
  if (swtchloop=="init")
    rk->InitAuxVector(eta_phi);

  std::string FormName=parameters("dolfwave_parameters")["form_name"].value_str();

  if ((FormName=="LPT2z0dg")*(dbcapply)) NotImplemented("Dirichlet in LPT2z0dg",__LINE__,__FILE__);

  unsigned int startloop=0;
  unsigned int endloop=0;

  if (swtchloop=="init"){startloop=0; endloop=3;}
  else if (swtchloop=="loop"){startloop=3; endloop=4;}

  for (unsigned int i=startloop;i<endloop;i++)
    {
      rk->SolveExp4(pcrk_b,L,*M,eta_phi,
                    TimeStep,*dbc,dbcapply,*Mf,
                    AlgBackEnd,*petsclu/*,*epetralu*/,
                    i,1);

      TimeAdd(0.5*TimeStep);

      rk->SolveExp4(pcrk_b,L,*M,eta_phi,
                    TimeStep,*dbc,dbcapply,*Mf,
                    AlgBackEnd,*petsclu/*,*epetralu*/,
                    i,2);

      rk->SolveExp4(pcrk_b,L,*M,eta_phi,
                    TimeStep,*dbc,dbcapply,*Mf,
                    AlgBackEnd,*petsclu/*,*epetralu*/,
                    i,3);

      TimeAdd(0.5*TimeStep);

      rk->SolveExp4(pcrk_b,L,*M,eta_phi,
                      TimeStep,*dbc,dbcapply,*Mf,
                      AlgBackEnd,*petsclu/*,*epetralu*/,
                      i,4);

      StepUpdate();
      info("#dw# Step=%d \t Simulated time= %.4f",i+1,Time);

      if ((reassemble=="MatricesReAssemble")*
          (bool(parameters("dolfwave_parameters")["lu_reuse_factorization"])==false))
        MatricesAssemble();
    }

  if(swtchloop=="loop")
    {
      Update(eta_phi,"RK");
    }
}




// }}}-------------------------------------------------

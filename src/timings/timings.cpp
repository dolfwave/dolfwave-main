// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  DolfwaveClassTime.cpp
//
// First added:  2009-04-14
// Last changed: 2016-06-28
//
// Compiled with DOLFIN:1.6.0
// }}}------------------------------------------------------------------------

#include "../DwClass.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


// {{{ Timer Functions
void dolfin::dolfwave::Dolfwave::TimerStart()
{
  maintimer.reset(new Timer("X:DOLFWAVE:END"));
  maintimer->start();
}

void dolfin::dolfwave::Dolfwave::TimerStop()
{
  maintimer->stop();
  // output timings to std::cout (via dolfin)
  list_timings(TimingClear::keep, {TimingType::wall});
  // output timings to logfile (dolfwave_logfile.txt) (via dolfin)
  set_output_stream(*logfile);
  list_timings(TimingClear::keep, {TimingType::wall});
}
// }}}----------------------------


// {{{ Time Update ----------------------------------
void dolfin::dolfwave::Dolfwave::TimeUpdate( )
{
  Time+=TimeStep;
}

void dolfin::dolfwave::Dolfwave::TimeAdd(double tm)
{
  Time+=tm;
}
// }}}-----------------------------------------------


// {{{ Step Update
void dolfin::dolfwave::Dolfwave::StepUpdate( )
{
  Step++;
}

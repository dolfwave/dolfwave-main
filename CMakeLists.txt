# Copyright (C) 2010 Nuno Lopes
# Licensed under the GNU LGPL Version 3.
#
# First added:  2009-07-10
# Last changed: 2016-02-03
#
# This CMakeLists.txt file builds and installs the DOLFWAVE library using cmake

# Require CMake 2.8
cmake_minimum_required (VERSION 2.8)
#------------------------------------------------------------------------------
# The project name and version
project (DOLFWAVE)
set (DOLFWAVE_VERSION_MAJOR 0)
set (DOLFWAVE_VERSION_MINOR 3)
set (DOLFWAVE_VERSION_YEAR 17)
set (DOLFWAVE_VERSION_MONTH 2)
set (DOLFWAVE_VERSION "${DOLFWAVE_VERSION_MAJOR}.${DOLFWAVE_VERSION_MINOR}.${DOLFWAVE_VERSION_YEAR}.${DOLFWAVE_VERSION_MONTH}")
add_definitions(-DPACKAGE_VERSION="${DOLFWAVE_VERSION}")
#------------------------------------------------------------------------------

message("Project ${PROJECT_NAME} ${DOLFWAVE_VERSION}")
# General configuration
# configure a header file to pass some of the CMake settings
# to the source code
configure_file (
  "${PROJECT_SOURCE_DIR}/DwConfig.h.in"
  "${PROJECT_BINARY_DIR}/DwConfig.h"
  )

# Set CMake options, see `cmake --help-policy CMP000x`
if (COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
  cmake_policy(SET CMP0004 OLD)
endif()

# Set location of our FindFoo.cmake modules
set (DOLFWAVE_CMAKE_DIR "${PROJECT_SOURCE_DIR}/cmake" CACHE INTERNAL "")
set (CMAKE_MODULE_PATH "${PROJECT_CMAKE_DIR}/modules" CACHE INTERNAL "")


# Since build.sh and buildWithFFC.sh are available, the following lines are commented.
# If the FFC ufl forms need to be recompiled then one should set the FFC_COMPILE_FORMS variable to ON
# set (FFC_COMPILE_FORMS OFF CACHE)
# set (FFC_COMPILE_FORMS ON)

if (FFC_COMPILE_FORMS)
  # Find all the input files to be compiled by ffc
  FILE(GLOB inFiles RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/src/1hdforms/"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/1hdforms/*.ufl")

  FOREACH(infileName ${inFiles})
    MESSAGE(STATUS "Compile with ffc the file: ${infileName}")
    # Compile with FFC all the UFL forms
    execute_process(COMMAND ffc -l dolfin -O -fsplit ${infileName} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/src/1hdforms)
  ENDFOREACH(infileName)


  # Find all the input files to be compiled by ffc
  FILE(GLOB inFiles RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/src/2hdforms/"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/2hdforms/*.ufl")

  FOREACH(infileName ${inFiles})
    MESSAGE(STATUS "Compile with ffc the file: ${infileName}")
    # Compile with FFC all the UFL forms
    execute_process(COMMAND ffc -l dolfin -O -fsplit ${infileName} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/src/2hdforms)
  ENDFOREACH(infileName)

  # Find all the input files to be compiled by ffc
  FILE(GLOB inFiles RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/src/1hd1sforms/"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/1hd1sforms/*.ufl")

  FOREACH(infileName ${inFiles})
    MESSAGE(STATUS "Compile with ffc the file: ${infileName}")
    # Compile with FFC all the UFL forms
    execute_process(COMMAND ffc -l dolfin -O  ${infileName} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/src/1hd1sforms)
  ENDFOREACH(infileName)
else()
  message("WARNING: FORM FILES WERE NOT RECOMPILED: EDIT CMakeLists.txt and  set (FFC_COMPILE_FORMS ON)")
  message("WARNING: run buildWithFFC.sh to compile rebuild the ufl forms  and recompile dolfwave")
  message("WARNING: run build.sh to recompile dolfwave without rebuilding the ufl forms")

endif()

# Find DOLFIN config file
find_package(DOLFIN)

if (DOLFIN_FOUND)
  message("Located dolfin-config.cmake file...")
else()
  message("Could not locate dolfin-config.cmake file.  Get dolfin working first!")
endif()

# Need to get VTK config because VTK uses advanced VTK features which
# mean it's not enough to just link to the DOLFIN target. See
# http://www.vtk.org/pipermail/vtk-developers/2013-October/014402.html
find_package(VTK HINTS ${VTK_DIR} $ENV{VTK_DIR} NO_MODULE QUIET)


# Compiler definitions
set(DOLFWAVE_CXX_DEFINITIONS "${DOLFWAVE_CXX_DEFINITIONS} ${DOLFIN_CXX_DEFINITIONS}")
message("DOLFWAVE_CXX_DEFINITIONS=${DOLFWAVE_CXX_DEFINITIONS}")

# Add special DOLFIN compiler flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${DOLFIN_CXX_FLAGS}")
message("CMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}")

#--------------------------------------------------------------------------------
# Include dolfin directories
include_directories(${DOLFIN_INCLUDE_DIRS} ${DOLFIN_3RD_PARTY_INCLUDE_DIRS})

list(APPEND DOLFWAVE_INCLUDE_DIRECTORIES ${DOLFIN_INCLUDE_DIRS})
message("DOLFWAVE_INCLUDE_DIRECTORIES=${DOLFWAVE_INCLUDE_DIRECTORIES}")

#------------------------------------------------------------------------------
# Configurable options for how we want to build
option(BUILD_SHARED_LIBS "Build DOLFWAVE with shared libraries." ON)
option(CMAKE_USE_RELATIVE_PATHS "Use relative paths in makefiles and projects." OFF)
option(DOLFWAVE_WITH_LIBRARY_VERSION "Build with library version information." ON)
# Default build type (can be overridden by user)
if (NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "RelWithDebInfo" CACHE STRING
    "Choose the type of build, options are: Debug MinSizeRel Release RelWithDebInfo." FORCE)
endif()

#------------------------------------------------------------------------------
# Installation of DOLFWAVE library
# Append the library version information to the library target properties
if (DOLFWAVE_WITH_LIBRARY_VERSION)
  set(DOLFWAVE_LIBRARY_PROPERTIES ${DOLFWAVE_LIBRARY_PROPERTIES}
    VERSION ${DOLFWAVE_VERSION}
    SOVERSION ${DOLFWAVE_VERSION_MAJOR}
    YEARVERSION ${DOLFWAVE_VERSION_YEAR}
    MONTHVERSION ${DOLFWAVE_VERSION_MONTH}
  )
endif()


# Set DOLFWAVE install sub-directories
set(DOLFWAVE_BIN_DIR "${CMAKE_SOURCE_DIR}/bin")
set(DOLFWAVE_LIB_DIR "${CMAKE_SOURCE_DIR}/lib")
set(DOLFWAVE_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/include")
set(DOLFWAVE_PKGCONFIG_DIR "${CMAKE_SOURCE_DIR}/lib/pkgconfig")
set(DOLFWAVE_SHARE_DIR "${CMAKE_SOURCE_DIR}/share")
set(DOLFWAVE_MAN_DIR "${CMAKE_SOURCE_DIR}/share/doc")


install(DIRECTORY src/ DESTINATION ${DOLFWAVE_INCLUDE_DIR}
  FILES_MATCHING
  PATTERN "dolfwave.h"
  PATTERN "*forms*" EXCLUDE
  PATTERN "*timings*" EXCLUDE
  PATTERN "*outputs*" EXCLUDE
  PATTERN "*inputs*" EXCLUDE
  PATTERN "*functions*" EXCLUDE
  PATTERN "*factory*" EXCLUDE
  PATTERN "*dbcs*" EXCLUDE
  PATTERN "*init*" EXCLUDE
  PATTERN "*predictor*" EXCLUDE
  PATTERN "*runge*" EXCLUDE
  PATTERN "*debug*" EXCLUDE
  PATTERN "*la*" EXCLUDE
  PATTERN "*mesh*" EXCLUDE
  PATTERN "*spaces*" EXCLUDE
  PATTERN "*matrix*" EXCLUDE
  PATTERN "CMakeFiles" EXCLUDE
  )

install(DIRECTORY src/ DESTINATION ${DOLFWAVE_LIB_DIR}
  FILES_MATCHING
   PATTERN "libdolfwave.so"
  PATTERN "*forms*" EXCLUDE
  PATTERN "*timings*" EXCLUDE
  PATTERN "*outputs*" EXCLUDE
  PATTERN "*inputs*" EXCLUDE
  PATTERN "*factory*" EXCLUDE
  PATTERN "*functions*" EXCLUDE
  PATTERN "*dbcs*" EXCLUDE
  PATTERN "*init*" EXCLUDE
  PATTERN "*predictor*" EXCLUDE
  PATTERN "*runge*" EXCLUDE
  PATTERN "*debug*" EXCLUDE
  PATTERN "*la*" EXCLUDE
  PATTERN "*mesh*" EXCLUDE
  PATTERN "*spaces*" EXCLUDE
  PATTERN "*matrix*" EXCLUDE
  PATTERN "CMakeFiles" EXCLUDE
  PERMISSIONS WORLD_EXECUTE
 )

install(DIRECTORY doc/ DESTINATION ${DOLFWAVE_MAN_DIR}
  PATTERN "*.org" EXCLUDE)

install(DIRECTORY css DESTINATION ${DOLFWAVE_SHARE_DIR})

#------------------------------------------------------------------------------
#Add source directory
add_subdirectory(src)

# Print post-install message
 add_subdirectory(cmake/post-install)
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Get command for building (make, nmake, ...)
build_command(MAKE_COMMAND make)
# Strip off any options in MAKE_COMMAND
string(REGEX REPLACE "([^ ]) .*" "\\1" MAKE_COMMAND "${MAKE_COMMAND}")

#------------------------------------------------------------------------------
# Add demos and install demo source files and mesh files

# # Add demo but do not add to default target
add_subdirectory(demo EXCLUDE_FROM_ALL)

# # Add target "demo" for building the demos
 add_custom_target(demo
   COMMAND ${MAKE_COMMAND}
   WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/demo")

# # Install the demo source files
install(DIRECTORY demo DESTINATION ${DOLFWAVE_SHARE_DIR}
  FILES_MATCHING
  PATTERN "CMakeLists.txt"
  PATTERN "*.cpp"
  PATTERN "*.ufl"
  PATTERN "*.h"
  PATTERN "*.py"
  PATTERN "*.xml*"
  PATTERN "*.gif"
  PATTERN "*.png"
  PATTERN "CMakeFiles" EXCLUDE)

#-----------------------------------------------------------------------------
# # Add tests but do not add to default target
add_subdirectory(tests EXCLUDE_FROM_ALL)

# # Add target "tests" for building the demos
 add_custom_target(tests
   COMMAND ${MAKE_COMMAND}
   WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/tests")

# # Install the demo source files
install(DIRECTORY tests DESTINATION ${DOLFWAVE_SHARE_DIR}
  FILES_MATCHING
  PATTERN "CMakeLists.txt"
  PATTERN "*.cpp"
  PATTERN "*.ufl"
  PATTERN "*.h"
  PATTERN "*.py"
  PATTERN "*.xml*"
  PATTERN "CMakeFiles" EXCLUDE)


# Install meshes (meshes directory)
install(DIRECTORY meshes DESTINATION ${DOLFWAVE_SHARE_DIR})
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Add "make uninstall" target
configure_file(
  "${DOLFWAVE_CMAKE_DIR}/templates/cmake_uninstall.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
  IMMEDIATE @ONLY)

add_custom_target(uninstall
  "${CMAKE_COMMAND}" -P "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake")
#------------------------------------------------------------------------------

# Generate a snapshot with bzr
add_custom_target(snapshot
  bzr export --format=tgz ${PROJECT_NAME}${DOLFWAVE_VERSION}.tgz )
#------------------------------------------------------------------------------

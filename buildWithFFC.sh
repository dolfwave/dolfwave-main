#!/bin/bash
# $1 number of processors
ARG1=${1:-1}

rm -rf CMakeFiles
rm -f CMakeCache.txt


cmake -DFFC_COMPILE_FORMS:BOOL=ON CMakeLists.txt

echo "#dw#  Using  $ARG1  processors to compile ########"

make -j $ARG1

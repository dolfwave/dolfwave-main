#+TITLE:     [DOLFWAVE 0.3.12.24]
#+AUTHOR:    Nuno David Lopes
#+LANGUAGE:  en
#### +TEXT:      First Steps towards a Boussinesq FEniCS  Application.
#### +TEXT:      Updates, Changes and TODOS.
#### +TEXT:      Warning: there is no such thing as a STABLE version.
#+OPTIONS:   H:1 num:t toc:t \n:nil @:t ::t |:t ^:t *:t TeX:t LaTeX:t
#+STARTUP:   showall
#+SEQ_TODO: TODO  INPROGRESS CHECKING  | NOTUSED REMOVED
#+SEQ_TODO: | DONE
#+STYLE:    <link rel="stylesheet" type="text/css" href="../css/mystyle.css" />


* CODE DOWNLOAD
Go to  [[https://launchpad.net/dolfwave][DOLFWAVE at Launchpad]] (bugs,questions,etc...)

*  DEMOS
Most of the demos are included and published in the papers and talks listed on [[http://ptmat.fc.ul.pt/~ndl/][Homepage]].
** [[file:demo2hd.org][2HD examples]]
In these demos, we use DOLFWAVE to solve several Boussinesq-type systems of
equations(for surface elevation and velocity potential).
We use the standard FEM  to solve the 2nd-order Zhao et al. system (see, e.g., Tsunami and
harbor demos). On the other hand, we use  C/DG-FEM formulations for the 4th-order Boussinesq systems
(see, e.g., hlandslide and vlandslide demos).
#+attr_html: width="50%"
[[file:../demo/2HD/hLandslide/LPTcdgHnl/demo.gif]]
-----------------------------------------------------------------------------------
** [[file:demo1hd.org][1HD demos]]
In these demos, we use DOLFWAVE to solve the 1HD versions of the previous models.
#+attr_html: width="60%"
[[file:../demo/1HD/hLandslide/demo.png]]
------------------------------------------------------------------------------------
** [[file:demo1hd1s.org][1HD1S demos]]
In these demos, we use DOLFWAVE to solve several KdV--BBM-type equations (for
surface elevation).
We use the standard FEM  to solve the BBM equation (see solitonsBBM demo). On
the other hand, we use several C/DG-FEM formulations for KdV and KdV--BBM equations
(see solitonsKdVBBM, solitaryKdVcdg2, solitaryKdVcdgdim, solitaryKdVcdgP1P2 demos).
#+attr_html: width=60%
[[file:../demo/1HD1S/solitonsKdVBBMcdg/demo.png]]
------------------------------------------------------------------------------------
** Stab1HD
In these demos, we investigate some linear stability properties of the
models referred in the 2HD, 1HD and 1HD1S demos.
#+attr_html: width=60%
[[file:../demo/Stab1HD/StdPot/L_2_dx_0.02_hm_shelf_spectrum_StdPot.png]]
-----------------------------------------------------------------------------------
** [[file:../Changelog.html][Changelog]]
@<h4 align=right>
-------------------
[[http://ptmat.fc.ul.pt/~ndl/][Back to Homepage]]
--------------------
@</h4>

# README #


## Description: ##
  DOLFWAVE is a FEniCS based library for solving surface water waves problems.

- The main goal of DOLFWAVE is to provide a framework for the analysis, development and computation of models for surface water waves, based on finite element methods.

- It is based on several  Boussinesq type equations
 using the Surface Elevation/Velocity Potential Formulations.

## Version and Dependencies: ##
- DOLFWAVE 0.3 (Current) based on FEniCS 2016.1 (dolfin 1.6.0)


## Author: N. Lopes ##
- ADM-ISEL: https://www.isel.pt/organizacao/areas-departamentais/adm
- CMA-FCT: http://www.cma.fct.unl.pt/en

##  Feedback ##
Feedback, patches, and comments should be sent to to the author:

- Email: nlopes(_at_)adm.isel.pt




### How do I get set up? ###

* Summary of set up

1.  Install FEniCS 2016.1
2.  Clone the Dolfwave Repo at bitbucket
3.  Set the env variables as described in the Wiki
4. source the config file with:
```
$ source fenics_dolfwave.source
```
5.  Compile DOLFWAVE
```
$ buildWithFFC.sh
```
6. Run a demo to test, e.g., go to /demo/1HD/squarecdg/
```
$ cmake CMakeLists.txt
$ make
$ 1HD_squarecdg_demo
```

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
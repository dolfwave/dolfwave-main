#!/bin/sh

1HD_solitonsKdVBBM_demo 2 0.002   outKB_0dot002    >outKB_0dot002/logfile.txt   2>outKB_0dot002/logfile.txt &
1HD_solitonsKdVBBM_demo 2 0.003   outKB_0dot003    >outKB_0dot003/logfile.txt   2>outKB_0dot003/logfile.txt &
1HD_solitonsKdVBBM_demo 2 0.005   outKB_0dot005    >outKB_0dot005/logfile.txt   2>outKB_0dot005/logfile.txt &
1HD_solitonsKdVBBM_demo 2 0.008  outKB_0dot008   >outKB_0dot008/logfile.txt  2>outKB_0dot008/logfile.txt &
1HD_solitonsKdVBBM_demo 2 10.0   outKB_10dot0     >outKB_10dot0/logfile.txt    2>outKB_10dot0/logfile.txt &
#1HD_solitonsKdV_demo 1 1.    K_t1      >K_t1/logfile.txt     2>K_t1/logfile.txt &
#1HD_solitonsKdV_demo 1 0.1   K_t01     >K_t01/logfile.txt    2>K_t01/logfile.txt &
#1HD_solitonsKdV_demo 1 0.01  K_t001    >K_t001/logfile.txt   2>K_t001/logfile.txt &

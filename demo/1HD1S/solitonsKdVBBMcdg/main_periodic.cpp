// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This program provides a demo of the DOLFWAVE library.
// Solitary wave evolution by KdV-BBM with CDG method
// (See the BBM solitons demo)

#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;

/* Global Variables*/
const double A=-20; //
const double B=100; // $x\in[A.B]$
const double EPSILON=1.0;
const double MU=1.0;


// Sub domain for Periodic boundary condition
class PeriodicBoundary : public SubDomain
{
  // Left boundary is "target domain" G
  bool inside(const Array<double>& x, bool on_boundary) const
  { return (std::abs(x[0]-A) < DOLFIN_EPS) && on_boundary; }
  // Map right boundary (H) to left boundary (G)
    void map(const Array<double>& x,  Array<double>& y) const
  {
    y[0] = x[0]-(B-A);
  }
};


class ElevationInitKdVBBM : public Expression
{
public:
  ElevationInitKdVBBM(const double delta0,const double epsilon,
                      const double delta1,const double delta2):
    delta0(delta0), epsilon(epsilon), delta1(delta1), delta2(delta2) {}
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double c1=2.;
    double c2=5./4.;
    // Two solitons solutions for the KdV-type equations in use.
    values[0]=2.*(c1-delta0)/epsilon*1./
      sqr(cosh(1./2.*sqrt((c1-delta0)/(delta2-c1*delta1))*(x[0]+10.)))+
      2.*(c2-delta0)/epsilon*1./
      sqr(cosh(1./2.*sqrt((c2-delta0)/(delta2-c2*delta1))*(x[0]-10.)));
  }
private:
  const double delta0;
  const double epsilon;
  const double delta1;
  const double delta2;
};

int main(int argc, char* argv[])
{
  bool swtch=true;
  if ((argc!=4)||(atoi(argv[1])>3))
    {
      info("Wrong number of arguments");
      info("argv[1]='2' for KdV-BBM or argv[1]='1' for KdV");
      info("argv[2]='penalty tau'");
      info("argv[3]='output_dir'");
      exit(EXIT_FAILURE);
    }
  std::stringstream output_dir;
  output_dir<<argv[3];


  // Options, Initialization, Mesh and User functions
  Dolfwave dw(
              480000 /*MaxSteps*/,
              0.0001 /*TimeStep*/,
              2400 /*WriteGap*/,
              "xKdVcdg_1D" /*FormName*/,
              "LU_P" /* uBLAS AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              output_dir.str() /*OutDir*/,
              "xyz"/*OutFormat*/
              );

  double kdvpa=0.0;
  double kdvpb=0.0;
  if (atoi(argv[1])==1)
    {
      dw.SetParameter("KdVPade",KDV_21_PADE);
      kdvpa=parameters("dolfwave_parameters")["KdVPade"];
    }
  else if (atoi(argv[1])==2)
    {
      dw.SetParameter("KdVPade",KDV_22_PADE);
      kdvpa=parameters("dolfwave_parameters")["KdVPade"];
    }


  double delta1=(kdvpa-1.)*MU*MU/6.; //Sec.-ord. disp. coeff.
  double delta2=kdvpa*MU*MU/6.; //Third-ord. disp. coeff.

  dw.SetParameter("xKdVDelta0",1.0);
  dw.SetParameter("xKdVEpsilon",EPSILON);
  dw.SetParameter("xKdVDelta1",delta1);
  dw.SetParameter("xKdVDelta2",delta2);
  dw.SetParameter("PenaltyTau",atof(argv[2]));

  info(parameters,true);// Some info on the used parameters
  IntervalMesh mesh(1201,A,B);
  Constant urs(1.0);
  ElevationInitKdVBBM eta_init(parameters("dolfwave_parameters")["xKdVDelta0"],
                            parameters("dolfwave_parameters")["xKdVEpsilon"],
                            parameters("dolfwave_parameters")["xKdVDelta1"],
                            parameters("dolfwave_parameters")["xKdVDelta2"]);//initial condition for elevation

  //Auxiliar function for initializations
  Constant zero(0.0);
  // Create periodic boundary condition
  PeriodicBoundary periodic_boundary;
  dw.SpacesFunctionsVectorsInit(mesh, periodic_boundary); //Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,zero);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LinearFormsInit( );
  dw.InitialCondition(eta_init); //Initial Conditions L2 projection
  dw.FirstInvariant(mesh); // Volume
  dw.SecondInvariant(mesh); // Momentum
  dw.ThirdInvariant(mesh,urs); //  Energy
  dw.LUFactorization(true); //Re-use factorization
  // -----------------------------------------------------------------
  dw.PreviewEta(0);
  dw.SaveEta(0);
  dw.RKInit("exp4"); // Explicit 4th-order Runge-Kutta
  dw.RKSolve();//Runge-Kutta for 3 initial steps
  dw.PCInit(mesh,true);

  while(dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      if (!(dw.Step%dw.WriteGap))
        { // Plots
          dw.PreviewEta();
          dw.SaveEta();
          dw.FirstInvariant(mesh); // Volume
          dw.SecondInvariant(mesh); // Momentum
          dw.ThirdInvariant(mesh,urs); //  Energy
          dw.Info(true);// Output some extra information on memory usage
        }
    }
  dw.PreviewEta();
  return (EXIT_SUCCESS);
}

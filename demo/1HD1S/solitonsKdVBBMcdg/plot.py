import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 1,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile

dir='output'

plottitle='Solitons interaction'


ax=subplot(111,autoscale_on='false')


step=1 #STEP IN NUMBER OF POINTS PER FILE SOLUTION

LMAX=120 #NUMBER OF FILES TO DRAW
LSTEP=4  #STEP BETWEEN FILES

for j in range (0,LMAX):
    filenumber=str(j*LSTEP).zfill(6)
    filename='outKB_0dot001/eta'+str(filenumber)+'.xyz'
    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=j*0.2+u1[i*step,1]
    ax.plot(us[:,0], us[:,1],"b", label='')


ax.legend(loc=1)


(xname, yname) = ('$x$', 't $\\in [0,48]$ ')
#title(plottitle,fontsize=18)
#grid()

#text1=text(0.0,25.0,'KdV-BBM equations')

ax.set_aspect(4.)
ax.set_xlim(-20.0,100.0)
ax.set_ylim(0.,26.)
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)


show()


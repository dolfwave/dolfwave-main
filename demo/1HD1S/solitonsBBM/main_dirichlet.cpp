//  Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This program provides a demo of the DOLFWAVE library.
// Regular Long Wave Equation by Benjamin Bona et al.
#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;
/* Global Variables*/
const double A=-20; //
const double B=100; // $x\in[A.B]$
const double H=1.0;
const double g=1.0;

class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
    {
      return on_boundary;
    }
  };

class ElevationInitRlw : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double AMP=1.;
    double c1=1.+AMP;
    double c2=1.+AMP/4.;
    values[0]=2.*(c1-1.)/sqr(cosh(1./(2.*H)*
                                  sqrt(6.*(c1-1.)/c1)*(x[0]+10)))+
      2.*(c2-1.)/sqr(cosh(1./(2.*H)*
                                   sqrt(6.*(c2-1.)/c2)*(x[0]-10)));
  }
};

int main( )
{
  // Options, Initialization, Mesh and User functions
  Dolfwave dw(
              480000 /*MaxSteps*/,
              0.0001 /*TimeStep*/,
              2400 /*WriteGap*/,
              "BBM_1D" /*FormName*/,
              "LU_P" /* PETSc LU AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              "output_dirichlet_bcs" /*OutDir*/,
              "xyz"/*OutFormat*/
              );
  dw.SetParameter("Gravity",g);
  IntervalMesh mesh(1201,A,B);
  Constant depth(H); //Depth
  ElevationInitRlw eta_initRlw;//initial condition for elevation
  DirichletBoundary dirichletboundary;
  Constant zero(0.0);
  // {{{ Functions, Vectors and Matrices ----------------------
  dw.SpacesFunctionsVectorsInit(mesh); // Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LinearFormsInit(depth);
  dw.InitialCondition(eta_initRlw); //Initial Conditions L2 projection
  dw.DolfwaveDBC(zero,dirichletboundary); //Setting Dirichlet Boundary Conditions
  dw.LUFactorization(true); //Re-use factorization
  // ----------------------------------------------------------------
  dw.PreviewEta();
  dw.SaveEta();
  dw.RKInit("exp4" /*RKtype*/);
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
  //dw.PCInit(mesh,true);

  info(parameters,true);//Some info on the parameters

  while(dw.Step<dw.MaxSteps+1)
    {
      //  dw.PCSolve();
      dw.RKSolveLoop( );
      if (!(dw.Step%dw.WriteGap))
        {
          dw.PreviewEta();
          dw.SaveEta();
        }
      dw.Info();
    }
  return (EXIT_SUCCESS);
}

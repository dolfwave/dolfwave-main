// Copyright (C) 2012 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// First added:  2013-01-11
// Last changed: 2015-05-13
// Compiled with DOLFWAVE:0.3.13.0
// This program provides a demo of the DOLFWAVE library.
// Solitary wave evolution generic
// xKdV-BBM with CDG method


#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

/* Global Variables*/
const double A=-1.; //
const double B=1.;  // $x\in[A.B]$




class DBC: public Expression
{
 public:
  DBC (const double& t): t(t)  {}
  void eval(Array<double> & values, const Array<double> & x) const
  {
    // Definition of periodic left BC
    double time=t;
    if (x[0] < A+DOLFIN_EPS)
      values[0]=sin(20.0*DW_PI*time)*tanh(5.0*time);
    else
    values[0]=0.0;
  }
private:
  const double& t;
};


class DtDxDBC: public Expression
{
 public:
  DtDxDBC (const double& t): t(t)  {}
  void eval(Array<double> & values, const Array<double> & x) const
  {
    // Definition of periodic left BC
    double time=t;
    if (x[0]<A+DOLFIN_EPS)
      values[0]=40.*(20.*DW_PI*tanh(5*time)*sin(A-x[0]+20*DW_PI*time)
                      +5.*1./(cosh(5.0*time)*cosh(5.0*time))*cos(A-x[0]+20*DW_PI*time));
    else
    values[0]=0.0;
  }
private:
  const double& t;
};



// --------------------------------------------------------------------

int main(int argc, char* argv[])
{

  bool swtch=true;
  if (argc!=3)
    {
      info("Wrong number of arguments");
      info("argv[1]='2' for KdV-BBM or argv[1]='1' for KdV");
      info("argv[2]=output_dir");
      exit(EXIT_FAILURE);
    }
  std::stringstream output_dir;
  output_dir<<argv[2];
  //  Options, Initialization, Mesh and User functions
  Dolfwave dw(180000 /*MaxSteps*/,
              0.00001 /*TimeStep*/,
              1000 /*WriteGap*/,
              "xKdVcdg_1D" /*FormName*/,
              "LU_P" /*AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              output_dir.str() /*OutDir*/,
              "xyz"/*OutFormat*/);

  double kdvpa=0.0;
   //  double INV1,INV2,INV3;

  if (atoi(argv[1])==1)
    {
    dw.SetParameter("KdVPade",KDV_21_PADE);
    kdvpa=parameters("dolfwave_parameters")["KdVPade"];
    }
  else if (atoi(argv[1])==2)
    {
    dw.SetParameter("KdVPade",KDV_22_PADE);
    kdvpa=parameters("dolfwave_parameters")["KdVPade"];
    }



  double delta2=0.00001; //Third-ord. disp. coeff.
  double delta1=(1.-1./kdvpa)*delta2; //Sec.-ord. disp. coeff.

  dw.SetParameter("xKdVDelta0",1.0);
  dw.SetParameter("xKdVEpsilon",0.0);
  dw.SetParameter("xKdVDelta1",delta1);
  dw.SetParameter("xKdVDelta2",delta2);
  dw.SetParameter("PenaltyTau",0.0001);

  info(parameters,true);// Some info on the used parameters
  IntervalMesh mesh(106,A,B);
  Constant IC(0.0);

  //DBoundary dboundary;

  //initial condition for elevation
  dw.SpacesFunctionsVectorsInit(mesh); // Init. Spaces Functions & Vectors

  Constant pf(8.0);
  DBC u0(dw.Time);
  DtDxDBC u0_dt_dx(dw.Time);

  dw.BilinearFormInit(mesh);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  // For the initial condition tau must be non zero
  dw.LinearFormsInit(pf,u0,u0_dt_dx); // Passing u0_dt_dx as initial condition


  dw.InitialCondition(IC); //Initial Conditions L2 projection


  dw.LUFactorization(true); //Re-use factorization
  //  dw.PreviewEta(0);//Preview and Save eta initial step
  //  dw.SaveEta(0);
  dw.RKInit("exp4"); // Explicit 4th-order Runge-Kutta
  dw.RKSolve();//Runge-Kutta for 3 initial steps
  dw.PCInit(mesh,false);


  while(dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      dw.Info(false);
      dw.PointPlot(-0.95067,"eta1HD1S","x0.xy");
      dw.PointPlot(-0.80846,"eta1HD1S","x1.xy");
      dw.PointPlot(-0.58728,"eta1HD1S","x2.xy");
      dw.PointPlot(-0.30872,"eta1HD1S","x3.xy");
      dw.PointPlot(0,"eta1HD1S","x4.xy");
      dw.PointPlot(0.99965,"eta1HD1S","x5.xy");
      if (!(dw.Step%dw.WriteGap))
        {
          //dw.PreviewEta();
          //dw.SaveEta();
          }
    }

  return (EXIT_SUCCESS);
}

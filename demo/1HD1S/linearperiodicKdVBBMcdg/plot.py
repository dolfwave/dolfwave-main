import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 18,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Solitary wave'


ax=subplot(111,autoscale_on='false')


step=1

filename='output_kdv/x2.xy'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],"b--", label='KdV')

filename='output_kdvbbm/x2.xy'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],"k-",linewidth=2, label='KdV-BBM')


(xname, yname) = ('Time', 'Amplitude')
#text1=text(0.0,6.0,'KdV-BBM equations with  inner penalty CDG method')
#title(plottitle,fontsize=18)
#grid()


ax.set_aspect(0.8)
ax.set_xlim(0,1.8)
ax.set_ylim(-1.2,1.2)
ax.legend(loc='best')
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)

show()

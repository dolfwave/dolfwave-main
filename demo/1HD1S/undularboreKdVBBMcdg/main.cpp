// Copyright (C) 2012 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// First added:  2013-01-11
// Last changed - nlopes: 2017-02-15
// Compiled with DOLFWAVE:0.3.16.26
// This program provides a demo of the DOLFWAVE library.
// Solitary wave evolution generic
// xKdV-BBM with CDG method


#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

/* Global Variables*/
const double A=-20.; //
const double B=140.;  // $x\in[A.B]$
const double EPSILON=1.; // 1.
const double MU=1./6.;
const double D=1.;
const double U0=0.1;



class UndularBore: public Expression
{
public:
  void eval(Array<double> & values,const Array<double> & x) const
  {
    /* Definition of the ondular bore  */

    values[0]=1./2.*U0*(1.-tanh(x[0]/D));
  }
};
// --------------------------------------------------------------------

int main(int argc, char* argv[])
{
  bool swtch=true;
  if (argc!=2)
    {
      info("Wrong number of arguments");
      info("argv[1]='2' for KdV-BBM or argv[1]='1' for KdV");
      exit(EXIT_FAILURE);
    }


  //  Options, Initialization, Mesh and User functions
  Dolfwave dw(500000 /*MaxSteps*/,
              0.00025 /*TimeStep*/,
              200 /*WriteGap*/,
              "xKdVcdg_1D" /*FormName*/,
              "LU_P" /*AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/);


  double kdvpa=0.0;
  //  double INV1,INV2,INV3;

  if (atoi(argv[1])==1)
    {
    dw.SetParameter("KdVPade",KDV_21_PADE);
    kdvpa=parameters("dolfwave_parameters")["KdVPade"];
    }
  else if (atoi(argv[1])==2)
    {
    dw.SetParameter("KdVPade",KDV_22_PADE);
    kdvpa=parameters("dolfwave_parameters")["KdVPade"];
    }


  double delta1=(kdvpa-1.)*MU*MU/6.; //Sec.-ord. disp. coeff.
  double delta2=kdvpa*MU*MU/6.; //Third-ord. disp. coeff.

  dw.SetParameter("xKdVDelta0",1.0);
  dw.SetParameter("xKdVEpsilon",EPSILON);
  dw.SetParameter("xKdVDelta1",delta1);
  dw.SetParameter("xKdVDelta2",delta2);
  dw.SetParameter("PenaltyTau",0.01);


  info(parameters,true);// Some info on the used parameters
  IntervalMesh mesh(1600,A,B);
  UndularBore undular_bore;


  //DBoundary dboundary;

  //initial condition for elevation
  dw.SpacesFunctionsVectorsInit(mesh); // Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  Constant zero(0.0);
  dw.LinearFormsInit(zero,zero,zero);

  //dw.DolfwaveDBC(dbc,dboundary);//Dirichlet B.C. definition
  dw.InitialCondition(undular_bore); //Initial Conditions L2 projection


  dw.LUFactorization(true); //Re-use factorization
  //dw.PreviewEta(0);//Preview and Save eta initial step
  dw.SaveEta(0);
  dw.RKInit("exp4"); // Explicit 4th-order Runge-Kutta
  dw.RKSolve();//Runge-Kutta for 3 initial steps
  dw.PCInit(mesh,true);

  //  dw.MSparsityPlot( );
  //  dw.MfSparsityPlot( );
  //  dw.GetMatrixM( );
  //  dw.GetMatrixMf( );


  while(dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      dw.Info(false);
      if (!(dw.Step%dw.WriteGap))
        {
          dw.SaveEta();
          //dw.PreviewEta();
        }
    }
  dw.PreviewEta();
  return (EXIT_SUCCESS);
}

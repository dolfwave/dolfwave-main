import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 1,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Solitary wave'


ax=subplot(111,autoscale_on='false')


step=1 #STEP IN NUMBER OF POINTS PER FILE SOLUTION


filename='outputK1N55tau0_0/eta000001.xyz'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],"k+-", label='Num. KdV $\\tau=0.1$')

filename='outputK1N55tau0_0/eta000001.xyz'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],"b*-", label='Num. KdV-BBM $\\tau=0$')


time=40.

alpha=1.0
x=arange(-20,200.0,0.1)
d0=1.0
mu=0.31623
epsilon=3./2.*0.1
d1=(alpha-1)*mu**2/6.0
d2=alpha*mu**2/6.0
c=d0+d0/80.
print str(sqrt((c-d0)/(d2-c*d1)))

ax.plot(x,3.*(c-d0)/epsilon*1./cosh(0.5*sqrt((c-d0)/(d2-c*d1))*(x-c*time))**2,'k--',label='KdV Exact')

alpha=-.9
x=arange(-20,200.0,0.1)
d0=1.0
epsilon=3./2.*0.1
d1=(alpha-1)*mu**2/6.0
d2=alpha*mu**2/6.0
c=d0+d0/80.
print str(sqrt((c-d0)/(d2-c*d1)))

ax.plot(x,3.*(c-d0)/epsilon*1./cosh(0.5*sqrt((c-d0)/(d2-c*d1))*(x-c*time))**2,'b',label='KdV-BBM Exact')
ax.legend(loc=(-.2,.7))


(xname, yname) = ('$x$', '$t=40$')
#text1=text(0.0,6.0,'KdV-BBM equations with  inner penalty CDG method')
#title(plottitle,fontsize=18)
#grid()


ax.set_aspect(60.)
ax.set_xlim(110,130.0)
ax.set_ylim(-0.01,.3)
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)

show()

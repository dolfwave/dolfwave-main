import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Solitary wave'


ax=subplot(111,autoscale_on='false')


step=1 #STEP IN NUMBER OF POINTS PER FILE SOLUTION


#filename='outputKdV0dot0/second_invariant.tv'
#u1 = fromfile(filename,sep=" ")
#u1.shape=len(u1)//2,2
#us=ones((len(u1)//(step),2))
#for i in range(0,len(u1)//(step)):
#    us[i,0]=u1[i*step,0]
#    us[i,1]=u1[i*step,1]
#ax.plot(us[:,0], us[:,1],"k+-", label='KdV $\\tau=0$')


#filename='outputKdV0dot01/second_invariant.tv'
#u1 = fromfile(filename,sep=" ")
#u1.shape=len(u1)//2,2
#us=ones((len(u1)//(step),2))
#for i in range(0,len(u1)//(step)):
#    us[i,0]=u1[i*step,0]
#    us[i,1]=u1[i*step,1]
#ax.plot(us[:,0], us[:,1],"b*-", label='KdV $\\tau=0.01$')


filename='outputKdV0dot1/second_invariant.tv'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],"r*-", label='KdV $\\tau=0.1$')

filename='outputKdV1dot0/second_invariant.tv'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],"k--", label='KdV $\\tau=1$')

time=120.

alpha=1.0
x=arange(-20,200.0,0.1)

ax.legend(loc='best')


(xname, yname) = ('$x$', '$I_2$')
#text1=text(0.0,6.0,'KdV-BBM equations with  inner penalty CDG method')
#title(plottitle,fontsize=18)
#grid()


ax.set_aspect(800)
ax.set_xlim(0,120.0)
#ax.set_ylim(-0.01,.3)
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)

show()


#!/usr/bin/python

import os
import numpy as np
#1HD1S_solitaryxKdVcdg_demo 40000 2 output 0.01 55

#tau_list=[0.0,0.0001,0.001,0.01,0.1]
#N_list=[55, 110, 220, 440, 880, 1760, 3520]
N_list=[1760]
#tau_list=[0.001, 0.005, 0.01, 0.015, 0.02, 0.025]
tau_list=[0.001]
gamma_list=np.linspace(0,5,6)

#[2.75001,2.75002,2.75003,2.75004,2.75005,2.75006,2.75007,2.75008,2.75009]

# the end point of the range is not included
for N in N_list:
    for tau in tau_list:
        for gamma in gamma_list:#range(0,41):
            #gamma=0.25*g
            cmm2='1HD1S_solitaryxKdVcdg_demo 20000 1 '+str(gamma)+' '+str(tau)+' '+str(N)+' outputall'+' >/dev/null 2>/dev/null '
            #cmm2='1HD1S_solitaryxKdVcdg_demo 20000 1 '+str(gamma)+' '+str(tau)+' '+str(N)+' outputK1N'+str(N)+\
            #  'gamma'+str(gamma).replace('.','_')+'tau'+str(tau).replace('.','_')+' >/dev/null 2>/dev/null '
            print(str(cmm2))
            os.system(str(cmm2))

// Copyright (C) 2012 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// First added:  2013-01-11
// Last changed: 2015-05-19
// Compiled with DOLFWAVE:0.3.15.0
// This program provides a demo of the DOLFWAVE library.
// Solitary wave evolution generic
// xKdV-BBM with CDG method


#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

/* Global Variables*/
const double A=-20.; //
const double B=200.;  // $x\in[A.B]$
const double EPSILON=0.1;
const double MU=0.31623;
const double URS=MU*MU/EPSILON;



class ExactSolKdVBBM: public Expression
{
public:
  ExactSolKdVBBM(const double t, const double delta0, const double epsilon,
                 const double delta1, const double delta2)  :
    t(t), delta0(delta0), epsilon(epsilon), delta1(delta1), delta2(delta2) {}
  void eval(Array<double> & values,const Array<double> & x) const
  {
    /*
      Definition of the solitary wave solution
      for the adimensional generic KdV-BBM equations
    */
    double time=t;
    double c=delta0+delta0/80.; //Free parameter for speed/wave height
    double b=3./2.*epsilon; //Nonlinear coeff.
    double Psi=3.*(c-delta0)/b;
    double Theta=1./2.*sqrt((c-delta0)/(delta2-c*delta1));
    values[0]=Psi/sqr(cosh(Theta*(x[0]-c*time)));

  };
private:
  const double t;
  const double delta0;
  const double epsilon;
  const double delta1;
  const double delta2;
};
// --------------------------------------------------------------------

int main(int argc, char* argv[])
{
  bool swtch=true;
  if (argc!=6)
    {
      info("Wrong number of arguments");
      info("argv[1]=num_time_steps");
      info("argv[2]='3' for xKdV-BBM (TODO) or argv[1]='2' for KdV-BBM or argv[1]='1' for KdV");
      info("argv[3]=output_dir");
      info("argv[4]=penalty_tau");
      info("argv[5]=num_interval_divisions");
      exit(EXIT_FAILURE);
    }
  std::stringstream output_dir;
  output_dir<<argv[3];
  //  Options, Initialization, Mesh and User functions
  Dolfwave dw(atoi(argv[1]) /*MaxSteps*/,
              0.001 /*TimeStep*/,
              1000 /*WriteGap*/,
              "xKdVcdg_1D" /*FormName*/,
              "LU_P" /* PETSc LU AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              output_dir.str() /*OutDir*/,
              "xyz"/*OutFormat*/);

  double kdvpa=0.0;
  double kdvpb=0.0;
  double INV1,INV2,INV3;

  if (atoi(argv[2])==1)
    {
      dw.SetParameter("KdVPade",KDV_21_PADE);
      kdvpa=parameters("dolfwave_parameters")["KdVPade"];
      // Invariants evaluated in A..B
      INV1=1.154708661;
      INV2=0.1924514493;
      INV3=0.02886771739;
    }
  else if (atoi(argv[2])==2)
    {
      dw.SetParameter("KdVPade",KDV_22_PADE);
      kdvpa=parameters("dolfwave_parameters")["KdVPade"];
      // Invariants evaluated in A..B
      INV1=1.168340355;
      INV2=0.1956268782;
      INV3=0.02943437959;
    }
  else if (atoi(argv[2])==3)
    {
      dw.SetParameter("xKdVPadeA",KDV_24_PADE_A);
      dw.SetParameter("xKdVPadeB",KDV_24_PADE_B);
      kdvpa=parameters("dolfwave_parameters")["xKdVPadeA"];
      kdvpb=parameters("dolfwave_parameters")["xKdVPadeB"];
      warning("Exact solutions not implemented yet");
    }


  double delta1=(kdvpa-1.)*MU*MU/6.; //Sec.-ord. disp. coeff.
  double delta2=kdvpa*MU*MU/6.; //Third-ord. disp. coeff.
  double delta3=-kdvpb*MU*MU*MU*MU*19./360. ;//Fourth-ord. disp. coeff.

  dw.SetParameter("xKdVDelta0",1.0);
  dw.SetParameter("xKdVEpsilon",EPSILON);
  dw.SetParameter("xKdVDelta1",delta1);
  dw.SetParameter("xKdVDelta2",delta2);
  dw.SetParameter("xKdVDelta3",delta3);
  dw.SetParameter("PenaltyTau",atof(argv[4]));

  info(parameters,true);// Some info on the used parameters
  IntervalMesh mesh(atoi(argv[5]),A,B);




  ExactSolKdVBBM exact_sol(dw.Time,
                           parameters("dolfwave_parameters")["xKdVDelta0"],
                           parameters("dolfwave_parameters")["xKdVEpsilon"],
                           parameters("dolfwave_parameters")["xKdVDelta1"],
                           parameters("dolfwave_parameters")["xKdVDelta2"]);


  //initial condition for elevation
  dw.SpacesFunctionsVectorsInit(mesh); // Init. Spaces Functions & Vectors
  Constant zero(0.0);
  dw.BilinearFormInit(mesh,zero);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LinearFormsInit(zero,zero,zero,zero,zero);
  dw.InitialCondition(exact_sol); //Initial Conditions L2 projection

  double inv1=dw.FirstInvariant(mesh);
  double inv2=dw.SecondInvariant(mesh);
  Constant urs(URS);
  double inv3=dw.ThirdInvariant(mesh,urs);

  // A Trick to get the l2 and linf norms of the exact solution
  double l2exact=dw.VectorNormError(mesh,zero,"l2",false);
  double linfexact=dw.VectorNormError(mesh,zero,"linf",false);

  double l2error=dw.VectorNormError(mesh,exact_sol,"l2");
  double linferror=dw.VectorNormError(mesh,exact_sol,"linf");

  dw.LUFactorization(true); //Re-use factorization
  //dw.PreviewEta(0);//Preview eta initial step
  dw.SaveEta(0);//Save eta initial step


  dw.RKInit("exp4"); // Explicit 4th-order Runge-Kutta
  dw.RKSolve();//Runge-Kutta for 3 initial steps

  dw.PCInit(mesh,true);


  while(dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );

      double inv1new=dw.FirstInvariant(mesh);
      double inv2new= dw.SecondInvariant(mesh);
      double inv3new=dw.ThirdInvariant(mesh,urs);

      double l2errornew=dw.VectorNormError(mesh,exact_sol,"l2");
      double linferrornew=dw.VectorNormError(mesh,exact_sol,"linf");

      if(inv1new>inv1) inv1=inv1new;
      if(inv2new>inv2) inv2=inv2new;
      if(inv3new>inv3) inv3=inv3new;
      l2error+=l2errornew;
      if(linferrornew>linferror) linferror=linferrornew;
      dw.Info(false);
      dw.PreviewEta();
    }

  dw.Comment.str("");
  dw.Comment<<"The l2 norm of the exact solution is "<< l2exact <<std::endl;
  dw.Comment<<"The linf norm of the exact solution is "<< linfexact <<std::endl;
  dw.Comment<<"The avarerage  l2 error is "
            << l2error/(dw.MaxSteps*l2exact)<<std::endl;
  dw.Comment<<"The Max  linf error is "
            <<linferror/linfexact <<std::endl;
  dw.Comment<<"The Max  Inv1  is "<<inv1<<" (Ts="<<dw.Step
            <<") and the relative error is "
            <<fabs(inv1-INV1)/INV1 <<std::endl;
  dw.Comment<<"The Max  Inv2  is "<<inv2<<" (Ts="<<dw.Step
            <<") and the relative error is "
            << fabs(inv2-INV2)/INV2 <<std::endl;
  dw.Comment<<"The Max  Inv3  is "<<inv3 <<" (Ts="<<dw.Step
            <<") and the relative error is "
            << fabs(inv3-INV3)/INV3<<std::endl;
  dw.Comment <<"T"<<"able Line (%)"<<std::endl;
  dw.Comment << atoi(argv[5])<<" "<<atof(argv[4])<<" "
             <<l2error/(dw.MaxSteps*l2exact)<<" "<<linferror/linfexact<< " "
             <<fabs(inv1-INV1)/INV1<<" "
             <<fabs(inv2-INV2)/INV2<<" "
             <<fabs(inv3-INV3)/INV3<<" "<<std::endl;

  dw.SaveEta();
  return (EXIT_SUCCESS);
}

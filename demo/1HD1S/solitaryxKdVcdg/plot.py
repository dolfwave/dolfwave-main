import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 1,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Solitary wave'


ax=subplot(111,autoscale_on='false')


step=1 #STEP IN NUMBER OF POINTS PER FILE SOLUTION

LMAX=150 #NUMBER OF FILES TO DRAW
LSTEP=1  #STEP BETWEEN FILES

for j in range (0,LMAX):
    filenumber=str(j*LSTEP).zfill(6)
    filename='output_kdvbbm_periodic/eta'+str(filenumber)+'.xyz'
    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=j*0.05+2.*u1[i*step,1]
    ax.plot(us[:,0], us[:,1],"b", label='')


ax.legend(loc=1)


(xname, yname) = ('$x$', 't $\\in [0,120]$ ')
#text1=text(0.0,6.0,'KdV equations with  inner penalty CDG method')
#title(plottitle,fontsize=18)
#grid()


ax.set_aspect(10.)
ax.set_xlim(-50.0,50.0)
ax.set_ylim(0.,8.6)
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)


show()

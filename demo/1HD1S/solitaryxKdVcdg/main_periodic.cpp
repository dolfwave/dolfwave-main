// Copyright (C) 2012 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// First added:  2013-01-11
// Last changed: 2015-06-09
// Compiled with DOLFWAVE:0.3.15.0
// This program provides a demo of the DOLFWAVE library.
// Solitary wave evolution generic
// xKdV-BBM with CDG method


#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

/* Global Variables*/
const double A=-50.; //
const double B=50.;  // $x\in[A.B]$
const double EPSILON=0.1;
const double MU=0.31623;
const double URS=MU*MU/EPSILON;


// Sub domain for Periodic boundary condition
class PeriodicBoundary : public SubDomain
{
  // Left boundary is "target domain" G
  bool inside(const Array<double>& x, bool on_boundary) const
  { return (std::abs(x[0]-A) < DOLFIN_EPS) && on_boundary; }
  // Map right boundary (H) to left boundary (G)
    void map(const Array<double>& x,  Array<double>& y) const
  {
    y[0] = x[0]-(B-A);
  }
};


class ElevationInitKdVBBM : public Expression
{
public:
  ElevationInitKdVBBM(const double delta0, const double epsilon,
                      const double delta1, const double delta2):
    delta0(delta0), epsilon(epsilon), delta1(delta1), delta2(delta2) {}
  void eval(Array<double> & values,const Array<double> & x) const
  {
    /*
      Definition of the solitary wave solution
      for the adimensional generic KdV-BBM equations
    */
    double c=delta0+delta0/80.; //Free parameter for speed/wave height
    double b=3./2.*epsilon; //Nonlinear coeff.
    double Psi=3.*(c-delta0)/b;
    double Theta=1./2.*sqrt((c-delta0)/(delta2-c*delta1));
    values[0]=Psi/sqr(cosh(Theta*(x[0])));

  };
private:
  const double delta0;
  const double epsilon;
  const double delta1;
  const double delta2;
};
// --------------------------------------------------------------------

int main(int argc, char* argv[])
{
  bool swtch=true;
  if (argc!=6)
    {
      info("Wrong number of arguments");
      info("argv[1]=num_time_steps");
      info("argv[2]='3' for xKdV-BBM (TODO) or argv[1]='2' for KdV-BBM or argv[1]='1' for KdV");
      info("argv[3]=output_dir");
      info("argv[4]=penalty_tau");
      info("argv[5]=num_interval_divisions");
      exit(EXIT_FAILURE);
    }
  std::stringstream output_dir;
  output_dir<<argv[3];
  //  Options, Initialization, Mesh and User functions
  Dolfwave dw(atoi(argv[1]) /*MaxSteps*/,
              0.001 /*TimeStep*/,
              1000 /*WriteGap*/,
              "xKdVcdg_1D" /*FormName*/,
              "LU_P" /* PETSc LU AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              output_dir.str() /*OutDir*/,
              "xyz"/*OutFormat*/);

  double kdvpa=0.0;
  double kdvpb=0.0;
  if (atoi(argv[2])==1)
    {
      dw.SetParameter("KdVPade",KDV_21_PADE);
      kdvpa=parameters("dolfwave_parameters")["KdVPade"];
    }
  else if (atoi(argv[2])==2)
    {
      dw.SetParameter("KdVPade",KDV_22_PADE);
      kdvpa=parameters("dolfwave_parameters")["KdVPade"];
    }
  else if (atoi(argv[2])==3)
    {
      info("Not implemented yet");
      exit(EXIT_FAILURE);
    }


  double delta1=(kdvpa-1.)*MU*MU/6.; //Sec.-ord. disp. coeff.
  double delta2=kdvpa*MU*MU/6.; //Third-ord. disp. coeff.
  double delta3=-kdvpb*MU*MU*MU*MU*19./360. ;//Fourth-ord. disp. coeff.

  dw.SetParameter("xKdVDelta0",1.0);
  dw.SetParameter("xKdVEpsilon",EPSILON);
  dw.SetParameter("xKdVDelta1",delta1);
  dw.SetParameter("xKdVDelta2",delta2);
  dw.SetParameter("PenaltyTau",atof(argv[4]));

  info(parameters,true);// Some info on the used parameters
  IntervalMesh mesh(atoi(argv[5]),A,B);




  ElevationInitKdVBBM eta_init(parameters("dolfwave_parameters")["xKdVDelta0"],
                               parameters("dolfwave_parameters")["xKdVEpsilon"],
                               parameters("dolfwave_parameters")["xKdVDelta1"],
                               parameters("dolfwave_parameters")["xKdVDelta2"]);

  //Auxiliar function for initializations
  Constant zero(0.0);
  // Create periodic boundary condition
  PeriodicBoundary periodic_boundary;
  //initial condition for elevation
  dw.SpacesFunctionsVectorsInit(mesh,periodic_boundary); // Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,zero);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LinearFormsInit(zero,zero,zero,zero,zero);
  dw.InitialCondition(eta_init); //Initial Conditions L2 projection
  dw.LUFactorization(true); //Re-use factorization
  dw.PreviewEta(0);//Preview eta initial step
  dw.SaveEta(0);//Save eta initial step
  dw.RKInit("exp4"); // Explicit 4th-order Runge-Kutta
  dw.RKSolve();//Runge-Kutta for 3 initial steps

  dw.PCInit(mesh,true);

  while(dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      if (!(dw.Step%dw.WriteGap))
        {
          dw.PreviewEta();
          dw.SaveEta();
          dw.Info(true);
        }
    }
  dw.PreviewEta();
  return (EXIT_SUCCESS);
}

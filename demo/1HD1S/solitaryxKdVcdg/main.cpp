// Copyright (C) 2012 Nuno David Lopes.-----------------------------------
// e-mail: nlopes@adm.isel.pt
// Licensed under the GNU LGPL Version 3.0 or later.
// First added - nlopes:  2013-01-11
// Last changed - nlopes: 2017-05-16
// Compiled with DOLFWAVE: 0.3.17
// This program provides a demo of the DOLFWAVE library.
// Solitary wave evolution generic
// xKdV-BBM with CDG method


#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

/* Global Variables*/
const double XL=-20.; //
const double XR=200.;  // $x\in[XL,XR]$
const double EPSILON=0.1;
const double MU=0.31623;
const double URS=MU*MU/EPSILON;




/*
Dirichlet 0.0 at boundary
Neumann 0.0 at right boudary
*/

class LeftBd : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
    {
      return near(x[0],XL) && on_boundary;
    }
  };

class RightBd : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
    {
      return near(x[0],XR) && on_boundary;
    }
  };


class DrchltSubD : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary;
  }
};


class ExactSolKdVBBM: public Expression
{
public:
  ExactSolKdVBBM(const double t, const double delta0, const double epsilon,
                 const double delta1, const double delta2)  :
    t(t), delta0(delta0), epsilon(epsilon), delta1(delta1), delta2(delta2) {}
  void eval(Array<double> & values,const Array<double> & x) const
  {
    /*
      Definition of the solitary wave solution
      for the adimensional generic KdV-BBM equations
    */
    double time=t;
    double c=delta0+delta0/80.; //Free parameter for speed/wave height
    double b=3./2.*epsilon; //Nonlinear coeff.
    double Psi=3.*(c-delta0)/b;
    double Theta=1./2.*sqrt((c-delta0)/(delta2-c*delta1));
    values[0]=Psi/sqr(cosh(Theta*(x[0]-c*time)));

  };
private:
  const double t;
  const double delta0;
  const double epsilon;
  const double delta1;
  const double delta2;
};
// --------------------------------------------------------------------


int main(int argc, char* argv[])
{

  if (argc!=7)
    {
      info("Wrong number of arguments");
      info("argv[1]=num_time_steps");
      info("argv[2]=argv[1]='2' for KdV-BBM or argv[1]='1' for KdV");
      info("argv[3]=gamma");
      info("argv[4]=penalty_tau");
      info("argv[5]=num_interval_divisions");
      info("argv[6]=output_dir");
      exit(EXIT_FAILURE);
    }
  std::stringstream output_dir;
  output_dir<<argv[6];
  //  Options, Initialization, Mesh and User functions
  Dolfwave dw(atoi(argv[1]) /*MaxSteps*/,
              0.001 /*TimeStep*/,
              1000 /*WriteGap*/,
              "xKdVcdg_1D" /*FormName*/,
              "LU_P" /* PETSc LU AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              output_dir.str() /*OutDir*/,
              "xyz"/*OutFormat*/);

  double kdvpa=0.0;
  double INV1,INV2,INV3;

  if (atoi(argv[2])==1)
    {
      dw.SetParameter("KdVPade",KDV_21_PADE);
      kdvpa=parameters("dolfwave_parameters")["KdVPade"];
      // Invariants evaluated in [XL,XR]
      INV1=1.154708661;
      INV2=0.1924514493;
      INV3=0.02886771739;
    }
  else if (atoi(argv[2])==2)
    {
      dw.SetParameter("KdVPade",KDV_22_PADE);
      kdvpa=parameters("dolfwave_parameters")["KdVPade"];
      // Invariants evaluated in [XL,XR]
      INV1=1.168340355;
      INV2=0.1956268782;
      INV3=0.02943437959;
    }
  else if (atoi(argv[2])==3)
    {
      info("Not implemented yet");
      exit(EXIT_FAILURE);
    }


  double delta1=(kdvpa-1.)*MU*MU/6.; //Sec.-ord. disp. coeff.
  double delta2=kdvpa*MU*MU/6.; //Third-ord. disp. coeff.

  dw.SetParameter("xKdVDelta0",1.0);
  dw.SetParameter("xKdVEpsilon",EPSILON);
  dw.SetParameter("xKdVDelta1",delta1);
  dw.SetParameter("xKdVDelta2",delta2);
  dw.SetParameter("xKdVGamma",atof(argv[3]));
  dw.SetParameter("PenaltyTau",atof(argv[4]));
  dw.SetParameter("VerbosityLevel",0);

  double tau = parameters("dolfwave_parameters")["PenaltyTau"];
  double gamma = parameters("dolfwave_parameters")["xKdVGamma"];

  info(parameters,true);// Some info on the used parameters
  IntervalMesh mesh(atoi(argv[5]),XL,XR);
  MeshFunction<std::size_t> boundary_parts(mesh,mesh.topology().dim()-1,0);
  boundary_parts.set_all(0);
  RightBd Xr;
  LeftBd  Xl;
  Xr.mark(boundary_parts,1);
  Xl.mark(boundary_parts,2);



  ExactSolKdVBBM exact_sol(dw.Time,
                           parameters("dolfwave_parameters")["xKdVDelta0"],
                           parameters("dolfwave_parameters")["xKdVEpsilon"],
                           parameters("dolfwave_parameters")["xKdVDelta1"],
                           parameters("dolfwave_parameters")["xKdVDelta2"]);


  //initial condition for elevation
  dw.SpacesFunctionsVectorsInit(mesh); // Init. Spaces Functions & Vectors
  dw.BilinearFormInit(boundary_parts);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  Constant pf(0.0), u0(0.0), u0_dt_dx(0.0), zero(0.0);
  dw.LinearFormsInit(pf,u0,u0_dt_dx,boundary_parts);
  dw.InitialCondition(exact_sol); //Initial Conditions L2 projection
  DrchltSubD dbc;
  dw.DolfwaveDBC(zero,dbc); //Setting Dirichlet Boundary Conditions

  double inv1=dw.FirstInvariant(mesh);
  double inv2=dw.SecondInvariant(mesh);
  Constant urs(URS);
  double inv3=dw.ThirdInvariant(mesh,urs);

  // A Trick to get the l2 and linf norms of the exact solution
  double l2exact=dw.VectorNormError(mesh,zero,"l2",false);
  double linfexact=dw.VectorNormError(mesh,zero,"linf",false);

  double l2error=dw.VectorNormError(mesh,exact_sol,"l2");
  double linferror=dw.VectorNormError(mesh,exact_sol,"linf");

  dw.LUFactorization(true); //Re-use factorization
  //dw.PreviewEta(0);//Preview eta initial step
  dw.SaveEta(0);//Save eta initial step


  dw.RKInit("exp4"); // Explicit 4th-order Runge-Kutta
  dw.RKSolve();//Runge-Kutta for 3 initial steps

  dw.PCInit(mesh,true);


  while((dw.Step<dw.MaxSteps)&&(!dw.IsDiverging)&&(!dw.IsNull)) {
    dw.PCSolve( );

    double inv1new=dw.FirstInvariant(mesh);
    double inv2new= dw.SecondInvariant(mesh);

    if (inv2new>1.5*INV2) dw.IsDiverging = true;
    if (inv2new<0.0000001) dw.IsNull = true;

    double inv3new=dw.ThirdInvariant(mesh,urs);

    double l2errornew=dw.VectorNormError(mesh,exact_sol,"l2");
    double linferrornew=dw.VectorNormError(mesh,exact_sol,"linf");

    if(inv1new>inv1) inv1=inv1new;
    if(inv2new>inv2) inv2=inv2new;
    if(inv3new>inv3) inv3=inv3new;
    l2error+=l2errornew;
    if(linferrornew>linferror) linferror=linferrornew;

    std::cout<<"## dw ## linf error"<< linferror<<std::endl;

    if (!(dw.Step%dw.WriteGap))
      {      //  Plots and infos
        //dw.PreviewEta();
        // dw.SaveEta();
        dw.Info(false);
      }
  }

  //  dw.Comment.str("");
  dw.Comment<<"The l2 norm of the exact solution is "<< l2exact <<std::endl;
  dw.Comment<<"The linf norm of the exact solution is "<< linfexact <<std::endl;
  dw.Comment<<"The avarerage  l2 relative error is "
            << l2error/(dw.MaxSteps*l2exact)<<" (%)" <<std::endl;
  dw.Comment<<"The Max  linf relative error is "
            <<linferror/linfexact <<" (%)" <<std::endl;
  dw.Comment<<"The Max  Inv1  is "<<inv1<<" (Ts="<<dw.Step
            <<") and the relative error is "
            <<fabs(inv1-INV1)/INV1 <<std::endl;
  dw.Comment<<"The Max  Inv2  is "<<inv2<<" (Ts="<<dw.Step
            <<") and the relative error is "
            << fabs(inv2-INV2)/INV2 <<std::endl;
  dw.Comment<<"The Max  Inv3  is "<<inv3 <<" (Ts="<<dw.Step
            <<") and the relative error is "
            << fabs(inv3-INV3)/INV3<<std::endl;
  dw.Comment <<"First Table Line (%)"<<std::endl;
  dw.Comment << atoi(argv[5])<<" "<<atof(argv[4])<<" "
             <<l2error/(dw.MaxSteps*l2exact)<<" "<<linferror/linfexact<< " "
             <<fabs(inv1-INV1)/INV1<<" "
             <<fabs(inv2-INV2)/INV2<<" "
             <<fabs(inv3-INV3)/INV3<<" "<<std::endl;
  dw.Comment <<"Second Table Line"<<std::endl;
  dw.Comment <<"Is"<<"Diverging Is"<<"Null LastStep Gamma"<<std::endl;
  dw.Comment <<dw.IsDiverging<<"           "<< dw.IsNull<<"      "<<dw.Step<<"     "<<gamma<<std::endl;


  std::stringstream table_line;
  table_line<<tau<<" "<<dw.IsDiverging<<" "<< dw.IsNull<<" "<<dw.Time<<" "<<gamma<<std::endl;

  std::ofstream table_file("table.txt",std::ios_base::app);
  table_file<<table_line.str()<<std::flush;
  table_file.close();

  dw.SaveEta();
  return (EXIT_SUCCESS);
}

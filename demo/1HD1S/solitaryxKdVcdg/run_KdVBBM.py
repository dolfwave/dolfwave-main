#!/usr/bin/python

import os

#1HD1S_solitaryxKdVcdg_demo 40000 2 output 0.01 55

#tau_list=[0.0,0.0001,0.001,0.01,0.1]
N_list=[55, 110, 220, 440, 880, 1760]#, 3520]
#N_list=[3520]
tau_list=[0.01]
#N_list=[55,110]

# the end point of the range is not included
for N in N_list[:]:
    for tau in tau_list:
        cmm2='1HD1S_solitaryxKdVcdg_demo 200000 2 outputK2N'+str(N)+'tau'+str(tau).replace('.','_')+'_dt_0.00005'+\
              ' '+str(tau)+' '+str(N)+' > /dev/null 2>/dev/null &'
        print(str(cmm2))
        os.system(str(cmm2))

// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by Dirichlet boundary conditions

#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;
// Dirichlet Boundary conditions
class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return (x[0]<DOLFIN_EPS) && on_boundary;
  }
};

const double HH=0.4572;
const double AA=0.01;
const double OO=3.8162534366;
const double SM_EPS=0.0218722659668;
const double KK=2.03245311395;
const double MM=0.147892751569;
const double LL=3.09142939832;
const double BB=0.0271856093798;
const double CC=0.0552534764387;

class DBC : public Expression
{
public:
  DBC(const double& t) :Expression(2), t(t){}
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double time=t;
    /* Wave input data*/
    double a=AA; /* amplitude  */
    double omega=OO; /* period */
    /*******************/
    /* note: b=c/k potencial magnitude*/
    /*b\approx a*omega*(1+(2/5)*(kH)^2/(k^2 H))->from dispersion relation */
    double k=KK;
    double c=CC;
    values[0]=a*sin(-omega*time);
    values[1]=-(c/k)*cos(-omega*time)+c/k;

  }
private:
  const double& t;
};

int main(int argc, char *argv[])
{
  bool preview=true;
  if (argc!=2)
    {
      preview=false;
      info("No preview");
    }
  else if (argc==2)
    {
      std::stringstream strng;strng<<argv[1];
      if (strng.str()=="preview")
        {
          preview=true;
          info("Preview");
        }
      else
        {
          preview=false;
          info("No preview");
        }
    }

  // Dolfwave Initialization
  Dolfwave dw(30000  /*MaxSteps*/,
              0.0005 /*TimeStep*/,
              200 /*WriteGap*/,
              "Zhao" /*FormName*/,
              "LU_P"/*LU solver by PETSc*/ ,
              "dolfin_interactive" /*Plotter*/,
              "output" /*OutDir*/,
              "raw"/*OutFormat*/,
              "Using data provided by datacalculator.py for DirichletBC"
              );

  info(parameters,true);

  info(" *****************DIRICHLET BOUNDARY DATA**************** ");
  info("(H)      Water Depth at Dirichlet BC      = %.5f",HH);
  info("(A)      Wave  Amp at Dirichlet BC        = %.5f",AA);
  info("(omega)  Wave ang. freq at Dirichlet BC   = %.5f",OO);
  info("(P)      Wave Period at Dirichlet BC      = %.5f",OO*2*DOLFIN_PI);
  info("(eps)    Small amp. param. at Dirichlet BC= %.5f",SM_EPS);
  info("(K)      Wave Number (x) at Dirichlet BC  = %.5f",KK);
  info("(mu)     Long wave param. at Dirichlet BC = %.5f",MM);
  info("(L)      Wave Lenght at Dirichlet BC      = %.5f",LL);
  info("(b)      Wave pot. magn. at Dirichlet BC  = %.5f",BB);
  info("(c)      Wave vel. amp. at Dirichlet BC   = %.5f",CC);


  Mesh mesh("../../../../meshes/pillar/rectpillar.xml.gz");//Mesh definition
  mesh.order();
  Constant depth(HH);//Depth
  Constant zero(0.0);
  DirichletBoundary dirichletboundary;
  DBC bc(dw.Time);
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth);
  dw.LinearFormsInit(depth,
                     zero /*sp_eta*/,
                     zero /*sp_lap_eta*/,
                     zero   /*sp_phi*/,zero /*sp_lap_phi */,
                     zero /*source function*/);
  dw.DolfwaveDBC(bc,dirichletboundary);//Dirichlet B.C. definition
  dw.MatricesAssemble( );
  dw.LUFactorization();//Linear solver (limited yet)
  dw.RKInit("exp4");
  dw.RKSolve(); //first 3 steps

  if (preview) dw.PreviewEta(3); //Preview third step
  dw.SaveEta(1);//Saving the first step
  dw.PCInit(mesh,false); //P.Corrector initialization
  while(dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
          std::stringstream pteA;
          pteA<<"output/lineA"<<dw.Step<<".xyz";
          dw.LinePlot(0.,0.5,20.,0.5,100,"eta",pteA.str());
          std::stringstream pteB;
          pteB<<"output/lineB"<<dw.Step<<".xyz";
          dw.LinePlot(0.,2.0,13.,2.0,100,"eta",pteB.str());
          std::stringstream pteC;
          pteC<<"output/lineC"<<dw.Step<<".xyz";
          dw.LinePlot(15.,2.0,20.,2.0,50,"eta",pteC.str());
          if (preview) dw.PreviewEta();
          dw.SaveEta();
        }
      info("Step=%d \t Simulated time= %.4f",dw.Step,dw.Time);}

  return (EXIT_SUCCESS);
}

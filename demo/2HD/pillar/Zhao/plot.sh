# script para gerar o filme associado � solu��o
#!/bin/sh

#Solution number
i=$1

stri=$(printf "%03d" $i);
xd3d -bord=3 -box=18 -hidden -exag=25  -iso=1 -persp=2 -rotx=-0 -roty=0 -rotz=-180 -zoom=1.521 -xcentr=0.962 -ycentr=-1.98 -pscol=eta$i.ps -psdef=info,port -fich=output/eta000$stri.raw  rectangulo.nopo;

convert eta$i.ps demo$i.png;

rm -rf eta$i.ps;

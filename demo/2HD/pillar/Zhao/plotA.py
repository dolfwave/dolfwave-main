import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 1,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Demo 2HD Pillar Zhao ; Line (0.,.5)-(20.0,.5)'


ax=subplot(111,autoscale_on='false')


step=1 #STEP IN NUMBER OF POINTS PER FILE SOLUTION

LMAX=150 #NUMBER OF FILES TO DRAW
LSTEP=200  #STEP BETWEEN FILES

for j in range (1,LMAX):
    filename='output/lineA'+str(j*LSTEP)+'.xyz'

    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=j*0.01+3*u1[i*step,1]
    if (j==1) or (j==20) or (j==40) or (j==60)\
           or (j==80) or (j==100) or (j==120) or (j==140) or (j==149):
        ax.plot(us[:,0], us[:,1],"black", label='',alpha=1.0)
    else:
        ax.plot(us[:,0], us[:,1],"b", label='',alpha=.3)

ax.legend(loc=1)


(xname, yname) = ('$x$', 'time $0-15$ s')
title(plottitle,fontsize=18)
#grid()


ax.set_aspect(5.)
ax.set_xlim(.0,20.0)
ax.set_ylim(0.,1.6)
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)


show()


// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  main.cpp
//  This program provides a demo of the DOLFWAVE library.
//  Initial Conditions given in files "eta_init" and "phi_init"

#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;
int main()
{
  //  Clear and maintimer.start()
  Dolfwave dw(
              2000 /*MaxSteps*/,
              0.001 /*TimeStep*/,
              50 /*WriteGap*/,
              "Zhao" /*FormName*/,
              "LU_P" /*AlgBackEND*/,
              "xd3d" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/);
  dw.TimeAdd(0.03); //Starting point of Time
  /*
     Remember that time should be updated
     This is important when time-dependent Dirichlet BC
     are applied (which is not the case)

     Note that the mesh must be the same used previously
     for the generation of the files

  */
  // Mesh, boundary definition, Function Spaces
  info(parameters,true);
  RectangleMesh mesh(0,0,6,6,35,35,"left");
  mesh.order();
  Constant depth(0.45);//Depth
  Constant sponge(0.0);//Sponge Layer
  Constant src(0.0);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
    // Functions, Vectors and Matrices ----------------------
  dw.BilinearFormInit(mesh,depth);
  dw. MatricesAssemble( );
  dw.InitialCondition("eta_init","phi_init"); //Load Int.Cond. from files eta_init and phi_init (raw format)
  dw.PreviewEta(0);//Preview and Save initial condition
  dw.SaveEta(0);
  dw.LinearFormsInit(depth,sponge /*sp_eta*/,sponge /*sp_lap_eta*/,
                     sponge /*sp_phi*/,sponge/*sp_lap_phi */,
                     src);
  dw.LUFactorization( ); //Seting the linear Solver (limited yet)
  dw.RKInit("exp4");
  dw.RKSolve( ); // Runge-Kutta explicit fist 3 steps
  dw.PCInit(mesh);  //PC Initialization
  while(dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      //  Plot information------------------------
      if (!(dw.Step%dw.WriteGap))
        {
          dw.PreviewEta();
          dw.SaveEta();
        }
      dw.Info(false);
    }

  return (EXIT_SUCCESS);
}

// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Standard Gaussian Wave
//  Using Viper for preview.
#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;

class ElevationInit : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    values[0]=0.045*exp(-2.*(sqr(x[0]-3.0)+sqr(x[1]-3.0)));
  }
};
int main(int argc,char *argv[])
{
  /// Options, Initialization, Mesh and User functions
  Dolfwave dw(
              5000 /*MaxSteps*/,
              0.0005 /*TimeStep*/,
              100 /*WriteGap*/,
              "Zhao" /*FormName*/,
              "LU_P" /*AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/,
              "Ring test with Zhao et al.:mpirun -n # 2HD_squarezhao_demo"
              );
  parameters("dolfwave_parameters")["pred_corr_blow_up"]=3;
  info(parameters,true);
  RectangleMesh mesh(Point(0.0,0.0),Point(6.0,6.0),35,35,"left");
  Constant depth(0.45);//Depth
  Constant sponge(0.0);//Sponge Layer
  Constant src(0.0);//Source Function
  Constant phi_init(0.0);//initial condition for potential
  ElevationInit eta_init;//initial condition for elevation
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LinearFormsInit(depth,sponge /*sp_eta*/,sponge /*sp_lap_eta*/,
                     sponge /*sp_phi*/,sponge/*sp_lap_phi */,src);
  dw.InitialCondition(eta_init,phi_init); //Initial Conditions L2 projection
  dw.PreviewEta(); //Preview Solution
  dw.LUFactorization(true); //If uBLAS the factorize
  dw.RKInit(/*RKType "euler" or the default "exp4"*/ );
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
  dw.PCInit(mesh,true);
  while (dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      if(!(dw.Step%dw.WriteGap))
        {
          dw.Info(false);}
    }
  dw.PreviewEta(); //Preview Solution
  return (EXIT_SUCCESS);
}

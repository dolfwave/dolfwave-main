// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Standard Gaussian Wave
#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

class ElevationInit : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    values[0]=0.045*exp(-2.*(sqr(x[0]-3.0)+sqr(x[1]-3.0)));
  }
};

int main( )
{
  // {{{ Options, Initialization, Mesh and User functions
  Dolfwave dw(
              40001 /*MaxSteps*/,
              0.0005 /*TimeStep*/,
              100 /*WriteGap*/,
              "LPTcdg" /*FormName*/,
              "LU_P" /*AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "pvd"/*OutFormat*/,
              "Low order non linear terms only" /*Comment*/
              );
  info(parameters,true);
  dw.SetParameter("PenaltyTau",0.01);
  RectangleMesh mesh(0,0,6,6,25,25,"crossed");
  Constant depth(0.45);//Depth
  Constant zero(0.0); //phi init cond.
  Constant tension(0.0);
  ElevationInit eta_init;//initial condition for elevation
  // }}} -----------------------------------------------------------
  // {{{ Functions, Vectors and Matrices ----------------------
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,depth);//BilinearForm a initialization
  dw.LinearFormsInit(depth,depth,zero,zero,zero,tension,zero,zero);
  dw.InitialCondition(eta_init,zero); //Initial Conditions L2 projection
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LUFactorization(true); //Reuse  Factorization
  dw.RKInit("exp4");
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
  dw.PCInit(mesh,false);
  while( dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      if(!(dw.Step%dw.WriteGap))
        {
          std::stringstream pte;
          pte<<"output/centralline"<<dw.Step<<".xyz";
          dw.LinePlot(0.,3.,6.,3.,25,"eta",pte.str());
          dw.PreviewEta();
          dw.SaveEta();
          dw.Info(true);
        }
      dw.Info(false);
    }
  return (EXIT_SUCCESS);
}

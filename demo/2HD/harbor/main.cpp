// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This demo provides a real scale application for  the DOLFWAVE library.

#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;


//For Dirichlet Boundary conditions and information
const double HH=13.716;
const double AA=0.25;
const double OO=0.647146697204;
const double SM_EPS=0.0182268883056;
const double KK=0.0618455787546;
const double MM=0.135006993543;
const double LL=101.594736984;
const double BB=3.97150985223;
const double CC=0.245620325341;


// {{{ User defined Functions---------------------------------------------------
class DirichletBoundary : public SubDomain
  {
    bool inside(const Array<double>& x, bool on_boundary) const
    {
      return
        (x[0] < -449.) && on_boundary;
    }
  };
// Dirichlet Boundary conditions
class DBC : public Expression
{
public:
  DBC(const double& t): Expression(2), t(t) {}
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double time=t;
    /* Wave input data*/
    double a=AA; /* amplitude 0.3m-> 0.6 wave height */
    double omega=OO; /* period 2*Pi \approx 7s */
    /*******************/
    /* note: b=c/k potencial magnitude*/
    /*b\approx a*omega*(1+(2/5)*(kH)^2/(k^2 H))->from dispersion relation */
    double k=KK;
    double c=CC;
    values[0]= a*sin(-omega*time);
    values[1]=-(c/k)*cos(-omega*time)+c/k;

  }
private:
  const double& t;
};

//Depth function
class Depth : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double retrn=0.0;

    if (x[0]<300.0)
      retrn=13.716;
    else if
      ((x[0]>= 300.0)&&
       (x[0]<= 510.0))
      retrn=13.716+(300.-x[0])/25.0;
    else
      retrn=5.316;

    values[0]=retrn;
  }
};
//Sponge Layer definition
class SpongeLayer : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double n1=1.5;
    double n2=0.01;
    double n3=0.1;
    double xs=510.;
    double x2=660.;
    values[0]=0.0;
    double centre1x=-120.0;
    double centre1y=300.0;
    double centre2x=-120.0;
    double centre2y=-120.0;
    if (x[0]>xs)
      values[0]+=n1*(std::pow(exp((x[0]-xs)/(x2-xs)),n2)-1.)/(exp(1.)-1.);
    if (std::sqrt(sqr(x[0]-centre1x)+sqr(x[1]-centre1y))<150.0)
      values[0]+=n1*
        (std::pow(exp((150.0-std::sqrt(sqr(x[0]-centre1x)
                                       +sqr(x[1]-centre1y)))/150.0),n3)-1.)/(exp(1.)-1.);
    if (std::sqrt(sqr(x[0]-centre2x)+sqr(x[1]-centre2y))<120.0)
      values[0]+=n1*
        (std::pow(exp((120.0-std::sqrt(sqr(x[0]-centre2x)+sqr(x[1]-centre2y)))/120.0),n3)-1.)
        /(exp(1.)-1.);
  }
};
// }}}End User defined Functions ----------------------------------------------
int main()
{
  // Dolfwave start
  Dolfwave dw(3000 /*MaxSteps*/,
              0.0005 /*TimeStep*/,
              100 /* WriteGap*/,
              "Zhao" /*FormName*/,
              "GMRES_P"/*GMRES provided by PETSc*/ ,
              "dolfin_animated" /*Preview */,
              "output" /*OutDir*/,
              "pvd"/*OutFormat*/);
  info(parameters,true);

  info(" *****************DIRICHLET BOUNDARY DATA**************** ");
  info("(H)      Water Depth at Dirichlet BC      = %.5f",HH);
  info("(A)      Wave  Amp at Dirichlet BC        = %.5f",AA);
  info("(omega)  Wave ang. freq at Dirichlet BC   = %.5f",OO);
  info("(P)      Wave Period at Dirichlet BC      = %.5f",OO*2*DOLFIN_PI);
  info("(eps)    Small amp. param. at Dirichlet BC= %.5f",SM_EPS);
  info("(K)      Wave Number (x) at Dirichlet BC  = %.5f",KK);
  info("(mu)     Long wave param. at Dirichlet BC = %.5f",MM);
  info("(L)      Wave Lenght at Dirichlet BC      = %.5f",LL);
  info("(b)      Wave pot. magn. at Dirichlet BC  = %.5f",BB);
  info("(c)      Wave vel. amp. at Dirichlet BC   = %.5f",CC);


  //  Mesh and boundary definition
  Mesh mesh("harbor.xml.gz");
  mesh.order();
  Depth depth;//Depth
  SpongeLayer sponge;//Sponge Layer
  Constant zero(0.0);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);
  // Boundary conditions definition
  DirichletBoundary dirichletboundary;
  DBC bc(dw.Time);
  dw.DepthPlot(mesh,depth);//Save depth
  dw.SpongePlot(mesh,sponge);//Save sponge
  dw.BilinearFormInit(mesh,depth);//Bilinear forms init
  dw.MatricesAssemble( );  //Matrices assemble and Dir. Bc apply
  dw.LinearFormsInit(depth,zero /*sp_eta*/,zero /*sp_lap_eta*/,
                     zero   /*sp_phi*/,sponge /*sp_lap_phi */,
                     zero /*source function*/);
  dw.DolfwaveDBC(bc,dirichletboundary); //Setting Dirichlet Boundary Conditions
  dw.LUFactorization(true);//Reuse factorization
  dw.RKInit("exp4");
  dw.RKSolve( );
  dw.PCInit(mesh,false);
  while(dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      //  Plot information------------------------
      if(!(dw.Step%dw.WriteGap))
        {
          dw.SaveEta();//Save Eta
          dw.HorVelPlot(mesh,false,true);
        }
      dw.Info(false);
    }
  return(EXIT_SUCCESS);
}

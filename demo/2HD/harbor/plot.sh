# script para gerar o filme associado � solu��o
#!/bin/sh

#Solution number
i=$1

stri=$(printf "%04d" $i);

xd3d -bord=3 -box=18 -hidden  -hidden -exag=75  -fich=output/eta00$stri.raw -iso=1 -persp=2 -rotx=-0 -roty=0 -rotz=-180 -psdef=info,port -pscol=eta$i.ps harbor.avoir2D

convert eta$i.ps demo$i.png;

rm -rf eta$i.ps;

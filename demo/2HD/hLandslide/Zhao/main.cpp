// Copyright (C) 2009 Nuno David Lopes.---------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This program provides a demo of the DOLFWAVE library.
// Wave generation by an object moving on a horizontal bottom: Zhao model
// -----------------------------------------------------------------------

#include "Depth.h"
#include <sstream>

int main( )
{
  Dolfwave dw(12000/*MaxSteps*/,
              0.0005 /*TimeStep*/,
              20  /*WriteGap*/,
              "Zhao" /*FormName*/,
              "LU_P" /* PETScLU*/ ,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/
              );

  info(parameters,true);
  RectangleMesh mesh(-2.,-3.,10.5,3.,35,15,"crossed");
  Depth depth(dw.Time);// Time variable depth
  Constant zero(0.0);// Sponge Layer
  SourceEta srceta(dw.Time);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth);// Bilinear form initialization
  dw.LinearFormsInit(depth,zero /*sp_eta*/,zero /*sp_lap_eta*/,
                     zero /*sp_phi*/, zero/*sp_lap_phi */,srceta);
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false); // Do not reuse LU factorization

  int j=0;
  std::stringstream pte;
  pte<<"output/line"<<j<<".ep";
  j++;

  dw.SaveEta(); //Saving the initial condition
  dw.LinePlot(-2.0,0.0,10.5,0.0,600,"eta",pte.str());
  dw.DepthPlot(mesh,depth,false);
  dw.Volume(mesh,depth,"output/volume.tv");

  dw.RKInit();
  dw.RKSolve("MatricesReAssemble"); //first 3 steps

  dw.PCInit(mesh); //P.Corrector initialization

  while(dw.Step<dw.MaxSteps)
    {
      dw.MatricesAssemble();
      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
           dw.SaveEta(); //Saving eta
          std::stringstream pte;
          pte<<"output/line"<<j<<".ep";
          j++;
          dw.LinePlot(-2.0,0.0,10.5,0.0,601,"eta",pte.str());
          dw.DepthPlot(mesh,depth,false);
          dw.Volume(mesh,depth,"output/volume.tv");
        }
      info("Step=%d \t Simulated time= %.6f",dw.Step,dw.Time);
    }
  return (EXIT_SUCCESS);
}

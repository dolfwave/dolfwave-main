
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.cpp
//Fri Dec  4 16:13:40 WET 2009
//
// To compile  with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------


#include "Depth.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


void Depth::eval(Array<double> & values, const Array<double> & x) const
  {
    double Dh=0.045;
    double X=x[0];
    double Y=x[1];
    values[0]=0.45 - Dh*(1.0 + tanh(1.0 + 2.0*Y))*(1.0 + tanh(1.0 - 2.0*Y))*(1.0 + tanh(1.0 + 2.0*X - 2.0*t))*(1.0 + tanh(1.0 + 2.0*t - 2.0*X))/pow((1 + tanh(1)),4);
   }




void SourceEta::eval(Array<double> & values, const Array<double> & x) const
   {
    double Dh=0.045;
    double X=x[0];
    double Y=x[1];
    values[0]=(Dh*(1 + tanh(1.0 + 2.0*Y))*(1 + tanh(1.0 - 2.0*Y))*(1 + tanh(1.0 + 2.0*X - 2.0*t))*(2.0 - 2.0*pow(tanh(1.0 + 2.0*t - 2.0*X),2)) - Dh*(1 + tanh(1.0 + 2.0*Y))*(1 + tanh(1.0 - 2.0*Y))*(1 + tanh(1.0 + 2.0*t - 2.0*X))*(2.0 - 2.0*pow(tanh(1.0 + 2.0*X - 2.0*t),2)))/(1 + 4*tanh(1) + 6*pow(tanh(1),2) + 4*pow(tanh(1),3) + pow(tanh(1),4));

   }


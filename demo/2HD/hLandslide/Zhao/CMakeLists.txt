# Require CMake 2.8
cmake_minimum_required(VERSION 2.8)

project(2HD_hLandslide_zhao_demo)
set(SOURCES main.cpp Depth.cpp)


# Include directories
include_directories($ENV{DOLFWAVE_SOURCE_PATH})
find_package(DOLFIN)
# Include directories
include_directories(${DOLFIN_INCLUDE_DIRS} ${DOLFIN_3RD_PARTY_INCLUDE_DIRS})
# Compiler definitions
add_definitions(${DOLFIN_CXX_DEFINITIONS})
# Add special DOLFIN compiler flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${DOLFIN_CXX_FLAGS}")

# Executable
add_executable(${PROJECT_NAME} ${SOURCES})
# Target libraries
target_link_libraries(${PROJECT_NAME} $ENV{DOLFWAVE_LIB_SO} $ENV{DOLFIN_LIB_SO}  $ENV{MPI_CXX_LIB_SO} $ENV{MPI_LIB_SO})

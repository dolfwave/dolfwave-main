## {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
##
## e-mail: ndl@ptmat.fc.ul.pt
## http://ptmat.fc.ul.pt/~ndl/
##
## ---------------------------------------------------------------------------
## Licensed under the GNU LGPL Version 3.0 or later.
## ---------------------------------------------------------------------------
##  LPTsourcescalculator.py
##
## First added:  2009-10-19
## Last changed: 2010-06-21
##
## }}}------------------------------------------------------------------------

from sympy import *
from sympy import __version__ as sympy_version
import commands

print 'sympy version is '+ sympy_version
print 'Works with versions >= SymPy 0.6.3'


#Use capital X  to simplify integration with DOLFIN

X=Symbol('X',real=True, unbounded=True)
Y=Symbol('Y',real=True, unbounded=True)
t=Symbol('t',real=True, unbounded=True)

#define the depth function

Dh=Symbol('Dh',real=True, unbounded=True)
B3=Symbol('B3',real=True, unbounded=True)
B4=Symbol('B4',real=True, unbounded=True)
alpha=Symbol('alpha',real=True, unbounded=True)
beta1=Symbol('beta1',real=True, unbounded=True)
S0=Symbol('S0',real=True, unbounded=True)


## DADOS

_Dh=0.045
_B=1.
_S0=1.0


## Depth definition ##
def S(t):
    return _S0*t

def xc(t):
    return S(t)

def xl(t):
    return xc(t)-0.5

def xr(t):
    return xc(t)+0.5



def h(X,Y,t):
    return 0.45\
           -Dh/((1+tanh(1))**4)*(1.+tanh(2.*(X-xl(t))))*\
           (1.-tanh(2.*(X-xr(t))))*(1.+tanh(2.*Y+1.0))*\
           (1.-tanh(2.*Y-1.0))

#####################################################
print 'the depth function h'
depthpy=h(X,Y,t)
depth=ccode(depthpy)
print depthpy
print '\n'



#### OUTPUT ######
print 'first time derivative of h dh/dt\n'
dhdt=diff(h(X,Y,t),t)
dtdepth=ccode(dhdt)
print dhdt
print'\n'

print 'second time derivative of h d/dt dh/dt\n'

d2hdt2=diff(h(X,Y,t),t,2)
dtdtdepth=ccode((together(d2hdt2)))
print d2hdt2
print'\n'

print 'Source function for eta is S_1(h)=\n'
srcetapy=-diff(h(X,Y,t),t)\
          -1./2.*diff(h(X,Y,t)**2*diff(diff(h(X,Y,t),t),X),X)\
          -1./2.*diff(h(X,Y,t)**2*diff(diff(h(X,Y,t),t),Y),Y)\
          +B3*h(X,Y,t)**4*diff(diff(diff(h(X,Y,t),X,2)+diff(h(X,Y,t),Y,2),X,2)\
                               +diff(diff(h(X,Y,t),X,2)+diff(h(X,Y,t),Y,2),Y,2),t)\
                               +alpha*(1.-beta1)*(diff(h(X,Y,t)*diff(h(X,Y,t)*diff(h(X,Y,t),t),X),X)\
                                                  +diff(h(X,Y,t)*diff(h(X,Y,t)*diff(h(X,Y,t),t),Y),Y))


srceta= ccode(together(srcetapy))


print srcetapy
print '\n'



print 'Source function for phi is S_2(h)=\n'
srcphipy=-1./2.*diff(h(X,Y,t),t)**2+alpha*diff(h(X,Y,t)*diff(h(X,Y,t),t),t)\
          -B4*h(X,Y,t)**3*(diff(diff(h(X,Y,t),t,2),X,2)+diff(diff(h(X,Y,t),t,2),Y,2))

srcphi=ccode(together(srcphipy))

print srcphipy




######## print an Depth.h and Depth.cpp file to use #include "Depth.h" in the main.cpp ##########
date=commands.getoutput('date')
file = open('Depth.h','w')
print >>file, '''
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.h
//'''+str(date)+'''
//
// To compile  with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#ifndef __DEPTH_H
#define __DEPTH_H

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;


class Depth : public Expression
{
 public:
  Depth (const double& t): t(t)
  {}
  void eval(Array<double> & values, const Array<double> & x) const;
 private:
  const double& t;
 };

class DtDepth : public Expression
 {
  public:
   DtDepth (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
 private:
   const double& t;
 };

class DtDtDepth : public Expression
 {
  public:
   DtDtDepth (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
 private:
   const double& t;
 };

class SourceEta : public Expression
{
  public:
   SourceEta (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
 private:
   const double& t;
 };

class SourcePhi : public Expression
{
 public:
   SourcePhi (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
 private:
   const double& t;
 };

#endif'''

file = open('Depth.cpp','w')

print >>file, '''
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.cpp
//'''+str(date)+'''
//
// To compile  with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------


#include "Depth.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


void Depth::eval(Array<double> & values, const Array<double> & x) const
  {
    double Dh='''+str(_Dh)+''';
    double X=x[0];
    double Y=x[1];
    values[0]='''+str(depth)+''';


   }



void DtDepth::eval(Array<double> & values, const Array<double> & x) const
{
     double Dh='''+str(_Dh)+''';
     double X=x[0];
     double Y=x[1];
     values[0]='''+str(dtdepth)+''';

}


void DtDtDepth::eval(Array<double> & values, const Array<double> & x) const
   {
     double Dh='''+str(_Dh)+''';
     double X=x[0];
     double Y=x[1];
     values[0]='''+str(dtdtdepth)+''';

}

void SourceEta::eval(Array<double> & values, const Array<double> & x) const
   {
    double Dh='''+str(_Dh)+''';
    double X=x[0];
    double Y=x[1];
    double alpha=0.5917517028;
    double beta1=4.971623160e-7;
    double B3=pow(alpha,3)/3.-sqr(alpha)+5./6.*alpha-alpha*beta1/2.-5./24.;
    values[0]='''+str(srceta)+''';

   }



void SourcePhi::eval(Array<double> & values, const Array<double> & x) const
   {
    double Dh='''+str(_Dh)+''';
    double X=x[0];
    double Y=x[1];
    double alpha=0.5917517028;
    double B4=-pow(alpha,3)/3.+sqr(alpha)-alpha/2.;
    values[0]='''+str(srcphi)+''';
} '''

#!/bin/sh

#d1="tau50dt001"
#d2="tau10dt001"
d3="tau1dt001"
#d4="tau01dt001"
#d5="tau5dt0005"
#d6="tau1dt0005"
#d7="tau01dt0005"
#d8="tau1dt0005"

#mkdir $d1
#mkdir $d2
mkdir $d3
#mkdir $d4
#mkdir $d5
#mkdir $d6
#mkdir $d7
#mkdir $d8


#3dHNL.bin "PETScLU" 5.0  6001   0.001   10  $d1   >  $d1/logfile.txt 2>$d1/stdERROR.txt&
#3dHNL.bin "PETScLU" 1.0  6001   0.001   10  $d2  >  $d2/logfile.txt 2>$d2/stdERROR.txt&
2HD_horizontalLandslide_demo  0.1  6001   0.001   10  $d3  >  $d3/logfile.log 2>$d3/stdERROR.log&
#3dHNL.bin "PETScLU" 0.1  6001   0.001   10  $d4   >  $d4/logfile.txt 2>$d4/stdERROR.txt&
#3dHNL.bin "PETScLU" 5.0  12001  0.0005  20  $d5  >  $d5/logfile.txt 2>$d5/stdERROR.txt&
#3dHNL.bin "PETScLU" 1.0  12001  0.0005  20  $d6 >  $d6/logfile.txt 2>$d6/stdERROR.txt&
#3dHNL.bin "PETScLU" 0.01 12001  0.0005  20  $d7  >  $d7/logfile.txt 2>$d7/stdERROR.txt&
#tinylandslide.bin "PETScLU" .1 12001  0.0005 20  $d8 >  $d8/logfile.txt 2>$d8/stdERROR.txt&

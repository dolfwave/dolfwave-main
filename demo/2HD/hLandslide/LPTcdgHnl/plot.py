#!/usr/bin/env python
import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 1,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Horizontal landslide central line LPTcdgHnl'


ax=subplot(111,autoscale_on='false')
alignment = {'horizontalalignment':'center', 'verticalalignment':'baseline'}
family = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
font0 = FontProperties()
font1 = font0.copy()
font1.set_size(14)#
step=1
LMAX=61
LINIT=5
LSTEP=10

for j in range (0,LMAX):
    filename='tau1dt001/line'+str(j*LSTEP)+'.ep'

    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=j*0.02+15.*u1[i*step,1]
    ax.plot(us[:,0], us[:,1],"b", label='')


ax.legend(loc=1)


(xname, yname) = ('$x$ (m)', '$t\in [0,6]$ (s) ')
title(plottitle,fontsize=18)
grid()


ax.set_aspect(4.0)
ax.set_xlim(0,12.5)
ax.set_ylim(-.2,1.5)
ax.set_xlabel(xname,fontsize=18)
ax.set_ylabel(yname,fontsize=24)


show()















# script para gerar o filme associado � solu��o
#!/bin/sh

#Solution number
i=$1

stri=$(printf "%03d" $i);
xd3d -bord=0  -box=18 -exagz=25 -iso=z -iso=1 -table=8  -rotz=-180 -ps=depth$i.ps -psdef=info,port tau1dt001/depth000$stri.xyz;

convert depth$i.ps demodepth$i.png;

rm -rf depth$i.ps;

// Copyright (C) 2009 Nuno David Lopes.---------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by a moving bottom: LPT model
//  Using an inner penalty CDG method
//  See mvbottom for CG
// Using Viper  pr� post-processor and Xd3d with the files output.

#include "Depth.h"
#include <sstream>

int main(int argc,char *argv[])
{

  if (argc!=6)
    {
      info("argv[1]=tau argv[2]=MaxTimeSteps\n");
      info("argv[3]=TimeStep argv[4]=WriteGap argv[5]=dirname\n");
      info("See the script runall.sh\n");
      exit(EXIT_FAILURE);
    }


  double pentau=atof(argv[1]);
  int maxts=atoi(argv[2]);
  double tstep=atof(argv[3]);
  int wrtgap=atoi(argv[4]);


  std::stringstream outputeta;
  std::stringstream outputvolume;
  std::stringstream cmmnd;
  std::stringstream dir;


  dir<<argv[5];
  outputvolume<<dir.str()<<"/volume.tv";

  Dolfwave dw(
              maxts  /*MaxSteps*/,
              tstep /*TimeStep*/,
              wrtgap /*WriteGap*/,
              "LPTcdgHnl" /*FormName*/,
              "LU_P" /* AlgBackEND*/ ,
              "dolfin_animated" /*Plotter*/,
              dir.str() /*OutDir*/,
              "xyz"/*OutFormat*/,
              "With higher-order nonlinear terms" /*Comments*/
              );

  dw.SetParameter("PenaltyTau",pentau);

  RectangleMesh mesh(-2.,-3.,10.5,3.,35,15,"crossed");
  Depth depth(dw.Time);//time variable depth
  DtDepth dtdepth(dw.Time);
  DtDtDepth dtdtdepth(dw.Time);
  Constant spng(0.0);//Sponge Layer
  Constant tension(0.0);//Sponge Layer
  SourceEta srceta(dw.Time);//Source Function
  SourcePhi srcphi(dw.Time);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,depth);// Bilinear form initialization
  dw.LinearFormsInit(depth,depth,dtdepth,dtdtdepth,
                     spng,tension,srceta,srcphi);//Linear form initialization
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false);

  // For ploting  line profiles
  std::stringstream pte;
  int j=0;
  pte<<dir.str()<<"/line"<<j<<".ep";
  j++;

  dw.SaveEta();
  dw.LinePlot(-2.0,0.0,10.5,0.0,601,"eta",pte.str());
  dw.DepthPlot(mesh,depth,false);
  dw.Volume(mesh,depth,outputvolume.str());

  dw.RKInit();
  dw.RKSolve("MatricesReAssemble"); //first 3 steps
  dw.PCInit(mesh); //P.Corrector initialization
  while( dw.Step<dw.MaxSteps+1)
    {
      dw.MatricesAssemble();
      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
          dw.SaveEta(); //ploting and saving
          std::stringstream pte;
          pte<<dir.str()<<"/line"<<j<<".ep";
          j++;
          dw.LinePlot(-2.0,0.0,10.5,0.0,601,"eta",pte.str());
          dw.DepthPlot(mesh,depth,false);
          dw.Volume(mesh,depth,outputvolume.str());
        }
      info("Step=%d \t Simulated time= %.6f",dw.Step,dw.Time);
    }
  return (EXIT_SUCCESS);
}

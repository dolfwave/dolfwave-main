// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This program provides a demo of the DOLFWAVE library.
// Standard Gaussian Wave

// First added:  2011-03-29
// Last changed: 2014-07-08
// Compiled with DOLFWAVE:0.2.11.43

#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;

class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return
      (x[0] < DOLFIN_EPS) && on_boundary;
  }
};

class Depth : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double retrn=0.0;

    if (x[0]<8.0-sqrt((x[1]+3.)*(3.-x[1])))
      retrn=0.4572;
    else if
      ((x[0]>= (8.0-sqrt((x[1]+3.)*(3.-x[1]))))&&
       (x[0]<= (14.0-sqrt((x[1]+3.)*(3.-x[1])))))
      retrn=0.4572+(8.-sqrt((x[1]+3.)*(3.-x[1]))-x[0])/20.0;
    else
      retrn=0.1572;

    values[0]=retrn;
  }
};

class SpongeLayer : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double n1=1.5;
    double n2=1.5;
    double xs=15.;
    double x2=20.;

    double retrn=0.0;
    if (x[0]>xs)
      {
        double ldef=(x[0]-xs)/(x2-xs);
        double px=std::pow(ldef,n2);
        retrn=n1*(exp(px)-1.)/(exp(1.)-1.);
      }
    else retrn=0.0;

    values[0]=retrn;
  }
};

class DBC : public Expression
{
public:
  DBC(const double& t): Expression(2), t(t) {}
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double time=t;
    double a=0.01;
    double omega=3.1142485275;
    values[0]=a*sin(-omega*time); /*Elevation*/
    double b=0.044861357;
    double k=1.6845;
    values[1]=-(b/k)*cos(-omega*time)+b/k;/*Potential*/
  }
private:
  const double& t;
};



int main(int argc,char *argv[])
{
  /// Options, Initialization, Mesh and User functions
  Dolfwave dw(
              200000 /*MaxSteps*/,
              0.0001 /*TimeStep*/,
              100 /*WriteGap*/,
              "Zhao" /*FormName*/,
              "GMRES_P" /*AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "pvd"/*OutFormat*/,
              "Shoaling slope test with Zhao et al.:mpirun -n # 2HD_periodiczhao_demo. Post-processing could be done with Paraview for instance"
              );
  info(parameters,true);
  RectangleMesh mesh(0,-3,20.,3.,120,40,"crossed");
  Depth depth;//Depth
  SpongeLayer sponge;//Sponge Layer
  Constant zero(0.0);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth);//BilinearForm a initialization
  DirichletBoundary dirichletboundary;
  DBC bc(dw.Time);
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LinearFormsInit(depth,sponge /*sp_eta*/,zero /*sp_lap_eta*/,
                     zero /*sp_phi*/,zero/*sp_lap_phi */,zero);
  dw.DepthPlot(mesh,depth,false);
  //dw.SpongePlot(mesh,sponge,false);
  dw.Volume(mesh,depth);
  dw.DolfwaveDBC(bc,dirichletboundary); //Setting Dirichlet Boundary Conditions
  dw.LUFactorization(true);
  dw.RKInit(/*RKType "euler" or the default "exp4"*/ );
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
  dw.PCInit(mesh,false);
  while( dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      dw.Info(false);
      dw.Volume(mesh,depth);
      if(!(dw.Step%dw.WriteGap))
        dw.SaveEta();//Save Eta
    }
  return (EXIT_SUCCESS);
}

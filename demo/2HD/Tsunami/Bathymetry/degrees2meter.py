from pylab import *
from numpy import fromfile

outfile = open('PtCoastMeters.xyz', 'w')


filename='PtCoast-9421.xyz'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//3,3
us=ones((len(u1),3))

scale=111319

for i in range(0,len(u1)): #len(u1)):
    us[i,0]=u1[i,0]
    us[i,1]=u1[i,1]
    us[i,2]=u1[i,2]
    #print str(scale*math.cos(us[i,1]*math.pi/180)*us[i,0])+' '+str(scale*us[i,1])+' '+str(us[i,2])
    if (us[i,2]>-1.):
        z=1
    else:
        z=-us[i,2]
    strng=str(scale*us[i,0])+' '+str(scale*us[i,1])+' '+str(z)+'\n'
    outfile.write(strng)


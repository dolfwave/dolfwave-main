#include <dolfwave.h>
#include "Proj.h"

using namespace dolfin;

class DepthData{
public:
  DepthData(int s){
    size=s;
    X=new double[size];
    Y=new double[size];
    Z=new double[size];
  }

  int vsize(void ){return size;}
  double getX(int i){return X[i];}
  double getY(int i){return Y[i];}
  double getZ(int i){return Z[i];}
  void putX(int i,double value){X[i]=value;}
  void putY(int i,double value){Y[i]=value;}
  void putZ(int i,double value){Z[i]=value;}
  private:
  int size;
  double *X;
  double *Y;
  double *Z;
};


class Depth : public Expression
{
public:
  Depth(DepthData &noaa) : noaa(noaa) {}

  void eval(Array<double> & values,const Array<double> & x) const
  {
    //Define a threshold for interpolation
    double xx=x[0];
    double yy=x[1];
    //We have to consider the simmetric of the Depth
    double zz=-20000; //Initial Bathymetry (for Debugging)
    double threshold=5000*5000; //distance threshold m^2
    double dist=threshold;


    for(int i=0;i<noaa.vsize();i++)
      {
        dist=((xx-noaa.getX(i))*(xx-noaa.getX(i))
              +(yy-noaa.getY(i))*(yy-noaa.getY(i)));
        if (dist<threshold)
          {
            zz=noaa.getZ(i);
            threshold=dist; //Decrease threshold for better interpolation
          }
          }
    values[0]=zz;
  }
private:
  DepthData &noaa;
};

int main()
{
  Mesh mesh("../Mesh/xml/CoastalLineRefined.xml");
  DepthData noaa(108661);
  double value;
  std::ifstream infile("PtCoastMeters.xyz",std::ios::in);

  for(int i=0;i<noaa.vsize();i++)
    {
      infile >>value; noaa.putX(i,value);
      infile >>value; noaa.putY(i,value);
      infile >>value; noaa.putZ(i,value);
    }
  Proj::FunctionSpace V(mesh);
  Proj::BilinearForm a(V,V);
  Proj::LinearForm L(V);
  Depth depth(noaa);
  L.depth=depth;
  Function bathymetry(V);
  solve(a == L , bathymetry);


  // Plot mesh function
  plot(bathymetry);

  // Write mesh function to file (new style)
  File out("Bathymetry.pvd");
  out << bathymetry;
  File outxml("Bathymetry.xml");
  outxml << bathymetry;
}

// Copyright (C) 2009 Nuno David Lopes.---------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This program provides a demo of the DOLFWAVE library.
// Wave generation by a moving bottom: Zhao formulation
// A toy tsunami in a real (unscaled geometry)

#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;


const double old_left_X=0.0;
const double old_bottom_Y=0.0;
const double old_right_X= 700. ;
const double old_top_Y= 582. ;
const double new_left_X=-2003742.0; //Real Scales
const double new_bottom_Y=3673527.0;
const double new_right_X=-667914.0;
const double new_top_Y=4786717.0;


int main()
{

  Mesh msh("Cropedmesh3.xml");
  Mesh scaled_msh(msh);
  File out("RealScale.xml");

  Rescale(scaled_msh,old_left_X,old_bottom_Y,
          old_right_X,old_top_Y,new_left_X,
          new_bottom_Y,
          new_right_X,new_top_Y);

  out<<scaled_msh;
  plot(msh);
  plot(scaled_msh);

  return (EXIT_SUCCESS);
}

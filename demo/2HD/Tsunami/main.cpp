// Copyright (C) 2011 Nuno David Lopes.---------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This program provides a demo of the DOLFWAVE library.
// Wave generation by a moving bottom: Zhao formulation
// A Tsunami in the portuguese Coast.
// Reflective walls in all the boundaries.

#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;

const double xS=-1.25e6; // X-coordinate for the centre of the Quake
const double yS=4.0e6;  // Y-coordinate for the centre of the Quake
const double yL=3.75e6;  // Y-coordinate for line of vanishing layer (bottom)
const double yU=4.7e6;  // Y-coordinate for line of vanishing layer (bottom)
const double xL=-1.9e6;  // X-coordinate for line of vanishing layer (left)
const double MAXPERBATH=5.; //Maximum perturbation over the bathymetry
const int maxstepnumber=120000;
const double timestep=0.05;
const double endtime=4.0;
const double gama=3.e-9;

class Quake : public Expression
{
public:
  Quake(const double &t): t(t) {}

  void eval(Array<double> &values, const Array<double> & x) const
  {
    double time=t;
    if (time<endtime)
      values[0]=-MAXPERBATH*sin(DW_PI*time/(2.0*endtime))*
        exp(-gama*((x[0]-xS)*(x[0]-xS)+(x[1]-yS)*(x[1]-yS))) ;
    else values[0]=0.0;
  }
private:
  const double &t;
};

class Source : public Expression
{
public:
  Source(const double &t): t(t) {}

  void eval(Array<double> & values, const Array<double> & x) const
  {
    double time=t;

     if (time<endtime)
       values[0]=MAXPERBATH*DW_PI/(2*endtime)*cos(DW_PI*time/(2.*endtime))*
        exp(-gama*((x[0]-xS)*(x[0]-xS)+(x[1]-yS)*(x[1]-yS))) ;
    else values[0]=0.0;
  }
private:
  const double &t;
};

class RadiationBoundaries  : public Expression
{
public:
  void eval(Array<double> & values, const Array<double> & x) const
  {
    if ((x[1]<yL)||(x[0]<xL)||(x[1]>yU))
      values[0]=DW_HUGE;
    else values[0]=1.0;
  }
};

class VanishingLayer  : public Expression
{
public:
  void eval(Array<double> & values, const Array<double> & x) const
  {
    if (x[1]<yL)
      values[0]=exp(-0.3e-14*((x[1]-yL)*(x[1]-yL)));
    else if (x[1]>yU)
      values[0]=exp(-0.3e-14*((x[1]-yU)*(x[1]-yU)));
    else if (x[0]<xL)
       values[0]=exp(-0.3e-14*((x[0]-xL)*(x[0]-xL)));
    else values[0]=1.0;
  }
};


int main()
{

  Dolfwave dw(maxstepnumber /*MaxSteps*/,
              timestep /*TimeStep*/,
              100 /*WriteGap*/,
              "Zhao"/*Form Name*/,
              "GMRES_P"/* AlgBackEND*/ ,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "pvd"/*OutFormat*/);
  Mesh msh("Mesh/xml/CoastalLineRefined.xml");
  Quake quake(dw.Time);//time variable depth
  Constant zero(0.0);
  Source srceta(dw.Time);//Source Function
  RadiationBoundaries rbs;
  VanishingLayer vlayer;

  dw.SpacesFunctionsVectorsInit(msh);//Init. Spaces Functions & Vectors
  dw.LoadBathymetry(msh,"Bathymetry.xml",false);
  dw.BilinearFormInit(msh,quake,rbs);// Bilinear form initialization (2)
  dw.LinearFormsInit(quake,zero,zero,zero,zero,srceta,rbs);//Linear form initialization


  dw.MatricesAssemble( );//Matrices Assemble
  dw.RKInit( );
  dw.RKSolve("MatricesReAssemble"); //first 3 steps
  dw.SaveEta(1); // Saving initial step
  dw.DepthPlot(msh,quake,false);
  parameters("dolfwave_parameters")["pred_corr_epsilon"]=0.01;
  dw.PCInit(msh,true); //P.Corrector initialization
  while(dw.Step<dw.MaxSteps)
    {
      // We just need to reassemble when the bottom is changing
      if (dw.Time<endtime)
          dw.MatricesAssemble();

      dw.PCSolve( );//PC step
      dw.ApplyVanishingLayer(vlayer);
      if (!(dw.Step%dw.WriteGap))
        {
          dw.SaveEta(); //Saving eta
          dw.HorVelPlot(msh,false,true); //Horizontal Velocities
        }
      dw.Info(false);
    }

  return (EXIT_SUCCESS);
}

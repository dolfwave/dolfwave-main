// {{{  Copyright (C) 2009 Nuno David Lopes.---------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by a moving bottom: Zhao formulation
// Using Viper  pr� post-processor and Xd3d with the files output.
#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;

class Depth : public Expression
{
public:
  Depth(const double &t): t(t) {}

  void eval(Array<double> &values, const Array<double> & x) const
  {
    double time=t;
    double endtime=0.5;
    double D=0.04575;
    double xS=8.0;
    double omega=DOLFIN_PI;
    double gamma=3.0;
    //(Time derivative of Depth)(x-1)
    if (time<endtime)
      values[0]=0.4572-D*sin(omega*time)*
        exp(-gamma*((x[0]-xS)*(x[0]-xS)+(x[1]-xS)*(x[1]-xS))) ;
    else values[0]=0.4572;
  }
private:
  const double &t;
};
class Source : public Expression
{
public:
  Source(const double &t): t(t) {}

  void eval(Array<double> & values, const Array<double> & x) const
  {
    double time=t;
    double endtime=0.5;
    double D=0.04572;
    double xS=8.0;
    double omega=DOLFIN_PI;
    double gamma=3.0;
    if (time<endtime)
      values[0]=D*omega*cos(omega*time)*
        exp(-gamma*((x[0]-xS)*(x[0]-xS)+(x[1]-xS)*(x[1]-xS))) ;
    else values[0]=0.0;
  }
private:
  const double &t;
};

int main()
{
  Dolfwave dw(
              40000  /*MaxSteps*/,
              0.00025 /*TimeStep*/,
              50 /*WriteGap*/,
              "Zhao"/*Form Name*/,
              "LU_P"/* AlgBackEND*/ ,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/
              );
  Mesh mesh("square.xml");
  mesh.order();
  Depth depth(dw.Time);//time variable depth
  Constant sponge(0.0);//Sponge Layer
  Source srceta(dw.Time);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth);// Bilinear form initialization (2)
  dw.LinearFormsInit(depth,sponge,sponge,sponge,sponge,srceta);//Linear form initialization
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false);//Linear solver (limited yet)
  dw.RKInit( );
  dw.RKSolve("MatricesReAssemble"); //first 3 steps
  dw.PCInit(mesh); //P.Corrector initialization
  dw.DepthPlot(mesh,depth,false);
  while(dw.Step <dw.MaxSteps)
    {
      // We just need to reassemble when the bottom is changing
      // ENDTIME=2000*dw.TimeStep
      if (dw.Step<2001)
          dw.MatricesAssemble();

      // Now lets get
      // to the Factorized LU
      if (dw.Step==2001)
        dw.LUFactorization(true);

      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
          dw.SaveEta();//Save Eta
          dw.DepthPlot(mesh,depth,false);
        }

      dw.PointPlot(8.,8.,"eta","output/p1.te");
      dw.PointPlot(8.,8.,"phi","output/p1.tp");
      dw.PointPlot(0.,0.,"eta","output/p2.te");
      dw.PointPlot(0.,0.,"phi","output/p2.tp");
      dw.Volume(mesh,depth,"output/volume.tv");

      info("Step=%d \t Simulated time= %.4f",dw.Step,dw.Time);}

  return (EXIT_SUCCESS);
}

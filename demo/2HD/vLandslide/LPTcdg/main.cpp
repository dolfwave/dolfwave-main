// {{{  Copyright (C) 2009 Nuno David Lopes.---------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by an impulsive moving bottom: LPTcdg method
//  Using an inner penalty CDG method
//  See mvbottom for CG
//  Using Viper  pr� post-processor and Xd3d with the files output.
#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;



// End time for the moving bottom
const double ENDTIME=0.5;

class Depth : public Expression
{
 public:
  Depth (const double& t): t(t){}
  void eval(Array<double> & values, const Array<double> & x) const
  {
    double time=t;
    double endtime=ENDTIME;
    double C=0.4572;
    double D=0.04572;
    double X0=8.0;
    double Y0=8.0;
    double w=3.14159265;//omega
    double g=3.0;//gamma
    double X=x[0];
    double Y=x[1];
    if (time<endtime)
       values[0]=C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w);
     else values[0]=C;
   }
private:
   const double& t;
 };

class DtDepth : public Expression
 {
  public:
   DtDepth (const double& t): t(t) {}
   void eval(Array<double> & values, const Array<double> & x) const
   {
    double time=t;
    double endtime=ENDTIME;
    double D=0.04572;
    double X0=8.0;
    double Y0=8.0;
    double w=3.14159265;//omega
    double g=3.0;//gamma
    double X=x[0];
    double Y=x[1];
    if (time<endtime)
       values[0]=-D*w*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)));
     else values[0]=0.0;
   }
 private:
   const double& t;
 };

class DtDtDepth : public Expression
 {
  public:
   DtDtDepth (const double& t): t(t) {}
   void eval(Array<double> & values, const Array<double> & x) const
   {
    double time=t;
    double endtime=ENDTIME;
    double D=0.04572;
    double X0=8.0;
    double Y0=8.0;
    double w=3.14159265;//omega
     double g=3.0;//gamma
    double X=x[0];
    double Y=x[1];
    if (time<endtime)
       values[0]=D*pow(w,2)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w);
     else values[0]=0.0;
   }
 private:
   const double& t;
 };

class SourceEta : public Expression
{
  public:
  SourceEta (const double& t): t(t) {}
  void eval(Array<double> & values, const Array<double> & x) const
   {
    double time=t;
    double endtime=ENDTIME;
    double C=0.4572;
    double D=0.04572;
    double X0=8.0;
    double Y0=8.0;
    double w=3.14159265;//omega
    double g=3.0;//gamma
    double X=x[0];
    double Y=x[1];
    double alpha=0.5917517028;
    double beta1=4.971623160e-7;
    double B3=pow(alpha,3)/3.-sqr(alpha)+5./6.*alpha-alpha*beta1/2.-5./24.;
    if (time<endtime)
       values[0]=B3*pow((C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w)),4)*(-32*D*w*pow(g,2)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) - D*w*pow(g,4)*pow((-2*X0 + 2*X),4)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) - D*w*pow(g,4)*pow((-2*Y0 + 2*Y),4)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) + 12*D*w*pow(g,3)*pow((-2*X0 + 2*X),2)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) + 12*D*w*pow(g,3)*pow((-2*Y0 + 2*Y),2)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) + D*w*pow(g,3)*(-8*X0 + 8*X)*(-2*X0 + 2*X)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) + D*w*pow(g,3)*(-8*Y0 + 8*Y)*(-2*Y0 + 2*Y)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) - 2*D*w*pow(g,4)*pow((-2*X0 + 2*X),2)*pow((-2*Y0 + 2*Y),2)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))) + alpha*(1.0 - beta1)*((C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*(-2*g*w*pow(D,2)*cos(t*w)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + 3*w*pow(D,2)*pow(g,2)*pow((-2*X0 + 2*X),2)*cos(t*w)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + 2*D*g*w*(C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) - D*w*pow(g,2)*pow((-2*X0 + 2*X),2)*(C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))) + (C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*(-2*g*w*pow(D,2)*cos(t*w)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + 3*w*pow(D,2)*pow(g,2)*pow((-2*Y0 + 2*Y),2)*cos(t*w)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + 2*D*g*w*(C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) - D*w*pow(g,2)*pow((-2*Y0 + 2*Y),2)*(C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))) + D*g*(-2*X0 + 2*X)*(-g*w*pow(D,2)*(-2*X0 + 2*X)*cos(t*w)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + D*g*w*(C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*(-2*X0 + 2*X)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))))*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + D*g*(-2*Y0 + 2*Y)*(-g*w*pow(D,2)*(-2*Y0 + 2*Y)*cos(t*w)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + D*g*w*(C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*(-2*Y0 + 2*Y)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))))*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w)) - w*pow(D,2)*pow(g,2)*pow((-2*X0 + 2*X),2)*(C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*cos(t*w)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) - w*pow(D,2)*pow(g,2)*pow((-2*Y0 + 2*Y),2)*(C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*cos(t*w)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + D*w*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) - 2*D*g*w*pow((C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w)),2)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) + 0.5*D*w*pow(g,2)*pow((C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w)),2)*pow((-2*X0 + 2*X),2)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2))) + 0.5*D*w*pow(g,2)*pow((C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w)),2)*pow((-2*Y0 + 2*Y),2)*cos(t*w)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)));
     else values[0]=0.0;
   }
private:
   const double& t;
 };

class SourcePhi : public Expression
{
 public:
  SourcePhi (const double& t): t(t) {}
  void eval(Array<double> & values, const Array<double> & x) const
   {
    double time=t;
    double endtime=ENDTIME;
    double C=0.4572;
    double D=0.04572;
    double X0=8.0;
    double Y0=8.0;
    double w=3.14159265;//omega
     double g=3.0;//gamma
    double X=x[0];
    double Y=x[1];
    double alpha=0.5917517028;
    double B4=-pow(alpha,3)/3.+sqr(alpha)-alpha/2.;
    if (time<endtime)
        values[0]=alpha*(D*pow(w,2)*(C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w))*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + pow(D,2)*pow(w,2)*pow(cos(t*w),2)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)))) - B4*pow((C - D*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w)),3)*(-4*D*g*pow(w,2)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + D*pow(g,2)*pow(w,2)*pow((-2*X0 + 2*X),2)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w) + D*pow(g,2)*pow(w,2)*pow((-2*Y0 + 2*Y),2)*exp(-g*(pow((X - X0),2) + pow((Y - Y0),2)))*sin(t*w)) - 0.5*pow(D,2)*pow(w,2)*pow(cos(t*w),2)*exp(-2*g*(pow((X - X0),2) + pow((Y - Y0),2)));
    else values[0]=0.0;
   }
private:
   const double& t;
 };


int main()
{
  Dolfwave dw(
              40000  /*MaxSteps*/,
              0.00025 /*TimeStep*/,
              50 /*WriteGap*/,
              "LPTcdg" /*FormName*/,
              "LU_P"/* AlgBackEND*/ ,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/,
              "With no higher-order nonlinear terms. In this example we start with non factorized LU and, when no changes to the system matrices occurs,we change to Factorized solver. Constant depth component is used in Bi/LinearFomrs higher order terms." /*Comments*/
              );
  info(parameters,true);
  //dw.SetParameter("SymmetrySwitch",0.0);
  dw.SetParameter("PenaltyTau",0.1);
  dw.SetParameter("Alpha",1.-sqrt(7.)/7);
  dw.SetParameter("Beta1",0.0);
  dw.SetParameter("Beta2",0.0);
  RectangleMesh mesh(0.,0.,20.,20.,35,35,"crossed");
  mesh.order();
  Depth depth(dw.Time);//time variable depth
  Constant ctd(0.4572);//constant component of depth
  DtDepth dtdepth(dw.Time);
  DtDtDepth dtdtdepth(dw.Time);
  Constant sponge(0.0);//Sponge Layer
  Constant tension(0.0);//Sponge Layer
  SourceEta srceta(dw.Time);//Source Function
  SourcePhi srcphi(dw.Time);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,ctd);// Bilinear form initialization
  dw.LinearFormsInit(depth,ctd,dtdepth,dtdtdepth,
                     sponge,tension,srceta,srcphi);//Linear form initialization
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false);//Do not reuse LU Factorization
  dw.Info(0); // Some extra information

  dw.SaveEta(0); //Save initial step
  dw.DepthPlot(mesh,depth,false);
  dw.RKInit();
  dw.RKSolve("MatricesReAssemble"); //first 3 steps
  dw.PCInit(mesh); //P.Corrector initialization
  // Predictor-Corrector
  while( dw.Step<dw.MaxSteps)
    {
      // We just need to reassemble when the bottom is changing
      // ENDTIME=2000*dw.TimeStep
      if (dw.Step<2001)
          dw.MatricesAssemble();

      // Now lets get
      // to the Factorized LU
      if (dw.Step==2001)
        dw.LUFactorization(true);

      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
          dw.SaveEta(); //Saving Eta
          dw.DepthPlot(mesh,depth,false);
        }

      dw.PointPlot(8.,8.,"eta","output/p1.te");
      dw.PointPlot(8.,8.,"phi","output/p1.tp");
      dw.PointPlot(0.,0.,"eta","output/p2.te");
      dw.PointPlot(0.,0.,"phi","output/p2.tp");
      dw.Volume(mesh,depth,"output/volume.tv");


      info("Step=%d \t Simulated time= %.4f",dw.Step,dw.Time);}
  return (EXIT_SUCCESS);
}

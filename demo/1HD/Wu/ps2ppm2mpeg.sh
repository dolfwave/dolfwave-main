# Animation script
#!/bin/sh



for ((i=0;i<400;i+=1))
do  stri=$(printf "%04d" $i);
    title=$(printf "(Wu_fKdV_example)_with_LPT_CDG-FEM");
    xgraphic  -pstex=eta$i.ps -big1  -titgen=$title -titx="m" -tity="m" -legtype=1  -leg1="4th-order" -leg2="bottom" -leg3="2nd-order"   -legpos=1 -mark=2 -markco=0  -ymax=1.0 LPTcdg/output/eta00$stri.xyz LPTcdg/output/depth00$stri.xyz  Zhao/output/eta00$stri.xyz;
convert eta$i.ps eta$i.ppm;
done;


ppmtompeg mpeg2encode;

rm -f *.ps *.ppm;





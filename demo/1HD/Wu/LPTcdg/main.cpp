// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by a moving bottom: Modified 4th-order equations
//  Using an inner penalty CDG method
//  To compile with DOLFIN 0.9.9
#include "Depth.h"

using namespace dolfin;
using namespace dolfin::dolfwave;

int main()
{
  Dolfwave dw(
              400000  /*MaxSteps*/,
              0.0001 /*TimeStep*/,
              1000 /*WriteGap*/,
              "LPTcdg_1D" /*FormName*/,
              "LU_P"/* SolverType*/ ,
              "gnuplot" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/,
              "Wu 81 test with LPT equations and CDG method" /*Comments*/
              );
  info(parameters,true);
  IntervalMesh mesh(701,-40.,100.);
  Depth depth(dw.Time);//time variable depth
  Constant ctd(1.0);//constant component of depth
  Constant dtdepth(0.0);
  Constant dtdtdepth(0.0);
  Constant sponge(0.0);//Sponge Layer
  Constant tension(0.0);//Sponge Layer
  SourceEta srceta(dw.Time);//Source Function
  Constant srcphi(0.0);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,ctd);// Bilinear form initialization
  dw.LinearFormsInit(depth,ctd,dtdepth,dtdtdepth,
                     sponge,tension,srceta,srcphi);//Linear form init
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false);// Do not re-use LU factorization
  dw.Info(0); // Some extra information
  dw.RKInit("exp4");
  dw.PreviewEta(1);//Preview and save the first eta solution
  dw.SaveEta(1);
  dw.DepthPlot(mesh,depth,false);
  dw.RKSolve("MatricesReAssemble"); //first 3 steps
  dw.PCInit(mesh,false); //PredCorr  with no multi-step correction
  while(dw.Step<dw.MaxSteps)
    {
      dw.MatricesAssemble();
      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
          //dw.PreviewEta();
          dw.SaveEta();
          dw.DepthPlot(mesh,depth,false);
          dw.Volume(mesh,depth,"volume.tv");
        }
      dw.Info(false);
    }
  return (EXIT_SUCCESS);
}

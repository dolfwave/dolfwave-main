
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.h
//Thu Jun 24 13:49:15 WEST 2010
//
// To compile  with DOLFIN:0.9.7
// }}}------------------------------------------------------------------------

#ifndef __DEPTH_H
#define __DEPTH_H

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;


class Depth : public Expression
{
 public:
  Depth (const double& t): t(t)
  {}
  void eval(Array<double> & values, const Array<double> & x) const;
private:
   const double& t;
 };


class SourceEta : public Expression
{
  public:
   SourceEta (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
private:
   const double& t;
 };


#endif

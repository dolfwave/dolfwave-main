
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.cpp
//Thu Jun 24 13:49:15 WEST 2010
//
// To compile  with DOLFIN:0.9.7
// }}}------------------------------------------------------------------------


#include "Depth.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


void Depth::eval(Array<double> & values, const Array<double> & x) const
  {
    double Dh=0.1;
    double C0=3.1321;
    double X=x[0];
    double XT=X-C0*t;
    if ((XT<1.)*(XT>-1.))
      values[0]=1.0 - Dh*(1.0 + cos(3.14159265*X - 3.14159265*C0*t));
    else
      values[0]=1.0;
   }




void SourceEta::eval(Array<double> & values, const Array<double> & x) const
   {
    double Dh=0.1;
    double C0=3.1321;
    double X=x[0];
    double XT=X-C0*t;
    if ((XT<1.)*(XT>-1.))
     values[0]=3.14159265*C0*Dh*sin(3.14159265*X - 3.14159265*C0*t);
   else
     values[0]=0.0;
   }
 

// Copyright (C) 2009 Nuno David Lopes.----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by a moving bottom: Modified 2nd-order equations
//  To compile with DOLFIN 0.9.9

#include "Depth.h"
using namespace dolfin;
using namespace dolfin::dolfwave;

int main()
{
  Dolfwave dw(
              260000  /*MaxSteps*/,
              0.0001 /*TimeStep*/,
              1000 /*WriteGap*/,
              "Zhao_1D" /*FormName*/,
              "LU_P"/* solver type*/ ,
              "gnuplot" /*Plotter*/,
              "output" /*OutDir*/,
              "pvd"/*OutFormat*/,
              "Case by Wu (fKdV equations 81)  sugested and data provided by R.Barros" /*Comments*/
              );
  IntervalMesh mesh(1401,-40.,100.);
  Depth depth(dw.Time);//time variable depth
  Constant zero(0.0);//Sponge Layer and transparent bcs penalization
  SourceEta srceta(dw.Time);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,zero);// Bilinear form initialization
  dw.LinearFormsInit(depth,zero,zero,
                     zero,zero,srceta,zero);//Linear form initialization
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false);//do not re-use LU factorization
  dw.Info(0); // Some extra information
  dw.RKInit("exp4");
  dw.RKSolve("MatricesReAssemble"); //first 3 steps
  dw.PreviewEta(3);// Preview and save eta at third step
  dw.SaveEta(3);
  dw.DepthPlot(mesh,depth,false);
  dw.PCInit(mesh,false); //PredCor  with no multi-step correction
  while( dw.Step<dw.MaxSteps)
    {
      dw.MatricesAssemble();
      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
          //dw.PreviewEta();
          dw.SaveEta();
          dw.DepthPlot(mesh,depth,false);
          dw.Volume(mesh,depth);
        }
      dw.Info(false);
    }
  return (EXIT_SUCCESS);
}

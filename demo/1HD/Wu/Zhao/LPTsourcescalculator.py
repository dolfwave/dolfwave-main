## {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
##
## e-mail: ndl@ptmat.fc.ul.pt
## http://ptmat.fc.ul.pt/~ndl/
##
## ---------------------------------------------------------------------------
## Licensed under the GNU LGPL Version 3.0 or later.
## ---------------------------------------------------------------------------
##  LPTsourcescalculator.py
##
## First added:  2009-10-19
## Last changed: 2010-06-24
##
## }}}------------------------------------------------------------------------

from sympy import *
from sympy import __version__ as sympy_version
import commands

print 'sympy version is '+ sympy_version
print 'Works with versions >= SymPy 0.6.3'


#Use capital X  to simplify integration with DOLFIN

X=Symbol('X',real=True, unbounded=True)
t=Symbol('t',real=True, unbounded=True)

#define the depth function

Dh=Symbol('Dh',real=True, unbounded=True)
B3=Symbol('B3',real=True, unbounded=True)
B4=Symbol('B4',real=True, unbounded=True)
alpha=Symbol('alpha',real=True, unbounded=True)
beta1=Symbol('beta1',real=True, unbounded=True)
C0=Symbol('C0',real=True, unbounded=True)


## DADOS

_Dh=0.1
_C0=3.1321 ## velocity in m/s


## Depth definition ##


def h(X,t):
    return 1.-Dh*(1.+cos(3.14159265*(X-C0*t)))


#####################################################
print 'the depth function h'
depthpy=h(X,t)
depth=ccode(depthpy)
print depthpy
print '\n'



#### OUTPUT ######
print 'first time derivative of h dh/dt\n'
dhdt=diff(h(X,t),t)
dtdepth=ccode(dhdt)
print dhdt
print'\n'

print 'Source function for eta is S_1(h)=\n'

srcetapy= -diff(h(X,t),t)


srceta= ccode(together(srcetapy))

print srcetapy
print '\n'





######## print an Depth.h and Depth.cpp file to use #include "Depth.h" in the main.cpp ##########
date=commands.getoutput('date')
file = open('Depth.h','w')
print >>file, '''
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.h
//'''+str(date)+'''
//
// To compile  with DOLFIN:0.9.7
// }}}------------------------------------------------------------------------

#ifndef __DEPTH_H
#define __DEPTH_H

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;


class Depth : public Expression
{
 public:
  Depth (const double& t): t(t)
  {}
  void eval(Array<double> & values, const Array<double> & x) const;
private:
   const double& t;
 };


class SourceEta : public Expression
{
  public:
   SourceEta (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
private:
   const double& t;
 };


#endif'''

file = open('Depth.cpp','w')

print >>file, '''
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.cpp
//'''+str(date)+'''
//
// To compile  with DOLFIN:0.9.7
// }}}------------------------------------------------------------------------


#include "Depth.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


void Depth::eval(Array<double> & values, const Array<double> & x) const
  {
    double Dh='''+str(_Dh)+''';
    double C0='''+str(_C0)+''';
    double X=x[0];
    double XT=X-C0*t;
    if ((XT<1.)*(XT>-1.))
      values[0]='''+str(depth)+''';
    else
      values[0]=1.0;
   }




void SourceEta::eval(Array<double> & values, const Array<double> & x) const
   {
    double Dh='''+str(_Dh)+''';
    double C0='''+str(_C0)+''';
    double X=x[0];
    double XT=X-C0*t;
    if ((XT<1.)*(XT>-1.))
     values[0]='''+str(srceta)+''';
   else
     values[0]=0.0;
   }
 '''

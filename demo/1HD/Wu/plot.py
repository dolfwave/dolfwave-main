import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 1,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Wu 81(test with fKdV) Simulated with Zhao Vs LPTcdg.'
ax=subplot(111,autoscale_on='false')
subplots_adjust(left=0.13,bottom=0.13,right=0.92,top=0.92)

step=1
LMAX=65
LSTEP=5

for j in range (0,LMAX):
    if j==0:
        filename='LPTcdg/output/eta000000.xyz'
    elif j*LSTEP<10:
        filename='LPTcdg/output/eta00000'+str(j*LSTEP)+'.xyz'
    elif j*LSTEP<100:
        filename='LPTcdg/output/eta0000'+str(j*LSTEP)+'.xyz'
    elif j*LSTEP<1000:
        filename='LPTcdg/output/eta000'+str(j*LSTEP)+'.xyz'
    else:
        filename='LPTcdg/output/eta00'+str(j*LSTEP)+'.xyz'

    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=3.5+j*0.05+u1[i*step,1]
    if j==30:
        ax.plot(us[:,0], us[:,1],"b", label='')
    else :
        ax.plot(us[:,0], us[:,1],"b", label='',alpha=0.3)

step=1
LMAX=50
LSTEP=5

for j in range (0,LMAX):
    if j==0:
        filename='Zhao/output/eta000000.xyz'
    elif j*LSTEP<10:
        filename='Zhao/output/eta00000'+str(j*LSTEP)+'.xyz'
    elif j*LSTEP<100:
        filename='Zhao/output/eta0000'+str(j*LSTEP)+'.xyz'
    elif j*LSTEP<1000:
        filename='Zhao/output/eta000'+str(j*LSTEP)+'.xyz'
    else:
        filename='Zhao/output/eta00'+str(j*LSTEP)+'.xyz'

    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=j*0.05+u1[i*step,1]
    if j==30:
        ax.plot(us[:,0], us[:,1],"b", label='')
    else :
        ax.plot(us[:,0], us[:,1],"b", label='',alpha=0.3)


for j in range (0,LMAX):
    if j==0:
        filename='LPTcdg/output/depth000000.xyz'
    elif j*LSTEP<10:
        filename='LPTcdg/output/depth00000'+str(j*LSTEP)+'.xyz'
    elif j*LSTEP<100:
        filename='LPTcdg/output/depth0000'+str(j*LSTEP)+'.xyz'
    elif j*LSTEP<1000:
        filename='LPTcdg/output/depth000'+str(j*LSTEP)+'.xyz'
    else:
        filename='LPTcdg/output/depth00'+str(j*LSTEP)+'.xyz'


    d1 = fromfile(filename,sep=" ")
    d1.shape=len(d1)//2,2
    ds=ones((len(d1)//(step),2))
    for i in range(0,len(d1)//(step)):
        ds[i,0]=d1[i*step,0]
        ds[i,1]=-2.0+j*0.05+d1[i*step,1]
    if j==30:
        ax.plot(ds[:,0], ds[:,1],"r", label='')
    else :
        ax.plot(ds[:,0], ds[:,1],"r", label='',alpha=0.3)



ax.legend(loc=1)


alignment = {'horizontalalignment':'center', 'verticalalignment':'baseline'}
family = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
font0 = FontProperties()
font1 = font0.copy()
font1.set_size(14)#
text0 = text(20., -1, '$-h(x,t)$', fontproperties=font1,**alignment)
text1 = text(32., 7.00, 'LPTcdgHnl\_1D', fontproperties=font1,**alignment)
text1 = text(52., .5, 'Zhao\_1D', fontproperties=font1,**alignment)

(xname, yname) = ('$x$', 'time $0-40$ (s)')
title(plottitle,fontsize=18)
ax.set_aspect(10.0)
ax.set_xlim(-40.0,100.)
ax.set_ylim(-3.1,8.5)
xlabel(xname,fontsize=24)
ylabel(yname,fontsize=24)
show()















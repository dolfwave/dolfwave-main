// Copyright (C) 2009 Nuno David Lopes.---------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by a moving bottom: Modified 2nd-order equations
//  Using an inner penalty CDG method
// -----------------------------------------------------------------------

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

// End time for the moving bottom
const double ENDTIME=1.;
const double ALPHA=1.-sqrt(7.)/7.;
const double BETA1=0.0;
const double BETA2=0.0;

class Depth : public Expression
{
public:
  Depth (const double& t): t(t)  {}
  void eval(Array<double> & values, const Array<double> & x) const
  {
    double time=t;
    double endtime=ENDTIME;
    double C=0.4572;
    double D=0.04572;
    double X0=8.0;
    double w=3.14159265;//omega
    double g=3.0;//gamma
    double X=x[0];
    if (time<endtime)
      values[0]=C - D*exp(-g*pow((X - X0),2))*sin(t*w);
    else values[0]=C;
  }
private:
  const double& t;
};

class DtDepth : public Expression
{
public:
  DtDepth (const double& t): t(t) {}
  void eval(Array<double> & values, const Array <double> & x) const
  {
    double time=t;
    double endtime=ENDTIME;
    double D=0.04572;
    double X0=8.0;
    double w=3.14159265;//omega
    double g=3.0;//gamma
    double X=x[0];
    if (time<endtime)
      values[0]=-D*w*cos(t*w)*exp(-g*pow((X - X0),2));
    else values[0]=0.0;
  }
private:
  const double& t;
};

class DtDtDepth : public Expression
{
public:
  DtDtDepth (const double& t): t(t) {}
  void eval(Array<double> & values, const Array<double> & x) const
  {
    double time=t;
    double endtime=ENDTIME;
    double D=0.04572;
    double X0=8.0;
    double w=3.14159265;//omega
    double g=3.0;//gamma
    double X=x[0];
    if (time<endtime)
      values[0]=D*pow(w,2)*exp(-g*pow((X - X0),2))*sin(t*w);
    else values[0]=0.0;
  }
private:
  const double& t;
};

class SourceEta : public Expression
{
public:
  SourceEta (const double& t): t(t) {}
  void eval(Array<double> & values, const Array<double> & x) const
  {
    double time=t;
    double endtime=ENDTIME;
    double C=0.4572;
    double D=0.04572;
    double X0=8.0;
    double w=3.14159265;//omega
    double g=3.0;//gamma
    double X=x[0];
    double alpha=ALPHA;
    double beta1=BETA1;
    double B3=pow(alpha,3)/3.-sqr(alpha)+5./6.*alpha-alpha*beta1/2.-5./24.;
    if (time<endtime)
      values[0]=B3*pow((C - D*exp(-g*pow((X - X0),2))*sin(t*w)),4)*(-12*D*w*pow(g,2)*cos(t*w)*exp(-g*pow((X - X0),2)) - D*w*pow(g,4)*pow((-2*X0 + 2*X),4)*cos(t*w)*exp(-g*pow((X - X0),2)) + 8*D*w*pow(g,3)*pow((-2*X0 + 2*X),2)*cos(t*w)*exp(-g*pow((X - X0),2)) + D*w*pow(g,3)*(-8*X0 + 8*X)*(-2*X0 + 2*X)*cos(t*w)*exp(-g*pow((X - X0),2))) + alpha*(1.0 - beta1)*((C - D*exp(-g*pow((X - X0),2))*sin(t*w))*(-2*g*w*pow(D,2)*cos(t*w)*exp(-2*g*pow((X - X0),2))*sin(t*w) + 3*w*pow(D,2)*pow(g,2)*pow((-2*X0 + 2*X),2)*cos(t*w)*exp(-2*g*pow((X - X0),2))*sin(t*w) + 2*D*g*w*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*cos(t*w)*exp(-g*pow((X - X0),2)) - D*w*pow(g,2)*pow((-2*X0 + 2*X),2)*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*cos(t*w)*exp(-g*pow((X - X0),2))) + D*g*(-2*X0 + 2*X)*(-g*w*pow(D,2)*(-2*X0 + 2*X)*cos(t*w)*exp(-2*g*pow((X - X0),2))*sin(t*w) + D*g*w*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*(-2*X0 + 2*X)*cos(t*w)*exp(-g*pow((X - X0),2)))*exp(-g*pow((X - X0),2))*sin(t*w)) - w*pow(D,2)*pow(g,2)*pow((-2*X0 + 2*X),2)*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*cos(t*w)*exp(-2*g*pow((X - X0),2))*sin(t*w) + D*w*cos(t*w)*exp(-g*pow((X - X0),2)) - D*g*w*pow((C - D*exp(-g*pow((X - X0),2))*sin(t*w)),2)*cos(t*w)*exp(-g*pow((X - X0),2)) + 0.5*D*w*pow(g,2)*pow((C - D*exp(-g*pow((X - X0),2))*sin(t*w)),2)*pow((-2*X0 + 2*X),2)*cos(t*w)*exp(-g*pow((X - X0),2));
    else values[0]=0.0;
  }
private:
  const double& t;
};

class SourcePhi : public Expression
{
public:
  SourcePhi (const double& t): t(t)
  {}
  void eval(Array<double> & values, const Array<double> & x) const
  {
    double time=t;
    double endtime=ENDTIME;
    double C=0.4572;
    double D=0.04572;
    double X0=8.0;
    double w=3.14159265;//omega
    double g=3.0;//gamma
    double X=x[0];
    double alpha=ALPHA;
    double B4=-pow(alpha,3)/3.+sqr(alpha)-alpha/2.;
    if (time<endtime)
      values[0]=alpha*(D*pow(w,2)*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*exp(-g*pow((X - X0),2))*sin(t*w) + pow(D,2)*pow(w,2)*pow(cos(t*w),2)*exp(-2*g*pow((X - X0),2))) - B4*pow((C - D*exp(-g*pow((X - X0),2))*sin(t*w)),3)*(-2*D*g*pow(w,2)*exp(-g*pow((X - X0),2))*sin(t*w) + D*pow(g,2)*pow(w,2)*pow((-2*X0 + 2*X),2)*exp(-g*pow((X - X0),2))*sin(t*w)) - 0.5*pow(D,2)*pow(w,2)*pow(cos(t*w),2)*exp(-2*g*pow((X - X0),2));
    else values[0]=0.0;
  }
private:
  const double& t;
};

int main()
{
  Dolfwave dw(
              40000  /*MaxSteps*/,
              0.00025 /*TimeStep*/,
              50 /*WriteGap*/,
              "LPTcdg_1D" /*FormName*/,
              "LU_P"/* AlgBackEND*/ ,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/,
              "With no higher-order nonlinear terms. In this example we start with non factorized LU and, when no changes to the system matrices occur,we change to Factorized solver. Constant depth component is used in Bi/LinearFomrs." /*Comments*/
              );
  dw.SetParameter("Alpha",ALPHA);
  dw.SetParameter("Beta1",BETA1);
  dw.SetParameter("Beta2",BETA2);

  IntervalMesh mesh(101,0.,20.);
  Depth depth(dw.Time);//time variable depth
  Constant ctd(0.4572);//constant component of depth
  DtDepth dtdepth(dw.Time);
  DtDtDepth dtdtdepth(dw.Time);
  Constant sponge(0.0);//Sponge Layer
  Constant tension(0.0);//Sponge Layer
  SourceEta srceta(dw.Time);//Source Function
  SourcePhi srcphi(dw.Time);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,ctd);// Bilinear form initialization
  dw.LinearFormsInit(depth,ctd,
                     dtdepth,dtdtdepth,
                     sponge,tension,
                     srceta,srcphi);//Linear form initialization
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false);// Do not reuse lu factorization
  dw.Info(); // Some extra information
  info(parameters,true);

  dw.RKInit("exp4");
  dw.RKSolve("MatricesReAssemble"); //first 3 steps

  dw.SaveEta(3); //Saving third step of eta
  dw.DepthPlot(mesh,depth,true);
  dw.PCInit(mesh); //P.Corrector initialization


  // Predictor-Corrector
  while( dw.Step<dw.MaxSteps)
    {
      // We just need to reassemble when the bottom is changing
      // ENDTIME=4000*dw.TimeStep
      if (dw.Step<4001)
          dw.MatricesAssemble();

      // Now lets get to the Facturized LU
      if (dw.Step==4001)
        dw.LUFactorization(true); //Reuse lu factorization

      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
          dw.PreviewEta();
          dw.SaveEta();
          dw.DepthPlot(mesh,depth,false);
        }
      dw.Info(false);
    }
  return (EXIT_SUCCESS);
}

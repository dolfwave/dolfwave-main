
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.h
//
//
// To compile  with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#ifndef __DEPTH_H
#define __DEPTH_H

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

class Depth : public Function
{
 public:
  Depth (const double& t): t(t)
  {}
private:
  void eval(double* values, const double* x) const
  {
    double time=t;
    double endtime=ENDTIME;
        double C=0.4572;
        double D=0.04572;
        double X0=8.0;
        double w=3.14159265;//omega
        double g=3.0;//gamma
        double X=x[0];
        if (time<endtime)
           values[0]=C - D*exp(-g*pow((X - X0),2))*sin(t*w);
         else values[0]=C;
       }
       const double& t;
     };

    class DtDepth : public Function
     {
      public:
       DtDepth (const double& t): t(t)
       {}
     private:
       void eval(double* values, const double* x) const
       {
        double time=t;
        double endtime=ENDTIME;
        double D=0.04572;
        double X0=8.0;
        double w=3.14159265;//omega
        double g=3.0;//gamma
        double X=x[0];
        if (time<endtime)
           values[0]=-D*w*cos(t*w)*exp(-g*pow((X - X0),2));
         else values[0]=0.0;
       }
       const double& t;
     };

    class DtDtDepth : public Function
     {
      public:
       DtDtDepth (const double& t): t(t)
       {}
     private:
       void eval(double* values, const double* x) const
       {
        double time=t;
        double endtime=ENDTIME;
        double D=0.04572;
        double X0=8.0;
        double w=3.14159265;//omega
        double g=3.0;//gamma
        double X=x[0];
        if (time<endtime)
           values[0]=D*pow(w,2)*exp(-g*pow((X - X0),2))*sin(t*w);
         else values[0]=0.0;
       }
       const double& t;
     };

    class SourceEta : public Function
    {
      public:
       SourceEta (const double& t): t(t)
       {}
     private:
       void eval(double* values, const double* x) const
       {
        double time=t;
        double endtime=ENDTIME;
        double C=0.4572;
        double D=0.04572;
        double X0=8.0;
        double w=3.14159265;//omega
        double g=3.0;//gamma
        double X=x[0];
        double alpha=ALPHA;
        double beta1=BETA1;
        double B3=pow(alpha,3)/3.-sqr(alpha)+5./6.*alpha-alpha*beta1/2.-5./24.;
        if (time<endtime)
           values[0]=B3*pow((C - D*exp(-g*pow((X - X0),2))*sin(t*w)),4)*(-12*D*w*pow(g,2)*cos(t*w)*exp(-g*pow((X - X0),2)) - D*w*pow(g,4)*pow((-2*X0 + 2*X),4)*cos(t*w)*exp(-g*pow((X - X0),2)) + 8*D*w*pow(g,3)*pow((-2*X0 + 2*X),2)*cos(t*w)*exp(-g*pow((X - X0),2)) + D*w*pow(g,3)*(-8*X0 + 8*X)*(-2*X0 + 2*X)*cos(t*w)*exp(-g*pow((X - X0),2))) + alpha*(1.0 - beta1)*((C - D*exp(-g*pow((X - X0),2))*sin(t*w))*(-2*g*w*pow(D,2)*cos(t*w)*exp(-2*g*pow((X - X0),2))*sin(t*w) + 3*w*pow(D,2)*pow(g,2)*pow((-2*X0 + 2*X),2)*cos(t*w)*exp(-2*g*pow((X - X0),2))*sin(t*w) + 2*D*g*w*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*cos(t*w)*exp(-g*pow((X - X0),2)) - D*w*pow(g,2)*pow((-2*X0 + 2*X),2)*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*cos(t*w)*exp(-g*pow((X - X0),2))) + D*g*(-2*X0 + 2*X)*(-g*w*pow(D,2)*(-2*X0 + 2*X)*cos(t*w)*exp(-2*g*pow((X - X0),2))*sin(t*w) + D*g*w*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*(-2*X0 + 2*X)*cos(t*w)*exp(-g*pow((X - X0),2)))*exp(-g*pow((X - X0),2))*sin(t*w)) - w*pow(D,2)*pow(g,2)*pow((-2*X0 + 2*X),2)*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*cos(t*w)*exp(-2*g*pow((X - X0),2))*sin(t*w) + D*w*cos(t*w)*exp(-g*pow((X - X0),2)) - D*g*w*pow((C - D*exp(-g*pow((X - X0),2))*sin(t*w)),2)*cos(t*w)*exp(-g*pow((X - X0),2)) + 0.5*D*w*pow(g,2)*pow((C - D*exp(-g*pow((X - X0),2))*sin(t*w)),2)*pow((-2*X0 + 2*X),2)*cos(t*w)*exp(-g*pow((X - X0),2));
         else values[0]=0.0;
       }
       const double& t;
     };

    class SourcePhi : public Function
    {
     public:
       SourcePhi (const double& t): t(t)
       {}
     private:
       void eval(double* values, const double* x) const
       {
        double time=t;
        double endtime=ENDTIME;
        double C=0.4572;
        double D=0.04572;
        double X0=8.0;
        double w=3.14159265;//omega
        double g=3.0;//gamma
        double X=x[0];
        double alpha=ALPHA;
        double B4=-pow(alpha,3)/3.+sqr(alpha)-alpha/2.;
        if (time<endtime)
            values[0]=alpha*(D*pow(w,2)*(C - D*exp(-g*pow((X - X0),2))*sin(t*w))*exp(-g*pow((X - X0),2))*sin(t*w) + pow(D,2)*pow(w,2)*pow(cos(t*w),2)*exp(-2*g*pow((X - X0),2))) - B4*pow((C - D*exp(-g*pow((X - X0),2))*sin(t*w)),3)*(-2*D*g*pow(w,2)*exp(-g*pow((X - X0),2))*sin(t*w) + D*pow(g,2)*pow(w,2)*pow((-2*X0 + 2*X),2)*exp(-g*pow((X - X0),2))*sin(t*w)) - 0.5*pow(D,2)*pow(w,2)*pow(cos(t*w),2)*exp(-2*g*pow((X - X0),2));
        else values[0]=0.0;
       }
       const double& t;
     };

    #endif

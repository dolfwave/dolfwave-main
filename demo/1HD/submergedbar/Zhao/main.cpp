// Copyright (C) 2009 Nuno David Lopes.----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Solitary wave over a submerged bar

#include <dolfwave.h>
using namespace dolfin::dolfwave;

class ElevationInit : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  { //Wave parameters (see Walkley thesis)
    double c=sqrt(1.025), H=0.4;
    double ca=-0.4, cb=ca+1.0/3.0;
    double center=-5.0;
    double a1=(H/3.0)*(sqr(c)-1)/(cb-ca*sqr(c));
    double a2=-(H/2.0)*sqr((sqr(c)-1)/c)*(cb+2.0*ca*sqr(c))/(cb-ca*sqr(c));
    double k=(1.0/(2.0*H))*sqrt((sqr(c)-1)/(cb-ca*sqr(c)));
    values[0]=a1/sqr(cosh(k*(x[0]-center)))+a2/sqr(sqr(cosh(k*(x[0]-center))));
  }
};

class PotentialInit :  public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  { //Wave parameters
    double c=sqrt(1.025), H=0.4;
    double ca=-0.4, cb=ca+1.0/3.0;
    double center=-5.0;
    double a3=sqrt(H*G_E)*(sqr(c)-1)/c;
    double k=(1.0/(2.0*H))*sqrt((sqr(c)-1)/(cb-ca*sqr(c)));
    double cnst=4.0*a3/(2.0*k*(1+exp(2.0*k*(-25.)))); //Constant of integration
    values[0]=-4.0*a3/(2.0*k*(1+exp(2.0*k*(x[0]-center))))+cnst;
  }
};


class Depth :  public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double retrn=0.0;
    if(x[0]<=6.0)
      retrn=0.4;
    else if(x[0]<=12.0)
      retrn=-0.05*x[0]+0.7;
    else if(x[0]<=14.0)
      retrn=0.1;
    else if(x[0]<=17.0)
      retrn=0.1*x[0]-1.3;
    else
      retrn=0.4;

    values[0]=retrn;
  }
};

int main(int argc, char *argv[])
{
  bool preview=true;
  if (argc!=2)
    {
      preview=false;
      info("No preview");
    }
  else if (argc==2)
    {
      std::stringstream strng;strng<<argv[1];
      if (strng.str()=="preview")
        {
          preview=true;
          info("Preview");
        }
      else
        {
          preview=false;
          info("No preview");
        }
    }

  // {{{ Options, Initialization, Mesh and User functions
  Dolfwave dw(
              40001 /*MaxSteps*/,
              0.001 /*TimeStep*/,
              100   /*WriteGap*/,
              "Zhao_1D" /*FormName*/,
              "LU_P" /*SolverType*/,
              "gnuplot" /*Plotter*/,
              "output" /*OutDir*/,
              "pvd"/*OutFormat*/
              );
  IntervalMesh mesh(201,-25.,25.);
  Depth depth; //Depth
  Constant zero(0.0);//Sponge Layers and source function
  PotentialInit phi_init;//initial condition for potential
  ElevationInit eta_init;//initial condition for elevation
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,zero);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LinearFormsInit(depth, zero /*sp_eta*/, zero /*sp_lap_eta*/,
                     zero /*sp_phi*/, zero/*sp_lap_phi */, zero, zero);
  dw.InitialCondition(eta_init,phi_init); //Initial Conditions L2 projection
  dw.Volume(mesh,depth);
  dw.LUFactorization( ); //Solver type
  dw.DepthPlot(mesh,depth,preview); //Plotting Depth function
  dw.RKInit("exp4" /*RKtype*/);
  if (preview) dw.PreviewEta();
  dw.SaveEta();
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
  dw.PCInit(mesh);
  while( dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( ); //Predictor-Corrector

      if (!(dw.Step%dw.WriteGap))
        { //  Plot information------------------------
          if (preview) dw.PreviewEta();
          dw.SaveEta();
          dw.Volume(mesh,depth);
          dw.Info();
        }
    }
  return (EXIT_SUCCESS);
}

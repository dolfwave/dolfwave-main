import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 1,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Solitary wave with reflection'


ax=subplot(111,autoscale_on='false')


step=1 #STEP IN NUMBER OF POINTS PER FILE SOLUTION

LMAX=131 #NUMBER OF FILES TO DRAW
LSTEP=30  #STEP BETWEEN FILES

for j in range (0,LMAX):
    if j==0:
        filename='output/eta000000.xyz'
    elif j*LSTEP<10:
        filename='output/eta00000'+str(j*LSTEP)+'.xyz'
    elif j*LSTEP<100:
        filename='output/eta0000'+str(j*LSTEP)+'.xyz'
    elif j*LSTEP<1000:
        filename='output/eta000'+str(j*LSTEP)+'.xyz'
    else:
        filename='output/eta00'+str(j*LSTEP)+'.xyz'

    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=j*0.007+3*u1[i*step,1]
    if j==30 or j==70 or j==125:
        ax.plot(us[:,0], us[:,1],"b", label='')
    else :
        ax.plot(us[:,0], us[:,1],"b", label='',alpha=0.5*((j-1.)*(LMAX-j))/((LMAX/2.-1.)*LMAX/2))

ax.legend(loc=1)


(xname, yname) = ('$x$', 'time $0-100$ s')
title(plottitle,fontsize=18)
#grid()


ax.set_aspect(60.)
ax.set_xlim(-25.0,100.0)
ax.set_ylim(0.,1.)
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)


show()


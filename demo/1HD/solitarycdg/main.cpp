//  Copyright (C) 2009 Nuno David Lopes.----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Solitary wave evolution
// -----------------------------------------------------------------------

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

/* Global Variables*/

const double A=-25.; //
const double B=100.; // $x\in[A.B]$

class ElevationInit : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double delta_c=0.025;
    double H=0.4;
    double centro=-5.0;
    double a,a1,a2,b,c;
    double const_alpha=-0.4;
    double const_beta=const_alpha+1.0/3.0;
    c=sqrt(1.0+delta_c);
    a1=(H/3.0)*(sqr(c)-1)/(const_beta-const_alpha*sqr(c));
    a2=-(H/2.0)*sqr((sqr(c)-1)/c)*(const_beta+2.0*const_alpha*sqr(c))/(const_beta- const_alpha*sqr(c));
    a=sqrt(H*G_E)*(sqr(c)-1)/c;
    b=(1.0/(2.0*H))*sqrt((sqr(c)-1)/(const_beta-const_alpha*sqr(c)));
    //Velocity
    values[0]=a1/sqr(cosh(b*(x[0]-centro)))+a2/sqr(sqr(cosh(b*(x[0]-centro))));
  }
};

class PotentialInit : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double delta_c=0.025;
    double H=0.4;
    double centro=-5.0;
    double a,a1,a2,b,c;
    double const_alpha=-0.4;
    double const_beta=const_alpha+1.0/3.0;
    c=sqrt(1.0+delta_c);
    a1=(H/3.0)*(sqr(c)-1)/(const_beta-const_alpha*sqr(c));
    a2=-(H/2.0)*sqr((sqr(c)-1)/c)*(const_beta+2.0*const_alpha*sqr(c))/(const_beta- const_alpha*sqr(c));
    a=sqrt(H*G_E)*(sqr(c)-1)/c;
    b=(1.0/(2.0*H))*sqrt((sqr(c)-1)/(const_beta-const_alpha*sqr(c)));
    double retrn;
    double cnst=4.0*a/(2.0*b*(1+exp(2.0*b*(A))));
    //Potential
    retrn=-4.0*a/(2.0*b*(1+exp(2.0*b*(x[0]-centro))))+cnst;

    values[0]=retrn;
  }
};


int main( )
{
  //  Options, Initialization, Mesh and User functions
  Dolfwave dw(
              200001 /*MaxSteps*/,
              0.0005 /*TimeStep*/,
              50 /*WriteGap*/,
              "LPTcdg_1D" /*FormName*/,
              "LU_P" /* PETSc AlgBackEND*/,
              "gnuplot" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/
              );
  IntervalMesh mesh(601,A,B);
  Constant depth(0.4); //Depth
  Constant zero(0.0);
  Constant tension(0.0);//Tension Function
  PotentialInit phi_init;//initial condition for potential
  ElevationInit eta_init;//initial condition for elevation
  // }}} -----------------------------------------------------------
  // {{{ Functions, Vectors and Matrices ----------------------
  dw.SpacesFunctionsVectorsInit(mesh); // Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,depth);//BilinearForm a initialization
  dw.LinearFormsInit(depth,depth,zero,zero,zero,tension,zero,zero);
  dw.InitialCondition(eta_init,phi_init); //Initial Conditions L2 projection
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LUFactorization(true ); //Re-use lu factorization
  dw.Info();
  info(parameters,true); //Some informations
  dw.RKInit("exp4" /*RKtype*/);
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps

  // Loop with Predictor-Corrector method------------------
  dw.PCInit(mesh,false); //Do not use multi-step correction
  while(dw.Step<dw.MaxSteps)
    {
      dw.PCSolve( );
      if (!(dw.Step%dw.WriteGap))
        {
          // Plot
          //dw.PreviewEta();
          dw.SaveEta();
          dw.Volume(mesh,depth);
          dw.Info();
        }
    }
  dw.PreviewEta();
  return (EXIT_SUCCESS);
}

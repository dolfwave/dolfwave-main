// Copyright (C) 2009 Nuno David Lopes.----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by a moving bottom
//  Using an inner penalty CDG method

#include "Depth.h"
// ALPHA, BETA1, BETA2 are Defined in Depth.h

using namespace dolfin;
using namespace dolfin::dolfwave;



int main()
{
  Dolfwave dw(
              40000  /*MaxSteps*/,
              0.00025 /*TimeStep*/,
              50 /*WriteGap*/,
              "LPTcdg_1D" /*FormName*/,
              "LU_P"/* AlgBackEND*/ ,
              "gnuplot" /*Plotter*/,
              "output" /*OutDir*/,
              "pvd"/*OutFormat*/,
              "With no higher-order nonlinear terms. In this example we start with non factorized LU and, when no changes to the system matrices occur,we change to Factorized solver. Constant depth component is used in Bi/LinearFomrs." /*Comments*/
              );
  dw.SetParameter("Alpha",ALPHA);
  dw.SetParameter("Beta1",BETA1);
  dw.SetParameter("Beta2",BETA2);

  IntervalMesh mesh(301,-10.,20.);
  Depth depth(dw.Time);//time variable depth
  Constant ctd(0.45);//constant component of depth
  DtDepth dtdepth(dw.Time);
  DtDtDepth dtdtdepth(dw.Time);
  Constant sponge(0.0);//Sponge Layer
  Constant tension(0.0);//Sponge Layer
  SourceEta srceta(dw.Time);//Source Function
  SourcePhi srcphi(dw.Time);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,ctd);// Bilinear form initialization
  dw.LinearFormsInit(depth,ctd,dtdepth,dtdtdepth,
                     sponge,tension,srceta,srcphi);//Linear form initialization
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false);// Do not re-use LU factorization
  dw.Info(0); // Some extra information

  dw.RKInit("exp4");
  dw.SaveEta(1); //ploting and saving initial step
  dw.DepthPlot(mesh,depth,true);

  dw.RKSolve("MatricesReAssemble"); //first 3 steps
  dw.PCInit(mesh); //P.Corrector initialization


  // Predictor-Corrector
  while(dw.Step<dw.MaxSteps)
    {
      dw.MatricesAssemble();

      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
          dw.PreviewEta();
          dw.SaveEta();
          dw.DepthPlot(mesh,depth,false);
          dw.Volume(mesh,depth);
        }
      info("Step=%d \t Simulated time= %.4f",dw.Step,dw.Time);}
  return (EXIT_SUCCESS);
}

#!/usr/bin/env python
import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 1,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Horizontal landslide demo with LPTcdg\_1D'


ax=subplot(111,autoscale_on='false')
alignment = {'horizontalalignment':'center', 'verticalalignment':'baseline'}
family = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
font0 = FontProperties()
font1 = font0.copy()
font1.set_size(14)#
text0 = text(14., -.08, '$-h(x,t)$', fontproperties=font1,**alignment)
text1 = text(0., 0.42, '4o-W-CDG-P2', fontproperties=font1,**alignment)
text2 = text(15., 0.42, '$\\eta$', fontproperties=font1,**alignment)
text7 = text(-12., 0.4, '$10$', fontproperties=font1,**alignment)
text8 = text(-12., -0.005, '$0$', fontproperties=font1,**alignment)
text9 = text(-12., -0.105, '$10$', fontproperties=font1,**alignment)
text10 = text(-12., -0.51, '$0$', fontproperties=font1,**alignment)
font1.set_size(18)#
text11 = text(-12., .5, '$t$ (s)', fontproperties=font1,**alignment)
step=1
LMAX=41
LINIT=5
LSTEP=20

for j in range (0,LMAX):
    if j==0:
        filename='output/eta000000.xyz'
    elif j<LINIT:
        filename='output/eta0000'+str(j*LSTEP)+'.xyz'
    else:
        filename='output/eta000'+str(j*LSTEP)+'.xyz'

    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=j*0.01+u1[i*step,1]
    ax.plot(us[:,0], us[:,1],"b", label='')


for j in range (0,LMAX):
    if j==0:
        filename='output/depth000000.xyz'
    elif j<LINIT:
        filename='output/depth0000'+str(j*LSTEP)+'.xyz'
    else:
        filename='output/depth000'+str(j*LSTEP)+'.xyz'
    d1 = fromfile(filename,sep=" ")
    d1.shape=len(d1)//2,2
    ds=ones((len(d1)//(step),2))
    for i in range(0,len(d1)//(step)):
        ds[i,0]=d1[i*step,0]
        ds[i,1]=-0.05+j*0.01+d1[i*step,1]
    ax.plot(ds[:,0], ds[:,1],"brown", label='')


ax.legend(loc=1)


(xname, yname) = ('$x$ (m)', '')
title(plottitle,fontsize=18)
grid()


ax.set_aspect(23.0)
ax.set_xlim(-10.0,20.0)
ax.set_ylim(-0.51,.5)
ax.set_xlabel(xname,fontsize=18)
ax.set_ylabel(yname,fontsize=24)


show()















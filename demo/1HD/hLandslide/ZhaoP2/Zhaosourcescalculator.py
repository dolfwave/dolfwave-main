## {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
##
## e-mail: ndl@ptmat.fc.ul.pt
## http://ptmat.fc.ul.pt/~ndl/
##
## ---------------------------------------------------------------------------
## Licensed under the GNU LGPL Version 3.0 or later.
## ---------------------------------------------------------------------------
##  LPTsourcescalculator.py
##
## First added:  2009-10-19
## Last changed: 2010-11-03
##
## }}}------------------------------------------------------------------------

from sympy import *
from sympy import __version__ as sympy_version
import commands

print 'sympy version is '+ sympy_version
print 'Works with versions >= SymPy 0.6.3'


#Use capital X  to simplify integration with DOLFIN

X=Symbol('X',real=True, unbounded=True)
t=Symbol('t',real=True, unbounded=True)

#define the depth function

Dh=Symbol('Dh',real=True, unbounded=True)
S0=Symbol('S0',real=True, unbounded=True)


## DADOS

_Dh=0.045
_B=1.
_S0=1.0 ## velocity in m/s


## Depth definition ##
def S(t):
    return _S0*t

def xc(t):
    return S(t)

def xl(t):
    return xc(t)-0.5

def xr(t):
    return xc(t)+0.5



def h(X,t):
    return 0.45-Dh/((1+tanh(1))**2)*(1.+tanh(2.*(X-xl(t))))*\
           (1.-tanh(2.*(X-xr(t))))

#####################################################
print 'the depth function h'
depthpy=h(X,t)
depth=ccode(depthpy)
print depthpy
print '\n'



#### OUTPUT ######
print 'Source function for eta is S_1(h)=\n'

srcetapy= -diff(h(X,t),t)


srceta= ccode(together(srcetapy))

print srcetapy
print '\n'





######## print an Depth.h and Depth.cpp file to use #include "Depth.h" in the main.cpp ##########
date=commands.getoutput('date')
file = open('Depth.h','w')
print >>file, '''
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.h
//'''+str(date)+'''
//
// To compile  with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#ifndef __DEPTH_H
#define __DEPTH_H

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;


class Depth : public Expression
{
 public:
   Depth (const double& t): t(t) {}
   void eval(Array<double> & values, const Array<double> & x) const;
 private:
   const double& t;
 };


class SourceEta : public Expression
{
  public:
   SourceEta (const double& t): t(t) {}
   void eval(Array<double> & values, const Array<double> & x) const;
  private:
   const double& t;
 };


#endif'''

file = open('Depth.cpp','w')

print >>file, '''
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.cpp
//'''+str(date)+'''
//
// To compile  with DOLFIN:0.9.9
// }}}------------------------------------------------------------------------


#include "Depth.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


void Depth::eval(Array<double> & values, const Array<double> & x) const
  {
    double Dh='''+str(_Dh)+''';
    double X=x[0];
    values[0]='''+str(depth)+''';


   }



void SourceEta::eval(Array<double> & values, const Array<double> & x) const
   {
    double Dh='''+str(_Dh)+''';
    double X=x[0];
    values[0]='''+str(srceta)+''';

   }
'''

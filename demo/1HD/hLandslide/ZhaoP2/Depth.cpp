
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.cpp
//Wed Nov  3 12:37:29 WET 2010
//
// To compile  with DOLFIN:0.9.9
// }}}------------------------------------------------------------------------


#include "Depth.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


void Depth::eval(Array<double> & values, const Array<double> & x) const
  {
    double Dh=0.045;
    double X=x[0];
    values[0]=0.45 - Dh*(1.0 + tanh(1.0 + 2.0*X - 2.0*t))*(1.0 + tanh(1.0 + 2.0*t - 2.0*X))/pow((1 + tanh(1)),2);


   }



void SourceEta::eval(Array<double> & values, const Array<double> & x) const
   {
    double Dh=0.045;
    double X=x[0];
    values[0]=(Dh*(1 + tanh(1.0 + 2.0*X - 2.0*t))*(2.0 - 2.0*pow(tanh(1.0 + 2.0*t - 2.0*X),2)) - Dh*(1 + tanh(1.0 + 2.0*t - 2.0*X))*(2.0 - 2.0*pow(tanh(1.0 + 2.0*X - 2.0*t),2)))/(1 + 2*tanh(1) + pow(tanh(1),2));

   }


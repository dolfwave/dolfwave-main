
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.cpp
//Mon Nov 30 15:39:18 WET 2009
//
// To compile  with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------


#include "Depth.h"

using namespace dolfin;
using namespace dolfin::dolfwave;


void Depth::eval(Array<double> & values, const Array<double> & x) const
  {
    double Dh=0.05;
    double B=1.0;
    double xl=0.951436445422;
    double hl=0.1;
    double x0c=2.379;
    double SS=0.502754139782;
    double t0=3.7130381096;
    double S0=4.71238898038;
    double CT=0.994521895368;
    double TT=0.105104235266;
    double X=x[0];
    if (X>xl)
          values[0]=TT*X - 0.25*Dh*(1.0 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*(1.0 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS));
     else  values[0]=hl;

   }



void DtDepth::eval(Array<double> & values, const Array<double> & x) const
{
     double Dh=0.05;
     double B=1.0;
     double xl=0.951436445422;
     double x0c=2.379;
     double SS=0.502754139782;
     double t0=3.7130381096;
     double S0=4.71238898038;
     double CT=0.994521895368;
     double X=x[0];
     if (X>xl)
         values[0]=0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1.0 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)/(SS*t0*cosh(t/t0)) - 0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1.0 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)/(SS*t0*cosh(t/t0));
     else  values[0]=0.0;
}


void DtDtDepth::eval(Array<double> & values, const Array<double> & x) const
   {
     double Dh=0.05;
     double B=1.0;
     double xl=0.951436445422;
     double x0c=2.379;
     double SS=0.502754139782;
     double t0=3.7130381096;
     double S0=4.71238898038;
     double CT=0.994521895368;
     double X=x[0];
     if (X>xl)
         values[0]=(0.5*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)) + 0.25*CT*Dh*S0*SS*pow(cosh(t/t0),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 0.25*CT*Dh*S0*SS*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 0.5*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 0.25*CT*Dh*S0*SS*pow(cosh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.25*CT*Dh*S0*SS*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.5*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))/(pow(SS,2)*pow(t0,2)*pow(cosh(t/t0),2));
     else  values[0]=0.0;
}

void SourceEta::eval(Array<double> & values, const Array<double> & x) const
   {
    double Dh=0.05;
    double B=1.0;
    double xl=0.951436445422;
    double x0c=2.379;
    double SS=0.502754139782;
    double t0=3.7130381096;
    double S0=4.71238898038;
    double CT=0.994521895368;
    double TT=0.105104235266;
    double X=x[0];
    double alpha=0.5917517028;
    double beta1=4.971623160e-7;
    double B3=pow(alpha,3)/3.-sqr(alpha)+5./6.*alpha-alpha*beta1/2.-5./24.;
    if (X>xl)
       values[0]=(B3*pow((TT*X - 0.25*Dh*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))),4)*(4.0*CT*Dh*S0*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),3)*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 4.0*CT*Dh*S0*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),3)*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 4*CT*Dh*S0*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),4)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) + 4.0*CT*Dh*S0*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),4)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) + 10.0*CT*Dh*S0*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),3)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0) + 10.0*CT*Dh*S0*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),3)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0) + 22.0*CT*Dh*S0*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 10.0*CT*Dh*S0*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 10.0*CT*Dh*S0*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 20.0*CT*Dh*S0*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 20.0*CT*Dh*S0*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 22.0*CT*Dh*S0*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) + 20.0*CT*Dh*S0*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) + 20.0*CT*Dh*S0*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.5*pow(SS,2)*pow((TT*X - 0.25*Dh*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))),2)*(0.5*CT*Dh*S0*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 0.5*CT*Dh*S0*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) + CT*Dh*S0*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) + 1.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) + 1.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 1.0*CT*Dh*S0*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)) + alpha*pow(SS,2)*(1 - beta1)*((TT*X - 0.25*Dh*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)))*((TT*X - 0.25*Dh*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)))*(0.5*CT*Dh*S0*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 0.5*CT*Dh*S0*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) + CT*Dh*S0*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) + 1.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) + 1.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0)*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 1.0*CT*Dh*S0*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)) + (0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0))*(0.5*Dh*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)) + 0.5*Dh*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 0.5*Dh*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 2*(SS*TT + 0.25*Dh*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.25*Dh*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)))*(-0.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0) + 0.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 0.5*CT*Dh*S0*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))) + ((TT*X - 0.25*Dh*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)))*(-0.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0) + 0.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 0.5*CT*Dh*S0*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + (0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0))*(SS*TT + 0.25*Dh*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.25*Dh*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))))*(SS*TT + 0.25*Dh*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.25*Dh*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)))) - 0.5*pow(SS,2)*(TT*X - 0.25*Dh*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)))*(2*SS*TT + 0.5*Dh*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.5*Dh*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)))*(-0.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*sinh(t/t0) + 0.5*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 0.5*CT*Dh*S0*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 0.25*CT*Dh*S0*pow(SS,4)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 0.25*CT*Dh*S0*pow(SS,4)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0))/(pow(SS,5)*t0*cosh(t/t0));
     else values[0]=0.0;
   }



void SourcePhi::eval(Array<double> & values, const Array<double> & x) const
   {
    double Dh=0.05;
    double B=1.0;
    double xl=0.951436445422;
    double x0c=2.379;
    double SS=0.502754139782;
    double t0=3.7130381096;
    double S0=4.71238898038;
    double CT=0.994521895368;
    double TT=0.105104235266;
    double X=x[0];
    double alpha=0.5917517028;
    double B4=-pow(alpha,3)/3.+sqr(alpha)-alpha/2.;
    if (X>xl)
        values[0]=(alpha*pow(SS,2)*((TT*X - 0.25*Dh*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)))*(0.5*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)) + 0.25*CT*Dh*S0*SS*pow(cosh(t/t0),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 0.25*CT*Dh*S0*SS*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 0.5*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 0.25*CT*Dh*S0*SS*pow(cosh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.25*CT*Dh*S0*SS*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.5*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + pow((0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)),2)) - B4*pow((TT*X - 0.25*Dh*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))),3)*(-2.0*Dh*pow(CT,2)*pow(S0,2)*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)) - 2.0*Dh*pow(CT,2)*pow(S0,2)*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)) - 2*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),3)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 0.5*CT*Dh*S0*SS*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(cosh(t/t0),2)*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 0.5*CT*Dh*S0*SS*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(sinh(t/t0),2)*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 2.0*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),3)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 4.0*Dh*pow(CT,2)*pow(S0,2)*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(sinh(t/t0),2)*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) + 4.0*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)) + 4.0*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)) - 0.5*CT*Dh*S0*SS*pow((1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(sinh(t/t0),2)*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.5*CT*Dh*S0*SS*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(cosh(t/t0),2)*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 4.0*Dh*pow(CT,2)*pow(S0,2)*pow((1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)),2)*pow(sinh(t/t0),2)*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) + CT*Dh*S0*SS*pow(cosh(t/t0),2)*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + CT*Dh*S0*SS*pow(sinh(t/t0),2)*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) + 1.5*CT*Dh*S0*SS*pow(cosh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) + 1.5*CT*Dh*S0*SS*pow(cosh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) + 6.0*Dh*pow(CT,2)*pow(S0,2)*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 1.0*CT*Dh*S0*SS*pow(cosh(t/t0),2)*pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 1.0*CT*Dh*S0*SS*pow(sinh(t/t0),2)*pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2)*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 1.5*CT*Dh*S0*SS*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS) - 1.5*CT*Dh*S0*SS*pow(sinh(t/t0),2)*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS)) - 0.5*pow(SS,2)*pow((0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 - tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0) - 0.25*CT*Dh*S0*(1 - pow(tanh((X - x0c - 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS),2))*(1 + tanh((X - x0c + 0.5*B*CT - CT*S0*log(cosh(t/t0)))/SS))*sinh(t/t0)),2))/(pow(SS,4)*pow(t0,2)*pow(cosh(t/t0),2));
    else values[0]=0.0;
}

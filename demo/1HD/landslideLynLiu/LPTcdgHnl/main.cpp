// Copyright (C) 2009 Nuno David Lopes.----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by a moving bottom: Modified 2nd-order equations
//  Using an inner penalty CDG method
//  See mvbottom for CG

#include "Depth.h"

int main( )
{
  Dolfwave dw(
              120000  /*MaxSteps*/,
              0.00005 /*TimeStep*/,
              1000 /*WriteGap*/,
              "LPTcdgHnl_1D" /*FormName*/,
              "LU_P" /* AlgBackEND*/ ,
              "gnuplot" /*Plotter*/,
              "output" /*OutDir*/,
              "pvd"/*OutFormat*/,
              "LUFactorization is not used. Good luke" /*Comments*/);
  parameters("dolfwave_parameters")["pred_corr_blow_up"]=10;
  info(parameters,true);
  IntervalMesh mesh(55,0.0,10.5);
  Depth depth(dw.Time);//time variable depth
  DtDepth dtdepth(dw.Time);
  DtDtDepth dtdtdepth(dw.Time);
  Constant spng(0.0);//Sponge Layer
  Constant tension(0.0);//Sponge Layer
  SourceEta srceta(dw.Time);//Source Function
  SourcePhi srcphi(dw.Time);//Source Function
  dw.SpacesFunctionsVectorsInit(mesh);//Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh,depth,depth);// Bilinear form initialization
  dw.LinearFormsInit(depth,depth,dtdepth,dtdtdepth,
                     spng,tension,srceta,srcphi);//Linear form initialization
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false); //Do not re-use factorization
  dw.DepthPlot(mesh,depth,false);
  dw.Volume(mesh,depth);

  dw.RKInit("exp4");
  dw.RKSolve("MatricesReAssemble"); //first 3 steps
  dw.PCInit(mesh); //P.Corrector initialization
  dw.SaveEta(3); //ploting and saving
  while(dw.Step<dw.MaxSteps)
    {
      dw.MatricesAssemble();
      dw.PCSolve( );//PC step
      if (!(dw.Step%dw.WriteGap))
        {
          dw.DepthPlot(mesh,depth,false);
          dw.PreviewEta(); //saving eta
          dw.SaveEta(); //saving eta
          dw.Volume(mesh,depth);
        }
      dw.Info(false);
    }
  return (EXIT_SUCCESS);
}

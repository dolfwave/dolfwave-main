// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This program provides a demo of the DOLFWAVE library.
// Standard Gaussian Wave in an interval
// ------------------------------------------------------------------------

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;
// {{{--------------------------------------------------------------------
class ElevationInit : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    values[0]=0.045*exp(-2.*((x[0]-3.0)*(x[0]-3.0)));
  }
};
int main( )
{
  // {{{ Options, Initialization, Mesh and User functions
  Dolfwave dw(
              5001 /*MaxSteps*/,
              0.0005 /*TimeStep*/,
              1000 /*WriteGap*/,
              "LPTcdg_1D" /*FormName*/,
              "LU_P" /* uBLAS AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/
              );
  IntervalMesh mesh(61,0,6);
  Constant depth(0.45);//Depth
  Constant zero(0.0);//Sponge Layer
  Constant tension(0.0);//Tension Function
  Constant  phi_init(0.0);//initial condition for potential
  ElevationInit eta_init;//initial condition for elevation
  info(parameters,true);//Some informations
  // {{{ Functions, Vectors and Matrices ----------------------
  dw.SpacesFunctionsVectorsInit(mesh); // Init. Spaces Functions & Vvectors
  dw.BilinearFormInit(mesh,depth,depth);//BilinearForm a initialization
  dw.LinearFormsInit(depth,depth,zero,zero,zero,tension,zero,zero);
  dw.InitialCondition(eta_init,phi_init); //Initial Conditions L2 projection
  dw.Volume(mesh,depth);
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LUFactorization(true); //Solver type
  dw.RKInit("exp4" /*RKtype*/);
  dw.PreviewEta(0);
  dw.SaveEta(0);
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
  // {{{Loop with Predictor-Corrector method------------------
  dw.PCInit(mesh,true);
  while(dw.Step<dw.MaxSteps)
   {
     dw.PCSolve( );

     if (!(dw.Step%dw.WriteGap))
       {      //  Plots
         dw.PreviewEta();
         dw.SaveEta();
         dw.Volume(mesh,depth);
       }
     dw.Info(true);
   }
 return (EXIT_SUCCESS);
}

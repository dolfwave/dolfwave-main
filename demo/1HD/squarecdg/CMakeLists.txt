# Require CMake 2.8
cmake_minimum_required(VERSION 2.8)

project(1HD_squarecdg_demo)
set(SOURCES main.cpp)


# Include directories
include_directories($ENV{DOLFWAVE_SOURCE_PATH})
find_package(DOLFIN)

# Need to get VTK config because VTK uses advanced VTK features which
# mean it's not enough to just link to the DOLFIN target. See
# http://www.vtk.org/pipermail/vtk-developers/2013-October/014402.html
find_package(VTK HINTS ${VTK_DIR} $ENV{VTK_DIR} NO_MODULE QUIET)

# Include directories
include_directories(${DOLFIN_INCLUDE_DIRS} ${DOLFIN_3RD_PARTY_INCLUDE_DIRS})
# Compiler definitions
add_definitions(${DOLFIN_CXX_DEFINITIONS})
# Add special DOLFIN compiler flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${DOLFIN_CXX_FLAGS}")

# Executable
add_executable(${PROJECT_NAME} ${SOURCES})
# Target libraries
if (APPLE)
target_link_libraries(${PROJECT_NAME} $ENV{DOLFWAVE_LIB_DYLIB} $ENV{DOLFIN_LIB_DYLIB} $ENV{BOOST_SYSTEM_LIB_DYLIB})
else()
target_link_libraries(${PROJECT_NAME} $ENV{DOLFWAVE_LIB_SO} $ENV{DOLFIN_LIB_SO}  $ENV{MPI_CXX_LIB_SO} $ENV{MPI_LIB_SO} $ENV{BOOST_SYSTEM_LIB_SO})
endif()

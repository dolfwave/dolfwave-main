# Copyright (C) 2009 Nuno David Lopes.-----------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
# Eigenvalue solver for stability of BBM equation
# First added:  2011-06-21
# Last changed: 2011-06-24


import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile

H=[0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]

def plotfig():
    ax=subplot(231)

    step=2
    for h in H[:]:
        filename='BBM/outputP2/H_'+str(h)+'_stability.dxhm'

        u1 = fromfile(filename,sep=" ")
        u1.shape=len(u1)//2,2
        us=ones((len(u1)//(step),2))
        for i in range(0,len(u1)//(step)):
            us[i,0]=u1[i*step,0]
            us[i,1]=u1[i*step,1]
        ax.plot(us[:,0], us[:,1],"bo",ms=7)



        ax.legend(loc=(0.6,0.5))

    ax=subplot(232)

    step=2
    for h in H[:]:
        filename='KdV/output/tau_0_H_'+str(h)+'_stability.dxhm'

        u1 = fromfile(filename,sep=" ")
        u1.shape=len(u1)//2,2
        us=ones((len(u1)//(step),2))
        for i in range(0,len(u1)//(step)):
            us[i,0]=u1[i*step,0]
            us[i,1]=u1[i*step,1]
        ax.plot(us[:,0], us[:,1],"bo",ms=7)



        ax.legend(loc=(0.6,0.5))

        ax=subplot(233)

    step=2
    for h in H[:]:
        filename='KdV-BBM/output/tau_0_H_'+str(h)+'_stability.dxhm'

        u1 = fromfile(filename,sep=" ")
        u1.shape=len(u1)//2,2
        us=ones((len(u1)//(step),2))
        for i in range(0,len(u1)//(step)):
            us[i,0]=u1[i*step,0]
            us[i,1]=u1[i*step,1]
            ax.plot(us[:,0], us[:,1],"bo",ms=7)



    ax.legend(loc=(0.6,0.5))

    #(xname, yname) = ('$H$', '$\\triangle x$ ({\\tt m})')
    title('BBM, KdV model over constant depth H (with $P_2$ Lagrange)',fontsize=18)
    #ax.set_aspect(.5)
    #ax.set_xlim(0.,1.1)
    #ax.set_ylim(0.,1.0)
    #xlabel(xname,fontsize=24)
    #ylabel(yname,fontsize=24)
    grid()
    #outputfile='modes.png'
    #savefig(outputfile)
    #clf()
    show()

plotfig()














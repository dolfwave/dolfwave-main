# Copyright (C) 2009 Nuno David Lopes.-----------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
# Eigenvalue solver for stability of BBM equation
# First added:  2011-01-21
# Last changed: 2012-03-22



import matplotlib as mpl
import matplotlib.pyplot as plt
from pylab import *
from numpy import fromfile
from matplotlib.font_manager import FontProperties


params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 12,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)




def ur(hm):
    hm/=10.
    return  (1-hm*2)*(hm<0.5)
def ug(hm):
    hm/=10.
    return (hm*2)*(hm<0.5) +(1-(2*hm-1))*(hm>0.48)
def ub(hm):
    hm/=10.
    return (2*hm-1)*(hm>0.48)


dx=[0.02,0.04,0.0806452,0.1,0.185185,0.25,0.294118,0.333333,0.5,0.625,0.714286,0.833333,1]
tau=[0, 0.01,0.05, 0.1, 0.25, 0.5, 0.75, 1, 5, 10]
ax=subplot(111)
subplots_adjust(left=0.13,bottom=0.13,right=0.92,top=0.92)
for dxx in dx[:]:
    for tauu in tau[:]:
        outfilepng='dx_spectrum_KdVH1_2.png'
        infile='KdV_first/tau_'+str(tauu)+'_H_1_dx_'+str(dxx)

        filename=infile+'omega.xy'

        u1 = fromfile(filename,sep=" ")
        u1.shape=len(u1)//2,2
        ax.plot(u1[:,0], u1[:,1],'o',color=(ur(tauu),ug(tauu),ub(tauu)))

(xname, yname) = ('$Re(\\omega)\,({\\tt s}^{-1})$', '$Im(\\omega)\,({\\tt s}^{-1})$')
# H1# ax.set_aspect(200.)
# H1# ax.set_xlim(-100000,100000.)
# H1# ax.set_ylim(-100,500)

ax.set_aspect(5.)
ax.set_xlim(-4,4.)
ax.set_ylim(-0.6,0.5)
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)

#title('KdV Model Spectrum',fontsize=18)
grid()
#savefig(outfilepng)
#print outfilepng
show()
clf()
















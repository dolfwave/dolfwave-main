# Copyright (C) 2009 Nuno David Lopes.-----------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
# Eigenvalue solver for stability of BBM equation
# First added:  2011-06-21
# Last changed: 2011-06-21


import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile


def plotfig():
    ax=subplot(111,autoscale_on='false')
    subplots_adjust(left=0.13,bottom=0.13,right=0.92,top=0.92)

    step=2

    filename='output/stability.dxhm'

    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=u1[i*step,1]
    ax.plot(us[:,0], us[:,1],"bo",ms=7,label="Unstable modes"  )



    ax.legend(loc=(0.6,0.5))



    (xname, yname) = ('$H=1$', '$\\triangle x$ ({\\tt m})')
    title('BBM model over constant depth',fontsize=18)
    ax.set_aspect(.5)
    ax.set_xlim(0.8,1.2)
    ax.set_ylim(0.,1.0)
    xlabel(xname,fontsize=24)
    ylabel(yname,fontsize=24)
    grid()
    outputfile='modes.png'
    savefig(outputfile)
    clf()

plotfig()














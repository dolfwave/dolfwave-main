#!/usr/bin/python

import os
#The arguments for the demo are
#a1='constant depth' (inside the loop)
a2=-9./10. #a2='pade_alpha'
a3=0.0 #a3='pade_beta'
a4=6./7. #a4='s1'
a5=0.0 #a5='q1'
a6=0.0 #a6='q2'
#a7='p_tau' (inside the loop)
a8='KdVBBM' #a8='output_dir'

cmm1='mkdir '+str(a8)
print(cmm1)
os.system(cmm1)

tau=0.
# the end point of the range is not included
for i  in range(151,201):
    tau=0.0005*i
    for j in range(1,11):
        H=j*0.1
        a1=H
        a7=tau
        tt1='H_'
        tt2=a1
        tt3='Tau_'
        tt4=a7
        tt5='_logfile.txt'
        tt6='_erro.txt'
        logfile=str(tt1)+str(tt2)+str(tt3)+str(tt4)+str(tt5)
        erro=str(tt1)+str(tt2)+str(tt3)+str(tt4)+str(tt6)
        cmm2='lstability_xKdVBBM_demo '+ str(a1)+' '+str(a2)+' '+str(a3)+' '+str(a4)+' '+str(a5)+' '+str(a6)+' '+str(a7)+' '+str(a8)+\
              ' > '+str(a8)+'/'+str(logfile)+' 2>'+str(a8)+'/'+str(erro)+' '+'&'
        print(str(cmm2))
        os.system(str(cmm2))


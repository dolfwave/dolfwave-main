#!/bin/sh

#The arguments for the demo are
#a1='constant depth' (inside the loop)
a2=-79./63. #a2='pade_alpha'
a3=-223./1197. #a3='pade_beta'
a4=6./7. #a4='s1'
a5=6./7. #a5='q1'
a6=-8. #a6='q2'
#a7='p_tau' (inside the loop)
a8=xKdVBBM #a8='output_dir'

mkdir $a8

for tau in 0.0 0.01 0.05 0.1 0.25 0.5 0.75 1 5 10 100
do
    for H in 0.01 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1
    do
        a1=$H
        a7=$tau
        tt1=H_
        tt2=$a1
        tt3=Tau_
        tt4=$a7
        tt5=_logfile.txt
        tt6=_erro.txt
        logfile=$tt1$tt2$tt3$tt4$tt5
        erro=$tt1$tt2$tt3$tt4$tt6
        lstability_xKdVBBM_demo $a1 $a2 $a3 $a4 $a5 $a6 $a7 $a8 > $a8/$logfile 2>$a8/$erro &
    done
done

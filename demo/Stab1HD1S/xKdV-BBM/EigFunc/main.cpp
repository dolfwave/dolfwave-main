// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// Eigenvalue solver for stability of Standard potential formulation
// First added:  2011-01-21
// Last changed: 2013-11-15
// See also the DOLFIN waveguide demo:


#include <dolfin.h>
#include "../EigxKdVBBM.h"
#include <iomanip>
#include <dolfwave.h>

using namespace dolfin;

const double A=-5; //Domain [A,B]
const double B=5;


int main(int argc, char* argv[])
{
  if (argc!=9)
    {
      info("Wrong number of arguments");
      info("argv[1]='constant depth'");
      info("argv[2]='pade_alpha'");
      info("argv[3]='pade_beta'");
      info("argv[4]='s1'");
      info("argv[5]='q1'");
      info("argv[6]='q2'");
      info("argv[7]='p_tau'");
      info("argv[8]='output_dir'");
      exit(EXIT_FAILURE);
    }

  double H=atof(argv[1]); //Constant depth
  double alpha=atof(argv[2]);
  double beta=atof(argv[3]);
  double s1=atof(argv[4]);
  double q1=atof(argv[5]);
  double q2=atof(argv[6]);
  double tau=atof(argv[7]); // Penalty coefficient

  std::stringstream filestabplotname;
  filestabplotname<<argv[8]<<"/"<<"tau_"<<tau<<"_H_"<<H<<"_stability.dxhm";
  std::ofstream stabfile;
  stabfile.open(filestabplotname.str().c_str(),std::ios::app);

  for(unsigned int i=50;i<51;i++)
    {

      // Create meshes
      int ndx=2*(250/i);
      double dx=(B-A)/ndx;
      info("dx=%f",dx);
      IntervalMesh mesh(ndx,A, B);

      /* Define the forms that generates a generalized eigenproblem of the form
         [S]{h} = i omega [T]{h}
         with omega the angular frequency of the solutions */

      Constant depth(H); //Depth
      Constant gravity(dolfin::dolfwave::G_E);
      Constant p_tau(tau);
      Constant e(dx);
      Constant x_alpha(alpha);
      Constant x_beta(beta);
      Constant x_s1(s1);
      Constant x_q1(q1);
      Constant x_q2(q2);

      EigxKdVBBM::FunctionSpace V(mesh);
      EigxKdVBBM::Form_a t(V, V);
      t.h=depth;
      t.tau=p_tau;
      t.pade_alpha=x_alpha;
      t.pade_beta=x_beta;
      t.e=e;
      t.q1=x_q1 ;
      t.q2=x_q2;

      EigxKdVBBM::Form_L s(V, V);
      s.h=depth;
      s.g=gravity;
      s.pade_alpha=x_alpha;
      s.s1=x_s1;

      // Assemble the system matrices stiffness (S) and mass matrices (T)
      PETScMatrix S;
      PETScMatrix T;
      assemble(S, s);
      assemble(T, t);

      // Solve the eigen system
      SLEPcEigenSolver esolver(S,T);
      esolver.parameters["solver"] = "lapack";
      esolver.solve();

      // Vector for saving the spectrum
      int N=S.size(1);
      info("The number of eigenvalues is %d", N);
      double realvalues[N];
      double imvalues[N];
      double lr, lc;

      std::stringstream filename;
      filename<<argv[8]<<"/"<<"tau_"<<tau<<"_H_"<<H<<"_dx_"<<dx<<"omega.xy";
      std::ofstream omegafile(filename.str().c_str());


      Function eigenvectorreal(V);
      Function eigenvectorim(V);


      for (std::size_t j = 0; j < S.size(1); j++)
        {
          filename.str("");
          esolver.get_eigenpair(lr,lc,*(eigenvectorreal.vector()),*(eigenvectorim.vector()),j);
          filename<<argv[8]<<"/vector_real_eta"<<j<<".xyz";
          std::ofstream evctrealfile(filename.str().c_str());
          filename.str("");
          filename<<argv[8]<<"/vector_im_eta"<<j<<".xyz";
          std::ofstream evctimfile(filename.str().c_str());

          for (std::size_t i=0;i<S.size(1);i++)
           {
             Array<double> value(1);
             value[0]=0.0;
             double _p[1]={A+i*dx/2.};
             Array<double> p(1,_p);
             eigenvectorreal.eval(value,p);
             evctrealfile<< _p[0] <<" " <<value[0]<<std::endl;
             eigenvectorim.eval(value,p);
             evctimfile<< _p[0] <<" " <<value[0]<<std::endl;
           }

          evctrealfile.close();
          evctimfile.close();

          // We need to switch the real and imaginary parts (i omega)
          realvalues[j]=lc;
          imvalues[j]=lr;

          // Plot dx vs H once and if a imaginary part is found
          // For the first hm found
          if(imvalues[j]>0.00001) /* numerical error */
            {
              stabfile<<H<<"\t"<<dx<<std::endl;
            }
          omegafile.precision(25);
          omegafile<<lc<<std::fixed<<"\t"<<lr<<std::fixed<<std::endl;
        }
    }
  return EXIT_SUCCESS;
}


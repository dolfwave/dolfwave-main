#!/bin/sh

#The arguments for the demo are
a1=-9./10. #a1='pade_alpha'
a2=0.0 #a2='pade_beta'
a3=6./7. #a3='s1'
a4=0.0 #a4='q1'
a5=0.0 #a5='q2'
a6=0.1 #penalty tau
a7=KdVBBM #a8='output_dir'



mkdir $a7

tt5=logfile.txt
tt6=erro.txt
logfile=$tt5
erro=$tt6
EigFunc_xKdVBBM_demo $a1 $a2 $a3 $a4 $a5 $a6 $a7 > $a7/$logfile 2>$a7/$erro &

#!/bin/sh

#The arguments for the demo are
#a1='constant depth' (inside the loop)
a2=-9./10. #a2='pade_alpha'
a3=0.0 #a3='pade_beta'
a4=6./7. #a4='s1'
a5=0.0 #a5='q1'
a6=0.0 #a6='q2'
#a7='p_tau' (inside the loop)
a8=KdVBBM #a8='output_dir'

mkdir $a8

#for tau in 0.0 0.01 0.05 0.1 0.25 0.5 0.75 1 5 10 100
#for tau in 0.0 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95
for tau in 0.0 0.0001 0.0005 0.001 0.0015 0.002 0.0025 0.003 0.0035 0.004 0.0045 0.005 0.0055 0.006 0.0065 0.007 0.0075 0.008 0.0085 0.009 0.0095 0.01
do
    #for H in 0.01 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1
    for H in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1
    do
        a1=$H
        a7=$tau
        tt1=H_
        tt2=$a1
        tt3=Tau_
        tt4=$a7
        tt5=_logfile.txt
        tt6=_erro.txt
        logfile=$tt1$tt2$tt3$tt4$tt5
        erro=$tt1$tt2$tt3$tt4$tt6
        lstability_xKdVBBM_demo $a1 $a2 $a3 $a4 $a5 $a6 $a7 $a8 > $a8/$logfile 2>$a8/$erro &
    done
done

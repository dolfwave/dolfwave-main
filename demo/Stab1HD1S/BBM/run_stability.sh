#!/bin/sh

dir1=output
mkdir $dir1


for H in 0.01 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1
do
    tt1=H_
    tt2=$H
    tt5=_logfile.txt
    tt6=_erro.txt
    logfile=$tt1$tt2$tt5
    erro=$tt1$tt2$tt6
    1HD_lstability_BBM_demo $H $tau  > $dir1/$logfile 2>$dir1/$erro &
done

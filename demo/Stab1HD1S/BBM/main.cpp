// Copyright (C) 2011 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// Eigenvalue solver for stability of Standard potential formulation
// First added:  2011-06-21
// Last changed: 2013-02-06
// See also the DOLFIN waveguide demo:


#include <dolfin.h>
#include "EigBBM.h"
#include <iomanip>
#include <dolfwave.h>

using namespace dolfin;

const double A=-5; //Domain [A,B]
const double B=5;

int main(int argc,char* argv[])
{

  double H=atof(argv[1]); //Constant depth

  std::stringstream filestabplotname;
  filestabplotname<<"output/H_"<<H<<"_stability.dxhm";
  std::ofstream stabfile;
  stabfile.open(filestabplotname.str().c_str(),std::ios::app);

  for(unsigned int i=1;i<100;i++)
    {

      // Create meshes
      int ndx=2*(500/i);
      // To always catch the central point
      // Some meshes will be repeated
      double dx=(B-A)/ndx;
      info("dx=%f",dx);
      IntervalMesh mesh(ndx,A, B);

      // Define the forms that generates a generalized eigenproblem of the form
      // [S]{h} = i omega [T]{h}
      // with omega the angular frequency of the solutions
      Constant depth(H); //Depth
      Constant gravity(dolfin::dolfwave::G_E);
      EigBBM::FunctionSpace V(mesh);
      EigBBM::Form_a t(V, V);
      t.h=depth;

      EigBBM::Form_L s(V, V);
      s.h=depth;
      s.g=gravity;

      // Assemble the system matrices stiffness (S) and mass matrices (T)
      PETScMatrix S;
      PETScMatrix T;
      assemble(S, s);
      assemble(T, t);

      // Solve the eigen system
      SLEPcEigenSolver esolver(S, T);
      esolver.parameters["solver"] = "lapack";
      esolver.solve();

      // Vector for saving the spectrum
      int N=S.size(1);
      info("The number of eigenvalues is %d", N);
      double realvalues[N];
      double imvalues[N];
      double lr, lc;

      std::stringstream filename;
      filename<<"output/"<<"H_"<<H<<"_dx_"<<dx<<"omega.xy";
      std::ofstream omegafile(filename.str().c_str());


      for (unsigned int j = 0; j < S.size(1); j++)
        {
          esolver.get_eigenvalue(lr, lc, j);

          // We need to switch the real and imaginary parts (i omega)
          // info("Real(omega) = %g", lc);
          realvalues[j]=lc;
          // info("Im(omega) = %g", lr);
          imvalues[j]=lr;
          //Plot dx vs H once and if a imaginary part is found
          // For the first hm found
          if(imvalues[j]>0.00001) /* numerical error */
            {
              stabfile<<H<<"\t"<<dx<<std::endl;
            }

          omegafile.precision(25);
          omegafile<<lc<<std::fixed<<"\t"<<lr<<std::fixed<<std::endl;
        }
    }
  return 0;
}

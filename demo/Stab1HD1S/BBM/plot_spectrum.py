# Copyright (C) 2009 Nuno David Lopes.-----------------------------------
# Licensed under the GNU LGPL Version 3.0 or later.
# Eigenvalue solver for stability of BBM equation
# First added:  2011-01-21
# Last changed: 2011-06-23



import matplotlib as mpl
import matplotlib.pyplot as plt
from pylab import *
from numpy import fromfile
from matplotlib.font_manager import FontProperties


params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 12,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)




def ur(hm):
    return  (1-hm*2)*(hm<0.5)
def ug(hm):
    return (hm*2)*(hm<0.5) +(1-(2*hm-1))*(hm>0.48)
def ub(hm):
    return (2*hm-1)*(hm>0.48)

H=[0.01,0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
dx=[0.02,0.1,0.25,0.5,1]

ax=subplot(111)
subplots_adjust(left=0.13,bottom=0.13,right=0.92,top=0.92)
for h in H[:]:
    for dxx in dx[:]:
        outfilepng='dx_spectrum_BBM.png'
        infile='output/H_'+str(h)+'_dx_'+str(dxx)
        print outfilepng+'h'+str(h)+'dx'+str(dxx)

        filename=infile+'omega.xy'

        u1 = fromfile(filename,sep=" ")
        u1.shape=len(u1)//2,2
        for i in range(0,len(u1)//2):
            ax.plot(u1[:,0], u1[:,1],'o',color=(ur(dxx),ug(dxx),ub(dxx)))

(xname, yname) = ('$Re(\\omega)\,({\\tt s}^{-1})$', '$Im(\\omega)\,({\\tt s}^{-1})$')
title('BBM Model over constant Depth',fontsize=18)
xlabel(xname,fontsize=24)
ylabel(yname,fontsize=24)
grid()
savefig(outfilepng)
clf()
















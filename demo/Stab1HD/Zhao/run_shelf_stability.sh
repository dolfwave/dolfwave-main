#!/bin/sh

dir1=output_shelf
mkdir $dir1

1HD_lstability_Zhao_demo 0.5  > $dir1/L5logfile.txt 2>$dir1/4ERROr.txt &
1HD_lstability_Zhao_demo 1.0  > $dir1/L10logfile.txt 2>$dir1/10ERROr.txt &
1HD_lstability_Zhao_demo 2.0  > $dir1/L20logfile.txt 2>$dir1/20ERROr.txt &


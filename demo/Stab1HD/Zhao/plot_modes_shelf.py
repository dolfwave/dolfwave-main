import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile


def plotfig(l):
    ax=subplot(111,autoscale_on='false')
    subplots_adjust(left=0.13,bottom=0.13,right=0.92,top=0.92)

    step=2

    filename='output_shelf/L_'+str(l)+'_stability.dxhm'

    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    for i in range(0,len(u1)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=u1[i*step,1]
    ax.plot(us[:,0], us[:,1],"bo",ms=7,label="Unstable modes"  )



    ax.legend(loc=1)



    (xname, yname) = ('$h_m$', '$\\triangle x$ ({\\tt m})')
    title('Standard Potential Model over shelf geometry',fontsize=18)
    ax.set_aspect(1.0)
    ax.set_xlim(0.0,1.0)
    ax.set_ylim(0.,1.0)
    xlabel(xname,fontsize=24)
    ylabel(yname,fontsize=24)
    grid()
    outputfile='shelf_modes'+'L'+str(l)+'.png'
    savefig(outputfile)
    clf()

plotfig(0.5)
plotfig(1)
plotfig(2)













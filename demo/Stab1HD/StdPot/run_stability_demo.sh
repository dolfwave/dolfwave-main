#!/bin/sh


l1=0.5
l2=1.0
l3=2.0

dir1=output_shelf
mkdir $dir1

1HD_lstability_StdPot_demo $l1  $dir1> $dir1/L5logfile.txt  2>$dir1/4ERROr.txt &
1HD_lstability_StdPot_demo $l2  $dir1> $dir1/L10logfile.txt 2>$dir1/10ERROr.txt &
1HD_lstability_StdPot_demo $l3  $dir1> $dir1/L20logfile.txt 2>$dir1/20ERROr.txt &


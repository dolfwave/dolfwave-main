// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// Eigenvalue solver for stability of Standard potential formulation
// First added:  2011-01-21
// Last changed: 2013-01-04
// See Pederson and Lovholt 2009 and Fuhrman Thesis 2004
// Here we test the shelf geometries.
// See also the DOLFIN waveguide demo:
// Copyright (C) 2008 Evan Lezar (evanlezar@gmail.com


#include <dolfin.h>
#include "EigStdPot.h"
#include <iomanip>
#include <dolfwave.h>

using namespace dolfin;


// Depth geometry

class Depth :  public Expression
{
public:
  Depth (const double &XX1,const double &XX2,
         const double &XX3,const double &XX4,
         const double &YY1,const double &YY2,
         const double &YY3,const double &YY4):
    XX1(XX1),XX2(XX2),XX3(XX3),XX4(XX4),
    YY1(YY1),YY2(YY2),YY3(YY3),YY4(YY4) {};
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double retrn=0.0;
    double XY1[2]={XX1,YY1};
    double XY2[2]={XX2,YY2};
    double XY3[2]={XX3,YY3};
    double XY4[2]={XX4,YY4};
    double m=((XY2[1]-XY1[1])/(XY2[0]-XY1[0]));
    double m1=((XY4[1]-XY3[1])/(XY4[0]-XY3[0]));
    double b=XY1[1]-m*XY1[0];
    double b1=XY3[1]-m1*XY3[0];

    if(x[0]<=XY1[0])
      retrn=XY1[1];
    else if(x[0]<=XY2[0])
      retrn=m*x[0]+b;
    else if(x[0]<=XY3[0])
      retrn=XY2[1];
    else if(x[0]<=XY4[0])
      retrn=m1*x[0]+b1;
    else
      retrn=XY4[1];

    values[0]=retrn;
  };
private:
  const double &XX1;
  const double &XX2;
  const double &XX3;
  const double &XX4;
  const double &YY1;
  const double &YY2;
  const double &YY3;
  const double &YY4;
};


int main(int argc,char* argv[])
{
  double dh=0.02;
  double H=1.0;
  double hm=H;
  double A=-5.;
  double B=5.;
  double L=atof(argv[1]);
  double XX1=-L/2;
  double XX2=0.0;
  double XX3=0.0;
  double XX4=B;
  double YY1=H;
  double YY2,YY3,YY4;

  std::stringstream output_dir;
  output_dir<<"output_shelf";

  std::stringstream filestabplotname;
  filestabplotname<<output_dir.str()<<"/L_"<<L<<"_stability.dxhm";
  std::ofstream stabfile;
  stabfile.open(filestabplotname.str().c_str(),std::ios::app);


  for(unsigned int i=1;i<51;i++)
    {
      // Create mesh
      int ndx=2*(250/i);
      // To always catch the central point
      // Some meshes will be repeated
      double dx=(B-A)/ndx;
      info("dx=%f",dx);
      IntervalMesh mesh(ndx,A, B);
      hm=H;

      for(unsigned j=0;j<50;j++)
        {
          info("hm=%f",hm);
          YY2=hm;
          YY3=hm;
          YY4=hm;
          Depth depth(XX1,XX2,XX3,XX4,YY1,YY2,YY3,YY4); //Depth
          Constant g_e(9.81);
          EigStdPot::FunctionSpace V(mesh);
          EigStdPot::Form_a t(V, V);
          t.h=depth;
          EigStdPot::Form_L s(V, V);
          s.h=depth;
          s.g=g_e;
          // Assemble the system matrices stiffness (S) and mass matrices (T)
          PETScMatrix S;
          PETScMatrix T;
          assemble(S, s);
          assemble(T, t);

          // Solve the eigen system
          SLEPcEigenSolver esolver(S,T);
          //esolver.parameters["spectrum"] = "smallest real";
          esolver.parameters["solver"] = "lapack";
          esolver.solve();

          // Vector for saving the spectrum
          int N=S.size(1);
          info("The number of eigenvalues is %d", N);
          double realvalues[N];
          double imvalues[N];
          double lr, lc;

          std::stringstream filename;
          filename<<output_dir.str()<<"/L_"<<L<<"_dx_"<<
            dx<<"_hm_"<<hm<<"_omega.xy";
          std::ofstream omegafile(filename.str().c_str());


          for (unsigned int i = 0; i < S.size(1); i++)
            {
              esolver.get_eigenvalue(lr, lc, i);
              // We need to switch the real and imaginary parts (i omega)
              // info("Real(omega) = %g", lc);
              realvalues[i]=lc;
              // info("Im(omega) = %g", lr);
              imvalues[i]=lr;
              //Plot dx vs hm once and if a imaginary part is found
              // For the first hm found
              if(fabs(imvalues[i])>0.00001) /* numerical error */
                {
                  stabfile<<hm<<"\t"<<dx<<std::endl;
                }

              omegafile.precision(25);
              omegafile<<lc<<std::fixed<<"\t"<<lr<<std::fixed<<std::endl;
            }
      hm-=dh;
        }
    }
  return 0;
}

import matplotlib as mpl
import matplotlib.pyplot as plt
from pylab import *
from numpy import fromfile
from matplotlib.font_manager import FontProperties


params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 12,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)


L=[0.5,1,2]
dx=[0.02,0.1,0.25]

def ur(hm):
    return  (1-hm*2)*(hm<0.5)
def ug(hm):
    return (hm*2)*(hm<0.5) +(1-(2*hm-1))*(hm>0.48)
def ub(hm):
    return (2*hm-1)*(hm>0.48)





def plotfig(l,dxx):
    ax=subplot(111)
    subplots_adjust(left=0.13,bottom=0.13,right=0.92,top=0.92)
    step=1
    outfilepng='L_'+str(l)+'_dx_'+str(dxx)+'_hm_shelf_spectrum_StdPot.png'
    infile='output_shelf/L_'+str(l)+'_dx_'+str(dxx)+'_hm_'
    print outfilepng


    for j in range(0,50):
        hm=1.0-j*0.02
        if j==0 :
            filename=infile+'1_omega.xy'
        else:
            filename=infile+str(hm)+'_omega.xy'

        u1 = fromfile(filename,sep=" ")
        u1.shape=len(u1)//2,2
        us=ones((len(u1)//(step),2))
        for i in range(0,len(us)//(step)):
            us[i,0]=u1[i*step,0]
            us[i,1]=u1[i*step,1]
        ax.plot(us[:,0], us[:,1],'o',color=(ur(1-hm),ug(1-hm),ub(1-hm)))

    (xname, yname) = ('$Re(\\omega)\,({\\tt s}^{-1})$', '$Im(\\omega)\,({\\tt s}^{-1})$')
    title('Standard Potential Model over shelf geometry',fontsize=18)
    xlabel(xname,fontsize=24)
    ylabel(yname,fontsize=24)
    grid()
    savefig(outfilepng)
    clf()


plotfig(0.5,0.25)
plotfig(1,0.1)
plotfig(2,0.02)















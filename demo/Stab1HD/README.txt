In this demo we reproduce some of the results
that were presented in
"N.D. Lopes, P.J.S Pereira and L. Trabucho, "Improved
Boussinesq Equations for Surface Water Waves", Automated
Scientific Computing, Eds. A. Logg K.A. Mardal and
G.N. Wells, Springer 2011",
regarding the stability of some models.

import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 24,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='$t=4$'


ax=subplot(111,autoscale_on='false')


filename='PA/output/eta000040.xyz'
meshsize=25./501.;

u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=zeros((len(u1)//1,2))

for i in range(0,len(u1)//(1)):
    us[i,0]=u1[i,0]
    for j in range(0,i):
        us[i,1]=us[i,1]+u1[j,1]*meshsize

ax.plot(u1[:,0], u1[:,1], label='PA $w$')
ax.plot(us[:,0], us[:,1], label='PA $u$')


filename='JFR/output/eta000040.xyz'


u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=zeros((len(u1)//1,2))

us[0,1]=0.0
us[0,0]=-10.0
for i in range(1,len(u1)//(1)):
    us[i,0]=u1[i,0]
    us[i,1]=(u1[i,1]-u1[i-1,1])/meshsize


ax.plot(us[:,0], us[:,1], label='JFR $w$')
ax.plot(u1[:,0], u1[:,1], label='JFR $u$')

filename='JFR/output_M1/eta000040.xyz'


u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=zeros((len(u1)//1,2))

us[0,1]=0.0
us[0,0]=-10.0
for i in range(1,len(u1)//(1)):
    us[i,0]=u1[i,0]
    us[i,1]=(u1[i,1]-u1[i-1,1])/meshsize


ax.plot(us[:,0], us[:,1], label='JFR $w$')
ax.plot(u1[:,0], u1[:,1], label='JFR $u$')

ax.legend(loc=3)


(xname, yname) = ('$x$', '$w, u$ ')
#text1=text(-9.0,0.5,'JFR $\\delta=0.01$ $\\epsilon=0.3$')
#text1=text(-9.0,1.5,'PA com "viscosidade"')
title(plottitle,fontsize=18)
grid()


ax.set_aspect(4.0)
#ax.set_xlim(-2.0,2.0)
#ax.set_ylim(-2.,2.)
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)
#savefig("t1_3.png")

show()

// Copyright (C) 2012 Nuno David Lopes.-----------------------------------
// e-mail: nlopes@adm.isel.pt
// Licensed under the GNU LGPL Version 3.0 or later.
// First added - nlopes:  2013-01-11
// Last changed - nlopes: 2017-04-24
// Compiled with DOLFWAVE: 0.3.17
// This program provides a demo of the DOLFWAVE library.
// Solitary wave evolution generic
// xKdV-BBM with CDG method


#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

const double XL=0.0;
const double XR=1.0;



class LeftBd : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
    {
      return (x[0]<(XL+XR)/2.0) && on_boundary;
    }
  };

class RightBd : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
    {
      return (x[0]>(XL+XR)/2.0) && on_boundary;
    }
  };


class DchletBd : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary;
  }
};

class InitCond: public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double K= 5.*sqrt(pow(2.02,5.)/(2.*0.0005*6.*7.));
    values[0]=2.02/pow(cosh(K*(x[0]-0.5)),2./5.);
  }
};
// --------------------------------------------------------------------

int main(int argc, char* argv[])
{

  //  Options, Initialization, Mesh and User functions
  Dolfwave dw(1500000 /*MaxSteps*/,
              0.00000002 /*TimeStep*/,
              1000 /*WriteGap*/,
              "xKdVcdg_1D" /*FormName*/,
              "LU_P" /* PETSc LU AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/);


  dw.SetParameter("xKdVDelta0",0.0);
  dw.SetParameter("xKdVEpsilon",1.0);
  dw.SetParameter("xKdVDelta1",0.0);
  dw.SetParameter("xKdVPower",5.0);
  dw.SetParameter("xKdVDelta2",0.0005);
  dw.SetParameter("xKdVGamma",0.0);
  dw.SetParameter("PenaltyTau",0.1);
  dw.SetParameter("VerbosityLevel",0);

  info(parameters,true);// Some info on the used parameters


  IntervalMesh mesh(200,XL,XR);
  MeshFunction<std::size_t> boundary_parts(mesh,mesh.topology().dim()-1,0);
  boundary_parts.set_all(0);
  RightBd Xr;
  LeftBd  Xl;
  Xr.mark(boundary_parts,1);
  Xl.mark(boundary_parts,2);

  InitCond initcond;


  //initial condition for elevation
  dw.SpacesFunctionsVectorsInit(mesh); // Init. Spaces Functions & Vectors
  dw.BilinearFormInit(boundary_parts);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  Constant zero(0.0);
  dw.LinearFormsInit(zero,zero,zero,boundary_parts);
  dw.InitialCondition(initcond); //Initial Conditions L2 projection
  DchletBd dbc;
  dw.DolfwaveDBC(zero,dbc); //Setting Dirichlet Boundary Conditions
  // linf norm of the initial step (compare with zero to get the norm)
  double linfnorm=dw.VectorNorm(mesh,"linf");

  dw.LUFactorization(true); //Re-use factorization
  //dw.PreviewEta(0);//Preview eta initial step
  dw.SaveEta(0);//Save eta initial step


  dw.RKInit("exp4"); // Explicit 4th-order Runge-Kutta
  dw.RKSolve();//Runge-Kutta for 3 initial steps

  dw.PCInit(mesh,true);




  while((dw.Step<dw.MaxSteps)&&(!dw.IsDiverging))
    {
      dw.PCSolve( );
      if (!(dw.Step%dw.WriteGap))
        {      //  Plots and infos
          dw.PreviewEta();
          //dw.SaveEta();
          linfnorm=dw.VectorNorm(mesh,"linf");
          if (linfnorm>6.0) dw.IsDiverging = true ;
          dw.Info(false);
        }


    }

  dw.SaveEta();
  return (EXIT_SUCCESS);
}

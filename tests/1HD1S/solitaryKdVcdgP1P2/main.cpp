// Copyright (C) 2012 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This program provides a demo of the DOLFWAVE library.
// Solitary wave evolution by adimensional generic
// KdV-BBM with CDG method

#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;

/* Global Variables*/
const double A=-20.; //
const double B=200.;  // $x\in[A.B]$
const double eps=0.1;
const double mu=0.31623;

class ElevationInitKdV: public Expression
{
public:
  ElevationInitKdV(const double& delta0,const double& epsilon,
                   const double& delta1,const double& delta2):
    delta0(delta0), epsilon(epsilon), delta1(delta1), delta2(delta2) {}
  void eval(Array<double> & values,const Array<double> & x) const
  {
    /*
       Definition of the solitary wave solution
       for the adimensional generic KdV-BBM equations
    */

    double c=delta0+delta0/80.; //Free parameter for speed/wave height
    double b=3./2.*epsilon; //Nonlinear coeff.
    double Psi=3.*(c-delta0)/b;
    double Theta=1./2.*sqrt((c-delta0)/(delta2-c*delta1));
    values[0]=Psi/sqr(cosh(Theta*x[0]));
  }
private:
  const double &delta0;
  const double &epsilon;
  const double &delta1;
  const double &delta2;
};
// --------------------------------------------------------------------

int main(int argc, char* argv[])
{
  bool swtch=true;
  if (argc!=4)
    {
      info("Wrong number of arguments");
      info("argv[1]='2' for KdV-BBM or argv[1]='1' for KdV");
      info("argv[2]=penalty_tau");
      info("argv[3]=output_dir");
      exit(EXIT_FAILURE);
    }
  std::stringstream output_dir;
  output_dir<<argv[3];
  //  Options, Initialization, Mesh and User functions
  Dolfwave dw(120000 /*MaxSteps*/,
              0.001 /*TimeStep*/,
              1000 /*WriteGap*/,
              "aKdVcdgP1P2_1D" /*FormName*/,
              "LU_P" /* PETSc LU AlgBackEND*/,
              "dolfin_animated" /*Plotter*/,
              output_dir.str() /*OutDir*/,
              "xyz"/*OutFormat*/);

  if (atoi(argv[1])==1)
    dw.SetParameter("KdVPade",KDV_21_PADE);
  else if (atoi(argv[1])==2)
    dw.SetParameter("KdVPade",KDV_22_PADE);

  double kdvp=parameters("dolfwave_parameters")["KdVPade"];
  double delta1=(kdvp-1.)*mu*mu/6.; //Sec.-ord. disp. coeff.
  double delta2=kdvp*mu*mu/6.; //Third-ord. disp. coeff.

  dw.SetParameter("aKdVDelta0",1.0);
  dw.SetParameter("aKdVEpsilon",eps);
  dw.SetParameter("aKdVDelta1",delta1);
  dw.SetParameter("aKdVDelta2",delta2);
  dw.SetParameter("PenaltyTau",atof(argv[2]));
  info(parameters,true);// Some info on the used parameters
  IntervalMesh mesh(1001,A,B);
  ElevationInitKdV eta_init(parameters("dolfwave_parameters")["aKdVDelta0"],
                            parameters("dolfwave_parameters")["aKdVEpsilon"],
                            parameters("dolfwave_parameters")["aKdVDelta1"],
                            parameters("dolfwave_parameters")["aKdVDelta2"]);
  //initial condition for elevation
  dw.SpacesFunctionsVectorsInit(mesh); // Init. Spaces Functions & Vectors
  dw.BilinearFormInit(mesh);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LinearFormsInit( );
  dw.InitialCondition(eta_init); //Initial Conditions L2 projection
  Constant zero(0.0);
  //dw.Volume(mesh,zero);
  dw.LUFactorization(true ); //Re-use factorization
  dw.SaveEta(0);
  dw.PreviewEta(0);
  dw.RKInit("exp4"); // Explicit 4th-order Runge-Kutta
  dw.RKSolve();//Runge-Kutta for 3 initial steps
  dw.PCInit(mesh,true);

  for(int i=4; i<dw.MaxSteps+1;i++)
    {
      dw.PCSolve( );
      if (!(i%dw.WriteGap))
        { // Plots
          dw.PreviewEta();
          dw.SaveEta();
          //dw.Volume(mesh,zero);
          dw.Info(true);// Output some extra information on memory usage
        }
      dw.Info(false);
    }
  return (EXIT_SUCCESS);
}

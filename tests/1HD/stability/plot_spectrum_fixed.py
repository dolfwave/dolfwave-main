import matplotlib as mpl
import matplotlib.pyplot as plt
from pylab import *
from numpy import fromfile
from matplotlib.font_manager import FontProperties


params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 12,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

(xname, yname) = ('$Re(\\omega)$', '$Im(\\omega)$')
xlabel(xname,fontsize=24)
ylabel(yname,fontsize=24)

#ax=plt.figure()
#ax=Axes3D(fig)

L=[0.5,1,2]
dx=[0.02,0.04,0.1,0.142857,0.25,0.5,0.625,1]

hm=0.02

def ur(hm):
    return  (1-hm*2)*(hm<0.5)
def ug(hm):
    return (hm*2)*(hm<0.5) +(1-(2*hm-1))*(hm>0.48)
def ub(hm):
    return (2*hm-1)*(hm>0.48)





def plotfig(l,dxx,hm):
    ax=subplot(111)
    subplots_adjust(left=0.13,bottom=0.13,right=0.92,top=0.92)
    step=1
    outfilepng='L_'+str(l)+'_dx_'+str(dxx)+'_hm'+str(hm)+'_shelf_spectrum_StdPot.png'
    outfilepdf='L_'+str(l)+'_dx_'+str(dxx)+'_hm'+str(hm)+'_shelf_spectrum_stdPot.pdf'
    infile='LinearStdPot/output_shelf/L_'+str(l)+'_dx_'+str(dxx)+'_hm_'
    print outfilepng
    print outfilepdf


    filename=infile+str(hm)+'omega.xy'

    u1 = fromfile(filename,sep=" ")
    u1.shape=len(u1)//2,2
    us=ones((len(u1)//(step),2))
    #uz=ones(len(u1))
    for i in range(0,len(us)//(step)):
        us[i,0]=u1[i*step,0]
        us[i,1]=u1[i*step,1]
        #uz[i]=hm
    ax.plot(us[:,0], us[:,1],'o',color=(ur(1-hm),ug(1-hm),ub(1-hm)),label='$h_m=$'+str(hm))
    legend(loc='best')
    ax.ticklabel_format(axis='y', style='sci',scilimits=(2,2))
    grid()
    savefig(outfilepdf)
    savefig(outfilepng)
    clf()

for l in L[:] :
    for dxx in dx[:]:
        plotfig(l,dxx,hm)


#plt.legend(loc=(1.1,0))
#alignment = {'horizontalalignment':'center', 'verticalalignment':'baseline'}
#family = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
#font0 = FontProperties()
#font1 = font0.copy()
#font1.set_size(14)#
#text0 = text(20., -0.3, '$-h(x,t)$', fontproperties=font1,**alignment)
#text1 = text(30., 0.42, 'Zhao\_1D', fontproperties=font1,**alignment)



#ax.set_aspect(4.0)
#plt.set_xlim(0,10.0)
#plt.set_ylim(.0,10.)

#show()













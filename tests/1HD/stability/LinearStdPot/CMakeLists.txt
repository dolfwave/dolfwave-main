# Require CMake 2.8
cmake_minimum_required(VERSION 2.8)

project(1HD_lstability_StdPot_test)

# Set CMake behavior
cmake_policy(SET CMP0004 OLD)

# Get DOLFWAVE configuration data (dolfwave-config.cmake must be in DOLFWAVE_CMAKE_CONFIG_PATH)
find_library(DOLFWAVE_LIBRARY dolfwave)

# Compiler definitions
add_definitions(${DOLFWAVE_CXX_DEFINITIONS})

# Include directories
include_directories(${DOLFWAVE_SOURCE_DIR}/src)

# Executable
add_executable(${PROJECT_NAME} main.cpp)

# Target libraries
target_link_libraries(${PROJECT_NAME} dolfwave)

import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



ax=subplot(111,autoscale_on='false')
subplots_adjust(left=0.13,bottom=0.13,right=0.92,top=0.92)

step=1


outfilepdf='L_2_spike_stability.pdf'
outfilepng='L_2_spike_stability.png'

filename='LinearStdPot/output_spike/L_2_stability.dxhm'

u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],"o",markersize=4, label='Std.Pot.')


filename='LinearZhao/output_spike/L_2_stability.dxhm'

u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],"ro", markersize=10,label='ZTC')
ax.legend(loc=1)
#alignment = {'horizontalalignment':'center', 'verticalalignment':'baseline'}
#family = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
#font0 = FontProperties()
#font1 = font0.copy()
#font1.set_size(14)#
#text0 = text(20., -0.3, '$-h(x,t)$', fontproperties=font1,**alignment)
#text1 = text(30., 0.42, 'Zhao\_1D', fontproperties=font1,**alignment)


(xname, yname) = ('$h_m$ {\\tt (m)}', '$\\triangle x$ {\\tt (m)}')
ax.set_aspect(1.0)
ax.set_xlim(0,1.0)
ax.set_ylim(.0,1.)
xlabel(xname,fontsize=24)
ylabel(yname,fontsize=24)
grid()
#show()
savefig(outfilepdf)
savefig(outfilepng)
clf()














// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
//
// Licensed under the GNU LGPL Version 3.0 or later.
//
// EigZhaoA.ufl
//
// First added:  2011-01-21
// Last changed: 2011-01-21
//
//
// See Pederson and Lovholt 2009 and Fuhrman Thesis 2004
// See alse dolfin waveguide demo:
// Copyright (C) 2008 Evan Lezar (evanlezar@gmail.com).

#include <dolfin.h>
#include "EigZhao.h"
#include <iomanip>
using namespace dolfin;

// Testing the spike geometry



class Depth :  public Expression
{
public:
  Depth (const double &XX1,const double &XX2,
         const double &XX3,const double &XX4,
         const double &YY1,const double &YY2,
         const double &YY3,const double &YY4):
    XX1(XX1),XX2(XX2),XX3(XX3),XX4(XX4),
    YY1(YY1),YY2(YY2),YY3(YY3),YY4(YY4) {};
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double retrn=0.0;
    double XY1[2]={XX1,YY1};
    double XY2[2]={XX2,YY2};
    double XY3[2]={XX3,YY3};
    double XY4[2]={XX4,YY4};
    double m=((XY2[1]-XY1[1])/(XY2[0]-XY1[0]));
    double m1=((XY4[1]-XY3[1])/(XY4[0]-XY3[0]));
    double b=XY1[1]-m*XY1[0];
    double b1=XY3[1]-m1*XY3[0];

    if(x[0]<=XY1[0])
      retrn=XY1[1];
    else if(x[0]<=XY2[0])
      retrn=m*x[0]+b;
    else if(x[0]<=XY3[0])
      retrn=XY2[1];
    else if(x[0]<=XY4[0])
      retrn=m1*x[0]+b1;
    else
      retrn=XY4[1];

    values[0]=retrn;
  };
private:
  const double &XX1;
  const double &XX2;
  const double &XX3;
  const double &XX4;
  const double &YY1;
  const double &YY2;
  const double &YY3;
  const double &YY4;
};


int main(int argc, char *argv[])
{

  if (argc!=13)
    {
      info("argv[1]='number of dx' argv[2]='x[1]'  argv[3]='x[2]' argv[4]='x[3]' argv[5]='x[4]'");
      info("argv[6]='y[1]'  argv[7]='y[2]' argv[8]='y[3]' argv[9]='y[4]'");
      info("argv[10]='penalty tau'  argv[11]='nonlinear switch' argv[12]='output dir'");
      exit(EXIT_FAILURE);
    }

  double XX1=atof(argv[2]);
  double XX2=atof(argv[3]);
  double XX3=atof(argv[4]);
  double XX4=atof(argv[5]);

  double YY1=atof(argv[6]);
  double YY2=atof(argv[7]);
  double YY3=atof(argv[8]);
  double YY4=atof(argv[9]);


  int ndx=atoi(argv[1]);

  // Not used yet
  double penaltytau=atof(argv[10]);
  double nonlinearswitch=atof(argv[11]);

  // Create mesh
  Interval mesh(ndx,-5., 5.);
  double dx=10./ndx;

  // Define the forms - generates a generalized eigenproblem of the form
  // [S]{h} = i omega [T]{h}
  // with omega the angular frequency of the solutions

  //Depth depth(XX1,XX2,XX3,XX4,YY1,YY2,YY3,YY4); //Depth
  Constant depth(0.4);
  Constant zero(0.0);
  Constant one(1.0);
  Constant g_e(9.81);
  EigZhao::FunctionSpace V(mesh);
  EigZhao::Form_0 t(V, V);
  t.h=depth;
  t.tau=zero;
  t.e=one;

  EigZhao::Form_1 s(V, V);
  s.h=depth;
  s.g=g_e;
  s.sp_eta=zero;
  s.sp_lap_eta=zero;
  s.sp_phi=zero;
  s.sp_lap_phi=zero;

  // Assemble the system matrices stiffness (S) and mass matrices (T)
  PETScMatrix S;
  PETScMatrix T;
  assemble(S, s);
  assemble(T, t);

  // Solve the eigen system
  SLEPcEigenSolver esolver;
  esolver.parameters["spectrum"] = "smallest real";
  esolver.parameters["solver"] = "lapack";
  esolver.solve(S, T);

  // Vector for saving the spectrum
  int N=S.size(1);
  info("The number of eigenvalues is %d", N);
  double realvalues[N];
  double imvalues[N];
  double lr, lc;

  std::stringstream filename;
  double hm=YY2;
  filename<<argv[12]<<"/dx"<<dx<<"hm"<<hm<<"omega.xy";
  std::ofstream omegafile(filename.str().c_str());

  for (unsigned int i = 0; i < S.size(1); i++)
    {
      esolver.get_eigenvalue(lr, lc, i);

      // We need to switch the real and imaginary parts (i omega)
      info("Real(omega) = %g", lc);
      realvalues[i]=lc;
      info("Im(omega) = %g", lr);
      imvalues[i]=lr;
      omegafile.precision(25);
      omegafile<<lc<<std::fixed<<"\t"<<lr<<std::fixed<<std::endl;
    }

  return 0;
}


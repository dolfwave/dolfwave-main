import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 1,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile



plottitle='Unstable modes.'
ax=subplot(111,autoscale_on='false')
subplots_adjust(left=0.13,bottom=0.13,right=0.92,top=0.92)

step=2

filename='output_shelf/L_2_stability.dxhm'

u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],"b", label='')



#ax.legend(loc=1)


#alignment = {'horizontalalignment':'center', 'verticalalignment':'baseline'}
#family = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
#font0 = FontProperties()
#font1 = font0.copy()
#font1.set_size(14)#
#text0 = text(20., -0.3, '$-h(x,t)$', fontproperties=font1,**alignment)
#text1 = text(30., 0.42, 'Zhao\_1D', fontproperties=font1,**alignment)


#(xname, yname) = ('$x$', 'time $0-40$ (s)')
#title(plottitle,fontsize=18)
#ax.set_aspect(30.0)
#ax.set_xlim(-25.0,25.0)
#ax.set_ylim(-.8,0.7)
#xlabel(xname,fontsize=24)
#ylabel(yname,fontsize=24)
show()















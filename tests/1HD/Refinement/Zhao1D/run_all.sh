#!/bin/sh

#dir1="mesh21"
#dir2="mesh41"
#dir3="mesh51"
#dir4="mesh71"
#dir5="mesh81"
dir6="mesh101"
dir7="mesh121"
dir8="mesh151"

#mkdir $dir1
#mkdir $dir2
#mkdir $dir3
#mkdir $dir4
#mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8

#zhao1D_refinement_test 20  $dir1> $dir1/logfile.txt 2>$dir1/ERROr.txt &
#zhao1D_refinement_test 40   $dir2> $dir2/logfile.txt 2>$dir2/ERROr.txt &
#zhao1D_refinement_test 50  $dir3> $dir3/logfile.txt 2>$dir3/ERROr.txt &
#zhao1D_refinement_test 70  $dir4> $dir4/logfile.txt 2>$dir4/ERROr.txt &
#zhao1D_refinement_test 80  $dir5> $dir5/logfile.txt 2>$dir5/ERROr.txt &
zhao1D_refinement_test 100  $dir6> $dir6/logfile.txt 2>$dir6/ERROr.txt &
zhao1D_refinement_test 121 $dir7> $dir7/logfile.txt 2>$dir7/ERROr.txt &
zhao1D_refinement_test 151 $dir8> $dir8/logfile.txt 2>$dir8/ERROr.txt &

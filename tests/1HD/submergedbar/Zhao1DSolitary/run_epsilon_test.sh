#!/bin/sh

dir1="epsilon0_1_375dx_bc"
dir2="epsilon0_1_750dx_bc"
#dir3="epsilon0_5_750dx"
#dir4="epsilon0_6_750dx"
#dir5="epsilon0_7_750dx"
#dir6="epsilon0_8_750dx"
#dir7="epsilon0_9_750dx"
#dir8="epsilon1_750dx"

mkdir $dir1
mkdir $dir2
#mkdir $dir3
#mkdir $dir4
#mkdir $dir5
#mkdir $dir6
#mkdir $dir7
#mkdir $dir8

1HD_submergedbar_test 375 13. 15. 15. 0.1     $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
1HD_submergedbar_test 750 13. 15. 15. 0.1    $dir2 > $dir2/logfile.txt 2>$dir2/ERROr.txt &
#1HD_submergedbar_test 750 13. 15. 15. 0.02    $dir3> $dir3/logfile.txt 2>$dir3/ERROr.txt &
#1HD_submergedbar_test 750 13. 15. 15. 0.0167  $dir4> $dir4/logfile.txt 2>$dir4/ERROr.txt &
#1HD_submergedbar_test 750 13. 15. 15. 0.0143  $dir5> $dir5/logfile.txt 2>$dir5/ERROr.txt &
#1HD_submergedbar_test 750 13. 15. 15. 0.0125  $dir6> $dir6/logfile.txt 2>$dir6/ERROr.txt &
#1HD_submergedbar_test 750 13. 15. 15. 0.0111  $dir7> $dir7/logfile.txt 2>$dir7/ERROr.txt &
#1HD_submergedbar_test 750 13. 15. 15. 0.01    $dir8> $dir8/logfile.txt 2>$dir8/ERROr.txt &

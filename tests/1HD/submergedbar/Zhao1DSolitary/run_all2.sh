#!/bin/sh

dir1="high_step6_refined"
#dir2="high_step7_refined"
#dir3="high_step8_refined"
dir4="high_step9_refined"
#dir5="high_step10_refined"
dir6="high_step11_refined"
dir7="high_step115_refined"
dir8="high_step119_refined"

mkdir $dir1
#mkdir $dir2
#mkdir $dir3
mkdir $dir4
#mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8

1HD_submergedbar_test 6. 12. 14. 0.05     false  $dir1> $dir1/logfile.txt 2>$dir1/ERROr.txt &
#1HD_submergedbar_test 7. 12. 14. 0.05    false   $dir2> $dir2/logfile.txt 2>$dir2/ERROr.txt &
#1HD_submergedbar_test 8. 12. 14. 0.05    false   $dir3> $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_test 9. 12. 14. 0.05    false   $dir4> $dir4/logfile.txt 2>$dir4/ERROr.txt &
#1HD_submergedbar_test 10. 12. 14. 0.05  false    $dir5> $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_test 11. 12. 14. 0.05  false    $dir6> $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_test 11.5 12. 14. 0.05 false    $dir7> $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_test 11.9 12. 14. 0.05  false   $dir8> $dir8/logfile.txt 2>$dir8/ERROr.txt &

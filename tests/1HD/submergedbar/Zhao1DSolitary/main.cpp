// Copyright (C) 2009 Nuno David Lopes.----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Solitary wave over a submerged bar

#include <dolfwave.h>
using namespace dolfin::dolfwave;

const    double c=sqrt(1.025), H=0.4;
const    double ca=-0.4, cb=ca+1.0/3.0;
const    double center=-10.0;
const    double a1=(H/3.0)*(sqr(c)-1)/(cb-ca*sqr(c));
const    double a2=-(H/2.0)*sqr((sqr(c)-1)/c)*(cb+2.0*ca*sqr(c))/(cb-ca*sqr(c));
const    double k=(1.0/(2.0*H))*sqrt((sqr(c)-1)/(cb-ca*sqr(c)));
const    double a3=sqrt(H*g_e)*(sqr(c)-1)/c;
const    double cnst=4.0*a3/(2.0*k*(1+exp(2.0*k*(-25.)))); //Constant of integration

class ElevationInit : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    values[0]=a1/sqr(cosh(k*(x[0]-center)))+a2/sqr(sqr(cosh(k*(x[0]-center))));
  }
};

class PotentialInit :  public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    values[0]=-4.0*a3/(2.0*k*(1+exp(2.0*k*(x[0]-center))))+cnst;
  }
};

 class Depth :  public Expression
  {
  public:
    Depth (const double &XX1,const double &XX2,
           const double &XX3,const double &YY):
      XX1(XX1),
      XX2(XX2),
      XX3(XX3),
      YY(YY) {};
    void eval(Array<double> & values,const Array<double> & x) const
    {
      double retrn=0.0;
      double XY1[2]={XX1,0.4};
      double XY2[2]={XX2,YY};
      double XY3[2]={XX3,YY};
      double XY4[2]={17.,0.4};
      double m=((XY2[1]-XY1[1])/(XY2[0]-XY1[0]));
      double m1=((XY4[1]-XY3[1])/(XY4[0]-XY3[0]));
      double b=XY1[1]-m*XY1[0];
      double b1=XY3[1]-m1*XY3[0];

      if(x[0]<=XY1[0])
        retrn=XY1[1];
      else if(x[0]<=XY2[0])
        retrn=m*x[0]+b;
      else if(x[0]<=XY3[0])
        retrn=XY2[1];
      else if(x[0]<=XY4[0])
        retrn=m1*x[0]+b1;
      else
        retrn=XY4[1];

      values[0]=retrn;
    };
  private:
    const double &XX1;
    const double &XX2;
    const double &XX3;
    const double &YY;
  };



int main(int argc, char *argv[])
{
  if (argc!=7)
    {
      info("argv[1]='number of dx' argv[2]='x[1]'  argv[3]='x[2]' argv[4]='x[3]' argv[5]='y[2]=y[3]'  argv[6]='output dir'");
      exit(EXIT_FAILURE);
    }

 double XX1=atof(argv[2]);
 double XX2=atof(argv[3]);
 double XX3=atof(argv[4]);
 double YY=atof(argv[5]);
 int ndx=atoi(argv[1]);

  // {{{ Options, Initialization, Mesh and User functions
  Dolfwave dw(
              100001 /*MaxSteps*/,
              0.00025 /*TimeStep*/,
              400   /*WriteGap*/,
              "Zhao_1D" /*FormName*/,
              "LU_P" /*SolverType*/,
              "viper" /*Plotter*/,
              argv[6] /*OutDir*/,
              "xyz"/*OutFormat*/
              );
  Interval mesh(ndx,-25.,50.);
  Depth depth(XX1,XX2,XX3,YY); //Depth
  Constant zero(0.0);//Sponge Layers and source function
  PotentialInit phi_init;//initial condition for potential
  ElevationInit eta_init;//initial condition for elevation
  dw.FunctionSpaceInit(mesh); //Function Spaces Init
  dw.BilinearFormInit(mesh,depth);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.FunctionsInit( );// Solutions Functions (potential,elevation)
  dw.LinearFormsInit(depth, zero /*sp_eta*/, zero /*sp_lap_eta*/,
                     zero /*sp_phi*/, zero/*sp_lap_phi */, zero);
  dw.InitialCondition(eta_init,phi_init); //Initial Conditions L2 projection
  dw.Volume(mesh,depth);
  dw.VectorInit( );//Auxiliar vectors Initialization
  dw.LUFactorization( ); //Solver type
  dw.DepthPlot(mesh,depth,false); //Plotting Depth function
  dw.RKInit("exp4" /*RKtype*/);
  dw.Plot(mesh,false /*eta Plotter*/, false/*phi Plotter*/,
          true /*SaveEta*/,false /*SavePhi*/);
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
  dw.PCInit(mesh);
  for(dolfin::uint i=4; i<dw.MaxSteps+1;i++)
    {
      dw.PCSolve( ); //Predictor-Corrector

      if (!(i%dw.WriteGap))
        { //  Plot information------------------------
          dw.Plot(mesh,false /*eta Plotter*/, false/*phi Plotter*/,
                  true /*SaveEta*/,false /*SavePhi*/);
          dw.Volume(mesh,depth);
          dw.Info(i);
        }
    }
  return (EXIT_SUCCESS);
}

from math import *

#insert wave amp at incident Boundary
A=.01

#insert water depth at incident Boundary
H=0.4

#insert gravity acceleration
g=9.81

print '(A) Wave amplitude        '+str(A)
print '(H) Water Depth           '+str(H)


eps=A/H

print '(eps) Small amp. param    '+str(eps)
mu=sqrt(eps)
print '(mu) Long wave param      '+str(mu)

k=sqrt(eps)*2*pi/H
print '(k) Wave number (x-direc) '+str(k)

#Angular frequency by dispersion relation
omega=sqrt(g*H*k**2*(1.+(1./15.)*(k*H)**2)/(1.+(2./5.)*(k*H)**2))
print '(omega) Angular frequency '+str(omega)

P=2*pi/omega
print '(P) Wave period           '+str(P)


L=2*pi/k
print '(L) Wave lenght (x-direc) '+str(L)

b=A*omega*(1.+(2./5.)*(k*H)**2)/(k**2*H)
print '(b) Potential magnitude   '+str(b)

c=b*k
print '(c) Velocity amplitude    '+str(c)
print '''\n /* Start:C++ code from datacalculator.py */ '''
print 'const double HH='+str(H)+';'
print 'const double AA='+str(A)+';'
print 'const double OO='+str(omega)+';'
print 'const double SM_EPS='+str(eps)+';'
print 'const double KK='+str(k)+';'
print 'const double MM='+str(mu)+';'
print 'const double LL='+str(L)+';'
print 'const double BB='+str(b)+';'
print 'const double CC='+str(c)+';'
print ''' /* End: C++ code from datacalculator.py */ '''

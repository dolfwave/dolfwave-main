#!/bin/sh


ndx=300

sl=0.0

# nl=1.0
# stab=0.0

# dir1="sl0_nl_eps0_300dx"
# dir2="sl0_nl_eps01_300dx"
# dir3="sl0_nl_eps02_300dx"
# dir4="sl0_nl_eps03_300dx"
# dir5="sl0_nl_eps05_300dx"
# dir6="sl0_nl_eps06_300dx"
# dir7="sl0_nl_eps07_300dx"
# dir8="sl0_nl_eps09_300dx"



# mkdir $dir1
# mkdir $dir2
# mkdir $dir3
# mkdir $dir4
# mkdir $dir5
# mkdir $dir6
# mkdir $dir7
# mkdir $dir8

# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.3   0.3   0.3    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.2   0.2   0.2    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.1   0.1   0.1    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   .05   .05   .05    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .025  .025  .025   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .0125 .0125 .0125  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  0.01  0.01  0.01   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &


# nl=0.0
# stab=0.0

# dir1="sl0_l_eps0_300dx"
# dir2="sl0_l_eps01_300dx"
# dir3="sl0_l_eps02_300dx"
# dir4="sl0_l_eps03_300dx"
# dir5="sl0_l_eps05_300dx"
# dir6="sl0_l_eps06_300dx"
# dir7="sl0_l_eps07_300dx"
# dir8="sl0_l_eps09_300dx"



# mkdir $dir1
# mkdir $dir2
# mkdir $dir3
# mkdir $dir4
# mkdir $dir5
# mkdir $dir6
# mkdir $dir7
# mkdir $dir8

# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.3   0.3   0.3    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.2   0.2   0.2    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.1   0.1   0.1    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   .05   .05   .05    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  .025  .025  .025   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .0125 .0125 .0125  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  0.01  0.01  0.01   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



nl=1.0
stab=0.0001

dir1="sl0_stab_nl_eps0_300dx"
dir2="sl0_stab_nl_eps01_300dx"
dir3="sl0_stab_nl_eps02_300dx"
dir4="sl0_stab_nl_eps03_300dx"
dir5="sl0_stab_nl_eps05_300dx"
dir6="sl0_stab_nl_eps06_300dx"
dir7="sl0_stab_nl_eps07_300dx"
dir8="sl0_stab_nl_eps09_300dx"



mkdir $dir1
mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8

1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.3   0.3   0.3    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.2   0.2   0.2    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.1   0.1   0.1    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   .05   .05   .05    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  .025  .025  .025   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .0125 .0125 .0125  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  0.01  0.01  0.01   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &




sl=4.0

# nl=1.0
# stab=0.0

# dir1="sl4_0_nl_eps0_300dx"
# dir2="sl4_0_nl_eps01_300dx"
# dir3="sl4_0_nl_eps02_300dx"
# dir4="sl4_0_nl_eps03_300dx"
# dir5="sl4_0_nl_eps05_300dx"
# dir6="sl4_0_nl_eps06_300dx"
# dir7="sl4_0_nl_eps07_300dx"
# dir8="sl4_0_nl_eps09_300dx"



# mkdir $dir1
# mkdir $dir2
# mkdir $dir3
# mkdir $dir4
# mkdir $dir5
# mkdir $dir6
# mkdir $dir7
# mkdir $dir8

# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.3   0.3   0.3    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.2   0.2   0.2    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.1   0.1   0.1    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   .05   .05   .05    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  .025  .025  .025   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .0125 .0125 .0125  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  0.01  0.01  0.01   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &


# nl=0.0
# stab=0.0

# dir1="sl4_0_l_eps0_300dx"
# dir2="sl4_0_l_eps01_300dx"
# dir3="sl4_0_l_eps02_300dx"
# dir4="sl4_0_l_eps03_300dx"
# dir5="sl4_0_l_eps05_300dx"
# dir6="sl4_0_l_eps06_300dx"
# dir7="sl4_0_l_eps07_300dx"
# dir8="sl4_0_l_eps09_300dx"



# mkdir $dir1
# mkdir $dir2
# mkdir $dir3
# mkdir $dir4
# mkdir $dir5
# mkdir $dir6
# mkdir $dir7
# mkdir $dir8

# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.3   0.3   0.3    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.2   0.2   0.2    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.1   0.1   0.1    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   .05   .05   .05    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  .025  .025  .025   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .0125 .0125 .0125  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  0.01  0.01  0.01   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



nl=1.0
stab=0.0001

dir1="sl4_0_stab_nl_eps0_300dx"
dir2="sl4_0_stab_nl_eps01_300dx"
dir3="sl4_0_stab_nl_eps02_300dx"
dir4="sl4_0_stab_nl_eps03_300dx"
dir5="sl4_0_stab_nl_eps05_300dx"
dir6="sl4_0_stab_nl_eps06_300dx"
dir7="sl4_0_stab_nl_eps07_300dx"
dir8="sl4_0_stab_nl_eps09_300dx"



mkdir $dir1
mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8

1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.3   0.3   0.3    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.2   0.2   0.2    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.1   0.1   0.1    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   .05   .05   .05    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  .025  .025  .025   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .0125 .0125 .0125  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  0.01  0.01  0.01   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &




sl=4.8

# nl=1.0
# stab=0.0

# dir1="sl4_8_nl_eps0_300dx"
# dir2="sl4_8_nl_eps01_300dx"
# dir3="sl4_8_nl_eps02_300dx"
# dir4="sl4_8_nl_eps03_300dx"
# dir5="sl4_8_nl_eps05_300dx"
# dir6="sl4_8_nl_eps06_300dx"
# dir7="sl4_8_nl_eps07_300dx"
# dir8="sl4_8_nl_eps09_300dx"



# mkdir $dir1
# mkdir $dir2
# mkdir $dir3
# mkdir $dir4
# mkdir $dir5
# mkdir $dir6
# mkdir $dir7
# mkdir $dir8

# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.3   0.3   0.3    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.2   0.2   0.2    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.1   0.1   0.1    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   .05   .05   .05    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  .025  .025  .025   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .0125 .0125 .0125  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  0.01  0.01  0.01   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &


# nl=0.0
# stab=0.0

# dir1="sl4_8_l_eps0_300dx"
# dir2="sl4_8_l_eps01_300dx"
# dir3="sl4_8_l_eps02_300dx"
# dir4="sl4_8_l_eps03_300dx"
# dir5="sl4_8_l_eps05_300dx"
# dir6="sl4_8_l_eps06_300dx"
# dir7="sl4_8_l_eps07_300dx"
# dir8="sl4_8_l_eps09_300dx"



# mkdir $dir1
# mkdir $dir2
# mkdir $dir3
# mkdir $dir4
# mkdir $dir5
# mkdir $dir6
# mkdir $dir7
# mkdir $dir8

# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.3   0.3   0.3    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.2   0.2   0.2    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.1   0.1   0.1    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   .05   .05   .05    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  .025  .025  .025   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .0125 .0125 .0125  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
# 1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  0.01  0.01  0.01   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



nl=1.0
stab=0.0001

dir1="sl4_8_stab_nl_eps0_300dx"
dir2="sl4_8_stab_nl_eps01_300dx"
dir3="sl4_8_stab_nl_eps02_300dx"
dir4="sl4_8_stab_nl_eps03_300dx"
dir5="sl4_8_stab_nl_eps05_300dx"
dir6="sl4_8_stab_nl_eps06_300dx"
dir7="sl4_8_stab_nl_eps07_300dx"
dir8="sl4_8_stab_nl_eps09_300dx"



mkdir $dir1
mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8

1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.3   0.3   0.3    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.2   0.2   0.2    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   0.1   0.1   0.1    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4   .05   .05   .05    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  .025  .025  .025   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4 .0125 .0125 .0125  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sl 5. 5.0 5.0 0.4  0.01  0.01  0.01   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &

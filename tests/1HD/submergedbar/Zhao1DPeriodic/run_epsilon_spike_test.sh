#!/bin/sh


ndx=150

sp1=1.0
sp2=9.0

nl=1.0
stab=0.0

dir1="sp1_0_nl_eps0_150dx"
dir2="sp1_0_nl_eps01_150dx"
dir3="sp1_0_nl_eps02_150dx"
dir4="sp1_0_nl_eps03_150dx"
dir5="sp1_0_nl_eps05_150dx"
dir6="sp1_0_nl_eps06_150dx"
dir7="sp1_0_nl_eps07_150dx"
dir8="sp1_0_nl_eps09_150dx"



mkdir $dir1
mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8

1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &


nl=0.0
stab=0.0

dir1="sp1_0_l_eps0_150dx"
dir2="sp1_0_l_eps01_150dx"
dir3="sp1_0_l_eps02_150dx"
dir4="sp1_0_l_eps03_150dx"
dir5="sp1_0_l_eps05_150dx"
dir6="sp1_0_l_eps06_150dx"
dir7="sp1_0_l_eps07_150dx"
dir8="sp1_0_l_eps09_150dx"



mkdir $dir1
mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8

1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



nl=1.0
stab=0.0001

dir1="sp1_0_stab_nl_eps0_150dx"
dir2="sp1_0_stab_nl_eps01_150dx"
dir3="sp1_0_stab_nl_eps02_150dx"
dir4="sp1_0_stab_nl_eps03_150dx"
dir5="sp1_0_stab_nl_eps05_150dx"
dir6="sp1_0_stab_nl_eps06_150dx"
dir7="sp1_0_stab_nl_eps07_150dx"
dir8="sp1_0_stab_nl_eps09_150dx"



mkdir $dir1
mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8

1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.4   0.4   0.4  $stab $nl $dir1  > $dir1/logfile.txt 2>$dir1/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &




sp1=2.0
sp2=8.0

nl=1.0
stab=0.0


dir2="sp2_0_nl_eps01_150dx"
dir3="sp2_0_nl_eps02_150dx"
dir4="sp2_0_nl_eps03_150dx"
dir5="sp2_0_nl_eps05_150dx"
dir6="sp2_0_nl_eps06_150dx"
dir7="sp2_0_nl_eps07_150dx"
dir8="sp2_0_nl_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &


nl=0.0
stab=0.0


dir2="sp2_0_l_eps01_150dx"
dir3="sp2_0_l_eps02_150dx"
dir4="sp2_0_l_eps03_150dx"
dir5="sp2_0_l_eps05_150dx"
dir6="sp2_0_l_eps06_150dx"
dir7="sp2_0_l_eps07_150dx"
dir8="sp2_0_l_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



nl=1.0
stab=0.0001


dir2="sp2_0_stab_nl_eps01_150dx"
dir3="sp2_0_stab_nl_eps02_150dx"
dir4="sp2_0_stab_nl_eps03_150dx"
dir5="sp2_0_stab_nl_eps05_150dx"
dir6="sp2_0_stab_nl_eps06_150dx"
dir7="sp2_0_stab_nl_eps07_150dx"
dir8="sp2_0_stab_nl_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &




sp1=3.0
sp2=7.0

nl=1.0
stab=0.0


dir2="sp3_0_nl_eps01_150dx"
dir3="sp3_0_nl_eps02_150dx"
dir4="sp3_0_nl_eps03_150dx"
dir5="sp3_0_nl_eps05_150dx"
dir6="sp3_0_nl_eps06_150dx"
dir7="sp3_0_nl_eps07_150dx"
dir8="sp3_0_nl_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &


nl=0.0
stab=0.0


dir2="sp3_0_l_eps01_150dx"
dir3="sp3_0_l_eps02_150dx"
dir4="sp3_0_l_eps03_150dx"
dir5="sp3_0_l_eps05_150dx"
dir6="sp3_0_l_eps06_150dx"
dir7="sp3_0_l_eps07_150dx"
dir8="sp3_0_l_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



nl=1.0
stab=0.0001


dir2="sp3_0_stab_nl_eps01_150dx"
dir3="sp3_0_stab_nl_eps02_150dx"
dir4="sp3_0_stab_nl_eps03_150dx"
dir5="sp3_0_stab_nl_eps05_150dx"
dir6="sp3_0_stab_nl_eps06_150dx"
dir7="sp3_0_stab_nl_eps07_150dx"
dir8="sp3_0_stab_nl_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



sp1=4.0
sp2=6.0

nl=1.0
stab=0.0


dir2="sp4_0_nl_eps01_150dx"
dir3="sp4_0_nl_eps02_150dx"
dir4="sp4_0_nl_eps03_150dx"
dir5="sp4_0_nl_eps05_150dx"
dir6="sp4_0_nl_eps06_150dx"
dir7="sp4_0_nl_eps07_150dx"
dir8="sp4_0_nl_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &


nl=0.0
stab=0.0


dir2="sp4_0_l_eps01_150dx"
dir3="sp4_0_l_eps02_150dx"
dir4="sp4_0_l_eps03_150dx"
dir5="sp4_0_l_eps05_150dx"
dir6="sp4_0_l_eps06_150dx"
dir7="sp4_0_l_eps07_150dx"
dir8="sp4_0_l_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



nl=1.0
stab=0.0001


dir2="sp4_0_stab_nl_eps01_150dx"
dir3="sp4_0_stab_nl_eps02_150dx"
dir4="sp4_0_stab_nl_eps03_150dx"
dir5="sp4_0_stab_nl_eps05_150dx"
dir6="sp4_0_stab_nl_eps06_150dx"
dir7="sp4_0_stab_nl_eps07_150dx"
dir8="sp4_0_stab_nl_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



sp1=4.9
sp2=5.1

nl=1.0
stab=0.0


dir2="sp4_9_nl_eps01_150dx"
dir3="sp4_9_nl_eps02_150dx"
dir4="sp4_9_nl_eps03_150dx"
dir5="sp4_9_nl_eps05_150dx"
dir6="sp4_9_nl_eps06_150dx"
dir7="sp4_9_nl_eps07_150dx"
dir8="sp4_9_nl_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &


nl=0.0
stab=0.0


dir2="sp4_9_l_eps01_150dx"
dir3="sp4_9_l_eps02_150dx"
dir4="sp4_9_l_eps03_150dx"
dir5="sp4_9_l_eps05_150dx"
dir6="sp4_9_l_eps06_150dx"
dir7="sp4_9_l_eps07_150dx"
dir8="sp4_9_l_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &



nl=1.0
stab=0.0001


dir2="sp4_9_stab_nl_eps01_150dx"
dir3="sp4_9_stab_nl_eps02_150dx"
dir4="sp4_9_stab_nl_eps03_150dx"
dir5="sp4_9_stab_nl_eps05_150dx"
dir6="sp4_9_stab_nl_eps06_150dx"
dir7="sp4_9_stab_nl_eps07_150dx"
dir8="sp4_9_stab_nl_eps09_150dx"




mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8


1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.3   0.3   0.4    $stab $nl $dir2  > $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.2   0.2   0.4    $stab $nl $dir3  > $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   0.1   0.1   0.4    $stab $nl $dir4  > $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4   .05   .05   .4    $stab $nl $dir5  > $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  .025  .025  .4   $stab $nl $dir6  > $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4 .0125 .0125 .4  $stab $nl $dir7  > $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test $ndx $sp1 5. 5. $sp2 0.4  0.01  0.01  0.4   $stab $nl $dir8  > $dir8/logfile.txt 2>$dir8/ERROr.txt &

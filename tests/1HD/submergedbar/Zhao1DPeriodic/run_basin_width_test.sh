#!/bin/sh

dir1="short_linear_eps0_100dx"
dir2="short_linear_eps01_100dx"
dir3="short_linear_eps02_100dx"
dir4="short_linear_eps03_100dx"
dir5="short_linear_eps05_100dx"
dir6="short_linear_eps06_100dx"
dir7="short_linear_eps07_100dx"
dir8="short_linear_eps09_100dx"

mkdir $dir1
mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7
mkdir $dir8

1HD_submergedbar_periodic_test 100 5. 5. 5.0 0.4   0.0 $dir1 0.0> $dir1/logfile.txt 2>$dir1/ERROr.txt &
1HD_submergedbar_periodic_test 100 4.8 5. 5.2 0.3   0.0 $dir2 0.0> $dir2/logfile.txt 2>$dir2/ERROr.txt &
1HD_submergedbar_periodic_test 100 4.8 5. 5.2 0.2   0.0 $dir3 0.0> $dir3/logfile.txt 2>$dir3/ERROr.txt &
1HD_submergedbar_periodic_test 100 4.8 5. 5.2 0.1   0.0 $dir4 0.0> $dir4/logfile.txt 2>$dir4/ERROr.txt &
1HD_submergedbar_periodic_test 100 4.8 5. 5.2 0.05   0.0  $dir5 0.0> $dir5/logfile.txt 2>$dir5/ERROr.txt &
1HD_submergedbar_periodic_test 100 4.8 5. 5.2 0.025   0.0  $dir6 0.0> $dir6/logfile.txt 2>$dir6/ERROr.txt &
1HD_submergedbar_periodic_test 100 4.8 5. 5.2 0.0125    0.0 $dir7 0.0> $dir7/logfile.txt 2>$dir7/ERROr.txt &
1HD_submergedbar_periodic_test 100 4.8 5. 5.2 0.01    0.0 $dir8 0.0> $dir8/logfile.txt 2>$dir8/ERROr.txt &

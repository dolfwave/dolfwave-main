// Copyright (C) 2009 Nuno David Lopes.----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Solitary wave over a submerged bar

#include <dolfwave.h>
using namespace dolfin::dolfwave;


 /* Start:C++ code from datacalculator.py */
const double HH=0.4;
const double AA=0.01;
const double OO=4.30069055878;
const double SM_EPS=0.025;
const double KK=2.48364706645;
const double MM=0.158113883008;
const double LL=2.52982212813;
const double BB=0.0243111472098;
const double CC=0.0603803094497;
 /* End: C++ code from datacalculator.py */

// Dirichlet Boundary conditions
class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return (x[0]<DOLFIN_EPS) && on_boundary;
  }
};


class DBC : public Expression
{
public:
  DBC(const double& t) :Expression(2), t(t){}
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double time=t;
    /* Wave input data*/
    double a=AA; /* amplitude  */
    double omega=OO; /* period */
    /*******************/
    /* note: b=c/k potencial magnitude*/
    /*b\approx a*omega*(1+(2/5)*(kH)^2/(k^2 H))->from dispersion relation */
    double k=KK;
    double c=CC;
    values[0]=a*sin(-omega*time);
    values[1]=-(c/k)*cos(-omega*time)+c/k;

  }
private:
  const double& t;
};


class Depth :  public Expression
{
public:
  Depth (const double &XX1,const double &XX2,
         const double &XX3,const double &XX4,
         const double &YY1,const double &YY2,
         const double &YY3,const double &YY4):
    XX1(XX1),XX2(XX2),XX3(XX3),XX4(XX4),
    YY1(YY1),YY2(YY2),YY3(YY3),YY4(YY4) {};
  void eval(Array<double> & values,const Array<double> & x) const
  {
    double retrn=0.0;
    double XY1[2]={XX1,YY1};
    double XY2[2]={XX2,YY2};
    double XY3[2]={XX3,YY3};
    double XY4[2]={XX4,YY4};
    double m=((XY2[1]-XY1[1])/(XY2[0]-XY1[0]));
    double m1=((XY4[1]-XY3[1])/(XY4[0]-XY3[0]));
    double b=XY1[1]-m*XY1[0];
    double b1=XY3[1]-m1*XY3[0];

    if(x[0]<=XY1[0])
      retrn=XY1[1];
    else if(x[0]<=XY2[0])
      retrn=m*x[0]+b;
    else if(x[0]<=XY3[0])
      retrn=XY2[1];
    else if(x[0]<=XY4[0])
      retrn=m1*x[0]+b1;
    else
      retrn=XY4[1];

    values[0]=retrn;
  };
private:
  const double &XX1;
  const double &XX2;
  const double &XX3;
  const double &XX4;
  const double &YY1;
  const double &YY2;
  const double &YY3;
  const double &YY4;
};


int main(int argc, char *argv[])
{


 if (argc!=13)
    {
      info("argv[1]='number of dx' argv[2]='x[1]'  argv[3]='x[2]' argv[4]='x[3]' argv[5]='x[4]'");
      info("argv[6]='y[1]'  argv[7]='y[2]' argv[8]='y[3]' argv[9]='y[4]'");
      info("argv[10]='penalty tau'  argv[11]=' nonlinear switch' argv[12]='output dir'");
      exit(EXIT_FAILURE);
    }

 double XX1=atof(argv[2]);
 double XX2=atof(argv[3]);
 double XX3=atof(argv[4]);
 double XX4=atof(argv[5]);

 double YY1=atof(argv[6]);
 double YY2=atof(argv[7]);
 double YY3=atof(argv[8]);
 double YY4=atof(argv[9]);


 int ndx=atoi(argv[1]);

 // {{{ Options, Initialization, Mesh and User functions
 Dolfwave dw(
             50001 /*200000 MaxSteps*/,
             0.00025 /*TimeStep*/,
             100   /*WriteGap*/,
             "Zhao_1D" /*FormName*/,
             "LU_P" /*SolverType*/,
             "viper" /*Plotter*/,
             argv[12] /*OutDir*/,
             "xyz"/*OutFormat*/
             );

 double penaltytau=atof(argv[10]);
 double nonlinearswitch=atof(argv[11]);
 dw.SetParameter("PenaltyTau",penaltytau);
 dw.SetParameter("NonLinearSwitch",nonlinearswitch);
 info(parameters,true);

 info(" *****************DIRICHLET BOUNDARY DATA**************** ");
 info("(H)      Water Depth at Dirichlet BC      = %.5f",HH);
 info("(A)      Wave  Amp at Dirichlet BC        = %.5f",AA);
 info("(omega)  Wave ang. freq at Dirichlet BC   = %.5f",OO);
 info("(P)      Wave Period at Dirichlet BC      = %.5f",OO*2*DOLFIN_PI);
 info("(eps)    Small amp. param. at Dirichlet BC= %.5f",SM_EPS);
 info("(K)      Wave Number (x) at Dirichlet BC  = %.5f",KK);
 info("(mu)     Long wave param. at Dirichlet BC = %.5f",MM);
 info("(L)      Wave Lenght at Dirichlet BC      = %.5f",LL);
 info("(b)      Wave pot. magn. at Dirichlet BC  = %.5f",BB);
 info("(c)      Wave vel. amp. at Dirichlet BC   = %.5f",CC);

 //Interval mesh(ndx,-25.,50.);
 Interval mesh(ndx,0,15.);
 Depth depth(XX1,XX2,XX3,XX4,YY1,YY2,YY3,YY4); //Depth
 Constant zero(0.0);//Sponge Layers and source function
 DirichletBoundary dirichletboundary;
 DBC bc(dw.Time);
 dw.FunctionSpaceInit(mesh); //Function Spaces Init
 dw.BilinearFormInit(mesh,depth);//BilinearForm a initialization
 dw.MatricesAssemble( ); //Matrices M and Mf initialization
 dw.FunctionsInit( );// Solutions Functions (potential,elevation)
 dw.LinearFormsInit(depth, zero /*sp_eta*/, zero /*sp_lap_eta*/,
                    zero /*sp_phi*/, zero/*sp_lap_phi */, zero);
 dw.Volume(mesh,depth);
 dw.VectorInit( );//Auxiliar vectors Initialization
 dw.DolfwaveDBC(bc,dirichletboundary);//Dirichlet B.C. definition
 dw.LUFactorization( ); //Solver type
 dw.DepthPlot(mesh,depth,false); //Plotting Depth function
 dw.RKInit("exp4" /*RKtype*/);
 dw.Plot(mesh,false /*eta Plotter*/, false/*phi Plotter*/,
         true /*SaveEta*/,false /*SavePhi*/);
 dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
 dw.PCInit(mesh);
 for(dolfin::uint i=4; i<dw.MaxSteps+1;i++)
   {
     dw.PCSolve( ); //Predictor-Corrector

     if (!(i%dw.WriteGap))
       { //  Plot information------------------------
         dw.Plot(mesh,false /*eta Plotter*/, false/*phi Plotter*/,
                 true /*SaveEta*/,false /*SavePhi*/);
         dw.Volume(mesh,depth);
         dw.Info(i);
       }
   }
 return (EXIT_SUCCESS);
}

#!/bin/sh

dir1="tau0_1"
dir2="tau0_05"
dir3="tau0_01"
dir4="tau0_004"
dir5="tau0_001"
dir6="tau0_0005"
dir7="tau0_0001"

mkdir $dir1
mkdir $dir2
mkdir $dir3
mkdir $dir4
mkdir $dir5
mkdir $dir6
mkdir $dir7

2HD_ring_uns 0.1      $dir1> $dir1/logfile.txt 2>$dir1/ERROr.txt &
2HD_ring_uns  0.05   $dir2> $dir2/logfile.txt 2>$dir2/ERROr.txt &
2HD_ring_uns  0.01   $dir3> $dir3/logfile.txt 2>$dir3/ERROr.txt &
2HD_ring_uns  0.004 $dir4> $dir4/logfile.txt 2>$dir4/ERROr.txt &
2HD_ring_uns 0.001  $dir5> $dir5/logfile.txt 2>$dir5/ERROr.txt &
2HD_ring_uns 0.0005 $dir6> $dir6/logfile.txt 2>$dir6/ERROr.txt &
2HD_ring_uns 0.0001 $dir7> $dir7/logfile.txt 2>$dir7/ERROr.txt &

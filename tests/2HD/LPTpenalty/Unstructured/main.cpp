// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Standard Gaussian Wave
#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;

class ElevationInit : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    values[0]=0.045*exp(-2.*(sqr(x[0]-3.0)+sqr(x[1]-3.0)));
  }
};

int main(int argc, char* argv[] )
{
  if (argc!=3)
    {
      info ("argv[1]=PenaltyTau argv[2]=outputdir");
      exit(EXIT_FAILURE);
    }
  std::stringstream volume_file;
  volume_file<<argv[2]<<"/volume.tv";
  std::stringstream file_p1,file_p2;
  file_p1<<argv[2]<<"/p1.te";
  file_p2<<argv[2]<<"/p2.te";

  // {{{ Options, Initialization, Mesh and User functions
  Dolfwave dw(
              160001 /*MaxSteps*/,
              0.00025 /*TimeStep*/,
              50 /*WriteGap*/,
              "LPTcdg" /*FormName*/,
              "LU_P" /*AlgBackEND*/,
              "xd3d" /*Plotter*/,
              argv[2] /*OutDir*/,
              "xyz"/*OutFormat*/,
              "Used in LPT 2011  IJNMF Ex. 4.1"
              );

  double pentau=atof(argv[1]);
  info(parameters,true);
  dw.SetParameter("SymmetrySwitch",0.0);
  dw.SetParameter("PenaltyTau",pentau);
  Mesh mesh("square.xml");
  mesh.order();
  Constant depth(0.45);//Depth
  Constant zero(0.0); //phi init cond.
  Constant tension(0.0);
  ElevationInit eta_init;//initial condition for elevation
  // }}} -----------------------------------------------------------
  // {{{ Functions, Vectors and Matrices ----------------------
  dw.FunctionSpaceInit(mesh); //Function Spaces Init
  dw.BilinearFormInit(mesh,depth,depth);//BilinearForm a initialization
  dw.FunctionsInit( );// Solutions Functions (potential,elevation)
  dw.LinearFormsInit(depth,depth,zero,zero,zero,tension,zero,zero);
  dw.InitialCondition(eta_init,zero); //Initial Conditions L2 projection
  dw.VectorInit( );//Auxiliar vectors Initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.LUFactorization(true); //Reuse  Factorization
  dw.RKInit("exp4");
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
  dw.PCInit(mesh,true);

  for(int i=4; i<dw.MaxSteps+1;i++)
    {

      dw.PCSolve( );
      dw.Volume(mesh,depth,volume_file.str());
      if(!(i%dw.WriteGap))
        {
          dw.Plot(mesh,false /*eta Plotter*/, false/*phi Plotter*/,
                  true /*SaveEta*/,false /*SavePhi*/);
          dw.PointPlot(3.,3.,"eta",file_p1.str());
          dw.PointPlot(.0,.0,"eta",file_p2.str());
        }
      dw.Info(i,false);
    }
  return (EXIT_SUCCESS);
}

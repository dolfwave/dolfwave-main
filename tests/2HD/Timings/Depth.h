
// {{{ Copyright (C) 2009 Nuno David Lopes.-----------------------------------
//
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
//
// ---------------------------------------------------------------------------
// Licensed under the GNU LGPL Version 3.0 or later.
// ---------------------------------------------------------------------------
//  Depth.h
//Mon Jun 21 15:26:50 WEST 2010
//
// To compile  with DOLFIN:0.9.2
// }}}------------------------------------------------------------------------

#ifndef __DEPTH_H
#define __DEPTH_H

#include <dolfwave.h>

using namespace dolfin;
using namespace dolfin::dolfwave;


class Depth : public Expression
{
 public:
  Depth (const double& t): t(t)
  {}
  void eval(Array<double> & values, const Array<double> & x) const;
 private:
  const double& t;
 };

class DtDepth : public Expression
 {
  public:
   DtDepth (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
 private:
   const double& t;
 };

class DtDtDepth : public Expression
 {
  public:
   DtDtDepth (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
 private:
   const double& t;
 };

class SourceEta : public Expression
{
  public:
   SourceEta (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
 private:
   const double& t;
 };

class SourcePhi : public Expression
{
 public:
   SourcePhi (const double& t): t(t)
   {}
   void eval(Array<double> & values, const Array<double> & x) const;
 private:
   const double& t;
 };

#endif

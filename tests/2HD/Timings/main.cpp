// Copyright (C) 2009 Nuno David Lopes.---------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Wave generation by a moving bottom: LPT model
//  Using an inner penalty CDG method
//  See mvbottom for CG
// Using Viper  pr� post-processor and Xd3d with the files output.

#include "Depth.h"
#include <sstream>

int main(int argc,char *argv[])
{

  Dolfwave dw(
              10  /*MaxSteps*/,
              0.001 /*TimeStep*/,
              20 /*WriteGap*/,
              "LPTcdgHnl" /*FormName*/,
              "LU_P" /* AlgBackEND*/ ,
              "viper" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/,
              "With higher-order nonlinear terms" /*Comments*/
              );
  Rectangle mesh(-2.,-3.,10.5,3.,35,15,"crossed");
  Depth depth(dw.Time);//time variable depth
  DtDepth dtdepth(dw.Time);
  DtDtDepth dtdtdepth(dw.Time);
  Constant spng(0.0);//Sponge Layer
  Constant tension(0.0);//Sponge Layer
  SourceEta srceta(dw.Time);//Source Function
  SourcePhi srcphi(dw.Time);//Source Function
  dw.FunctionSpaceInit(mesh);//Function Spaces
  dw.BilinearFormInit(mesh,depth,depth);// Bilinear form initialization
  dw.FunctionsInit();  //Unknown functions
  dw.LinearFormsInit(depth,depth,dtdepth,dtdtdepth,
                     spng,tension,srceta,srcphi);//Linear form initialization
  dw.VectorInit();//Auxiliar vectors initialization
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false);
  dw.RKInit();
  dw.RKSolve("MatricesReAssemble"); //first 3 steps
  dw.PCInit(mesh); //P.Corrector initialization
  for(int i=4; i<dw.MaxSteps+1;i++)
    {
      dw.MatricesAssemble();
      dw.PCSolve( );//PC step
      dw.Info(i,true);
    }
  return (EXIT_SUCCESS);
}

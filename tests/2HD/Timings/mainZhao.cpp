// Copyright (C) 2009 Nuno David Lopes.---------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
// This program provides a demo of the DOLFWAVE library.
// Wave generation by an object moving on a horizontal bottom: Zhao model
// -----------------------------------------------------------------------

#include "ZhaoDepth.h"
#include <sstream>

int main( )
{
  Dolfwave dw(10/*MaxSteps*/,
              0.001 /*TimeStep*/,
              20  /*WriteGap*/,
              "Zhao" /*FormName*/,
              "LU_P" /* PETScLU*/ ,
              "viper" /*Plotter*/,
              "output" /*OutDir*/,
              "xyz"/*OutFormat*/
              );

  Rectangle mesh(-2.,-3.,10.5,3.,35,15,"crossed");
  Depth depth(dw.Time);// Time variable depth
  Constant zero(0.0);// Sponge Layer
  SourceEta srceta(dw.Time);//Source Function
  dw.FunctionSpaceInit(mesh);//Function Spaces
  dw.BilinearFormInit(mesh,depth);// Bilinear form initialization
  dw.FunctionsInit();  //Unknown functions
  dw.LinearFormsInit(depth,zero /*sp_eta*/,zero /*sp_lap_eta*/,
                     zero /*sp_phi*/, zero/*sp_lap_phi */,srceta);
  dw.VectorInit();//Auxiliar vectors initialization
  dw.MatricesAssemble( );//Matrices Assemble
  dw.LUFactorization(false); // Do not reuse LU factorization
  dw.RKInit();
  dw.RKSolve("MatricesReAssemble"); //first 3 steps

  dw.PCInit(mesh); //P.Corrector initialization

  for(dolfin::uint i=4; i<dw.MaxSteps+1;i++)
    {
      dw.MatricesAssemble();
      dw.PCSolve( );
      dw.Info(i,true);
    }
  return (EXIT_SUCCESS);
}

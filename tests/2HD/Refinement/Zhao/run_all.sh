#!/bin/sh

#dir1="mesh0_1"
#dir2="mesh0_2"
dir3="mesh0_3"
#dir4="mesh0_4"
#dir5="mesh0_5"
#dir6="mesh0_15"
#dir7="mesh0_17"
dir8="mesh0_25"

#mkdir $dir1
#mkdir $dir2
mkdir $dir3
#mkdir $dir4
#mkdir $dir5
#mkdir $dir6
#mkdir $dir7
mkdir $dir8

#zhao_refinement_test "square10x10_01.xml"  $dir1> $dir1/logfile.txt 2>$dir1/ERROr.txt &
#zhao_refinement_test "square10x10_02.xml"  $dir2> $dir2/logfile.txt 2>$dir2/ERROr.txt &
zhao_refinement_test "square10x10_03.xml"  $dir3> $dir3/logfile.txt 2>$dir3/ERROr.txt &
#zhao_refinement_test "square10x10_04.xml"  $dir4> $dir4/logfile.txt 2>$dir4/ERROr.txt &
#zhao_refinement_test "square10x10_05.xml"  $dir5> $dir5/logfile.txt 2>$dir5/ERROr.txt &
#zhao_refinement_test "square10x10_015.xml" $dir6> $dir6/logfile.txt 2>$dir6/ERROr.txt &
#zhao_refinement_test "square10x10_017.xml" $dir7> $dir7/logfile.txt 2>$dir7/ERROr.txt &
zhao_refinement_test "square10x10_025.xml" $dir8> $dir8/logfile.txt 2>$dir8/ERROr.txt &

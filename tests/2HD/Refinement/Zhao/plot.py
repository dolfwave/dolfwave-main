import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile





ax=subplot(111,autoscale_on='false')


step=1 #STEP IN NUMBER OF POINTS PER FILE SOLUTION

filename='mesh0_15/p1.te'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],'-',lw=1.5, label='5049 N.',alpha=1.0)

filename='mesh0_17/p1.te'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],'.', label='3964 N.',alpha=1.0)


filename='mesh0_2/p1.te'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],'-.', label='2815 N.',alpha=1.0)

filename='mesh0_25/p1.te'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],label='1873 N.',alpha=1.0)


filename='mesh0_3/p1.te'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],'--',lw=2., label='1364 N.',alpha=1.0)

filename='mesh0_4/p1.te'
u1 = fromfile(filename,sep=" ")
u1.shape=len(u1)//2,2
us=ones((len(u1)//(step),2))
for i in range(0,len(u1)//(step)):
    us[i,0]=u1[i*step,0]
    us[i,1]=u1[i*step,1]
ax.plot(us[:,0], us[:,1],'k', label='706 N.',alpha=1.0)



#filename='mesh0_5/p1.te'
#u1 = fromfile(filename,sep=" ")
#u1.shape=len(u1)//2,2
#us=ones((len(u1)//(step),2))
#for i in range(0,len(u1)//(step)):
#    us[i,0]=u1[i*step,0]
#    us[i,1]=u1[i*step,1]
#ax.plot(us[:,0], us[:,1], label='457 N.',alpha=1.0)

ax.legend(loc=(.01,.6))

(xname, yname) = ('$t$~({\\tt s})','$\eta$~({\\tt m})')
grid()


ax.set_aspect(40.)
ax.set_xlim(20.0,25.0)
ax.set_ylim(-0.06,0.1)
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)


show()


import matplotlib as mpl
from matplotlib.font_manager import FontProperties

params = {'axes.labelsize': 24,
          'text.fontsize': 18,
          'legend.fontsize': 18,
          'xtick.labelsize': 18,
          'ytick.labelsize': 18,
          'text.usetex': True}
mpl.rcParams.update(params)

from pylab import *
from numpy import fromfile





ax=subplot(111,autoscale_on='false')


step=1 #STEP IN NUMBER OF POINTS PER FILE SOLUTION


# Set the lenght of the arrays as u0_15 (the shorter one)
# Before the Blowup


## Central Point
filename='mesh0_15/p1.te'
u0_15 = fromfile(filename,sep=" ")
Len=len(u0_15)
print Len
u0_15.shape=len(u0_15)//2,2
u0_15s=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    u0_15s[i,0]=u0_15[i*step,0]
    u0_15s[i,1]=u0_15[i*step,1]



filename='mesh0_17/p1.te'
u0_17 = fromfile(filename,sep=" ")
u0_17.shape=len(u0_17)//2,2
u0_17s=ones((len(u0_17)//(step),2))
for i in range(0,len(u0_17)//(step)):
    u0_17s[i,0]=u0_17[i*step,0]
    u0_17s[i,1]=u0_17[i*step,1]


filename='mesh0_2/p1.te'
u0_2 = fromfile(filename,sep=" ")
u0_2.shape=len(u0_2)//2,2
u0_2s=ones((len(u0_2)//(step),2))
for i in range(0,len(u0_2)//(step)):
    u0_2s[i,0]=u0_2[i*step,0]
    u0_2s[i,1]=u0_2[i*step,1]

filename='mesh0_25_old/p1.te'
u0_25 = fromfile(filename,sep=" ")
u0_25.shape=len(u0_25)//2,2
u0_25s=ones((len(u0_25)//(step),2))
for i in range(0,len(u0_25)//(step)):
    u0_25s[i,0]=u0_25[i*step,0]
    u0_25s[i,1]=u0_25[i*step,1]


filename='mesh0_3_old/p1.te'
u0_3 = fromfile(filename,sep=" ")
u0_3.shape=len(u0_3)//2,2
u0_3s=ones((len(u0_3)//(step),2))
for i in range(0,len(u0_3)//(step)):
    u0_3s[i,0]=u0_3[i*step,0]
    u0_3s[i,1]=u0_3[i*step,1]


filename='mesh0_4/p1.te'
u0_4 = fromfile(filename,sep=" ")
u0_4.shape=len(u0_4)//2,2
u0_4s=ones((len(u0_4)//(step),2))
for i in range(0,len(u0_4)//(step)):
    u0_4s[i,0]=u0_4[i*step,0]
    u0_4s[i,1]=u0_4[i*step,1]

# ERRORS CALCULATION
E0_4v0_15=0
E0_3v0_15=0
E0_25v0_15=0
E0_2v0_15=0
E0_17v0_15=0

ErScale=0

error0_4v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_4v0_15[i,0]=u0_15s[i,0]
    error0_4v0_15[i,1]=(u0_4s[i,1]-u0_15s[i,1])\
                        *(u0_4s[i,1]-u0_15s[i,1])
    ErScale=ErScale+u0_15s[i,1]**2
    E0_4v0_15=E0_4v0_15+error0_4v0_15[i,1]

error0_3v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_3v0_15[i,0]=u0_15s[i,0]
    error0_3v0_15[i,1]=(u0_3s[i,1]-u0_15s[i,1])\
                        *(u0_3s[i,1]-u0_15s[i,1])
    E0_3v0_15=E0_3v0_15+error0_3v0_15[i,1]

error0_25v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_25v0_15[i,0]=u0_15s[i,0]
    error0_25v0_15[i,1]=(u0_25s[i,1]-u0_15s[i,1])\
                        *(u0_25s[i,1]-u0_15s[i,1])
    E0_25v0_15=E0_25v0_15+error0_25v0_15[i,1]

error0_2v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_2v0_15[i,0]=u0_15s[i,0]
    error0_2v0_15[i,1]=(u0_2s[i,1]-u0_15s[i,1])\
                        *(u0_2s[i,1]-u0_15s[i,1])
    E0_2v0_15=E0_2v0_15+error0_2v0_15[i,1]


error0_17v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_17v0_15[i,0]=u0_15s[i,0]
    error0_17v0_15[i,1]=(u0_17s[i,1]-u0_15s[i,1])\
                        *(u0_17s[i,1]-u0_15s[i,1])
    E0_17v0_15=E0_17v0_15+error0_17v0_15[i,1]







E=ones(5)
Er=ones(5)
N=ones(5)

N[0]=706
N[1]=1324
N[2]=1873
N[3]=2815
N[4]=3964
E[0]=E0_4v0_15
E[1]=E0_3v0_15
E[2]=E0_25v0_15
E[3]=E0_2v0_15
E[4]=E0_17v0_15

Er[0]=E0_4v0_15/ErScale
Er[1]=E0_3v0_15/ErScale
Er[2]=E0_25v0_15/ErScale
Er[3]=E0_2v0_15/ErScale
Er[4]=E0_17v0_15/ErScale

print 'Er[0]='+str(Er[0])
print 'Er[1]='+str(Er[1])
print 'Er[2]='+str(Er[2])
print 'Er[3]='+str(Er[3])
print 'Er[5]='+str(Er[4])


ax.plot(N,E,'*-',lw=2,label='Absolute  P=(5,5)${\\tt (m)}$');
ax.plot(N,Er,'o-',lw=2,label='Relative P=(5,5)${\\tt (m)}$');

#############################################################

## Corner Point
filename='mesh0_15/p2.te'
u0_15 = fromfile(filename,sep=" ")
Len=len(u0_15)
print Len
u0_15.shape=len(u0_15)//2,2
u0_15s=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    u0_15s[i,0]=u0_15[i*step,0]
    u0_15s[i,1]=u0_15[i*step,1]



filename='mesh0_17/p2.te'
u0_17 = fromfile(filename,sep=" ")
u0_17.shape=len(u0_17)//2,2
u0_17s=ones((len(u0_17)//(step),2))
for i in range(0,len(u0_17)//(step)):
    u0_17s[i,0]=u0_17[i*step,0]
    u0_17s[i,1]=u0_17[i*step,1]


filename='mesh0_2/p2.te'
u0_2 = fromfile(filename,sep=" ")
u0_2.shape=len(u0_2)//2,2
u0_2s=ones((len(u0_2)//(step),2))
for i in range(0,len(u0_2)//(step)):
    u0_2s[i,0]=u0_2[i*step,0]
    u0_2s[i,1]=u0_2[i*step,1]

filename='mesh0_25_old/p2.te'
u0_25 = fromfile(filename,sep=" ")
u0_25.shape=len(u0_25)//2,2
u0_25s=ones((len(u0_25)//(step),2))
for i in range(0,len(u0_25)//(step)):
    u0_25s[i,0]=u0_25[i*step,0]
    u0_25s[i,1]=u0_25[i*step,1]


filename='mesh0_3_old/p2.te'
u0_3 = fromfile(filename,sep=" ")
u0_3.shape=len(u0_3)//2,2
u0_3s=ones((len(u0_3)//(step),2))
for i in range(0,len(u0_3)//(step)):
    u0_3s[i,0]=u0_3[i*step,0]
    u0_3s[i,1]=u0_3[i*step,1]


filename='mesh0_4/p2.te'
u0_4 = fromfile(filename,sep=" ")
u0_4.shape=len(u0_4)//2,2
u0_4s=ones((len(u0_4)//(step),2))
for i in range(0,len(u0_4)//(step)):
    u0_4s[i,0]=u0_4[i*step,0]
    u0_4s[i,1]=u0_4[i*step,1]

# ERRORS CALCULATION
E0_4v0_15=0
E0_3v0_15=0
E0_25v0_15=0
E0_2v0_15=0
E0_17v0_15=0

ErScale=0

error0_4v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_4v0_15[i,0]=u0_15s[i,0]
    error0_4v0_15[i,1]=(u0_4s[i,1]-u0_15s[i,1])\
                        *(u0_4s[i,1]-u0_15s[i,1])
    ErScale=ErScale+u0_15s[i,1]**2
    E0_4v0_15=E0_4v0_15+error0_4v0_15[i,1]

error0_3v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_3v0_15[i,0]=u0_15s[i,0]
    error0_3v0_15[i,1]=(u0_3s[i,1]-u0_15s[i,1])\
                        *(u0_3s[i,1]-u0_15s[i,1])
    E0_3v0_15=E0_3v0_15+error0_3v0_15[i,1]

error0_25v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_25v0_15[i,0]=u0_15s[i,0]
    error0_25v0_15[i,1]=(u0_25s[i,1]-u0_15s[i,1])\
                        *(u0_25s[i,1]-u0_15s[i,1])
    E0_25v0_15=E0_25v0_15+error0_25v0_15[i,1]

error0_2v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_2v0_15[i,0]=u0_15s[i,0]
    error0_2v0_15[i,1]=(u0_2s[i,1]-u0_15s[i,1])\
                        *(u0_2s[i,1]-u0_15s[i,1])
    E0_2v0_15=E0_2v0_15+error0_2v0_15[i,1]


error0_17v0_15=ones((len(u0_15)//(step),2))
for i in range(0,len(u0_15)//(step)):
    error0_17v0_15[i,0]=u0_15s[i,0]
    error0_17v0_15[i,1]=(u0_17s[i,1]-u0_15s[i,1])\
                        *(u0_17s[i,1]-u0_15s[i,1])
    E0_17v0_15=E0_17v0_15+error0_17v0_15[i,1]



E=ones(5)
Er=ones(5)
N=ones(5)

N[0]=706
N[1]=1324
N[2]=1873
N[3]=2815
N[4]=3964
E[0]=E0_4v0_15
E[1]=E0_3v0_15
E[2]=E0_25v0_15
E[3]=E0_2v0_15
E[4]=E0_17v0_15

Er[0]=E0_4v0_15/ErScale
Er[1]=E0_3v0_15/ErScale
Er[2]=E0_25v0_15/ErScale
Er[3]=E0_2v0_15/ErScale
Er[4]=E0_17v0_15/ErScale

print 'Er[0]='+str(Er[0])
print 'Er[1]='+str(Er[1])
print 'Er[2]='+str(Er[2])
print 'Er[3]='+str(Er[3])
print 'Er[5]='+str(Er[4])


ax.plot(N,E,'*--',lw=2,label='Absolute  P=(0,0)${\\tt (m)}$');
ax.plot(N,Er,'o--',lw=2,label='Relative P=(0,0)${\\tt (m)}$');


# return locs, labels where locs is an array of tick locations and
# labels is an array of tick labels.
#locs, labels = xticks()


# set the locations and labels of the xticks
xticks([N[0], N[1], N[2], N[3], N[4]])

grid()
ax.set_aspect(30000.)
ax.set_xlim(N[0],N[4])
ax.set_ylim(0.,.07)
ax.legend(loc=1)
plottitle='l$^2$-errors in central and corner points for $t\\in[0,30]({\\tt s})$'
title(plottitle,fontsize=18)
(xname, yname) = ('Number of nodes','l$^2$-Error')
ax.set_xlabel(xname,fontsize=24)
ax.set_ylabel(yname,fontsize=24)

show()


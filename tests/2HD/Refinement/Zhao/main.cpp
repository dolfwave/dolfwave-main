// Copyright (C) 2009 Nuno David Lopes.-----------------------------------
// e-mail: ndl@ptmat.fc.ul.pt
// http://ptmat.fc.ul.pt/~ndl/
// Licensed under the GNU LGPL Version 3.0 or later.
//  This program provides a demo of the DOLFWAVE library.
//  Standard Gaussian Wave
//  Using Viper for preview.
#include <dolfwave.h>
using namespace dolfin;
using namespace dolfin::dolfwave;

class ElevationInit : public Expression
{
  void eval(Array<double> & values,const Array<double> & x) const
  {
    values[0]=0.1*exp(-0.4*(sqr(x[0]-5.0)+sqr(x[1]-5.0)));
  }
};
int main(int argc,char *argv[])
{
 if (argc!=3)
    {
      info ("argv[1]='Mesh File' argv[2]=outputdir");
      exit(EXIT_FAILURE);
    }

  std::stringstream file_p1,file_p2;
  file_p1<<argv[2]<<"/p1.te";
  file_p2<<argv[2]<<"/p2.te";

  std::stringstream volume_file;
  volume_file<<argv[2]<<"/volume.tv";

  //int NumberOfIntervals=atoi(argv[1]);
  /// Options, Initialization, Mesh and User functions
  Dolfwave dw(
              500001 /* MaxSteps*/,
              0.0001 /* TimeStep*/,
              50 /* WriteGap*/,
              "Zhao" /*FormName*/,
              "LU_P" /*AlgBackEND*/,
              "viper" /*Plotter*/,
              argv[2] /*OutDir*/,
              "xyz"/*OutFormat*/,
              "Refinement test"
              );
  parameters("dolfwave_parameters")["pred_corr_blow_up"]=5;
  info(parameters,true);
  //Rectangle mesh(0,0,10,10,NumberOfIntervals,NumberOfIntervals,"crossed");
  Mesh mesh(argv[1]);
  Constant depth(0.5);//Depth
  Constant sponge(0.0);//Sponge Layer
  Constant src(0.0);//Source Function
  Constant phi_init(0.0);//initial condition for potential
  ElevationInit eta_init;//initial condition for elevation
  dw.FunctionSpaceInit(mesh); //Function Spaces Init
  dw.BilinearFormInit(mesh,depth);//BilinearForm a initialization
  dw.MatricesAssemble( ); //Matrices M and Mf initialization
  dw.FunctionsInit( );// Solutions Functions (potential,elevation)
  dw.LinearFormsInit(depth,sponge /*sp_eta*/,sponge /*sp_lap_eta*/,
                     sponge /*sp_phi*/,sponge/*sp_lap_phi */,src);
  dw.InitialCondition(eta_init,phi_init); //Initial Conditions L2 projection
  dw.Volume(mesh,depth,volume_file.str());
  dw.VectorInit( );//Auxiliar vectors Initialization
  dw.LUFactorization(true); //If uBLAS the factorize
  dw.RKInit(/*RKType "euler" or the default "exp4"*/ );
  dw.RKSolve( );//Runge-Kutta for the initialization of the 3 initial steps
  dw.PCInit(mesh,false);


  for(int i=4; i<dw.MaxSteps+1;i++)
    {
      dw.PCSolve( );
      dw.Volume(mesh,depth,volume_file.str());
      if(!(i%dw.WriteGap))
        {
          dw.Plot(mesh,false /*eta Plotter*/, false/*phi Plotter*/,
                  true /*SaveEta*/,false /*SavePhi*/);
          dw.PointPlot(5.,5.,"eta",file_p1.str());
          dw.PointPlot(.0,.0,"eta",file_p2.str());
        }
      dw.Info(i,false);
    }
  return (EXIT_SUCCESS);
}

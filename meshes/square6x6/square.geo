l=0.2;

Point(1)={0.,0.,0.,l};
Point(2)={0.,6.,0.,l};
Point(3)={6.,6.,0.,l};
Point(4)={6.,0.,0.,l};

Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};


Line Loop(5)={1,2,3,4};

Plane Surface(1)={5};

Physical Surface(100)={1};
